Pixel software for RCE controller

Requirements for compilation/running
  * Enterprise Linux 7 host 
  * gcc 7 greater (devtoolset-7 recommended)
    - refer to  https://www.softwarecollections.org/en/scls/rhscl/devtoolset-7 for installation
  * for the RCE talk python tools: paramiko and python-scp
    - install with: bash
        - bash> sudo yum install python-paramiko python-scp root
  * hhe new Atlas SDK (CentOS7 on RCE, gcc7 support)
       to instructions:  https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RCEGen3SDK
Obtaining and compiling the software
   - bash> git clone --recurse-submodules https://:@gitlab.cern.ch:8443/rce/pixelrce.git
   - bash> source /opt/rce/setup.sh
   - bash> cd pixelrce
   - bash> cd rce
   - bash> source scripts/setup-dev.sh
   - bash> ( cd build.rce && make ) && ( cd build.host && make)

   
Running the software:
Your RCE (IP node) should be aliased or named rce0
   - install and start the calibration server on the RCE
       - bash> scripts/rce_talk.py rce0 install
   - check the status/output of calibserver
       - bash > scripts/rce_talk.py rce0 status  
   - run calibGui
       - bash> source scripts/setup-env.sh
       - bash> calibGui