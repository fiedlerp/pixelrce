#!/bin/bash
#change default paths, if necessary
if [ -z ${ROOTSYS+x} ] ;then
export ROOTSYS=/usr
fi
if [ -z ${SDK_ROOT+x} ] ;then
if [ $1 = "oldsdk" ] ; then
export SDK_ROOT=/opt/AtlasRceSdk/V0.11.1
else
export SDK_ROOT=/opt/rce
fi
fi
if [ ! -z ${LCG_gcc_home+x} ] ;then
export LCG_gcc_home
fi

if [ -x $ROOTSYS/bin/root-config ]; then
_rootlibdir=$($ROOTSYS/bin/root-config --libdir)
else
echo Warning: no ROOT setup found in $ROOTSYS
fi
_topdir=$(dirname ${BASH_SOURCE})/..
_topdir=$(cd ${_topdir}/> /dev/null 2>&1 && pwd)

debug="Release"
version=""
if [ $# -eq 1 ] &&  [  $1 = "debug" ]  ; then
    debug="Debug"  
    version="-dbg"
fi
arm="build.rce$version"
slc="build.host$version" 
if [ -e  $SDK_ROOT/setup.sh ];then
source $SDK_ROOT/setup.sh
else
echo Warning: RCE SDK not found in $SDK_ROOT
fi


export LD_LIBRARY_PATH=${_topdir}/${slc}/lib:${LD_LIBRARY_PATH}
if [ -d ${_rootlibdir} ] ; then
export LD_LIBRARY_PATH=${_rootlibdir}:${LD_LIBRARY_PATH}
fi
export PATH=${PATH}:${_topdir}/${slc}/bin
unset _topdir
unset _rootlibdir
