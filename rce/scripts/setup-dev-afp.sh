#!/bin/bash
_basedir=$(dirname ${BASH_SOURCE})/..
_basedir=$(cd ${_basedir}/> /dev/null 2>&1 && pwd)
source ${_basedir}/scripts/setup-env-afp.sh $*
pushd .

if [ -d ${_basedir}/scripts ]; then
  if [ ! -d ${_basedir}/$arm ]; then
     echo Creating ${_basedir}/rce/$arm
     mkdir ${_basedir}/$arm
  fi
  if [ ! -d ${_basedir}/$slc ]; then
     echo Creating ${_basedir}/$slc
     mkdir ${_basedir}/$slc
  fi
  export MAKEFLAGS="-j12 QUICK=1"
  echo cd ${_basedir}/$arm
  cd ${_basedir}/$arm
  cmake  -DCMAKE_BUILD_TYPE="$debug"    -DCMAKE_TOOLCHAIN_FILE=${_basedir}/pixelrce/toolchain/arm-archlinux ${_basedir}/pixelrce 
  echo cd ${_basedir}/$slc
  cd ${_basedir}/$slc
  cmake  -DCMAKE_BUILD_TYPE="$debug"   -DCMAKE_TOOLCHAIN_FILE=${_basedir}/pixelrce/toolchain/linux-tdaq8 ${_basedir}/pixelrce 
else 
  echo "${_basedir}/scripts does not exist"
fi
popd
unset _basedir
