--------------------------------------------------------------
-- Reset the eport phases
-- Martin Kocian 04/2016
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity eportphasereset is
port(	sysClk40: 	    in std_logic;
        sysRst40:           in sl;
        gbtaddr:            in slv(6 downto 0);
        ic:                 out slv(1 downto 0);
        useic:              out sl:='0';
        done:               out sl:='0';
        go:                 in sl
);
end eportphasereset;

--------------------------------------------------------------

architecture CBP of eportphasereset is

   type state_type is (idle, run, writing);
   signal state: state_type:=idle;
   signal index : integer range 0 to 15 :=0;
   signal gowrite: sl:='0';
   signal icdone: sl:='0';
   signal value: slv(7 downto 0):=x"00";
   signal address: slv(15 downto 0):=x"0000";

begin
  process begin
    wait until rising_edge(sysClk40);
    if(state=idle)then
      useic<='0';
      if(go='1')then
        done<='0';
        index<=0;
        useic<='1';
        value<=x"55";
        address<=x"0054";
        state<=run;
      end if;
    elsif(state=writing)then
      if(gowrite='1')then
        gowrite<='0';
      elsif(icdone='1')then
        if(index=9)then
          done<='1';
          state<=idle;
        else
          if(index=4)then
            value<=x"00"; -- clear
            address<=x"0054"; -- start again
          else
            address<=unsigned(address)+24;
          end if;
          index<=index+1;
          state<=run;
        end if;
      end if;
    elsif(state=run)then
      gowrite<='1';
      state<=writing;
    end if;
  end process;

  ic_channel_inst: entity work.icwrite3xphytx
    port map(
      sysClk40 => sysClk40,
      sysRst40 => sysRst40,
      gbtaddr => gbtaddr,
      regaddress => address,
      value => value,
      ic => ic,
      done => icdone,
      go => gowrite);

end CBP;

--------------------------------------------------------------
