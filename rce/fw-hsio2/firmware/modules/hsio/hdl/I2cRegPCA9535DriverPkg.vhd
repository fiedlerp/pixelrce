library ieee;
use ieee.std_logic_1164.all;

package I2cRegPCA9535DriverPkg is 
  type I2cRegPCA9535DriverInType is record
    configure: std_logic;
    write: std_logic;
    writeValue: std_logic_vector(15 downto 0);
  end record;
  
  constant I2C_REG_PCA9535DRIVER_IN_INIT : I2cRegPCA9535DriverInType := (
    configure => '0',
    write => '0',
    writeValue => (others => '0')
  );
  
  type I2cRegPCA9535DriverOutType is record
    busy: std_logic;
    ack: std_logic;
    fail: std_logic;
  end record;
end I2cRegPCA9535DriverPkg;
