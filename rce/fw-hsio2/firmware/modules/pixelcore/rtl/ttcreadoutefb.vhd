--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;
use work.all;

--------------------------------------------------------------

entity ttcreadoutefb is
port(	clk: 	    in std_logic;
        clken:      in std_logic;
        clk40:      in std_logic;
        rst40:      in std_logic;
        regclk:     in std_logic;
        rstcounters:in std_logic;
        bcrl1acounter: out std_logic_vector(31 downto 0);
        l1abcrcounter: out std_logic_vector(31 downto 0);
        l1al1acounter: out std_logic_vector(31 downto 0);
        bcrveto:       in sl;
        vetoout:       out sl:='0';
        vetoFirstBcid: in slv(11 downto 0);
        vetoNumBcid:   in slv(11 downto 0);
        ttc_in:     in AtlasTTCRxOutType;
        enabled:    in std_logic;
        ecr:        out std_logic;
        bcr:        out std_logic;
	d_out:	    out std_logic_vector(52 downto 0):=(others => '0');
        ld:         out std_logic:='0'
);
end ttcreadoutefb;

--------------------------------------------------------------

architecture TTCREADOUTEFB of ttcreadoutefb is

  type state_type is (idle, bcrcount, l1acount);
  signal state: state_type:=idle;
  signal counter: slv(3 downto 0);
  signal bcrl1acounters: slv(31 downto 0);
  signal l1abcrcounters: slv(31 downto 0);
  signal l1al1acounters: slv(31 downto 0);
  signal ecrint, bcrint, ldint: std_logic;
  signal ldoutint, bcroutint: sl;
  signal vetoint, bcrvetos: sl;
  signal vetocounter: slv(11 downto 0):=(others=>'0');
  signal vetoFirstBcids, vetoNumBcids: slv(11 downto 0);

  begin

    process
    begin
        wait until rising_edge(clk);
        if(enabled='1' and clken='1' and ttc_in.trigL1='1') then -- L1A
          d_out(31 downto 24)<=ttc_in.eventRstCnt;
          d_out(23 downto 0)<=ttc_in.eventCnt;
          d_out(43 downto 32)<=ttc_in.bunchCnt;
          d_out(51 downto 44)<=ttc_in.bunchRstCnt;
          d_out(52)<='0';
          ldint<='1';
          ecrint<='0';
        elsif(enabled='1' and ttc_in.bc.valid='1' and ttc_in.bc.cmdData(1)='1') then -- ECR
          d_out(52)<='1';
          d_out(51 downto 0)<=(others => '0');
          ldint<='1';
          ecrint<='1';
        else
          ldint<='0';
          ecrint<='0';
        end if; 
        if(enabled='1' and ttc_in.bc.valid='1' and ttc_in.bc.cmdData(0)='1') then -- BCR
          bcrint<='1';
        else
          bcrint<='0';
        end if; 
    end process;		

    process begin
      wait until rising_edge(clk);
      if(bcrvetos='1')then
        if(clken='1')then
          if(enabled='1' and ttc_in.bunchCnt=vetoFirstBcids) then 
            vetocounter<=vetoNumBcids;
          elsif(vetocounter/=x"000")then
            vetocounter<=unsigned(vetocounter)-1;
            vetoint<='1';
          else 
            vetoint<='0';
          end if;
        end if;
      else
        vetoint<='0';
      end if;
    end process;
      
  process begin
    wait until rising_edge(clk40);
    if(rstcounters='1')then
      bcrl1acounters<=(others=>'0');
      l1abcrcounters<=(others=>'0');
      l1al1acounters<=(others=>'0');
    end if;
    case state is
      when idle =>
        if(bcroutint='1' and ldoutint='1')then
          bcrl1acounters<=unsigned(bcrl1acounters)+1;
        elsif(bcroutint='1')then
          counter<="0111";
          state<=bcrcount;
        elsif(ldoutint='1')then
          counter<="0011"; 
          state<=l1acount;
        end if;
      when bcrcount =>
        if(ldoutint='1')then
          bcrl1acounters<=unsigned(bcrl1acounters)+1;
        end if;
        if(counter="0000")then
          state <= idle;
        end if;
        counter<=unsigned(counter)-1;
      when l1acount => 
        if(bcroutint='1')then
          l1abcrcounters<=unsigned(bcrl1acounters)+1;
        elsif(ldoutint='1')then
          l1al1acounters<=unsigned(l1al1acounters)+1;
        end if;
        if(counter="0000")then
          state <= idle;
        end if;
        counter<=unsigned(counter)-1;
     end case;   
   end process;

  oneshot_ecr: entity work.oneshot
    port map(
      clk => clk40,
      rst => rst40,
      datain => ecrint,
      dataout => ecr);
  oneshot_bcr: entity work.oneshot
    port map(
      clk => clk40,
      rst => rst40,
      datain => bcrint,
      dataout => bcroutint);
  bcr<=bcroutint;
  oneshot_l1a: entity work.oneshot
    port map(
      clk => clk40,
      rst => rst40,
      datain => ldint,
      dataout => ldoutint);
  ld<=ldoutint;
  synch_vetoout: entity work.Synchronizer
   port map(
     clk => clk40,
     dataIn => vetoint,
     dataOut => vetoout);
  synch_vetoin: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => bcrveto,
     dataOut => bcrvetos);
   synchout_bcidfirst : entity work.SynchronizerVector
      generic map (
        WIDTH_G => 12)
      port map (
        clk        => clk,
        dataIn => vetoFirstBcid,
        dataOut => vetoFirstBcids);
   synchout_bcidfnum : entity work.SynchronizerVector
      generic map (
        WIDTH_G => 12)
      port map (
        clk        => clk,
        dataIn => vetoNumBcid,
        dataOut => vetoNumBcids);
   synchout_counter_bcrl1a : entity work.SynchronizerVector
      generic map (
        WIDTH_G => 32)
      port map (
        clk        => regClk,
        dataIn => bcrl1acounters,
        dataOut => bcrl1acounter);
   synchout_counter_l1abcr : entity work.SynchronizerVector
      generic map (
        WIDTH_G => 32)
      port map (
        clk        => regClk,
        dataIn => l1abcrcounters,
        dataOut => l1abcrcounter);
   synchout_counter_l1al1a : entity work.SynchronizerVector
      generic map (
        WIDTH_G => 32)
      port map (
        clk        => regClk,
        dataIn => l1al1acounters,
        dataOut => l1al1acounter);

      
--  ila: entity work.ila_0
--    port map(
--      clk => clk,
--      probe0(0)=>clken,
--      probe1(0)=>ttc_in.trigL1,
--      probe2(0)=>ttc_in.bc.valid,
--      probe3(0)=>ttc_in.bc.cmdData(0),
--      probe4(0)=>ttc_in.bc.cmdData(1),
--      probe5=>ttc_in.eventCnt,
--      probe6=>ttc_in.eventRstCnt,
--      probe7=>ttc_in.bunchCnt,
--      probe8=>ttc_in.bunchRstCnt,
--      probe9(0)=>ldint,
--      probe10(0)=>ldoutint,
--      probe11(0)=>bcrint,
--      probe12(0)=>vetoint);
--
end TTCREADOUTEFB;

--------------------------------------------------------------
