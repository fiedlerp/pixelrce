--------------------------------------------------------------
-- Trigger logic for pixel HSIO firmware
-- Martin Kocian 07/2014
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;
use work.arraytype.all;
library unisim;
use unisim.vcomponents.all;
--------------------------------------------------------------


entity triggerlogic is

port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        axiClk40:   in std_logic;
        axiRst40:   in std_logic;
        -- hardware inputs
        discin       : in std_logic_vector(3 downto 0);
        hitbusin     : in std_logic_vector(1 downto 0);
        -- HSIO trigger
        HSIObusy      : out std_logic;
        HSIOtrigger   : in std_logic_vector(1 downto 0);
        HSIObusyin    : in std_logic;
        hitbusout     : out std_logic;

        -- eudet trigger
        exttrigger    : in std_logic;
        extrst        : in std_logic;
        extbusy       : out std_logic;
        exttrgclk     : out std_logic;

        calibmode     : in std_logic_vector(1 downto 0);
        eudaqdone     : out std_logic;
        eudaqtrgword  : out std_logic_vector(14 downto 0);

        ttctrig       : in std_logic;
        trigenabled:   in std_logic;
        vetodis    :   in std_logic;
        paused:     in std_logic;
        fifothresh: in std_logic;
        triggermask: in std_logic_vector(15 downto 0);
        resetdelay: in std_logic;
        incrementdelay: in std_logic_vector(4 downto 0); 
        discop: in std_logic_vector(15 downto 0); 
        telescopeop: in std_logic_vector(2 downto 0);
        period     : in std_logic_vector(31 downto 0);
        cdtenabled: in std_logic;
        numbuffers: in std_logic_vector(4 downto 0);
        window: in std_logic_vector(15 downto 0);
        serbusy: in std_logic;
        tdcreadoutbusy: in std_logic;
        l1a: out std_logic;
        triggerword: out std_logic_vector(7 downto 0);
        busy: out std_logic;
        busyAtlas: out std_logic:='0';
        coincd: out std_logic
);
end triggerlogic;

--------------------------------------------------------------

architecture TRIGGERLOGIC of triggerlogic is


signal trgin: std_logic;
signal disc            : std_logic_vector(3 downto 0);
signal discrim         : std_logic;
signal delrst1         : std_logic;
signal delrst2         : std_logic;
signal delrstf         : std_logic;
signal delayrst        : std_logic;
signal exttrgclkeu    : std_logic;
signal busys           : std_logic;
signal l1as            : std_logic;
signal cycliccounter   : std_logic_vector(31 downto 0);
signal oldperiod       : std_logic_vector(31 downto 0):=(others => '0');
signal cyclic          : std_logic;
signal hitbus          : std_logic;
signal resettriggerword: std_logic;
signal oldtdcreadoutbusy: std_logic;
signal coinc           : std_logic;
signal stdc            : std_logic;
signal hsio            : std_logic;
signal cdtactive       : std_logic;
signal cdtbusy         : std_logic;


begin

    --HSIObusy <= busys or paused;
    HSIObusy <= busys;
    busy<=busys;
    l1a<=l1as;
    hitbusout<=hitbus;
             
    process(axiClk40, axiRst40) begin
      if(axiRst40='1')then
        delrst1<='0';
        delrst2<='0';
        delrstf<='0';
      elsif(rising_edge(axiClk40))then
        if(resetdelay='1')then
          delrst1<='1';
          delrst2<='1';
          delrstf<='1';
        else
          delrstf<=delrst1;
          delrst1<=delrst2;
          delrst2<='0';
        end if;
      end if; 
    end process;
        
    delayrst<= delrstf or axiRst40;

    discdelays: for I in 0 to 3 generate
      attribute IODELAY_GROUP                    : string;
      attribute IODELAY_GROUP of delay0   : label is "atlas_ttc_rx_delay_group";
      begin
      delay0 : IDELAYE2
        generic map (
          IDELAY_TYPE => "VARIABLE", -- Set to DEFAULT for -- Zero Hold Time Mode
          IDELAY_VALUE => 0 -- (0 to 63)
          )
        port map (
          DATAOUT => disc(I),
          IDATAIN => discin(I),
          C => axiClk40,
          CE => incrementdelay(I),
          INC => '1',
          CINVCTRL => '0',
          CNTVALUEIN => "00000",
          DATAIN=>'0',
          LD=>'0',
          LDPIPEEN=>'0',
          REGRST => delayrst 
          );
      end generate discdelays;

   discrim <= ((disc(0) xor discop(8)) or not discop(0)) and ((disc(1) xor discop(9)) or not discop(1)) and ((disc(2) xor discop(10)) or not discop(2)) and ((disc(3) xor discop(11)) or not discop(3)); --and ((disc(4) xor discop(12)) or not discop(4));

   with discop(15 downto 14) select
     hsio <= HSIOtrigger(0) when "00",
             HSIOtrigger(1) when "01",
             HSIOtrigger(0) or HSIOtrigger(1) when "10",
             HSIOtrigger(0) and HSIOtrigger(1) when "11",
             '0' when others;

   with telescopeop select
     hitbus<= hitbusin(0) when "001", 
     hitbusin(1) when "010", 
     hitbusin(0) or hitbusin(1) when "011", 
     hitbusin(0) and hitbusin(1) when "100", 
     '0' when others;

   trgin<= (triggermask(2) and exttrigger)
           or (triggermask(3) and hsio)
           or (triggermask(1) and cyclic)
           or (triggermask(4) and hitbus)
           or (triggermask(5) and ttctrig)
           or (triggermask(0) and discrim);

  -- Latching of trigger word
  process(clk,rst) begin
   if(rst='1')then
     oldtdcreadoutbusy<='0';
     resettriggerword<='1';
   elsif(rising_edge(clk))then
     oldtdcreadoutbusy<=tdcreadoutbusy;
     if(oldtdcreadoutbusy='1' and tdcreadoutbusy='0')then
       resettriggerword<='1';
     else
       resettriggerword<='0';
     end if;
   end if;
  end process;

   process(resettriggerword, trgin) begin
     if(resettriggerword='1')then
       triggerword<=x"00";
     elsif(rising_edge(trgin))then
       triggerword(7 downto 6)<="00";
       triggerword(5)<=triggermask(5) and ttctrig;
       triggerword(4)<=triggermask(4) and hitbus;
       triggerword(3)<=triggermask(3) and hsio;
       triggerword(2)<=triggermask(2) and exttrigger;
       triggerword(1)<=triggermask(1) and cyclic;
       triggerword(0)<=triggermask(0) and discrim;
     end if;
   end process;

  process(clk,rst) begin
   if(rst='1')then
     cycliccounter<=(others=>'0');
     cyclic<='0';
   elsif(rising_edge(clk))then
     oldperiod<=period;
     if(period/=x"00000000" and cycliccounter=period)then
       cycliccounter<=(others=>'0');
       cyclic<='1';
     elsif(period/=oldperiod)then
       cycliccounter<=(others=>'0');
       cyclic<='0';
     else
       cycliccounter<=unsigned(cycliccounter)+1;
       cyclic<='0';
     end if;
   end if;
  end process;

    cdt: entity work.complexdeadtime
      port map(
        clk=>clk,
        rst=>rst,
        enabled=>cdtactive,
        l1a=>l1as,
        busy=>cdtbusy,
        numbuffers=>numbuffers,
        window=>window);
    cdtactive<=cdtenabled and trigenabled and not paused;

    busyAtlas<=(trigenabled and (fifothresh or cdtbusy or paused or tdcreadoutbusy)) or not trigenabled;

    coin: entity work.coincidence
      port map(
        clk=>clk,
        rst=>rst,
        enabled=>trigenabled,
        vetodis=>vetodis,
        fifothresh=>fifothresh,
        trgin=>trgin,
        serbusy=>serbusy,
        extbusy=>HSIObusyin,
        complexbusy=>cdtbusy,
        tdcreadoutbusy=>tdcreadoutbusy,
        l1a=> l1as,
        busy=> busys,
        coinc=> coinc,
        coincd=> coincd
        );

   eudettrg: entity work.eudaqTrigger
     port map(
       clk => clk,
       rst => rst,
       busyin => busys,
       enabled =>calibmode(1),
       l1a  => l1as,
       trg  => exttrigger,
       done => eudaqdone,
       extbusy => extbusy,
       trgword => eudaqtrgword,
       trgclk => exttrgclkeu );

    exttrgclk<=exttrgclkeu or paused or not trigenabled;

end TRIGGERLOGIC;
