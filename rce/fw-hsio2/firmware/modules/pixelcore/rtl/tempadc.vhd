--------------------------------------------------------------
-- ADC Analog Devices 7998 readout
-- Martin Kocian 12/2014
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.StdRtlPkg.all;
use work.I2cPkg.all;
use work.all;

--------------------------------------------------------------

entity tempadc is
generic( PRESCALE_G: integer range 0 to 65535 := 79;
         MAPPING_G: NaturalArray(0 to 7) := (7,6,5,4,3,2,1,0) );
port(	clk: 	    in std_logic;
	d_out:	    out Slv16Array(11 downto 0):=(others => (others => '0'));
        trig:       in std_logic;
        ld:         out std_logic:= '0';
        adcAS:      out std_logic;
        scl:        inout std_logic;
        sda:        inout std_logic
);
end tempadc;

--------------------------------------------------------------

architecture TEMPADC of tempadc is

  type state_type is (idle, req, ack);
  signal state: state_type;
  signal adcI2cIn   : i2c_in_type;
  signal adcI2cOut  : i2c_out_type;
  signal i2cregmasterin: I2cRegMasterInType := I2C_REG_MASTER_IN_INIT_C;
  signal i2cregmasterout: I2cRegMasterOutType;
  signal index: natural range 0 to 7;
begin
   sda    <= adcI2cOut.sda when adcI2cOut.sdaoen = '0' else 'Z';
   adcI2cIn.sda <= to_x01z(sda);
   scl    <= adcI2cOut.scl when adcI2cOut.scloen = '0' else 'Z';
   adcI2cIn.scl <= to_x01z(scl);
   adcAS <='0';

   i2cregmasterin.regDataSize<="01";
   i2cregmasterin.endianness<='1';
   i2cregmasterin.i2cAddr<="0000100001";

   chanmap: for I in 0 to 3 generate
     d_out(I)<=toSlv(MAPPING_G(I*2), 8)& toSlv(MAPPING_G(I*2+1), 8);
   end generate chanmap;
   I2cRegMaster_Inst: entity work.I2cRegMaster
     generic map(
       PRESCALE_G => PRESCALE_G)
     port map(
       clk => clk,
       regIn => i2cregmasterin,
       regOut => i2cregmasterout,
       i2ci => adcI2cIn,
       i2co => adcI2cOut);

   process begin
     wait until (rising_edge(clk));
     if(state=idle)then
       ld<='0';
       if(trig='1') then
         state<=req;
         index<=0;
       end if;
     elsif (state=req)then
       i2cregmasterin.regAddr <= toSlv(128+index*16, 32);
       i2cregmasterin.regReq<='1';
       state<=ack;
     elsif(state=ack)then
       i2cregmasterin.regReq<='0';
       if(i2cregmasterout.regAck='1')then
         d_out(index+4)<=i2cregmasterout.regRdData(7 downto 0) & i2cregmasterout.regRdData(15 downto 8); 
         if(index=7)then
           ld<='1';
           state<=idle;
         else
           index<=index+1;
           state<=req;
         end if;
       end if;
     end if;
   end process;
       

end TEMPADC;

--------------------------------------------------------------
