--------------------------------------------------------------
-- Reset Si5338 until the phases between 40 MHz, 160 MHz, and 120 MHz match.
-- Martin Kocian 04/2016
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity calibrateClockbufPhase is
generic ( PHASE_G           : slv(1 downto 0):= "00");
port(	sysClk40: 	    in std_logic;
	clock160:	    in std_logic;
        clock120:           in std_logic;
        doneclockbuf:       in std_logic;
        bpmlocked:          in std_logic;
        resetclockbuf:      out std_logic;
        resetdoneclockbuf:  in std_logic;
        clken:              in std_logic;
        done:               out std_logic:='0';
        counters_dbg:       out Slv8Array(3 downto 0)
);
end calibrateClockbufPhase;

--------------------------------------------------------------

architecture CBP of calibrateClockbufPhase is

   type state_type is (idle, start, check, rstclockbuf, calibdone, tryagain);
   signal state: state_type:=idle;
   signal counters: Slv8Array(3 downto 0);
   signal waitctr: slv(31 downto 0);
   signal doneclockbuf160, resetdoneclockbuf160: sl;
   signal resetclockbuf160: sl:='0';
   signal phasecount: slv(1 downto 0):="00";

begin
  counters_dbg<=counters;
  synch_0: entity work.Synchronizer
   port map(
     clk => clock160,
     dataIn => doneclockbuf,
     dataOut => doneclockbuf160);
  synch_1: entity work.Synchronizer
   port map(
     clk => clock160,
     dataIn => resetdoneclockbuf,
     dataOut => resetdoneclockbuf160);
  synch_2: entity work.Synchronizer
   port map(
     clk => sysClk40,
     dataIn => resetclockbuf160,
     dataOut => resetclockbuf);

  process begin
    wait until rising_edge(clock160);
    if(clken='1')then
      phasecount<=PHASE_G;
    else
      phasecount<=unsigned(phasecount)+1;
    end if;
    if(state=idle)then
      if(doneclockbuf160='1')then
        state<=start;
        waitctr<=x"01000000";
      end if;
    elsif(state=start)then
     if(waitctr/=x"00000000")then
       waitctr<=unsigned(waitctr)-1;
       if(waitctr=x"00000001")then
         counters<=(others=>(others =>'0'));
       end if;
     elsif(counters(0)=x"ff" or counters(1)=x"ff" or counters(2)=x"ff" or counters(3)=x"ff")then
       waitctr<=x"00000000";
       state<=check;
     elsif(bpmlocked='1')then
       if(phasecount="00")then
         if(clock120='1')then
           counters(0)<=unsigned(counters(0))+1;
         end if;
       elsif(phasecount="01")then
         if(clock120='1')then
           counters(1)<=unsigned(counters(1))+1;
         end if;
       elsif(phasecount="10")then
         if(clock120='1')then
           counters(2)<=unsigned(counters(2))+1;
         end if;
       elsif(phasecount="11")then
         if(clock120='1')then
           counters(3)<=unsigned(counters(3))+1;
         end if;
       end if;
     end if;
   elsif(state=check)then
     if(counters(0)(7 downto 1)="1111111" and counters(1)(7 downto 1)="1111111" and counters(2)=x"00" and counters(3)=x"00")then
       state<=calibdone;
     else
       state<=tryagain;
     end if;
   elsif(state=tryagain)then
     if(waitctr/=x"00000000")then
       waitctr<=unsigned(waitctr)-1;
     else
       resetclockbuf160<='1';
       state<=rstclockbuf;
     end if;
   elsif(state=rstclockbuf)then
     if(resetdoneclockbuf160='1')then
       resetclockbuf160<='0';
       waitctr<=x"00100000";
       state<=start;
     end if;
   elsif(state=calibdone)then
     done<='1';
   end if;
  end process;

end CBP;

--------------------------------------------------------------
