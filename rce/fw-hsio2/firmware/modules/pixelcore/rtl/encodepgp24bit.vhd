--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

--------------------------------------------------------------

entity encodepgp24bit is
generic( CHANNEL: std_logic_vector(7 downto 0):=x"ff");
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        enabled:    in std_logic;
        maxlength:  in natural range 0 to 16383;
        isrunning:    out std_logic;
	d_in:	    in std_logic_vector(24 downto 0);
	d_next:	    in std_logic_vector(24 downto 0);
        marker:     in std_logic;
	d_out:	    out std_logic_vector(17 downto 0);
        ldin:       in std_logic;
        ldout:      out std_logic;
        dnextvalid: in std_logic;
        dvalid: in std_logic;
        datawaiting: in std_logic;
        moredatawaiting: in std_logic;
        overflow:   out std_logic;
        parout:     out std_logic_vector(1 downto 0);
        valid:      out std_logic
);
end encodepgp24bit;

--------------------------------------------------------------

architecture ENCODEPGP24BIT of encodepgp24bit is

signal running: std_logic:='0';
signal oldrunning: std_logic:='0';
signal headercounter: std_logic_vector(3 downto 0);
signal parity: std_logic_vector(1 downto 0);
signal trailer: std_logic;
signal msb: std_logic_vector(15 downto 0);
signal oldmarker: std_logic;
signal oldoldmarker: std_logic;
signal marked: std_logic;
signal eventmark: std_logic;
signal wordcount: natural range 0 to 16383:=0;
signal enddata: std_logic := '0';
signal skip: std_logic := '0';
signal ldouts: std_logic := '0';
signal nomore: std_logic := '0';
signal init: std_logic := '0';
signal olddatawaiting: std_logic := '0';

begin
  isrunning<=running;
  ldout<=ldouts;
  parout<=parity;
  ldouts<=(ldin and running and not uOr(headercounter) and (not parity(0) or skip) and not nomore) or init;
    process(rst, clk)
    begin
        if(rst='1') then
          d_out<=x"0000"&"00";
          --acounter<=x"0000";
          running<='0';
          headercounter<=x"0";
          parity<="00";
          trailer<='0';
          msb<=x"0000";
          valid<='0';
          oldmarker<='0';
          oldoldmarker<='0';
          marked<='0';
          eventmark<='0';
          overflow<='0';
          wordcount<=0;
          skip<='0';
          enddata<='0';
          nomore<='0';
          init<='0';
        elsif (clk'event and clk='1') then
          oldmarker<=marker;
          oldoldmarker<=oldmarker;
          olddatawaiting<=datawaiting;
          if(marker='1' and oldoldmarker='0')then
            eventmark<='1';
          elsif(marked='1')then
            eventmark<='0';
          end if;
          oldrunning<=running;
          if(ldin='1')then 
            if(running='0')then
              if(oldrunning='1')then
                valid<='0';
              elsif(enabled='1' )then  -- SOF
                headercounter<="1111";
                d_out<="01"&x"0000"; --  SOF
                running<='1';
                parity<="00";
                valid<='1';
                trailer<='0';
                wordcount<=0;
                enddata<='0';
                skip<='0';
                nomore<='0';
                init<='1';
              end if;
            else -- running
              if(headercounter/="0000")then
                valid<='1';
                headercounter<=unsigned(headercounter)-1;
                if(headercounter="1111")then
                  init<='0';
                  d_out(7 downto 0)<=(others=>'0'); 
                  d_out(15 downto 8)<=CHANNEL;
                  d_out(17 downto 16)<="00";
                elsif(headercounter="1101")then
                  d_out<="00"&eventmark&"000"&x"000";
                  marked<='1';
                elsif(headercounter="0001") then
                  if(d_in(24)='1' and olddatawaiting='0' and (dnextvalid='0' or d_next(24)='0'))then
                    nomore<='1';
                  end if;
                else
                  d_out<=(others=>'0');
                  marked<='0';
                end if;
              elsif(trailer='1')then
                valid<='1';
                d_out<="10"&x"0000";
                trailer<='0';
                running<='0';
              elsif(skip='1')then
                valid<='1';
                d_out(15 downto 0)<=msb;
                skip<='0';
                if(d_next(24)='1' and (moredatawaiting='0' or wordcount>MAXLENGTH))then 
                  nomore<='1';
                end if;
              elsif(enddata='1')then
                valid<='1';
                if(parity(0)='1')then
                  d_out(15 downto 0)<= msb(7 downto 0) & x"00";
                else
                  d_out(15 downto 0)<=x"0000";
                end if;
                if(parity="00" or parity="11")then
                  trailer<='1';
                  d_out(17 downto 16)<="00";
                else
                  trailer<='0';
                  d_out(17 downto 16)<="10";
                  running<='0';
                end if;
              else-- data 
                if(nomore='1')then
                  enddata<='1';
                elsif(moredatawaiting='0' or wordcount>MAXLENGTH)then 
                  if(ldouts='1' and d_next(24)='1')then
                    nomore<='1';
                  elsif(ldouts='0' and d_in(24)='1')then
                    nomore<='1';
                    enddata<='1';
                  end if;
                  if(wordcount>MAXLENGTH)then
                    overflow<='1';
                  end if;
                end if;
                valid<='1';
                wordcount<=wordcount+1;
                parity<=unsigned(parity)+1;
                d_out(17 downto 16)<="00";
                if(parity(0)='0')then 
                  msb(7 downto 0)<=d_in(7 downto 0);
                  d_out(15 downto 0)<=d_in(23 downto 8);
                  skip<='0';
                else
                  d_out(15 downto 0)<=msb(7 downto 0) & d_in(23 downto 16);
                  msb<=d_in(15 downto 0);
                  skip<='1';
                end if;
              end if;
            end if;      
          else
            valid<='0';
          end if;
 	end if;
    
    end process;		

end ENCODEPGP24BIT;

--------------------------------------------------------------
