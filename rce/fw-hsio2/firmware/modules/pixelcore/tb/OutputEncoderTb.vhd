-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-07
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity outputencodertb is end outputencodertb;

architecture testbed of outputencodertb is
   signal clk  : sl;
   signal rst  : sl;
   signal encoding: slv(1 downto 0) := "01";
   signal datain: slv(15 downto 0):= "0100110100100000";
   signal data: sl:='0';
   signal dataout: slv(7 downto 0);
   signal counter: integer range 0 to 15 := 0;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   process begin
   wait until rising_edge(clk);
   if(counter=15)then
     counter<=0;
   else
     counter<=counter+1;
   end if;
   data<=datain(15-counter);
   end process;
     
   fei4fifo : entity work.outputencoderphase
     port map (
       clock => clk,
       rst => '0',
       datain => data,
       encode => encoding,
       dataout => dataout);

end testbed;
