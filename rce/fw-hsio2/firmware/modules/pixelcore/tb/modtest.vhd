-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2016-04-27
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity modtest is end modtest;

architecture testbed of modtest is
   signal clk : sl;
   signal rst  : sl:='1';
   signal counter: integer range 0 to 255 := 0;
   signal shift: slv(3 downto 0):=x"4";
   signal bcid: slv(3 downto 0):=x"0";
   constant orbit: natural :=10;
   signal result: slv(3 downto 0):=x"0";
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 25 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);

   process begin
     wait until rising_edge(clk);
     counter <= counter+1;
     if(bcid=orbit-1)then
       bcid<=(others=>'0');
     else
       bcid<=unsigned(bcid)+1;
     end if;
     if(unsigned(bcid)>=unsigned(shift))then
       result<=unsigned(bcid)-unsigned(shift);
     else
       result<=unsigned(bcid)+orbit-unsigned(shift);
     end if;
   end process;

end testbed;
