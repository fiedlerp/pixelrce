-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-07
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity oneshottb is end oneshottb;

architecture testbed of oneshottb is
   signal clk, clk1  : sl;
   signal rst  : sl;
   signal datain : sl := '0';
   signal dataout : sl;
   signal counter: integer range 0 to 15 := 0;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 50 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 0 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);
   CLK_1 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 6 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 0 ns)  -- Hold reset for this long)
      port map (
         clkP => clk1,
         clkN => open,
         rst  => open,
         rstL => open);

   process begin
     wait until rising_edge(clk1);
     if(counter<15)then
       counter <= counter+1;
       if(counter=4)then
         datain<='1';
       else
         datain<='0';
       end if;
     end if;
   end process;

   oneshot_Inst : entity work.oneshot
      port map (
        clk => clk,
        rst => rst,
        dataIn => datain,
        dataOut => dataout);

end testbed;
