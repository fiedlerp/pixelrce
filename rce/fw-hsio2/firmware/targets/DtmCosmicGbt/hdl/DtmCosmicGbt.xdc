# StdLib
set_property ASYNC_REG true [get_cells -hierarchical *crossDomainSyncReg_reg*]

# IO Types
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsP]
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsM]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareP]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareM]

# PGP Clocks
create_clock -name locRefClk -period 4.0 [get_ports locRefClkP]
create_clock -period 8.333 -name gbtRefClk [get_ports {locRefClk0P}]
create_generated_clock -name gbtTxFrameClk [get_pins -hier -filter {NAME =~ U_CtrlClockManager7/*/CLKOUT0}]
#create_generated_clock -name gbtTxWordClk [get_pins -hier -filter {NAME =~ U_CtrlClockManager7/*/CLKOUT1}]
create_clock -name gbtRxOutClk -period 8.333 [get_pins  -hierarchical -filter {NAME =~ "*/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i/RXOUTCLK"}]
set_false_path -to [get_pins U_HsioCosmicCore1/CLKFEI490/CE1]
create_clock -name sysClk40Unbuf_cc -period 25 [get_pins U_HsioCosmicCore1/CLKFEI4/I0]
create_clock -name sysClk160Unbuf_cc -period 6.25 [get_pins U_HsioCosmicCore1/CLKFEI4/I1]
# create_clock -period 25 -name clk40 [get_pins U_ClkGen/SysClkGen_Inst/inst/mmcm_adv_inst/CLKOUT0]


set_clock_groups -group [get_clocks gbtRxOutClk] -asynchronous

create_clock -name gbtTxOutClk -period 8.333 [get_pins  -hierarchical -filter {NAME =~ "*/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i/TXOUTCLK"}]
set_clock_groups -group [get_clocks gbtTxOutClk] -asynchronous
#create_generated_clock -name pgpClk250 -source [get_ports locRefClkP] \
##    -multiply_by 5 -divide_by 8 [get_pins U_HsioPgpLane/U_PgpClkGen/CLKOUT0]

set_clock_groups -asynchronous \
      -group [get_clocks -include_generated_clocks fclk0] \
      -group [get_clocks -include_generated_clocks locRefClk] \
      -group [get_clocks -include_generated_clocks gbtRefClk] \
      -group [get_clocks -include_generated_clocks sysClk40Unbuf_cc] \
      -group [get_clocks -include_generated_clocks sysClk160Unbuf_cc]

set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks sysClk40Unbuf_cc] -group [get_clocks -include_generated_clocks sysClk160Unbuf_cc]

## RX Phase Aligner
##----------------- 
## Comment: The period of TX_FRAMECLK is 25ns but "TS_GBTTX_SCRAMBLER_TO_GEARBOX" is set to 16ns, providing 9ns for setup margin.
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/scrambler/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/txGearbox/*/D}] 16.000

## GBT RX:
##--------

## Comment: The period of RX_FRAMECLK is 25ns but "TS_GBTRX_GEARBOX_TO_DESCRAMBLER" is set to 20ns, providing 5ns for setup margin.
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/descrambler/*/D}] 20.000




set_property PACKAGE_PIN P18 [get_ports discin[0]]
set_property PACKAGE_PIN P19 [get_ports discin[1]]
set_property PACKAGE_PIN U18 [get_ports discin[2]]
set_property PACKAGE_PIN V18 [get_ports discin[3]]

set_property IOSTANDARD LVCMOS25 [get_ports discin]
#SFP 1
set_property PACKAGE_PIN W2 [get_ports dtmToRtmHsP]
set_property PACKAGE_PIN W1 [get_ports dtmToRtmHsM]
set_property PACKAGE_PIN V4 [get_ports rtmToDtmHsP]
set_property PACKAGE_PIN V3 [get_ports rtmToDtmHsM]
#LS0
set_property PACKAGE_PIN T17  [get_ports locRefClk0P]
set_property PACKAGE_PIN U17  [get_ports locRefClk0M]
#MGT
#set_property PACKAGE_PIN U6  [get_ports locRefClk0P]
#set_property PACKAGE_PIN U5  [get_ports locRefClk0M]

