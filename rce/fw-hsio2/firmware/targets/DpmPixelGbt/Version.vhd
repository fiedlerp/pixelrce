-------------------------------------------------------------------------------
-- Title         : Version Constant File
-- Project       : COB Zynq DTM
-------------------------------------------------------------------------------
-- File          : Version.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 05/07/2013
-------------------------------------------------------------------------------
-- Description:
-- Version Constant Module
-------------------------------------------------------------------------------
-- Copyright (c) 2012 by SLAC. All rights reserved.
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

package Version is

constant FPGA_VERSION_C : std_logic_vector(31 downto 0) := x"00000011"; -- MAKE_VERSION

constant BUILD_STAMP_C : string := "DpmPixelGbt: Vivado v2014.4 (x86_64) Built Thu May  4 17:59:36 CEST 2017 by mkocian";

end Version;

-------------------------------------------------------------------------------
-- Revision History:
-- 02/04/2015 (0xH0000001): New Timing.
-- 05/12/2016 (0xH0000011): Fixed timing problem
-------------------------------------------------------------------------------

