-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : GbtHsioDtmAppCore.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2015-07-13
-- Last update: 2016-08-04
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.AxiLitePkg.all;
use work.RceG3Pkg.all;

library unisim;
use unisim.vcomponents.all;

entity GbtHsioDtmAppCore is
   generic (
      TPD_G : time := 1 ns);
   port (
      -- Debug
      led                : out slv(1 downto 0);
      -- -- RTM High Speed
      -- dtmToRtmHsP        : out   sl;
      -- dtmToRtmHsM        : out   sl;
      -- rtmToDtmHsP        : in    sl;
      -- rtmToDtmHsM        : in    sl;
      -- -- RTM Low Speed
      -- dtmToRtmLsP        : inout slv(5 downto 0);
      -- dtmToRtmLsM        : inout slv(5 downto 0);
      -- -- DPM Signals
      -- dpmClkP            : out   slv(2 downto 0);
      -- dpmClkM            : out   slv(2 downto 0);
      -- dpmFbP             : in    slv(7 downto 0);
      -- dpmFbM             : in    slv(7 downto 0);
      -- Backplane Clocks
      bpClkIn            : in  slv(5 downto 0);
      bpClkOut           : out slv(5 downto 0);
      -- CPU System Clocks
      sysClk125          : in  sl;
      sysClk125Rst       : in  sl;
      sysClk200          : in  sl;
      sysClk200Rst       : in  sl;
      -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
      axiClk             : in  sl;
      axiRst             : in  sl;
      extAxilReadMaster  : in  AxiLiteReadMasterType;
      extAxilReadSlave   : out AxiLiteReadSlaveType;
      extAxilWriteMaster : in  AxiLiteWriteMasterType;
      extAxilWriteSlave  : out AxiLiteWriteSlaveType;
      -- DMA Interfaces
      dmaClk             : out slv(3 downto 0);
      dmaRst             : out slv(3 downto 0);
      dmaObMaster        : in  AxiStreamMasterArray(3 downto 0);
      dmaObSlave         : out AxiStreamSlaveArray(3 downto 0);
      dmaIbMaster        : out AxiStreamMasterArray(3 downto 0);
      dmaIbSlave         : in  AxiStreamSlaveArray(3 downto 0));         
end GbtHsioDtmAppCore;

architecture mapping of GbtHsioDtmAppCore is

   signal sysClk : sl;
   signal sysRst : sl;

begin

   ---------------------------------
   -- Clock and Reset Configurations
   ---------------------------------

   -- Set the application clock and reset
   sysClk <= sysClk125;
   sysRst <= sysClk125Rst;

   -- Set the DMA Clocks and Resets
   dmaClk <= (others => sysClk);
   dmaRst <= (others => sysRst);

   ---------------------
   -- DMA Configurations
   ---------------------
   U_AxiLiteEmpty_1: entity work.AxiLiteEmpty
      generic map (
         TPD_G            => TPD_G,
         AXI_ERROR_RESP_G => AXI_RESP_OK_C,
         NUM_WRITE_REG_G  => 1,
         NUM_READ_REG_G   => 1)
      port map (
         axiClk         => axiClk,          -- [in]
         axiClkRst      => axiRst,       -- [in]
         axiReadMaster  => extAxilReadMaster,   -- [in]
         axiReadSlave   => extAxilReadSlave,    -- [out]
         axiWriteMaster => extAxilWriteMaster,  -- [in]
         axiWriteSlave  => extAxilWriteSlave);   -- [out]


--    DMA_LOOPBACK: for i in 0 to 1 generate
--       dmaIbMaster(i) <= dmaObMaster(i);
--       dmaObSlave(i) <= dmaIbSlave(i);
--    end generate DMA_LOOPBACK;

   DMA_STATIC: for i in 0 to 3 generate
      dmaIbMaster(i) <= AXI_STREAM_MASTER_INIT_C;
      dmaObSlave(i) <= AXI_STREAM_SLAVE_FORCE_C;
   end generate DMA_STATIC;
   -- DMA[2] = Loopback Configuration
--    dmaIbMaster(2) <= dmaObMaster(2);
--    dmaObSlave(2)  <= dmaIbSlave(2);

--    -- DMA[1] = Loopback Configuration
--    dmaIbMaster(1) <= dmaObMaster(1);
--    dmaObSlave(1)  <= dmaIbSlave(1);

--    -- DMA[0] = DMA Inbound only, PRBS data
--    SsiPrbsTx_Inst : entity work.SsiPrbsTx
--       generic map (
--          -- General Configurations
--          TPD_G                      => 1 ns,
--          AXI_ERROR_RESP_G           => AXI_RESP_OK_C,
--          -- FIFO Configurations
--          BRAM_EN_G                  => true,
--          XIL_DEVICE_G               => "7SERIES",
--          USE_BUILT_IN_G             => false,
--          GEN_SYNC_FIFO_G            => false,
--          ALTERA_SYN_G               => false,
--          ALTERA_RAM_G               => "M9K",
--          CASCADE_SIZE_G             => 1,
--          FIFO_ADDR_WIDTH_G          => 9,
--          FIFO_PAUSE_THRESH_G        => 2**8,
--          -- PRBS Configurations
--          PRBS_SEED_SIZE_G           => 32,
--          PRBS_TAPS_G                => (0 => 31, 1 => 6, 2 => 2, 3 => 1),
--          -- AXI Stream Configurations
--          MASTER_AXI_STREAM_CONFIG_G => RCEG3_AXIS_DMA_CONFIG_C,
--          MASTER_AXI_PIPE_STAGES_G   => 0)
--       port map (
--          -- Master Port (mAxisClk)
--          mAxisClk        => sysClk,
--          mAxisRst        => sysRst,
--          mAxisMaster     => dmaIbMaster(0),
--          mAxisSlave      => dmaIbSlave(0),
--          -- Trigger Signal (locClk domain)
--          locClk          => axiClk,
--          locRst          => axiRst,
--          trig            => '1',
--          packetLength    => x"0000FFFF",
--          forceEofe       => '0',
--          busy            => open,
--          tDest           => X"00",
--          tId             => X"00",
--          -- Optional: Axi-Lite Register Interface (locClk domain)
--          axilReadMaster  => extAxilReadMaster,
--          axilReadSlave   => extAxilReadSlave,
--          axilWriteMaster => extAxilWriteMaster,
--          axilWriteSlave  => extAxilWriteSlave);

--    -- Blow off the outbound data
--    dmaObSlave(0) <= AXI_STREAM_SLAVE_FORCE_C;

   -----------------------
   -- Misc. Configurations
   -----------------------  

   led      <= (others => '0');
   bpClkOut <= (others => '0');

end mapping;
