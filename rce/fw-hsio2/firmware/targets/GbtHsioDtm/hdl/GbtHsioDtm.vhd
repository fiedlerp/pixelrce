-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : GbtHsioDtm.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2015-07-13
-- Last update: 2016-08-15
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'Example RCE Project'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'Example RCE Project', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;

-- GBT packages
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;

entity GbtHsioDtm is
   port (


      -- Reference Clock
      locRefClk0P : in sl;
      locRefClk0M : in sl;
      locRefClkP : in sl;
      locRefClkM : in sl;


      -- RTM High Speed
      dtmToRtmHsP : out sl;             -- to_dtm0_tx_p
      dtmToRtmHsM : out sl;             -- to_dtm0_tx_m
      rtmToDtmHsP : in  sl;             -- to_dtm0_rx_p
      rtmToDtmHsM : in  sl             -- to_dtm0_rx_m
      );
end GbtHsioDtm;

architecture TOP_LEVEL of GbtHsioDtm is

   constant TPD_C : time := 1 ns;


   signal sysClk125    : sl;
   signal sysClk125Rst : sl;

   -------------------------------------------------------------------------------------------------
   signal reset_from_genRst                      : std_logic;
   signal generalReset_from_user                 : std_logic;
--   signal fabricClk_from_userClockIbufgds : std_logic;
   signal gbtRefClk     : sl;
   signal gbtRefClkb     : sl;
   signal gbtTxFrameClk : sl;
   signal gbtTxWordClk : sl;

--   signal generalReset                    : sl;
   signal manualResetTx           : sl;
   signal manualResetRx           : sl;
   signal clkMuxSel               : sl;
   signal testPatternSel          : slv(1 downto 0);
   signal loopBack                : slv(2 downto 0);
   signal resetDataErrorSeenFlag  : sl;
   signal resetGbtRxReadyLostFlag : sl;
   signal txIsDataSel             : sl;
   --------------------------------------------------      
--   signal latOptGbtBankTx_from_gbtExmplDsgn         : sl;
--   signal latOptGbtBankRx_from_gbtExmplDsgn         : sl;
   signal txFrameClkPllLocked     : sl;
   signal mgtReady                : sl;
   signal rxBitSlipNbr            : slv(GBTRX_BITSLIP_NBR_MSB downto 0);
   signal rxWordClkReady          : sl;
   signal rxFrameClkReady         : sl;
   signal gbtRxReady              : sl;
   signal rxIsData                : sl;
   signal gbtRxReadyLostFlag      : sl;
   signal rxDataErrorSeen         : sl;
   signal rxExtrDataWidebusErSeen : sl;

   signal txData    : slv(83 downto 0);
   signal rxData    : slv(83 downto 0);
   --------------------------------------------------      
   signal txWidebus : slv(31 downto 0);
   signal rxWidebus : slv(31 downto 0);

   signal vioControl_from_icon   : slv(35 downto 0);
   signal txIlaControl_from_icon : slv(35 downto 0);
   signal rxIlaControl_from_icon : slv(35 downto 0);
   --------------------------------------------------
   signal sync_from_vio          : slv(11 downto 0);
   signal async_to_vio           : slv(17 downto 0);

   signal txFrameClk  : sl;
   signal txWordClk   : sl;
   signal rxFrameClk  : sl;
   signal rxWordClk   : sl;
   --------------------------------------------------                                    
   signal txMatchFlag : sl;
   signal rxMatchFlag : sl;

   signal errorCounter : slv(63 downto 0);
   signal wordCounter  : slv(63 downto 0);

   -- ILA component  --
   --================--
   -- Vivado synthesis tool does not support mixed-language
   -- Solution: http://www.xilinx.com/support/answers/47454.html
   component xlx_k7v7_vivado_debug port(
      CLK    : in sl;
      PROBE0 : in slv(83 downto 0);
      PROBE1 : in slv(31 downto 0);
      PROBE2 : in slv(3 downto 0);
      PROBE3 : in slv(0 downto 0)
      );
   end component;

   component xlx_k7v7_vio
      port (
         clk        : in  sl;
         probe_in0  : in  slv(0 downto 0);
         probe_in1  : in  slv(0 downto 0);
         probe_in2  : in  slv(0 downto 0);
         probe_in3  : in  slv(0 downto 0);
         probe_in4  : in  slv(0 downto 0);
         probe_in5  : in  slv(5 downto 0);
         probe_in6  : in  slv(0 downto 0);
         probe_in7  : in  slv(0 downto 0);
         probe_in8  : in  slv(0 downto 0);
         probe_in9  : in  slv(0 downto 0);
         probe_in10 : in  slv(0 downto 0);
         probe_in11 : in  slv(0 downto 0);
         probe_in12 : in  slv(0 downto 0);
         probe_out0 : out slv(0 downto 0);
         probe_out1 : out slv(0 downto 0);
         probe_out2 : out slv(1 downto 0);
         probe_out3 : out slv(2 downto 0);
         probe_out4 : out slv(0 downto 0);
         probe_out5 : out slv(0 downto 0);
         probe_out6 : out slv(0 downto 0);
         probe_out7 : out slv(0 downto 0);
         probe_out8 : out slv(0 downto 0)
         );
   end component;



begin

   -------------------------------------------------------------------------------------------------
   --IBUFDS_GTE2_REFCLK : IBUFDS_GTE2
   --   port map (
   --      I   => locRefClk0P,
   --      IB  => locRefClk0M,
   --      CEB => '0',
   --      O   => gbtRefClk);
   IBUFDS_GTE2_REFCLK1 : IBUFDS_GTE2
      port map (
         I   => locRefClkP,
         IB  => locRefClkM,
         CEB => '0',
         ODIV2   => sysClk125);

   PwrUpRst_Inst : entity work.PwrUpRst
      port map (
         clk    => sysClk125,
         rstOut => sysClk125Rst);
   PwrUpRst_Inst2 : entity work.PwrUpRst
      port map (
         clk    => sysClk125,
         arst   => generalReset_from_user,
         rstOut => reset_from_genRst);
   U_refclk : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                       port map ( I  => locRefClk0P  , IB=> locRefClk0M   , O => gbtRefClkb  );
   U_bufg: BUFG port map (I => gbtRefClkb, O => gbtRefClk);

   U_CtrlClockManager7 : entity work.ClockManager7
      generic map (
         TPD_G            => TPD_C,
         TYPE_G           => "MMCM",
         INPUT_BUFG_G     => true,
         FB_BUFG_G        => true,
         NUM_CLOCKS_G     => 1,
         BANDWIDTH_G      => "OPTIMIZED",
         CLKIN_PERIOD_G   => 8.333,
         DIVCLK_DIVIDE_G  => 1,
         CLKFBOUT_MULT_G  => 8,
         CLKOUT0_DIVIDE_G => 24)
      port map (
         clkIn     => gbtRefClk,
         rstIn     => sysClk125Rst,
         clkOut(0) => gbtTxFrameClk,
--         clkOut(1) => gbtTxWordClk,
         locked    => txFrameClkPllLocked);




   -------------------------------------------------------------------------------------------------
   -- Example design
   -------------------------------------------------------------------------------------------------
   U_xlx_k7v7_gbt_example_design_1 : entity work.xlx_k7v7_gbt_example_design
      generic map (
         GBT_BANK_ID           => 0,
         NUM_LINKS             => 1,
         TX_OPTIMIZATION       => STANDARD,
         RX_OPTIMIZATION       => STANDARD,
         TX_ENCODING           => GBT_FRAME,
         RX_ENCODING           => GBT_FRAME,
         DATA_GENERATOR_ENABLE => 1,
         DATA_CHECKER_ENABLE   => 1,
         MATCH_FLAG_ENABLE     => 1)
      port map (
         FRAMECLK_40MHZ => gbtTxFrameClk,  -- [in]
         XCVRCLK        => gbtRefClk,      -- [in]

         -- For debugging
         RX_FRAMECLK_O     => open,     -- [out]
         RX_WORDCLK_O      => open,     -- [out]
         TX_FRAMECLK_O     => open,     -- [out]
         TX_WORDCLK_O      => open,     -- [out]
         RX_WORDCLK_RDY_O  => open,     -- [out]
         RX_FRAMECLK_RDY_O => open,     -- [out]

         --
         GBTBANK_GENERAL_RESET_I                         => reset_from_genRst,        -- [in]
         GBTBANK_MANUAL_RESET_TX_I                       => manualResetTx,            -- [in]
         GBTBANK_MANUAL_RESET_RX_I                       => manualResetRx,            -- [in]
         GBTBANK_MGT_RX_P(1)                             => rtmToDtmHsP,              -- [in]
         GBTBANK_MGT_RX_N(1)                             => rtmToDtmHsM,              -- [in]
         GBTBANK_MGT_TX_P(1)                             => dtmToRtmHsP,              -- [out]
         GBTBANK_MGT_TX_N(1)                             => dtmToRtmHsM,              -- [out]
         GBTBANK_GBT_DATA_I(1)                           => (others => '0'),          -- [in]
         GBTBANK_WB_DATA_I(1)                            => (others => '0'),          -- [in]
         TX_DATA_O(1)                                    => txData,                   -- [out]
         WB_DATA_O(1)                                    => txWidebus,                -- [out]
         GBTBANK_GBT_DATA_O(1)                           => rxData,                   -- [out]
         GBTBANK_WB_DATA_O(1)                            => rxWidebus,                -- [out]
         GBTBANK_MGT_DRP_RST                             => '0',                      -- [in]
         GBTBANK_MGT_DRP_CLK                             => sysClk125,                -- [in]
         GBTBANK_TX_ISDATA_SEL_I(1)                      => txIsdataSel,              -- [in]
         GBTBANK_TEST_PATTERN_SEL_I                      => testPatternSel,           -- [in]
         GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(1)         => resetGbtRxReadyLostFlag,  -- [in]
         GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I(1)          => resetDataErrorSeenFlag,   -- [in]
         GBTBANK_GBTTX_READY_O(1)                        => open,                     -- [out]
         GBTBANK_GBTRX_READY_O(1)                        => gbtRxReady,               -- [out]
         GBTBANK_LINK_READY_O(1)                         => mgtReady,                 -- [out]
         GBTBANK_LINK1_BITSLIP_O                         => rxBitSlipNbr,             -- [out]
         GBTBANK_TX_MATCHFLAG_O                          => txMatchFlag,              -- [out]
         GBTBANK_GBTRXREADY_LOST_FLAG_O(1)               => gbtRxReadyLostFlag,       -- [out]
         GBTBANK_RXDATA_ERRORSEEN_FLAG_O(1)              => rxDataErrorSeen,          -- [out]
         GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O(1) => rxExtrDataWidebusErSeen,  -- [out]
         GBTBANK_RX_MATCHFLAG_O(1)                       => rxMatchFlag,              -- [out]
         GBTBANK_RX_ISDATA_SEL_O(1)                      => rxIsData,                 -- [out]
         RXDATA_ERROR_CNT                                => errorCounter,             -- [out]
         RXDATA_WORD_CNT                                 => wordCounter,              -- [out]
         GBTBANK_LOOPBACK_I                              => loopback,                 -- [in]
         GBTBANK_TX_POL(1)                               => '1',                      -- [in]
         GBTBANK_RX_POL(1)                               => '0');                     -- [in]



   vio : xlx_k7v7_vio
      port map (
         clk => gbtTxFrameClk,

         probe_in0(0)  => rxIsData,
         probe_in1(0)  => txFrameClkPllLocked,
         probe_in2(0)  => '0',
         probe_in3(0)  => mgtReady,
         probe_in4(0)  => rxWordClkReady,
         probe_in5     => rxBitSlipNbr,
         probe_in6(0)  => rxFrameClkReady,
         probe_in7(0)  => gbtRxReady,
         probe_in8(0)  => gbtRxReadyLostFlag,
         probe_in9(0)  => rxDataErrorSeen,
         probe_in10(0) => rxExtrDataWidebusErSeen,
         probe_in11(0) => '0',
         probe_in12(0) => '0',

         probe_out0(0) => generalReset_from_user,
         probe_out1(0) => open,         --clkMuxSel_from_user,
         probe_out2    => testPatternSel,
         probe_out3    => loopback,
         probe_out4(0) => resetDataErrorSeenFlag,
         probe_out5(0) => resetGbtRxReadyLostFlag,
         probe_out6(0) => txIsDataSel,
         probe_out7(0) => manualResetTx,
         probe_out8(0) => manualResetRx);

   txILa : xlx_k7v7_vivado_debug
      port map (
         CLK       => gbtTxFrameClk,
         PROBE0    => txData,
         PROBE1    => txWidebus,
         PROBE2    => (others => '0'),  --8b10b support removed
         PROBE3(0) => txIsDataSel);

   rxIla : xlx_k7v7_vivado_debug
      port map (
         CLK       => gbtTxFrameClk,
         PROBE0    => rxData,
         PROBE1    => rxWidebus,
         PROBE2    => (others => '0'),  --8b10b support removed
         PROBE3(0) => rxIsData);
end architecture TOP_LEVEL;
