##############################################################################
## This file is part of 'Example RCE Project'.
## It is subject to the license terms in the LICENSE.txt file found in the 
## top-level directory of this distribution and at: 
##    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
## No part of 'Example RCE Project', including this file, 
## may be copied, modified, propagated, or distributed except according to 
## the terms contained in the LICENSE.txt file.
##############################################################################

#SFP 1
set_property PACKAGE_PIN W2 [get_ports dtmToRtmHsP]
set_property PACKAGE_PIN W1 [get_ports dtmToRtmHsM]
set_property PACKAGE_PIN V4 [get_ports rtmToDtmHsP]
set_property PACKAGE_PIN V3 [get_ports rtmToDtmHsM]
#LS0
set_property PACKAGE_PIN T17  [get_ports locRefClk0P]
set_property PACKAGE_PIN U17  [get_ports locRefClk0M]
#MGT
#set_property PACKAGE_PIN U6  [get_ports locRefClk0P]
#set_property PACKAGE_PIN U5  [get_ports locRefClk0M]

set_property PACKAGE_PIN W6  [get_ports locRefClk1P]
set_property PACKAGE_PIN W5  [get_ports locRefClk1M]

create_clock -period 8.333 -name gbtRefClk [get_ports {locRefClk0P}]
create_clock -period 4.0 -name sysClk125 [get_ports {locRefClk1P}]

create_generated_clock -name gbtTxFrameClk [get_pins -hier -filter {NAME =~ U_CtrlClockManager7/*/CLKOUT0}]
#create_generated_clock -name gbtTxWordClk [get_pins -hier -filter {NAME =~ U_CtrlClockManager7/*/CLKOUT1}]

create_clock -name gbtRxOutClk -period 8.333 [get_pins  -hierarchical -filter {NAME =~ "*/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i/RXOUTCLK"}]
set_clock_groups -group [get_clocks gbtRxOutClk] -asynchronous

create_clock -name gbtTxOutClk -period 8.333 [get_pins  -hierarchical -filter {NAME =~ "*/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i/TXOUTCLK"}]
set_clock_groups -group [get_clocks gbtTxOutClk] -asynchronous

set_clock_groups -asynchronous \
    -group [get_clocks -include_generated_clocks sysClk125] \
    -group [get_clocks -include_generated_clocks gbtRefClk]

## RX Phase Aligner
##----------------- 
## Comment: The period of TX_FRAMECLK is 25ns but "TS_GBTTX_SCRAMBLER_TO_GEARBOX" is set to 16ns, providing 9ns for setup margin.
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/scrambler/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/txGearbox/*/D}] 16.000

## GBT RX:
##--------

## Comment: The period of RX_FRAMECLK is 25ns but "TS_GBTRX_GEARBOX_TO_DESCRAMBLER" is set to 20ns, providing 5ns for setup margin.
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/descrambler/*/D}] 20.000

