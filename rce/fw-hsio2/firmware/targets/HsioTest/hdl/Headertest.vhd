library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.StdRtlPkg.all;

entity Headertest is
  port (
    clk    : in sl;
    lvdso_p: out slv(15 downto 0);
    lvdso_n: out slv(15 downto 0);
    lvdsi_p: in slv(15 downto 0);
    lvdsi_n: in slv(15 downto 0);
    header1: out slv(39 downto 0) := (others => '0');
    header2: out slv(39 downto 0) := (others => '0')
  );
end headertest;

architecture headertest of Headertest is
  signal counter: slv(19 downto 0) := (others => '0');
  signal header1int: slv(39 downto 0) := (others => '0');
  signal header2int: slv(39 downto 0) := (others => '0');
begin
  process begin
  wait until rising_edge(clk);
  counter<=unsigned(counter)+1;
  if(counter=x"00000")then
    header1int <= (others => '0');
    header2int <= (others => '0');
  elsif(counter=x"fffff")then
    header1<=header1int;
    header2<=header2int;
  elsif(counter(3 downto 0)=x"1")then
    lvdso_p<=counter(19 downto 4);
    lvdso_n(15 downto 8)<=counter(11 downto 4);
    lvdso_n(7 downto 0)<=counter(19 downto 12);
     
  elsif(counter(3 downto 0)=x"e")then
    for I in 0 to 7 loop
      header1int((I/2)*10+(I mod 2)*4+1)<= header1int((I/2)*10+(I mod 2)*4+1) or (lvdsi_p(I) xor counter(4+I));
      header1int((I/2)*10+(I mod 2)*4+3)<= header1int((I/2)*10+(I mod 2)*4+3) or (lvdsi_p(I) xor counter(4+I));
      header1int((I/2)*10+(I mod 2)*4+2)<= header1int((I/2)*10+(I mod 2)*4+2) or (lvdsi_n(I) xor counter(12+I));
      header1int((I/2)*10+(I mod 2)*4+4)<= header1int((I/2)*10+(I mod 2)*4+4) or (lvdsi_n(I) xor counter(12+I));
  
      header2int((I/2)*10+(I mod 2)*4+1)<= header2int((I/2)*10+(I mod 2)*4+1) or (lvdsi_p(I+8) xor counter(12+I));
      header2int((I/2)*10+(I mod 2)*4+3)<= header2int((I/2)*10+(I mod 2)*4+3) or (lvdsi_p(I+8) xor counter(12+I));
      header2int((I/2)*10+(I mod 2)*4+2)<= header2int((I/2)*10+(I mod 2)*4+2) or (lvdsi_n(I+8) xor counter(4+I));
      header2int((I/2)*10+(I mod 2)*4+4)<= header2int((I/2)*10+(I mod 2)*4+4) or (lvdsi_n(I+8) xor counter(4+I));
    end loop;
  end if;
end process;
end headertest;
  
    
      
