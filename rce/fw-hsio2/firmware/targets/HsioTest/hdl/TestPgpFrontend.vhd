library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.SsiCmdMasterPkg.all;
use work.AxiLitePkg.all;
use work.Pgp2bPkg.all;
use work.HsioTestPkg.all;

entity TestPgpFrontend is
  generic 
  (
    TPD_G               : time                       := 1 ns;
    CASCADE_SIZE_G      : integer range 1 to (2**24) := 1;

    -- MGT Configurations
    CLK_DIV_G        : integer;
    CLK25_DIV_G      : integer;
    RX_OS_CFG_G      : bit_vector;
    RXCDR_CFG_G      : bit_vector;
    RXLPM_INCM_CFG_G : bit;
    RXLPM_IPCM_CFG_G : bit
  );
  port
  (
    clk:               in  sl;
    rst:               in  sl := '0';
    -- QPLL
    gtQPllOutRefClk  : in  slv(1 downto 0);
    gtQPllOutClk     : in  slv(1 downto 0);
    gtQPllLock       : in  slv(1 downto 0);
    gtQPllRefClkLost : in  slv(1 downto 0);
    gtQPllReset      : out slv(1 downto 0);
    -- GT Pins
    gtTxP            : out sl;
    gtTxN            : out sl;
    gtRxP            : in  sl;
    gtRxN            : in  sl;
    -- Clock, Resets, and Status Signals
    pgpClk              : in  sl;
    txReady             : out sl;
    rxReady             : out sl;
    sendOverflow        : out sl;
      
    -- Error Detection Signals (sAxisClk domain)
    trigIn          : in  sl := '0';
    txBusy          : out sl;
    updatedResults  : out sl;
    rxBusy          : out sl;
    errMissedPacket : out sl;
    errLength       : out sl;
    errDataBus      : out sl;
    errEofe         : out sl;
    errWordCnt      : out slv(31 downto 0);
    errbitCnt       : out slv(31 downto 0);
    packetRate      : out slv(31 downto 0);
    packetLength    : in  slv(31 downto 0);
    rxPacketLength  : out slv(31 downto 0);

    -- Power down control
    txPowerDown     : in slv(1 downto 0) := "00";
    rxPowerDown     : in slv(1 downto 0) := "00"
  );
end entity;

architecture TestPgpFrontend of TestPgpFrontend is
   -- Non VC Rx Signals
   signal pgpRxIn  : Pgp2bRxInType;
   signal pgpRxOut : Pgp2bRxOutType;

   -- Non VC Tx Signals
   signal pgpTxIn  : Pgp2bTxInType;
   signal pgpTxOut : Pgp2bTxOutType;

   -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
   signal pgpTxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpTxSlaves  : AxiStreamSlaveArray(3 downto 0);

   -- Frame Receive Interface - 1 Lane, Array of 4 VCs
   signal pgpRxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpRxCtrl    : AxiStreamCtrlArray(3 downto 0);
   signal sAxisCtrl    : AxiStreamCtrlType;
  -- Synchronized results
   signal updatedResultss : sl;
   signal rxBusys         : sl;
   signal errMissedPackets: sl;
   signal errLengths      : sl;
   signal errDataBuss     : sl;
   signal errEofes        : sl;
   signal errWordCnts     : slv(31 downto 0);
   signal errbitCnts      : slv(31 downto 0);
   signal packetRates     : slv(31 downto 0);
   signal packetLengths   : slv(31 downto 0);
   signal rxPacketLengths : slv(31 downto 0);
   signal txPowerDownSync : slv(1 downto 0);
   signal rxPowerDownSync : slv(1 downto 0);

   signal pgpRst: sl;
   --signal pgpRstnobuf: sl;

   -- Constants
   constant CLK_PERIOD_C       : time             := 10 ns;
   constant TPD_C              : time             := CLK_PERIOD_C/4;
   constant TX_PACKET_LENGTH_C : integer          := 16;

   constant USE_BUILT_IN_C  : boolean := false;
   constant GEN_SYNC_FIFO_C : boolean := false;
   
   constant AXI_STREAM_CONFIG_C : AxiStreamConfigType := SSI_PGP2B_CONFIG_C;
   
begin
   SysRst_Inst : entity work.RstSync
    port map(
      clk      => pgpClk,
      asyncRst => rst,
      syncRst  => pgpRst);  
    --BUFG_Inst_rst113 : BUFG
    --  port map (
    --    I => pgpRstnobuf,
    --    O => pgpRst);
  
   synchv_txpd: entity work.SynchronizerVector
   generic map(
     WIDTH_G => 2)
   port map(
     clk => pgpClk,
     dataIn => txPowerDown,
     dataOut => txPowerDownSync);
   synchv_rxpd: entity work.SynchronizerVector
   generic map(
     WIDTH_G => 2)
   port map(
     clk => pgpClk,
     dataIn => rxPowerDown,
     dataOut => rxPowerDownSync);
   Pgp2bGtp7MultiLane_Inst : entity work.Pgp2bGtp7MultiLaneWithPowerdown
      generic map (
         -- CPLL Settings -
         RXOUT_DIV_G        => CLK_DIV_G,
         TXOUT_DIV_G        => CLK_DIV_G,
         RX_CLK25_DIV_G     => CLK25_DIV_G,
         TX_CLK25_DIV_G     => CLK25_DIV_G,
         RX_OS_CFG_G        => RX_OS_CFG_G,
         RXCDR_CFG_G        => RXCDR_CFG_G,
         RXLPM_INCM_CFG_G   => RXLPM_INCM_CFG_G,
         RXLPM_IPCM_CFG_G   => RXLPM_IPCM_CFG_G,
         -- Configure PLL sources
         TX_PLL_G           => "PLL0",
         RX_PLL_G           => "PLL0")            
      port map (
         -- GT Clocking
         stableClk        => clk,
         gtQPllOutRefClk  => gtQPllOutRefClk,
         gtQPllOutClk     => gtQPllOutClk,
         gtQPllLock       => gtQPllLock,
         gtQPllRefClkLost => gtQPllRefClkLost,
         gtQPllReset      => gtQPllReset,
         -- Gt Serial IO
         gtTxP(0)         => gtTxP,
         gtTxN(0)         => gtTxN,
         gtRxP(0)         => gtRxP,
         gtRxN(0)         => gtRxN,
         -- Tx Clocking
         pgpTxReset       => pgpRst,
         pgpTxClk         => pgpClk,
         pgpTxMmcmReset   => open,
         pgpTxMmcmLocked  => '1',
         -- Rx clocking
         pgpRxReset       => pgpRst,
         pgpRxRecClk      => open,
         pgpRxClk         => pgpClk,
         pgpRxMmcmReset   => open,
         pgpRxMmcmLocked  => '1',
         -- Non VC Rx Signals
         pgpRxIn          => pgpRxIn,
         pgpRxOut         => pgpRxOut,
         -- Non VC Tx Signals
         pgpTxIn          => pgpTxIn,
         pgpTxOut         => pgpTxOut,
         -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
         pgpTxMasters     => pgpTxMasters,
         pgpTxSlaves      => pgpTxSlaves,
         -- Frame Receive Interface - 1 Lane, Array of 4 VCs
         pgpRxMasters     => pgpRxMasters,
         pgpRxMasterMuxed => open,
         PgpRxCtrl        => pgpRxCtrl,
         txPowerDown      => txPowerDownSync,
         rxPowerDown      => rxPowerDownSync
       );    
  
   -- VcPrbsTx (VHDL module to be tested)
   SsiPrbsTx_Inst : entity work.SsiPrbsTx
      generic map (
         TPD_G               => TPD_C,
         USE_BUILT_IN_G      => USE_BUILT_IN_C,
         GEN_SYNC_FIFO_G     => GEN_SYNC_FIFO_C,
         MASTER_AXI_STREAM_CONFIG_G => AXI_STREAM_CONFIG_C)
      port map (
         -- Master Port (mAxisClk)
         mAxisClk     => pgpClk,
         mAxisRst     => pgpRst,
         mAxisSlave   => pgpTxSlaves(0),
         mAxisMaster  => pgpTxMasters(0),
         
         -- Trigger Signal (locClk domain)
         locClk       => clk,
         locRst       => rst,
         trig         => trigIn,
         packetLength => packetLength,
         busy         => txBusy,
         tDest        => (others => '0'),
         tId          => (others => '0'));
   
   -- VcPrbsRx (VHDL module to be tested)
   SsiPrbsRx_Inst : entity work.SsiPrbsRx
      generic map (
         TPD_G               => TPD_C,
         USE_BUILT_IN_G      => USE_BUILT_IN_C,
         GEN_SYNC_FIFO_G     => GEN_SYNC_FIFO_C,
         SLAVE_AXI_STREAM_CONFIG_G => AXI_STREAM_CONFIG_C)
      port map (
         -- Streaming RX Data Interface (sAxisClk domain) 
         mAxisClk        => pgpClk,
         sAxisClk        => pgpClk,
         sAxisRst        => pgpRst,
         sAxisMaster     => pgpRxMasters(0),
         sAxisSlave      => open,
         sAxisCtrl       => pgpRxCtrl(0),
         -- Error Detection Signals (sAxisClk domain)
         updatedResults  => updatedResultss,
         busy            => rxBusys,
         errMissedPacket => errMissedPackets,
         errLength       => errLengths,
         errDataBus      => errDataBuss,
         errEofe         => errEofes,
         errWordCnt      => errWordCnts,
         errbitCnt       => errbitCnts,
         packetRate      => packetRates,
         packetLength    => rxPacketLengths);       

   synchos_1: entity work.SynchronizerOneShot
   port map(
     clk => clk,
     dataIn => updatedResultss,
     dataOut => updatedResults);

   synchv_0: entity work.SynchronizerVector
   generic map(
     WIDTH_G => 32)
   port map(
     clk => clk,
     dataIn => errWordCnts,
     dataOut => errWordCnt);

   synchv_1: entity work.SynchronizerVector
   generic map(
     WIDTH_G => 32)
   port map(
     clk => clk,
     dataIn => errbitCnts,
     dataOut => errbitCnt);

   synchv_2: entity work.SynchronizerVector
   generic map(
     WIDTH_G => 32)
   port map(
     clk => clk,
     dataIn => packetRates,
     dataOut => packetRate);

   synchv_3: entity work.SynchronizerVector
   generic map(
     WIDTH_G => 32)
   port map(
     clk => clk,
     dataIn => rxPacketLengths,
     dataOut => rxPacketLength);

   synch_0: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => rxBusys,
     dataOut => rxBusy);

   synch_1: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => errMissedPackets,
     dataOut => errMissedPacket);

   synch_2: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => errLengths,
     dataOut => errLength);

   synch_3: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => errDataBuss,
     dataOut => errDataBus);

   synch_4: entity work.Synchronizer
   port map(
     clk => clk,
     dataIn => errEofes,
     dataOut => errEofes);

   SyncOut_Inst : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 2)    
      port map (
         clk        => clk,
         dataIn(1)  => pgpTxOut.linkReady,
         dataIn(0)  => pgpRxOut.linkReady,
         dataOut(1) => txReady,
         dataOut(0) => rxReady);   

end TestPgpFrontend;
