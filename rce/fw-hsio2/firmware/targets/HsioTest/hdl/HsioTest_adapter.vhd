-------------------------------------------------------------------------------
-- Title         : HsioCosmic
-- Project       : ATLAS IBL
-------------------------------------------------------------------------------
-- File          : HsioTest.vhd
-- Author        : Martin Kocian, kocian@slac.stanford.edu
-- Author        : Johannes Agricola, johannes.agricola@cern.ch
-- Created       : 11/23/2014
-------------------------------------------------------------------------------
-- Description:
-- Test FGPA configuration for the HSIO 2 board
-------------------------------------------------------------------------------

library ieee;
library unisim;
use unisim.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.stdrtlpkg.all;
use work.HD44780DriverPkg.all;
use work.HsioTestPkg.all;
use work.all;


entity HsioTest is 
  port ( 
    iRefClk0B113P: in   sl;
    iRefClk0B113M: in   sl;

    iRefClk1B113P: in   sl;
    iRefClk1B113M: in   sl;

    iRefClk0B116P: in   sl;
    iRefClk0B116M: in   sl;

    iRefClk1B116P: in   sl;
    iRefClk1B116M: in   sl;

    iRefClk1B213P: in   sl;
    iRefClk1B213M: in   sl;

    iRefClk1B216P: in   sl;
    iRefClk1B216M: in   sl;
  
    oMgtB113TxP: out slv(2 downto 0);
    oMgtB113TxM: out slv(2 downto 0);
    iMgtB113RxP: in  slv(2 downto 0);
    iMgtB113RxM: in  slv(2 downto 0);

    oMgtB213TxP: out slv(3 downto 0);
    oMgtB213TxM: out slv(3 downto 0);
    iMgtB213RxP: in  slv(3 downto 0);
    iMgtB213RxM: in  slv(3 downto 0);

    oMgtB116TxP: out slv(2 downto 0);
    oMgtB116TxM: out slv(2 downto 0);
    iMgtB116RxP: in  slv(2 downto 0);
    iMgtB116RxM: in  slv(2 downto 0);

    oMgtB216TxP: out slv(3 downto 0);
    oMgtB216TxM: out slv(3 downto 0);
    iMgtB216RxP: in  slv(3 downto 0);
    iMgtB216RxM: in  slv(3 downto 0);

    led:           out  slv(7 downto 0) := (others => '0');
    dip_sw:        in   slv(7 downto 0);
    lemoout:       out sl := '1';
    lemoin:        in slv(6 downto 0);
    --lemoout:        out slv(7 downto 0);

    lvdso_p:       out slv(15 downto 0);
    lvdso_n:       out slv(15 downto 0);
    lvdsi_p:       in  slv(15 downto 0);
    lvdsi_n:       in  slv(15 downto 0);

    
    --zone 3
    serialout_p  : out std_logic_vector(17 downto 0);
    serialout_n  : out std_logic_vector(17 downto 0);
    serialin_p   : in std_logic_vector(17 downto 0);
    serialin_n   : in std_logic_vector(17 downto 0);
    clkA_p       : out std_logic;
    clkA_n       : out std_logic;
    clkB_p       : out std_logic;
    clkB_n       : out std_logic;
    iExttrigger    : in std_logic;
    iExtrst        : in std_logic;
    oExtbusy       : out std_logic;
    oExttrgclk     : out std_logic;

    --DTM low speed lines
    idpmFbP: in slv(3 downto 0);
    idpmFbM: in slv(3 downto 0);
    odpmFbP: out slv(3 downto 0);
    odpmFbM: out slv(3 downto 0);

    --DTM clocks
    dpmClkP: in slv(2 downto 0);
    dpmClkM: in slv(2 downto 0);

    i232DceTx:        in   sl;
    o232DceRx:        out  sl := '0';
    i232DceRts:       in   sl;
    o232DceCts:       out  sl := '1';
    o232DceDsr:       out  sl := '1';
    i232DceDtr:       in   sl;
    o232DceCd:        out  sl := '1';
    i232DceRi:        out  sl := '0';
    
    displayI2cSda: inout std_logic;
    displayI2cScl: inout std_logic;

    sw_softreset: in std_logic;
    sw_dtmboot: in std_logic;

    ttc_scl: inout std_logic;
    ttc_sda: inout std_logic;

    ttc_data_p: in std_logic;
    ttc_data_n: in std_logic;

    dtm_ps0_l: in std_logic;
    dtm_en_l:  out std_logic := '1';
    dtmI2cScl: inout std_logic;
    dtmI2cSda: inout std_logic
  );
end HsioTest;


architecture HsioTest of HsioTest is 
  signal clk: std_logic;
  signal rst: std_logic;
  signal fabric_clk: slv(5 downto 0);
  signal fabric_rst: slv(5 downto 0);
  signal freqOut113n1: slv(31 downto 0);
  signal locked113n1: sl;
  signal freqOut116n0: slv(31 downto 0);
  signal locked116n0: sl;
  signal freqOut116n1: slv(31 downto 0);
  signal locked116n1: sl;
  signal freqOut216n1: slv(31 downto 0);
  signal locked216n1: sl;
  signal freqOut213n1: slv(31 downto 0);
  signal locked213n1: sl;
  signal freqOutDtm0: slv(31 downto 0);
  signal lockedDtm0: sl;
  signal freqOutDtm1: slv(31 downto 0);
  signal lockedDtm1: sl;
  signal freqOutDtm2: slv(31 downto 0);
  signal lockedDtm2: sl;
  signal header1: slv(39 downto 0);
  signal header2: slv(39 downto 0);
  signal J25: Slv8Array(9 downto 0);
  signal J26: Slv8Array(9 downto 0);
  signal J27: Slv8Array(9 downto 0);
  signal txReady_int: slv(15 downto 0);
  signal rxReady_int: slv(15 downto 0);
  
  signal displayBuffer : HD44780DriverDisplayBufferType := (others => x"20");
  
  signal FIT1_Toggle:     std_logic;
  signal PIT1_Toggle:     std_logic;
  signal PIT2_Toggle:     std_logic;
  signal PIT3_Toggle:     std_logic;
  signal PIT4_Toggle:     std_logic;
  signal GPO1:            std_logic_vector(31 downto 0);
  signal GPO2:            std_logic_vector(31 downto 0);
  signal GPO3:            std_logic_vector(31 downto 0);
  signal GPO4:            std_logic_vector(31 downto 0);
  signal GPI1:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI2:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI3:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI4:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI1_Interrupt:  std_logic;
  signal GPI2_Interrupt:  std_logic;
  signal GPI3_Interrupt:  std_logic;
  signal GPI4_Interrupt:  std_logic;
  signal INTC_Interrupt:  std_logic_vector(15 downto 0) := (others => '0');
  signal INTC_IRQ:        std_logic;
  signal IO_Addr_Strobe:  std_logic;
  signal IO_Read_Strobe:  std_logic;
  signal IO_Write_Strobe: std_logic;
  signal IO_Address:      std_logic_vector(31 downto 0);
  signal IO_Byte_Enable:  std_logic_vector(3 downto 0);
  signal IO_Write_Data:   std_logic_vector(31 downto 0);
  signal IO_Read_Data:    std_logic_vector(31 downto 0) := (others => '0');
  signal IO_Ready:        std_logic := '0';

  signal QPllRefClk:      Slv2Array(3 downto 0);
  signal QPllClk:         Slv2Array(3 downto 0);
  signal QPllLock:        Slv2Array(3 downto 0);
  signal QPllRefClkLost:  Slv2Array(3 downto 0);
  signal QPllResetReg:    slv(7 downto 0);
  signal QPllReset:       Slv2Array(3 downto 0):= (others=>(others => '0'));
  signal userReset:       sl;
  signal userResetv:      slv(15 downto 0);
  signal timercounter:    slv(30 downto 0):=(others =>'0');
  signal dtmdata:         slv(7 downto 0) := (others => '0');
  signal dtmtrig:         sl:='0';
  signal dtmrw:           sl:='0';
  signal fbin:            slv(3 downto 0);
  signal fbout:           slv(3 downto 0);
  signal fb:              slv(3 downto 0);
  signal dtmclk:          slv(2 downto 0);
  signal temperature:     slv(15 downto 0);
  signal tempvalid:       sl;
  signal serDataRising:   sl;
  signal serDataFalling:  sl;
  type na4 is array (natural range 0 to 3) of NaturalArray(0 to 3);
  constant taps:            na4:=
    ((0 => 31, 1 => 6, 2 => 2, 3 => 1), 
    (0 => 30, 1 => 7, 2 => 4, 3 => 2), 
    (0 => 29, 1 => 8, 2 => 3, 3 => 1), 
    (0 => 28, 1 => 9, 2 => 6, 3 => 3)); 
  signal prbsreset:        slv(19 downto 0):=(others => '0');
  signal prbsfail:        slv(19 downto 0);
  signal prbstrig:        slv(19 downto 0):=(others => '0');
  signal prbsdone:        slv(19 downto 0);
  signal prbslen:         slv(31 downto 0):=x"00000010";
  signal prbserr:         Slv32Array(19 downto 0);
  signal clkSync:         sl;
  signal locClkEn:        sl;
  signal locClk:          sl;
  signal locRst:          sl;
  signal clkLocked:       sl;
  signal trigL1:          sl;
  signal trigsynch:       sl;
  signal rsttrigcounter:  sl:='0';
  signal trigcounterloc:  slv(31 downto 0):=(others =>'0');
  signal trigcounter:     slv(31 downto 0):=(others =>'0');
  constant NUMBER_OF_MGTS: natural := 16;
  signal MgtTestState: HsioMgtTestStateArray(NUMBER_OF_MGTS - 1 downto 0) := 
    (
      others => (
        PrbsPacketLength   => x"00001000",
        PrbsRxPacketLength => x"00000000",
        PrbsErrWordCnt     => x"00000000",
        PrbsErrbitCnt      => x"00000000",
        PrbsPacketRate     => x"00000000",
        txPowerDown => "11",
        rxPowerDown => "11",
        others => '0'
      )
    );
  signal MgtTestQpll: HsioMgtTestQPllArray(NUMBER_OF_MGTS - 1 downto 0) := (others => (others => (others => '0')));
  signal MgtTestRxTx: HsioMgtTestRxTxArray(NUMBER_OF_MGTS - 1 downto 0) := (others => (others => '0'));
  constant MgtGenerateMask: std_logic_vector(NUMBER_OF_MGTS - 1 downto 0) := "0000000000000000";
  signal initclockbuf: std_logic:='0';
  signal oldrst: std_logic:='0';
  signal ttc_data: std_logic;
  signal ttcphase: std_logic;
  signal clk_out1: std_logic;
  signal serialin: slv(19 downto 0);
  signal serialout: slv(19 downto 0);
  signal adclklocked: slv(17 downto 0);
  signal clkA, clkB: sl;

begin

  GEN_DPM_CLK:
  for I in 0 to 2 generate
    U_iclk_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
      port map ( I  => dpmClkP(I)  , IB=> dpmClkM(I)   , O => dtmclk(I)   );
  end generate GEN_DPM_CLK;
  GEN_DPM_LS:
  for I in 0 to 3 generate
    U_ols_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
      port map ( I  => fbout(I)   , O => odpmFbP(I) , OB => odpmFbM(I)  );
    
    U_ils_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
      port map ( I  => idpmFbP(I)  , IB=>idpmFbM(I)   , O => fbin(I)   );
  end generate GEN_DPM_LS;
  U_ttc_data_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
    port map ( I  => ttc_data_p  , IB=> ttc_data_n , O => ttc_data   );
  U_clkA_pn : OBUFDS   generic map ( IOSTANDARD=>"LVDS_25")
      port map ( I  => clkA  , O=> clkA_p   , OB => clkA_n   );
  U_clkB_pn : OBUFDS   generic map ( IOSTANDARD=>"LVDS_25")
      port map ( I  => clkB  , O=> clkB_p   , OB => clkB_n   );
  GEN_serialio:
  for I in 0 to 17 generate
    U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
      port map ( I  => serialout(I)   , O => serialout_p(I) , OB => serialout_n(I)  );
    
    U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
      port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialin(I)   );
  end generate GEN_serialio;

  oExttrgclk<=serialout(18);
  oExtbusy<=serialout(19);
  serialin(18)<=iExttrigger;
  serialin(19)<=iExtrst;

  clkgen_inst: entity work.TestClkGen
  port map(
    iRefClk0B113P => iRefClk0B113P,
    iRefClk0B113M => iRefClk0B113M,
    iRefClk1B113P => iRefClk1B113P,
    iRefClk1B113M => iRefClk1B113M,
    iRefClk0B116P => iRefClk0B116P,
    iRefClk0B116M => iRefClk0B116M,
    iRefClk1B116P => iRefClk1B116P,
    iRefClk1B116M => iRefClk1B116M,
    iRefClk1B213P => iRefClk1B213P,
    iRefClk1B213M => iRefClk1B213M,
    iRefClk1B216P => iRefClk1B216P,
    iRefClk1B216M => iRefClk1B216M, 
  
    sys_clk_out => clk,
    sys_rst_out => rst,
    clk_out => fabric_clk,
    
    QPllRefClk     => QPllRefClk,
    QPllClk        => QPllClk,
    QPllLock       => QPllLock,
    QPllRefClkLost => QPllRefClkLost,
    QPllResetReg   => QPllResetReg,
    QPllReset      => QPllReset
  ); 
  syncClockFreq_inst0: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 312.4e6,
    CLK_UPPER_LIMIT_G => 312.6e6
  )
  port map
  (
    freqOut => freqOut113n1,
    locked => locked113n1,
    clkIn => fabric_clk(CLK1B113),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst1: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 249.9e6,
    CLK_UPPER_LIMIT_G => 250.1e6
  )
  port map
  (
    freqOut => freqOut116n0,
    locked => locked116n0,
    clkIn => fabric_clk(CLK0B116),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst2: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 312.4e6,
    CLK_UPPER_LIMIT_G => 312.6e6
  )
  port map
  (
    freqOut => freqOut116n1,
    locked => locked116n1,
    clkIn => fabric_clk(CLK1B116),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst3: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 119.9e6,
    CLK_UPPER_LIMIT_G => 120.1e6
  )
  port map
  (
    freqOut => freqOut216n1,
    locked => locked216n1,
    clkIn => fabric_clk(CLK1B216),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst4: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 119.9e6,
    CLK_UPPER_LIMIT_G => 120.1e6
  )
  port map
  (
    freqOut => freqOut213n1,
    locked => locked213n1,
    clkIn => fabric_clk(CLK1B213),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst_dtm0: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 249.9e6,
    CLK_UPPER_LIMIT_G => 250.1e6
  )
  port map
  (
    freqOut => freqOutDtm0,
    locked => lockedDtm0,
    clkIn => dtmclk(0),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst_dtm1: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 99.9e6,
    CLK_UPPER_LIMIT_G => 100.1e6
  )
  port map
  (
    freqOut => freqOutDtm1,
    locked => lockedDtm1,
    clkIn => dtmclk(1),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst_dtm2: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 159.9e6,
    CLK_UPPER_LIMIT_G => 160.1e6
  )
  port map
  (
    freqOut => freqOutDtm2,
    locked => lockedDtm2,
    clkIn => dtmclk(2),
    locClk => clk,
    refClk => clk
  );

  LedTestPatternGenerator_inst: entity work.LedTestPatternGenerator
  generic map 
  (
    divider => 250000000
  )
  port map 
  (
    clk    => clk,
    led    => led,
    --led(7 downto 4)    => led(7 downto 4),
    --led(3 downto 0) => open,
    dip_sw => (others => '0')
  );
  --led(0)<=rxReady_int(1);
  --led(1)<=rxReady_int(2);
  --led(2)<=rxReady_int(8);
  --led(3)<=rxReady_int(9);

  headertest_inst: entity work.Headertest
  port map(
    clk => clk,
    lvdso_p => lvdso_p,
    lvdso_n => lvdso_n,
    lvdsi_p => lvdsi_p,
    lvdsi_n => lvdsi_n,
    header1 => header1,
    header2 => header2);

  clocktest_inst: entity work.Clocktest
  port map(
    clk=>clk,
    clkA=>clkA,
    clkB=>clkB,
    serialin=>serialin(17 downto 0),
    result=>adclklocked);
  
  prbstestgen: for i in 0 to 19 generate
    prbs_inst: entity work.prbstest 
      generic map( PRBS_TAPS_G => taps(I mod 4))
      port map(
        clk => clk,
        rst => prbsreset(I),
	err => prbserr(I),
        fail => prbsfail(I),
        trig => prbstrig(I),
        done => prbsdone(I),
        len => prbslen,
        d_in => serialin(I),
        d_out => serialout(I)
        );
    end generate prbstestgen;
  
  I2cHD44780Display_inst: entity work.I2cHD44780Display
  port map
  (
    clk => clk,
    rst => '0',
    displayI2cSda => displayI2cSda,
    displayI2cScl => displayI2cScl,
    dISPLayBuffer => displayBuffer
  );

  dtmI2c_inst: entity work.dtmi2c
    port map(
      clk => clk,
      d_out => dtmdata,
      trig => dtmtrig,
      rw => dtmrw,
      scl => dtmI2cScl,
      sda => dtmI2cSda
      );

  process 
  begin
    wait until rising_edge(clk);
    oldrst<=rst;
    if(rst='0' and oldrst='1')then
      initclockbuf<='1';
    else
      initclockbuf<='0';
    end if;
  end process;
  ttcclockbuf: entity work.clockbuf
    port map(
      clk => clk,
      trig => initclockbuf,
      fail => open,
      scl => ttc_scl,
      sda => ttc_sda
      );

  tempxadc_inst: entity work.tempxadc
    port map(
      clk => clk,
      rst => rst,
      d_out => temperature,
      valid => tempvalid
      );
--clk_wiz_inst : entity work.clk_wiz_0
--   port map ( 

--   -- Clock in ports
--   clk_in1 => dtmclk(2),
--  -- Clock out ports  
--   clk_out1 => clk_out1              
-- );
  -- AtlasTtcRxCdrInputs_Inst : entity work.AtlasTtcRxCdrInputs
  --    port map (
  --       -- CDR Signals
  --       clock          => clk_out1,
  --       --clock          => fabric_clk(CLK1B213),
  --       clk            => open,
  --       ttcRxData      => dtmclk(1),
  --       --ttcRxData      => ttc_data,
  --       data           => open,
  --       -- Emulation Trigger Signals
  --       emuSel         => '0',
  --       emuClk         => '0',
  --       emuData        => '0',
  --       -- Serial Data Signals
  --       serDataRising  => serDataRising,
  --       serDataFalling => serDataFalling,
  --       -- Clock Signals
  --       clkSync        => clkSync,
  --       locClk40MHz    => open,
  --       locClk80MHz    => open,
  --       locClk160MHz   => locClk);   
  -- AtlasTtcRxClkMon_Inst : entity work.AtlasTtcRxClkMon
  --    generic map (
  --       EN_LOL_PORT_G     => false,
  --       EN_SIG_DET_PORT_G => false)    
  --    port map (
  --       -- Status Monitoring
  --       clkLocked       => clkLocked,
  --       freqLocked      => open,
  --       cdrLocked       => open,
  --       sigLocked       => open,
  --       freqMeasured    => open,
  --       ignoreSigLocked => '1',
  --       ignoreCdrLocked => '1',
  --       lockedP         => open,
  --       lockedN         => open,
  --       -- Global Signals
  --       refClk125       => clk,
  --       locClk          => locClk,
  --       locRst          => rst);   

  -- AtlasTtcRxDeSer_Inst : entity work.AtlasTtcRxDeSer
  --    port map (
  --       -- Serial Data Signals
  --       serDataEdgeSel => ttcphase,
  --       serDataRising  => serDataRising,
  --       serDataFalling => serDataFalling,
  --       -- Level-1 Trigger
  --       trigL1         => trigL1,
  --       -- BC Encoded Message
  --       bcValid        => open,
  --       bcData         => open,
  --       bcCheck        => open,
  --       -- IAC Encoded Message
  --       iacValid       => open,
  --       iacData        => open,
  --       iacCheck       => open,
  --       -- Status Monitoring
  --       clkLocked      => clkLocked,
  --       bpmLocked      => open,
  --       bpmErr         => open,
  --       deSerErr       => open,
  --       -- Clock Signals
  --       clkSync        => clkSync,
  --       locClkEn       => locClkEn,
  --       locClk         => locClk,
  --       locRst         => rst);       
  --process(locclk, rsttrigcounter) 
  --begin
  --  if(rsttrigcounter='1') then
  --    trigcounterloc<=(others =>'0');
  --  elsif(rising_edge(locclk)) then
  --    if(trigL1='1')then
  --      trigcounterloc<=std_logic_vector(unsigned(trigcounterloc)+1);
  --    end if;
  --  end if;
  --end process;
  --syncvec: entity work.SynchronizerVector
  --  generic map(WIDTH_G => 32)
  --  port map(
  --    clk => clk,
  --    dataIn => trigcounterloc,
  --    dataOut => trigcounter);
  hsio_test_mb_inst : entity work.hsio_test_mb
  port map 
  (
    Clk => clk,
    Reset => rst,
    IO_Addr_Strobe => IO_Addr_Strobe,
    IO_Read_Strobe => IO_Read_Strobe,
    IO_Write_Strobe => IO_Write_Strobe,
    IO_Address => IO_Address,
    IO_Byte_Enable => IO_Byte_Enable,
    IO_Write_Data => IO_Write_Data,
    IO_Read_Data => IO_Read_Data,
    IO_Ready => IO_Ready,
    UART_Rx => i232DceTx,
    UART_Tx => o232DceRx,
    UART_Interrupt => open,
    FIT1_Toggle => FIT1_Toggle,
    PIT1_Toggle => PIT1_Toggle,
    PIT2_Toggle => PIT2_Toggle,
    PIT3_Toggle => PIT3_Toggle,
    PIT4_Toggle => PIT4_Toggle,
    GPO1 => GPO1,
    GPO2 => GPO2,
    GPO3 => GPO3,
    GPO4 => GPO4,
    GPI1 => GPI1,
    GPI2 => GPI2,
    GPI3 => GPI3,
    GPI4 => GPI4,
    GPI1_Interrupt => GPI1_Interrupt,
    GPI2_Interrupt => GPI2_Interrupt,
    GPI3_Interrupt => GPI3_Interrupt,
    GPI4_Interrupt => GPI4_Interrupt,
    INTC_Interrupt => INTC_Interrupt,
    INTC_IRQ => INTC_IRQ
  );

  process 
    variable addr: natural;
  begin
    wait until rising_edge(clk);
    IO_Read_Data <= (others => '0');
    IO_Ready <= IO_Addr_Strobe;
    case IO_Address(31 downto 16) is
      when x"C000" =>
        if (IO_Write_Strobe = '1') then
          addr := to_integer(unsigned(io_address(7 downto 2)));
          if (addr < 40) then
            displayBuffer(addr) <= IO_Write_Data(7 downto 0);
          end if;
        end if;
      when x"C001" =>
        if (IO_Read_Strobe = '1') then
          IO_Read_Data(7 downto 0) <= dip_sw;
          IO_Read_Data(8) <= sw_softreset;
          IO_Read_Data(9) <= sw_dtmboot;
        end if;
      when x"C002" => -- Headers
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= header1(31 downto 0);
          elsif(IO_Address(15 downto 0)=x"0004")then
            IO_Read_Data <= x"000000"&header1(39 downto 32);
          elsif(IO_Address(15 downto 0)=x"0008")then
            IO_Read_Data <= header2(31 downto 0);
          elsif(IO_Address(15 downto 0)=x"000c")then
            IO_Read_Data <= x"000000"&header2(39 downto 32);
          else
            IO_Read_Data <= (others => '1');
          end if;
        end if;
      when x"C003" => -- Zone 3 connector
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= J25(3) & J25(2) & J25(1) & J25(0);
          elsif(IO_Address(15 downto 0)=x"0004")then
            IO_Read_Data <= J25(7) & J25(6) & J25(5) & J25(4);
          elsif(IO_Address(15 downto 0)=x"0008")then
            IO_Read_Data <= x"0000" & J25(9) & J25(8);
          elsif(IO_Address(15 downto 0)=x"000c")then
            IO_Read_Data <= J26(3) & J26(2) & J26(1) & J26(0);
          elsif(IO_Address(15 downto 0)=x"0010")then
            IO_Read_Data <= J26(7) & J26(6) & J26(5) & J26(4);
          elsif(IO_Address(15 downto 0)=x"0014")then
            IO_Read_Data <= x"0000" & J26(9) & J26(8);
          elsif(IO_Address(15 downto 0)=x"0018")then
            IO_Read_Data <= J27(3) & J27(2) & J27(1) & J27(0);
          elsif(IO_Address(15 downto 0)=x"001c")then
            IO_Read_Data <= J27(7) & J27(6) & J27(5) & J27(4);
          elsif(IO_Address(15 downto 0)=x"0020")then
            IO_Read_Data <= x"0000" & J27(9) & J27(8);
          else
            IO_Read_Data <= (others => '1');
          end if;
        end if;
      when x"C004" =>
        if (IO_Read_Strobe = '1') then
          IO_Read_Data(6 downto 0) <= lemoin;
        end if;
      when x"C005" =>
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= locked113n1 & freqOut113n1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0004")then
            IO_Read_Data <= locked116n0 & freqOut116n0(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0008")then
            IO_Read_Data <= locked116n1 & freqOut116n1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"000c")then
            IO_Read_Data <= lockedDtm0 & freqOutDtm0(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0010")then
            IO_Read_Data <= lockedDtm1 & freqOutDtm1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0014")then
            IO_Read_Data <= lockedDtm2 & freqOutDtm2(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0018")then
            IO_Read_Data <= locked216n1 & freqOut216n1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"001c")then
            IO_Read_Data <= locked213n1 & freqOut213n1(30 downto 0);
          else
            IO_Read_Data <= (others => '0');
          end if;
        end if;
      when x"C006" =>
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= '0' & timercounter;
          end if;
        end if;
      when x"C007" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data <= (0=> dtm_ps0_l, others => '0');
            when x"0004" =>
              IO_Read_Data(7 downto 0) <= dtmdata;
            when others =>
          end case;
        elsif (IO_Write_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              dtm_en_l <= IO_Write_Data(0);
            when x"0004" =>
              dtmtrig <= IO_Write_Data(0);
              dtmrw <= IO_Write_Data(1);
            when others =>
          end case;
        end if;
      when x"C008" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0010" =>
              IO_Read_Data(19 downto 0) <= prbsdone;
            when x"0014" =>
              IO_Read_Data(19 downto 0) <= prbsfail;
            when others =>
              IO_Read_Data <= (others=>'0');
          end case;
        elsif (IO_Write_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              prbsreset <= IO_Write_Data(19 downto 0);
            when x"0004" =>
              prbstrig <= IO_Write_Data(19 downto 0);
            when x"0008" =>
              prbslen <= IO_Write_Data;
            when others =>
          end case;
        end if;
      when x"C009" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data(31) <= tempvalid;
              IO_Read_Data(15 downto 0) <= temperature;
            when others =>
          end case;
        end if;
      when x"C00A" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data <= trigcounter;
            when others =>
          end case;
        elsif (IO_Write_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              rsttrigcounter <= IO_Write_Data(0);
              ttcphase <= IO_Write_Data(1);
            when others =>
          end case;
        end if;
      when x"C00B" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data(17 downto 0) <= adclklocked;
            when others =>
          end case;
        end if;
      when others =>
    end case;
  end process;

  process begin
    wait until rising_edge(clk);
    timercounter<=std_logic_vector(unsigned(timercounter)+1);
  end process;

end HsioTest;
