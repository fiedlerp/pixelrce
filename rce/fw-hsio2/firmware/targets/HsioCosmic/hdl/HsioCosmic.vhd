-------------------------------------------------------------------------------
-- Title         : HsioCosmic
-- Project       : ATLAS IBL
-------------------------------------------------------------------------------
-- File          : HsioCosmic.vhd
-- Author        : Martin Kocian, kocian@slac.stanford.edu
-- Created       : 10/23/2014
-------------------------------------------------------------------------------
-- Description:
-- Top level logic for HSIO pixel firmware.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 by Martin Kocian. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 07/21/2008: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
Library Unisim;
use unisim.vcomponents.all;
USE ieee. std_logic_1164.ALL;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;
use work.Pgp3p125GbpsPkg.all;
use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;
use work.AxiLitePkg.all;
use work.I2cPkg.all;
use work.HD44780DriverPkg.all;
USE work.ALL;


entity HsioCosmic is 
  generic ( adapter: slv(7 downto 0):=x"00";
            hitbusreadout: sl:='0');
  -- 0 = CosmicNew
  -- 1 = CosmicII
  -- 2 = Opto
  -- 3 = Stave
  -- 4 = StaveRot
   port ( 

      -- PGP Crystal Clock Input, 156.25Mhz
      iMgtRefClkP   : in    std_logic;
      iMgtRefClkM   : in    std_logic;

      iMgtRefClk2P  : in    std_logic;
      iMgtRefClk2M  : in    std_logic;

      iMgtRefClk3P  : in    std_logic;
      iMgtRefClk3M  : in    std_logic;

      -- PGP Rx/Tx Lines
      iMgtRxN       : in    std_logic;
      iMgtRxP       : in    std_logic;
      oMgtTxN       : out   std_logic;
      oMgtTxP       : out   std_logic;

      -- SLink Rx/Tx Lines
      iSLinkRxN       : in    std_logic;
      iSLinkRxP       : in    std_logic;
      oSLinkTxN       : out   std_logic;
      oSLinkTxP       : out   std_logic;

     -- ATLAS Pixel module pins
      serialout_p  : out std_logic_vector(17 downto 0);
      serialout_n  : out std_logic_vector(17 downto 0);
      serialin_p   : in std_logic_vector(17 downto 0);
      serialin_n   : in std_logic_vector(17 downto 0);
      discinP      : in std_logic_vector(3 downto 0);
      clkA_p       : out std_logic;
      clkA_n       : out std_logic;
      clkB_p       : out std_logic;
      clkB_n       : out std_logic;
      --clockcheck   : out std_logic;
      hbcheck      : out std_logic;

      -- Reset button
      iResetInL     : in    std_logic;
      iPor          : in    std_logic;
      -- Reload firmware
      iExtreload    : in std_logic;
      -- LEDs
      led           : out slv(7 downto 0);
      fpga_status_led : out sl;
      usr_led       : out sl;
      -- Display
      displayI2cScl : inout sl;
      displayI2cSda : inout sl;

      adcI2cSclA: inout sl;
      adcI2cSdaA: inout sl;
      adcAsA: out sl;
      adcI2cSclB: inout sl;
      adcI2cSdaB: inout sl;
      adcAsB: out sl;

      sw_dtmboot: in std_logic;

      ttc_scl: inout std_logic;
      ttc_sda: inout std_logic;

      ttc_data_p: in std_logic; 
      ttc_data_n: in std_logic;

      ttc_busy_p: out std_logic;
      ttc_busy_n: out std_logic;
      ttc_sd_p: in std_logic;
      ttc_sd_n: in std_logic;
      ttc_locked_p: in std_logic;
      ttc_locked_n: in std_logic;
      
      -- Fiber modules
      TX_EN      : out std_logic;
      TX_DIS     : out std_logic;
      TX_RESET   : out std_logic;
      RX0_RX_EN  : out std_logic;
      RX0_EN_SD  : out std_logic;
      RX0_SQ_EN  : out std_logic;
      RX1_RX_EN  : out std_logic;
      RX1_EN_SD  : out std_logic;
      RX1_SQ_EN  : out std_logic;
      -- Debug
      oDebug        : out std_logic_vector(7 downto 0);

      -- Eudet trigger
      --iExttriggerP    : in std_logic;
      --iExttriggerN    : in std_logic;
      --iExtrstP        : in std_logic;
      --iExtrstN        : in std_logic;
      --oExtbusyP       : out std_logic;
      --oExtbusyN       : out std_logic;
      --oExttrgclkP     : out std_logic;
      --oExttrgclkN     : out std_logic;
      iExttrigger    : in std_logic;
      iExtrst        : in std_logic;
      oExtbusy       : out std_logic;
      oExttrgclk     : out std_logic;

      --HSIO trigger IF
      iHSIOtrigger    : in std_logic_vector(1 downto 0);
      iHSIOtriggerCMOS: in std_logic_vector(1 downto 0);
      oHSIOtrigger    : out std_logic;
      oHSIObusy       : out std_logic;
      iHSIObusy       : in std_logic;
      --DTM
      dtm_ps0_l: in std_logic;
      dtm_en_l:  out std_logic := '1';
      dtmI2cScl: inout std_logic;
      dtmI2cSda: inout std_logic
      
   );
end HsioCosmic;


-- Define architecture for top level module
architecture HsioCosmic of HsioCosmic is 

  procedure printText (displayBuffer : inout HD44780DriverDisplayBufferType;
                      at: in integer; newpos: out integer; txt: in string) is
  begin
    for i in 1 to txt'high loop
      displayBuffer(at+i-1)(7 downto 0):= std_logic_vector(conv_unsigned(character'pos(txt(i)), 8));
    end loop;
    newpos:=at+txt'high;
  end procedure printText;

  procedure placeNextDigit(displayBuffer : inout HD44780DriverDisplayBufferType;
                           at: in integer; newpos: out integer; txt: in std_logic_vector(7 downto 0)) is
  begin
    displayBuffer(at):=txt;
    newpos:=at+1;
  end procedure placeNextDigit;
  
   -- Local signals
   signal oReload        : std_logic;
   signal sysClk40      : std_logic;
   signal sysRst40      : std_logic;
   signal sysClk80     : std_logic;
   signal sysClk160     : std_logic;
   signal pgpClk         : std_logic;
   signal pgpRst       : std_logic;
   signal reload1        : std_logic;
   signal mgtRxN         : std_logic;
   signal mgtRxP         : std_logic;
   signal mgtTxN         : std_logic;
   signal mgtTxP         : std_logic;
   signal debug          : std_logic_vector(7 downto 0);
   signal clockidctrl    : std_logic;
   signal idctrlrst      : std_logic;
   signal exttrigger     : std_logic;
   signal exttriggerinv  : std_logic;
   signal extrst         : std_logic;
   signal extrstinv      : std_logic;
   signal exttriggero    : std_logic;
   signal extrsto        : std_logic;
   signal extbusy        : std_logic;
   signal extbusyinv     : std_logic;
   signal exttrgclk      : std_logic;
   signal exttrgclkinv   : std_logic;
   signal clockb          : std_logic_vector(7 downto 0);
   signal serialoutb     : std_logic_vector(31 downto 0);
   signal serialinb      : std_logic_vector(31 downto 0):=(others => '0');
   signal serialininv    : std_logic_vector(31 downto 0):=(others => '0');
   signal dispDigitA      : std_logic_vector(7 downto 0);
   signal dispDigitB      : std_logic_vector(7 downto 0);
   signal dispDigitC      : std_logic_vector(7 downto 0);
   signal dispDigitD      : std_logic_vector(7 downto 0);
   signal dispDigitE      : std_logic_vector(7 downto 0);
   signal dispDigitF      : std_logic_vector(7 downto 0);
   signal dispDigitG      : std_logic_vector(7 downto 0);
   signal dispDigitH      : std_logic_vector(7 downto 0);
   signal lockedid        : std_logic;
   signal oldlockedid     : std_logic;
   signal holdrst         : std_logic;
   signal holdctr         : std_logic_vector(24 downto 0);
   signal idcounter       : std_logic_vector(2 downto 0);

   signal sysClk160Unbuf    : std_logic;
   signal sysClk160Unbuf90  : std_logic;
   signal sysClk40Unbuf     : std_logic;
   signal sysClk40Unbuf90   : std_logic;

   signal sysClk100         : std_logic;
   signal sysRst100         : std_logic;
   signal axiClk40          : std_logic;
   signal axiRst40          : std_logic;
   signal disc           : std_logic_vector(3 downto 0);
   signal calibmode     : std_logic_vector(1 downto 0);
   signal eudaqdone     : std_logic;
   signal eudaqtrgword  : std_logic_vector(14 downto 0);
   signal trigenabled   : std_logic;
   signal paused        : std_logic;
   signal triggermask   : std_logic_vector(15 downto 0);
   signal resetdelay    : std_logic;
   signal incrementdelay: std_logic_vector(4 downto 0); 
   signal discop        : std_logic_vector(15 downto 0); 
   signal telescopeop   : std_logic_vector(2 downto 0);
   signal period        : std_logic_vector(31 downto 0);
   signal fifothresh    : std_logic;
   signal serbusy       : std_logic;
   signal tdcreadoutbusy: std_logic;
   signal l1a           : std_logic;
   signal triggerword   : std_logic_vector(7 downto 0);
   signal busy          : std_logic;
   signal rstFromCore   : std_logic;
   signal hitbus        : std_logic_vector(1 downto 0);
   signal QPllRefClk    : slv(1 downto 0);
   signal QPllClk       : slv(1 downto 0);
   signal QPllLock      : slv(1 downto 0);
   signal QPllRefClkLost: slv(1 downto 0);
   signal QPllReset     : slv(1 downto 0);
   signal counter       : slv(31 downto 0) := x"00000000";

   signal adcSel        : sl;
   signal trigAdc       : sl;
   signal trigAdcA      : sl;
   signal trigAdcB      : sl;
   signal sendAdcData   : sl;
   signal sendAdcDataA  : sl;
   signal sendAdcDataB  : sl;
   signal adcData       : Slv16Array(11 downto 0);
   signal adcDataA      : Slv16Array(11 downto 0);
   signal adcDataB      : Slv16Array(11 downto 0);
   -- I2C Text Display
   signal displayBuffer : HD44780DriverDisplayBufferType := (others => x"20");
   signal initclockbuf: sl:='0';
   signal oldPgpRst: sl:='0';
   signal ttc_clk_40: sl;
   signal inpClk160: sl;
   signal axiReadMasterTtc: AxiLiteReadMasterType;
   signal axiReadSlaveTtc: AxiLiteReadSlaveType;
   signal axiWriteMasterTtc: AxiLiteWriteMasterType;
   signal axiWriteSlaveTtc: AxiLiteWriteSlaveType;
   signal ttcStatusWord: Slv64Array(0 to 0);
   signal ttcStatusSend: sl; 
   signal iHSIOtriggerHB: slv(1 downto 0);
   signal clk_in_sel: sl;
   signal SysClkRst: sl;
   signal clockbufdone: sl;
   signal ttcClkOk : sl;
   signal atlasClk160MHz: sl;
   signal atlasClk160MHzEn: sl;
   signal ttc_rx_out: AtlasTTCRxOutType;
   signal ttc_sd: sl;
   signal ttc_locked: sl;
   signal ttc_input_ok, ttc_input_ok_synch, ttc_input_ok_synch_old: sl;
   signal clk_in_sel_ttc: sl:='0';
   signal clk_in_sel_old: sl:='1';
   signal rstttc: sl:='0';
   signal vetodis: sl;
   signal cdtenabled : sl;
   signal numbuffers: slv(4 downto 0);
   signal window: slv(15 downto 0);
   signal hitbusout : sl;
   signal triggerinor: slv(1 downto 0);
   signal busyAtlas: sl;
   signal trgstretch: slv(2 downto 0) := "000";
   -- Register delay for simulation
   constant tpd:time := 0.5 ns;

begin
  TX_EN<='1';
  TX_DIS<='0';
  TX_RESET<='1';
  RX0_RX_EN<='1';
  RX0_EN_SD<='0';
  RX0_SQ_EN<='0';
  RX1_RX_EN<='1';
  RX1_EN_SD<='0';
  RX1_SQ_EN<='0';
  
   --clockcheck<=serialinb(3);
   U_DTMstart: entity work.startDtm
     port map(
       clk => sysClk40,
       rst => sysRst40,
       done => open, --led(5),
       fail => open, --led(6),
       detectL => dtm_ps0_l,
       powerupL => dtm_en_l,
       sw_dtmbootL => sw_dtmboot,
       scl => dtmI2cScl,
       sda => dtmI2cSda);

   process 
   begin
     wait until rising_edge(pgpClk);
     oldPgpRst<=pgpRst;
     if(pgpRst='0' and oldPgpRst='1')then
       initclockbuf<='1';
     else
       initclockbuf<='0';
     end if;
   end process;
   ttcclockbuf: entity work.clockbuf
    generic map( PRESCALE_G => 100,
                 CONFIG_G => "CDR160_160_160")
    port map(
      clk => pgpClk,
      trig => initclockbuf,
      fail => open,
      done => clockbufdone,
      scl => ttc_scl,
      sda => ttc_sda
    );
   -- Clocks
   U_ClkGen: entity work.ClkGen 
     generic map(
       -- Quad PLL Configurations
       QPLL_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
       QPLL_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
       QPLL_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C)
     port map(
       extRstL => '1', --iResetInL,
       ttc_clk_40 => ttc_clk_40,
       sysClk40 => sysClk40,
       sysRst40 => sysRst40,
       sysClk80 => sysClk80,
       sysClk160 => sysClk160,
       sysClk40Unbuf => sysClk40Unbuf,
       sysClk40Unbuf90 => sysClk40Unbuf90,
       sysClk160Unbuf => sysClk160Unbuf,
       sysClk160Unbuf90 => sysClk160Unbuf90,
       sysClkLock => led(2),
       sysClkRst => rstttc, --not ttcStatusWord(0)(3),
       sysClk100 => sysClk100,
       sysRst100 => sysRst100,
       inpClk160 => inpClk160,
       axiClk40 => axiClk40,
       axiRst40 => axiRst40,
       pgpClk => pgpClk,
       pgpRst => pgpRst,
       QPllRefClk => QPllRefClk,
       QPllClk => QPllClk,
       QPllLock => QPllLock,
       QPllRefClkLost => QPllRefClkLost,
       QPllReset => QPllReset,
       gtClkP => iMgtRefClkP,
       gtClkN => iMgtRefClkM,
       gtClk2P => iMgtRefClk2P,
       gtClk2N => iMgtRefClk2M);
   fpga_status_led<=clk_in_sel;
   usr_led <= ttc_input_ok;

   -- No Pads for MGT Lines
   mgtRxN  <= iMgtRxN;
   mgtRxP  <= iMgtRxP;
   oMgtTxN <= mgtTxN;
   oMgtTxP <= mgtTxP;

   U_L1AOUT: if(hitbusreadout/='1') generate
   process(sysClk40) begin
     if(rising_edge(sysClk40))then
       trgstretch(0)<=l1a;
       trgstretch(1)<=trgstretch(0);
       trgstretch(2)<=trgstretch(1);
     end if;
   end process; 
   oHSIOtrigger <= l1a or trgstretch(0) or trgstretch(1) or trgstretch(2);
   -- not stretched
   --oHSIOtrigger <= l1a;
   end generate U_L1AOUT;
   U_HITBUSOUT: if(hitbusreadout='1') generate
     oHSIOtrigger <= hitbusout and trigenabled and not paused and not busy;
   end generate U_HITBUSOUT;
   

   U_clkA     : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => not sysClk40   , O => clkA_p , OB => clkA_n  );
   U_clkB       : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => not sysClk40   , O => clkB_p , OB => clkB_n  );

   U_ttcsd : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                           port map ( I  => ttc_sd_p  , IB=>ttc_sd_n   , O => ttc_sd   );
   U_ttclocked : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                           port map ( I  => ttc_locked_p  , IB=>ttc_locked_n   , O => ttc_locked   );
   ttc_input_ok <= ttc_sd and ttc_locked;
   --U_exttrgclk  : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
   --                        port map ( I  => exttrgclkinv   , O => oExttrgclkP , OB => oExttrgclkN  );
   --exttrgclkinv<= not exttrgclk;
   --U_extbusy    : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
   --                        port map ( I  => extbusyinv   , O => oExtBusyP , OB => oExtbusyN  );
   --extbusyinv<= not extbusy;
   --U_exttrigger : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
   --                        port map ( I  => iExttriggerP  , IB=>iExttriggerN   , O => exttriggerinv   );
   --exttrigger<= not exttriggerinv;
   --U_extrst     : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
   --                        port map ( I  => iExtrstP  , IB=>iExtrstN   , O => extrstinv   );
   --extrst<= not extrstinv;
   oExttrgclk<=exttrgclk;
   oExtBusy<=extbusy;
   exttrigger<=iExttrigger;
   extrst<=iExtrst;

   -- serialout_p(0) <= serialoutb(0);
   -- serialinb(0) <= serialin_p(0);
   SERIAL_O_DATAN: if(adapter/=x"03" and adapter/=x"04") generate
    SERIAL_O_DATA: for I in 0 to 17 generate
       U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                                 port map ( I  => serialoutb(I)   , O => serialout_p(I) , OB => serialout_n(I)  );
     end generate SERIAL_O_DATA;
   end generate SERIAL_O_DATAN;
   SERIAL_O_DATAS: if(adapter=x"03" or adapter=x"04") generate
     SERIAL_O_DATA: for I in 0 to 7 generate
       U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                                 port map ( I  => serialoutb(I)   , O => serialout_p(I*2) , OB => serialout_n(I*2)  );
     U_Clk320 : ODDR port map ( 
       Q  => clockb(I),
       CE => '1',
       C  => sysClk40,
       D1 => '0',      
       D2 => '1',      
       R  => '0',      
       S  => '0'
     );
       U_clockout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                                 port map ( I  => clockb(I)   , O => serialout_p(I*2+1) , OB => serialout_n(I*2+1)  );
     end generate SERIAL_O_DATA;
   end generate SERIAL_O_DATAS;
   SERIAL_I_DATAN: if(adapter/=x"04") generate
     SERIAL_I_DATA:
     for I in 0 to 17 generate
       U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
         port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialinb(I)   );
     end generate SERIAL_I_DATA;
   end generate SERIAL_I_DATAN;
   SERIAL_I_DATAS: if(adapter=x"04") generate
     SERIAL_I_DATA:
     for I in 0 to 17 generate
       U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
         port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialininv(I)   );
       serialinb(I)<=not serialininv(I);
     end generate SERIAL_I_DATA;
   end generate SERIAL_I_DATAS;
   iHSIOtriggerHB(0)<=serialinb(3) or iHSIOtrigger(0);
   iHSIOtriggerHB(1)<=serialinb(7) or iHSIOtrigger(1);
--   SERIAL_IO_DATA_FEI3:
--   for I in 11 to 14 generate
--     U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
--                               port map ( I  => serialoutb(I)   , O => serialout_p(I) , OB => serialout_n(I)  );
--
--     U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
--                              port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialinb(I)   );
--
--   end generate SERIAL_IO_DATA_FEI3;

   disc<= discinP;
   triggerinor <= iHSIOtrigger or iHSIOtriggerCMOS;
  U_triggerlogic: entity work.triggerlogic 
    port map(
      clk => sysClk40,
      rst => rstFromCore,
      axiClk40 => axiClk40,
      axiRst40 => axiRst40,
      -- hardware inputs
      discin => disc,
      hitbusin => hitbus,
      -- HSIO trigger
      HSIObusy => oHSIObusy,
      HSIOtrigger => triggerinor,
      --HSIOtrigger => iHSIOtriggerHB,
      --HSIObusyin => iHSIObusy,
      HSIObusyin => '0',
      hitbusout => hitbusout,
      
      -- eudet trigger
      exttrigger => exttrigger,
      extrst => extrst,
      extbusy => extbusy,
      exttrgclk => exttrgclk,
      
      calibmode => calibmode,
      eudaqdone => eudaqdone,
      eudaqtrgword => eudaqtrgword,
      
      ttctrig => ttc_rx_out.trigL1,
      trigenabled => trigenabled,
      vetodis => vetodis,
      paused => paused,
      triggermask => triggermask,
      resetdelay => resetdelay,
      incrementdelay => incrementdelay,
      discop => discop,
      telescopeop => telescopeop,
      period => period,
      cdtenabled => cdtenabled,
      numbuffers => numbuffers,
      window => window,
      fifothresh => fifothresh,
      serbusy => serbusy,
      tdcreadoutbusy => tdcreadoutbusy,
      l1a => l1a,
      triggerword => triggerword,
      busy =>busy,
      busyAtlas => busyAtlas,
      coincd =>open
);

   -- FPGA Core
   U_HsioCosmicCore1: entity work.HsioPixelCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 17,
                  rawFirstChannel => 19,
                  rawLastChannel => 18,
                  buffersizetdc => 14,
                  buffersizefe => 13,
                  encodingDefault => "00",
                  hitbusreadout => hitbusreadout,
                  atlasafp => '0',
                  CLK_DIV_G => CLK_DIV_C,
                  CLK25_DIV_G => CLK25_DIV_C,
                  RX_OS_CFG_G => RX_OS_CFG_C,
                  RXCDR_CFG_G => RXCDR_CFG_C,
                  RXLPM_INCM_CFG_G => RXLPM_INCM_CFG_C,
                  RXLPM_IPCM_CFG_G => RXLPM_IPCM_CFG_C)    
     port map (
      sysClk160 => sysClk160, sysClk80  => sysClk80,  
      sysClk40  => sysClk40,  sysRst40 => sysRst40,
      sysClk160Unbuf => sysClk160Unbuf, sysClk160Unbuf90 => sysClk160Unbuf90,
      sysClk40Unbuf => sysClk40Unbuf, sysClk40Unbuf90 => sysClk40Unbuf90,
      clk_in_sel => clk_in_sel, ttcClkOk => ttc_input_ok_synch,
      clk_in_sel_in => clk_in_sel_ttc, ttc_out => ttc_rx_out,
      rxClk160 => atlasClk160MHz, rxClk160En => atlasClk160MHzEn,
      sysClk100 => sysClk100, sysRst100 => sysRst100,
      axiClk40 => axiClk40, axiRst40 => axiRst40,
      pgpClk    => pgpClk, pgpRst   => pgpRst,
      gtQPllOutRefClk => QPllRefClk, gtQPllOutClk => QPllClk,
      gtQPllLock => QPllLock, gtQPllRefClkLost => QPllRefClkLost,
      gtQPllReset => QPllReset,
      reload => reload1, mgtRxN    => mgtRxN,
      mgtRxP     => mgtRxP,     mgtTxN    => mgtTxN,
      mgtTxP     => mgtTxP,
      SLinkRxN => iSLinkRxN, SLinkRxP  => iSLinkRxP, 
      SLinkTxN => oSLinkTxN, SLinkTxP => oSLinkTxP,
      serialin  => serialinb, serialout  => serialoutb, 
      l1a => l1a, 
      latchtriggerword => triggerword, busy =>busy,
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      pausedout => paused, present =>open, 
      trgenabledout => trigenabled, vetodisout => vetodis,
      rstFromCore => rstFromCore,
      fifothresh => fifothresh,  triggermask => triggermask,
      discop => discop, period => period,
      telescopeop => telescopeop, calibmodeout => calibmode,
      resetdelay => resetdelay, incrementdelay => incrementdelay, 
      sbusy => serbusy, tdcbusy => tdcreadoutbusy,
      hitbus => hitbus(0), cdtenabled => cdtenabled,
      numbuffers => numbuffers, window => window,
      debug     => debug, 
      axiReadMasterTtc => axiReadMasterTtc, axiReadSlaveTtc => axiReadSlaveTtc,
      axiWriteMasterTtc => axiWriteMasterTtc, axiWriteSlaveTtc => axiWriteSlaveTtc,
      exttriggero => exttriggero, extrsto => extrsto,
      dispDigitA => dispDigitA, dispDigitB => dispDigitB,
      dispDigitC => dispDigitC, dispDigitD => dispDigitD,
      dispDigitE => dispDigitE, dispDigitF => dispDigitF,
      dispDigitG => dispDigitG, dispDigitH => dispDigitH,
      txReady => led(0), rxReady => led(1),
      trigAdc => trigAdc, adcSel => adcSel,
      sendAdcData => sendAdcData, adcData => adcData
   );
   sendAdcData<= sendAdcDataA or sendAdcDataB;
   trigAdcA<=trigAdc and not adcSel;
   trigAdcB<=trigAdc and adcSel;
   with adcSel select
     adcData<=adcDataA when '0',
              adcDataB when '1',
              adcDataB when others;
   
   tempadc_instA: entity work.tempadc
     generic map(
       PRESCALE_G => 79,
       MAPPING_G => (7, 6, 5, 4, 3, 2, 1, 0) )
     port map(
       clk => sysClk40,
       d_out => adcDataA,
       trig => trigAdcA,
       ld => sendAdcDataA,
       adcAS => adcAsA,
       scl => adcI2cSclA,
       sda => adcI2cSdaA
       );
   tempadc_instB: entity work.tempadc
     generic map(
       PRESCALE_G => 79,
       MAPPING_G => (16, 15, 14, 13, 12, 11, 10, 9) )
     port map(
       clk => sysClk40,
       d_out => adcDataB,
       trig => trigAdcB,
       ld => sendAdcDataB,
       adcAS => adcAsB,
       scl => adcI2cSclB,
       sda => adcI2cSdaB
       );
   led(7 downto 3)<=debug(4 downto 0);
   process(pgpClk) begin
     if(rising_edge(pgpClk))then
       counter<=unsigned(counter)+1;
     end if;
   end process; 
   oReload <= not reload1 or not iExtreload or not iResetInL;       -- active low

   reload_inst: entity work.Iprog7Series
     port map(
       clk => sysClk40,
       rst => sysRst40,
       start => oReload);

   U_idelctrlclk: entity work.clock_200 port map(
     clk_in1  => pgpClk,
     clk_out1 => clockidctrl,
     reset    => pgpRst,
     locked => lockedid);  

   AtlasTtcRx_Inst: entity work.AtlasTtcRx
     port map(
       rstin => rstttc,
       clkP => iMgtRefClk3P,
       clkN => iMgtRefClk3M,
       dataP => ttc_data_p,
       dataN => ttc_data_n,
       busyIn => busyAtlas,
       busyP => ttc_busy_p,
       busyN => ttc_busy_n,
       emuSel => clk_in_sel,
       emuClk => inpClk160,
       emuData => '0',
       axiClk => axiClk40,
       axiRst => axiRst40,
       axiReadMaster => axiReadMasterTtc,
       axiReadSlave => axiReadSlaveTtc,
       axiWriteMaster => axiWriteMasterTtc,
       axiWriteSlave => axiWriteSlaveTtc,
       statusWords => ttcStatusWord,
       statusSend => ttcStatusSend,
       refClk200MHz => clockidctrl,
       refClkLocked => lockedid,
       atlasTtcRxOut => ttc_rx_out,
       atlasClk40MHz => ttc_clk_40,
       atlasClk80MHz => open,
       atlasClk160MHz => atlasClk160MHz,
       atlasClk160MHzEn => atlasClk160MHzEn,
       atlasClk160MHzRst => open);
   hbcheck<=serialinb(15);

  synch_0: entity work.Synchronizer
   port map(
     clk => axiClk40,
     dataIn => ttc_input_ok,
     dataOut => ttc_input_ok_synch);
   process begin
     wait until rising_edge(axiClk40);
     clk_in_sel_old<=clk_in_sel;
     ttc_input_ok_synch_old<=ttc_input_ok_synch;
     if(clk_in_sel/=clk_in_sel_old)then
       rstttc<='1';
     elsif(clk_in_sel='0' and ttc_input_ok_synch='0' and ttc_input_ok_synch_old='1') then
       clk_in_sel_ttc<='1';             --lost TTC clock. Switch back to internal clock
     else
       rstttc<='0'; 
       clk_in_sel_ttc<='0';
     end if; 
   end process;
   
--   ila0_inst: entity work.ila_0
--     port map(
--       clk => atlasClk160MHz,
--       probe0(0) => ttc_rx_out.trigL1,
--       probe1(0) => ttc_rx_out.ecrDet,
--       probe2 => ttcStatusWord(0)(14 downto 0),
--       probe3 => ttc_rx_out.bunchCnt,
--       probe4 => ttc_rx_out.bunchRstCnt,
--       probe5 => ttc_rx_out.eventCnt,
--       probe6 => ttc_rx_out.eventRstCnt,
--       probe7(0) => ttcStatusSend,
--       probe8(0) => atlasClk160MHzEn);
       
  
  I2cHD44780Display_inst: entity work.I2cHD44780Display
  port map
  (
    clk => sysClk40,
    rst => '0',
    displayI2cSda => displayI2cSda,
    displayI2cScl => displayI2cScl,
    dISPLayBuffer => displayBuffer
  );

  process(dispDigitA, dispDigitB, dispDigitC, dispDigitD, dispDigitE, dispDigitF,
          dispDigitG, dispDigitH)
   variable displayBufferVar : HD44780DriverDisplayBufferType := (others => x"20");
   variable textpos: integer :=0;
   begin
    displayBufferVar:=(others => x"20");
    printText(displayBufferVar, 0, textpos, "PGP: ");
    placeNextDigit(displayBufferVar, textpos, textpos, dispDigitA);
    printText(displayBufferVar, 10, textpos, "S-Link: ");
    placeNextDigit(displayBufferVar, textpos, textpos, dispDigitB);
    printText(displayBufferVar, 20, textpos, "V");
    placeNextDigit(displayBufferVar, textpos, textpos, dispDigitG);
    placeNextDigit(displayBufferVar, textpos, textpos, dispDigitH);
    if(dispDigitC=x"41") then
      printText(displayBufferVar, textpos, textpos, "AFP");
    end if; 
    if(dispDigitD=x"41") then
      printText(displayBufferVar, textpos, textpos, "HB");
    end if; 
    placeNextDigit(displayBufferVar, textpos, textpos, dispDigitF);
    printText(displayBufferVar, textpos, textpos, "-");
    if(adapter=x"00")then
      printText(displayBufferVar, textpos, textpos, "CosmicNew");
    elsif(adapter=x"01")then
      printText(displayBufferVar, textpos, textpos, "CosmicII");
    elsif(adapter=x"02")then
      printText(displayBufferVar, textpos, textpos, "Opto");
    elsif(adapter=x"03")then
      printText(displayBufferVar, textpos, textpos, "Stave");
    elsif(adapter=x"04")then
      printText(displayBufferVar, textpos, textpos, "StaveRot");
    end if;
    
    --displayBufferVar(21):= dispDigitB;
    --displayBufferVar(22):= dispDigitC;
    --displayBufferVar(23):= dispDigitD;
    --displayBufferVar(24):= dispDigitE;
    --displayBufferVar(25):= dispDigitF;
    --displayBufferVar(26):= dispDigitG;

    displayBuffer<=displayBufferVar;
   end process;

end HsioCosmic;
