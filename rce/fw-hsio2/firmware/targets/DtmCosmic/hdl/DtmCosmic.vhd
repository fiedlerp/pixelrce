-------------------------------------------------------------------------------
-- DtmHsio.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.numeric_std.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

use work.RceG3Pkg.all;
use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.Pgp3p125GbpsPkg.all;
use work.AtlasTtcRxPkg.all;

entity DtmCosmic is
   port (

      -- Debug
      led          : out   slv(1 downto 0);

      -- I2C
      i2cSda       : inout sl;
      i2cScl       : inout sl;

      -- Reference Clock
      locRefClkP  : in    sl;
      locRefClkM  : in    sl;

      -- Clock Select
      clkSelA     : out   sl;
      clkSelB     : out   sl;

      -- Base Ethernet
      ethRxCtrl   : in    slv(1 downto 0);
      ethRxClk    : in    slv(1 downto 0);
      ethRxDataA  : in    Slv(1 downto 0);
      ethRxDataB  : in    Slv(1 downto 0);
      ethRxDataC  : in    Slv(1 downto 0);
      ethRxDataD  : in    Slv(1 downto 0);
      ethTxCtrl   : out   slv(1 downto 0);
      ethTxClk    : out   slv(1 downto 0);
      ethTxDataA  : out   Slv(1 downto 0);
      ethTxDataB  : out   Slv(1 downto 0);
      ethTxDataC  : out   Slv(1 downto 0);
      ethTxDataD  : out   Slv(1 downto 0);
      ethMdc      : out   Slv(1 downto 0);
      ethMio      : inout Slv(1 downto 0);
      ethResetL   : out   Slv(1 downto 0);

      -- RTM High Speed
      dtmToRtmHsP : out   sl; -- to_dtm0_tx_p
      dtmToRtmHsM : out   sl; -- to_dtm0_tx_m
      rtmToDtmHsP : in    sl; -- to_dtm0_rx_p
      rtmToDtmHsM : in    sl; -- to_dtm0_rx_m

      -- RTM Low Speed
      --dtmToRtmLsP  : inout slv(5 downto 0);
      --dtmToRtmLsM  : inout slv(5 downto 0);
      busyOutP    : out sl;
      busyOutM    : out sl;
      lolInP      : in sl;
      lolInM      : in sl;
      sdInP       : in sl;
      sdInM       : in sl;

      --DTM low speed lines
      dpmClkP: out slv(2 downto 0);
      dpmClkM: out slv(2 downto 0);
      idpmFbP: in slv(3 downto 0);
      idpmFbM: in slv(3 downto 0);
      odpmFbP: out slv(3 downto 0);
      odpmFbM: out slv(3 downto 0);


      -- Backplane Clocks
      bpClkIn      : in    slv(5 downto 0);
      bpClkOut     : out   slv(5 downto 0);

      -- Spare Signals
      --plSpareP     : inout slv(4 downto 0);
      --plSpareM     : inout slv(4 downto 0);

      -- IPMI
      dtmToIpmiP   : out   slv(1 downto 0);
      dtmToIpmiM   : out   slv(1 downto 0);

      discin       : in    slv(3 downto 0)
   );
end DtmCosmic;

architecture STRUCTURE of DtmCosmic is

   constant TPD_C : time := 1 ns;

   -- Local Signals
   signal axiClk             : sl;
   signal axiClkRst          : sl;
   signal sysClk125          : sl;
   signal sysClk125Rst       : sl;
   signal sysClk200          : sl;
   signal sysClk200Rst       : sl;
   signal extAxilReadMaster  : AxiLiteReadMasterType;
   signal extAxilReadSlave   : AxiLiteReadSlaveType;
   signal extAxilWriteMaster : AxiLiteWriteMasterType;
   signal extAxilWriteSlave  : AxiLiteWriteSlaveType;
   signal locAxilReadMaster  : AxiLiteReadMasterArray(0 downto 0);
   signal locAxilReadSlave   : AxiLiteReadSlaveArray(0 downto 0);
   signal locAxilWriteMaster : AxiLiteWriteMasterArray(0 downto 0);
   signal locAxilWriteSlave  : AxiLiteWriteSlaveArray(0 downto 0);
   signal dmaClk             : slv(2 downto 0);
   signal dmaClkRst          : slv(2 downto 0);
   signal dmaState           : RceDmaStateArray(2 downto 0);
   signal dmaObMaster        : AxiStreamMasterArray(2 downto 0);
   signal dmaObSlave         : AxiStreamSlaveArray(2 downto 0);
   signal dmaIbMaster        : AxiStreamMasterArray(2 downto 0);
   signal dmaIbSlave         : AxiStreamSlaveArray(2 downto 0);
   signal dpmClkIn           : slv(2 downto 0);
   signal idpmFb              : slv(3 downto 0);
   signal odpmFb              : slv(3 downto 0);
   signal lol                 : sl;
   signal disc           : std_logic_vector(3 downto 0);
   signal calibmode     : std_logic_vector(1 downto 0);
   signal eudaqdone     : std_logic;
   signal eudaqtrgword  : std_logic_vector(14 downto 0);
   signal trigenabled   : std_logic;
   signal paused        : std_logic;
   signal triggermask   : std_logic_vector(15 downto 0);
   signal resetdelay    : std_logic;
   signal incrementdelay: std_logic_vector(4 downto 0); 
   signal discop        : std_logic_vector(15 downto 0); 
   signal telescopeop   : std_logic_vector(2 downto 0);
   signal period        : std_logic_vector(31 downto 0);
   signal fifothresh    : std_logic;
   signal serbusy       : std_logic;
   signal tdcreadoutbusy: std_logic;
   signal l1a           : std_logic;
   signal triggerword   : std_logic_vector(7 downto 0);
   signal busy          : std_logic;
   signal rstFromCore   : std_logic;
   signal hitbus        : std_logic_vector(1 downto 0);
   signal serialoutb     : std_logic_vector(31 downto 0);
   signal serialinb      : std_logic_vector(31 downto 0):=(others => '0');
   signal sysClk40      : std_logic;
   signal sysRst40      : std_logic;
   signal sysClk80     : std_logic;
   signal sysClk160     : std_logic;
   signal sysClk160Unbuf    : std_logic;
   signal sysClk160Unbuf90  : std_logic;
   signal sysClk40Unbuf     : std_logic;
   signal sysClk40Unbuf90   : std_logic;
   signal stableClk250      : std_logic;
   signal iHSIOtriggerHB: slv(1 downto 0):="00";
   signal vetodis: sl;                
   signal cdtenabled : sl;
   signal numbuffers: slv(4 downto 0);
   signal window: slv(15 downto 0);
   signal debug          : std_logic_vector(7 downto 0);
   signal busyOut        : sl:='0';
   signal sd             : sl;
 
   attribute IODELAY_GROUP                    : string;
   attribute IODELAY_GROUP of IDELAYCTRL_Inst : label is "atlas_ttc_rx_delay_group";

begin

   --------------------------------------------------
   -- Core
   --------------------------------------------------
   U_HsioCore: entity work.HsioCore 
      generic map (
         TPD_G          => TPD_C,
         RCE_DMA_MODE_G => RCE_DMA_AXIS_C
      ) port map (
         i2cSda              => i2cSda,
         i2cScl              => i2cScl,
         clkSelA             => clkSelA,
         clkSelB             => clkSelB,
         ethRxCtrl           => ethRxCtrl,
         ethRxClk            => ethRxClk,
         ethRxDataA          => ethRxDataA,
         ethRxDataB          => ethRxDataB,
         ethRxDataC          => ethRxDataC,
         ethRxDataD          => ethRxDataD,
         ethTxCtrl           => ethTxCtrl,
         ethTxClk            => ethTxClk,
         ethTxDataA          => ethTxDataA,
         ethTxDataB          => ethTxDataB,
         ethTxDataC          => ethTxDataC,
         ethTxDataD          => ethTxDataD,
         ethMdc              => ethMdc,
         ethMio              => ethMio,
         ethResetL           => ethResetL,
         dtmToIpmiP          => dtmToIpmiP,
         dtmToIpmiM          => dtmToIpmiM,
         sysClk125           => sysClk125,
         sysClk125Rst        => sysClk125Rst,
         sysClk200           => sysClk200,
         sysClk200Rst        => sysClk200Rst,
         axiClk              => axiClk,
         axiClkRst           => axiClkRst,
         extAxilReadMaster   => extAxilReadMaster,
         extAxilReadSlave    => extAxilReadSlave,
         extAxilWriteMaster  => extAxilWriteMaster,
         extAxilWriteSlave   => extAxilWriteSlave,
         dmaClk              => dmaClk,
         dmaClkRst           => dmaClkRst,
         dmaState            => dmaState,
         dmaObMaster         => dmaObMaster,
         dmaObSlave          => dmaObSlave,
         dmaIbMaster         => dmaIbMaster,
         dmaIbSlave          => dmaIbSlave,
         userInterrupt       => (others=>'0')
      );


   -------------------------------------
   -- AXI Lite Crossbar
   -- Base: 0xA0000000 - 0xAFFFFFFF
   -------------------------------------
   U_AxiCrossbar : entity work.AxiLiteCrossbar 
      generic map (
         TPD_G              => TPD_C,
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => 1,
         DEC_ERROR_RESP_G   => AXI_RESP_OK_C,
         MASTERS_CONFIG_G   => (

            -- Channel 0 = 0xA0000000 - 0xA000FFFF : PGP
            0 => ( baseAddr     => x"A0000000",
                   addrBits     => 16,
                   connectivity => x"FFFF")
         )
      ) port map (
         axiClk              => axiClk,
         axiClkRst           => axiClkRst,
         sAxiWriteMasters(0) => extAxilWriteMaster,
         sAxiWriteSlaves(0)  => extAxilWriteSlave,
         sAxiReadMasters(0)  => extAxilReadMaster,
         sAxiReadSlaves(0)   => extAxilReadSlave,
         mAxiWriteMasters    => locAxilWriteMaster,
         mAxiWriteSlaves     => locAxilWriteSlave,
         mAxiReadMasters     => locAxilReadMaster,
         mAxiReadSlaves      => locAxilReadSlave
      );


   --------------------------------------------------
   -- PPI Loopback
   --------------------------------------------------
   DMA_STATIC: for i in 1 to 2 generate
      dmaIbMaster(i) <= AXI_STREAM_MASTER_INIT_C;
      dmaObSlave(i) <= AXI_STREAM_SLAVE_FORCE_C;
   end generate DMA_STATIC;
   dmaClk(0)      <= sysClk200;
   dmaClkRst(0)   <= sysClk200Rst;

   IDELAYCTRL_Inst : IDELAYCTRL
      port map (
         RDY    => open,        -- 1-bit output: Ready output
         REFCLK => sysClk200,        -- 1-bit input: Reference clock input
         RST    => sysClk200Rst);        -- 1-bit input: Active high reset input
   U_ClkGen: entity work.ClkGenDtm
     port map(
       extRstL => '1', --iResetInL,
       stableClk250 => stableClk250,
       sysClk40 => sysClk40,
       sysRst40 => sysRst40,
       sysClk80 => sysClk80,
       sysClk160 => sysClk160,
       sysClk40Unbuf => sysClk40Unbuf,
       sysClk40Unbuf90 => sysClk40Unbuf90,
       sysClk160Unbuf => sysClk160Unbuf,
       sysClk160Unbuf90 => sysClk160Unbuf90,
       sysClkLock => open,
       sysClkRst => '0',
       gtClk2P => locRefClkP,
       gtClk2N => locRefClkM);

  U_triggerlogic: entity work.triggerlogic 
    port map(
      clk => sysClk40,
      rst => rstFromCore,
      axiClk40 => sysClk40,
      axiRst40 => sysRst40,
      -- hardware inputs
      discin => discin,
      hitbusin => hitbus,
      -- HSIO trigger
      HSIObusy => open,
      HSIOtrigger => "00",
      --HSIOtrigger => iHSIOtriggerHB,
      --HSIObusyin => iHSIObusy,
      HSIObusyin => '0',
      hitbusout => open,
      
      -- eudet trigger
      exttrigger => '0',
      extrst => '0',
      extbusy => open,
      exttrgclk => open,
      
      calibmode => calibmode,
      eudaqdone => eudaqdone,
      eudaqtrgword => eudaqtrgword,
      
      ttctrig => '0',
      trigenabled => trigenabled,
      vetodis => vetodis,
      paused => paused,
      triggermask => triggermask,
      resetdelay => resetdelay,
      incrementdelay => incrementdelay,
      discop => discop,
      telescopeop => telescopeop,
      period => period,
      cdtenabled => cdtenabled,
      numbuffers => numbuffers,
      window => window,
      fifothresh => fifothresh,
      serbusy => serbusy,
      tdcreadoutbusy => tdcreadoutbusy,
      l1a => l1a,
      triggerword => triggerword,
      busy =>busy,
      coincd =>open
);

   --------------------------------------------------
   -- PGP Lane
   U_HsioCosmicCore1: entity work.DtmPixelCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 0,
                  rawFirstChannel => 19,
                  rawLastChannel => 18,
                  buffersizetdc => 14,
                  buffersizefe => 13,
                  encodingDefault => "00",
                  hitbusreadout => '0',
                  atlasafp => '0',
                  CLK_DIV_G => CLK_DIV_C,
                  CLK25_DIV_G => CLK25_DIV_C,
                  RX_OS_CFG_G => RX_OS_CFG_C,
                  RXCDR_CFG_G => RXCDR_CFG_C,
                  RXLPM_INCM_CFG_G => RXLPM_INCM_CFG_C,
                  RXLPM_IPCM_CFG_G => RXLPM_IPCM_CFG_C)    
     port map (
      sysClk160 => sysClk160, sysClk80  => sysClk80,  
      sysClk40  => sysClk40,  sysRst40 => sysRst40,
      sysClk160Unbuf => sysClk160Unbuf, sysClk160Unbuf90 => sysClk160Unbuf90,
      sysClk40Unbuf => sysClk40Unbuf, sysClk40Unbuf90 => sysClk40Unbuf90,
      clk_in_sel => open, ttcClkOk => '0',
      clk_in_sel_in => '1', ttc_out => ATLAS_TTC_RX_OUT_INIT_C,
      rxClk160 => '0', rxClk160En => '0',
      sysClk100 => '0', sysRst100 => '0',
      axiClk40 => sysClk40, axiRst40 => sysRst40,
      pgpClk    => dmaClk(0), pgpRst   => dmaClkRst(0),
      gtQPllOutRefClk => (others =>'0'), gtQPllOutClk => (others=>'0'),
      gtQPllLock => (others=>'0'), gtQPllRefClkLost => (others=>'0'),
      gtQPllReset => open,
      reload => open, mgtRxN    => rtmToDtmHsM,
      mgtRxP     => rtmToDtmHsP,     mgtTxN    => dtmToRtmHsM,
      mgtTxP     => dtmToRtmHsP,
      SLinkRxN => '0', SLinkRxP  => '0', 
      SLinkTxN => open, SLinkTxP => open,
      txMaster => dmaObMaster(0),
      txSlave => dmaObSlave(0),
      rxMaster => dmaIbMaster(0),
      rxSlave  => dmaIbSlave(0),
      serialin  => serialinb, serialout  => serialoutb, 
      l1a => l1a, 
      latchtriggerword => triggerword, busy =>busy,
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      pausedout => paused, present =>open, 
      trgenabledout => trigenabled, vetodisout => vetodis,
      rstFromCore => rstFromCore,
      fifothresh => fifothresh,  triggermask => triggermask,
      discop => discop, period => period,
      telescopeop => telescopeop, calibmodeout => calibmode,
      resetdelay => resetdelay, incrementdelay => incrementdelay, 
      sbusy => serbusy, tdcbusy => tdcreadoutbusy,
      hitbus => hitbus(0), cdtenabled => cdtenabled,
      numbuffers => numbuffers, window => window,
      debug     => debug, 
      axiReadMasterTtc => open, axiReadSlaveTtc => AXI_LITE_READ_SLAVE_INIT_C,
      axiWriteMasterTtc => open, axiWriteSlaveTtc => AXI_LITE_WRITE_SLAVE_INIT_C,
      exttriggero => open, extrsto => open,
      dispDigitA => open, dispDigitB => open,
      dispDigitC => open, dispDigitD => open,
      dispDigitE => open, dispDigitF => open,
      dispDigitG => open, dispDigitH => open,
      txReady => open, rxReady => open,
      trigAdc => open, adcSel => open,
      sendAdcData => '0', adcData => (others => (others => '0'))
   );
   --------------------------------------------------

   -- Debug
   led <= (others=>'0');
   dpmclkin(2 downto 0)<=(0 => not sysClk40, others=>'0');

   -- Reference Cloc/afs/slac.stanford.edu/g/reseng/vol15/Xilinx/vivado_2014.1/SDK/2014.1/gnu/arm/lin/k
   --locRefClkP  : in    sl;
   --locRefClkM  : in    sl;

   -- RTM High Speed
   --dtmToRtmHsP : out   sl;
   --dtmToRtmHsM : out   sl;
   --rtmToDtmHsP : in    sl;
   --rtmToDtmHsM : in    sl;

   -- RTM Low Speed
   --dtmToRtmLsP  : inout slv(5 downto 0);
   --dtmToRtmLsM  : inout slv(5 downto 0);

   -- DPM Clock Signals
   U_DpmClkGen : for i in 0 to 2 generate
      U_DpmClkOut : OBUFDS
         port map(
            O      => dpmClkP(i),
            OB     => dpmClkM(i),
            I      => dpmclkin(i) 
         );
   end generate;

   -- DPM Feedback Signals
   U_DpmFbGen : for i in 0 to 3 generate
      U_DpmFbIn : IBUFDS
         generic map ( DIFF_TERM => true ) 
         port map(
            I      => idpmFbP(i),
            IB     => idpmFbM(i),
            O      => idpmFb(i)
         );
      U_DpmFbOut : OBUFDS
         port map(
            O      => odpmFbP(i),
            OB     => odpmFbM(i),
            I      => odpmFb(i)
         );
   end generate;
      U_BusyOut : OBUFDS
         port map(
            O      => busyOutP,
            OB     => busyOutM,
            I      => busyOut 
         );
      U_sdIn : IBUFDS
         generic map ( DIFF_TERM => true ) 
         port map(
            I      => sdInP,
            IB     => sdInM,
            O      => sd 
         );
      U_lolIn : IBUFDS
         generic map ( DIFF_TERM => true ) 
         port map(
            I      => lolInP,
            IB     => lolInM,
            O      => lol
         );
   odpmFb(0)<= serialoutb(0);
   serialinb(0)<=idpmFb(0);
   -- Backplane Clocks
   --bpClkIn      : in    slv(5 downto 0);
   --bpClkOut     : out   slv(5 downto 0);
   bpClkOut <= (others=>'0');

   -- Spare Signals
   --plSpareP     : inout slv(4 downto 0);
   --plSpareM     : inout slv(4 downto 0)

end architecture STRUCTURE;

