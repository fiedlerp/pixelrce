#ifndef PixelFEI4Config_HH
#define PixelFEI4Config_HH

#include <RCF/RCF.hpp>
#include <stdint.h>

namespace ipc{


  const long IPC_N_I4_PIXEL_COLUMNS = 80;
  const long IPC_N_I4_PIXEL_ROWS = 336;
  const long enable = 0;
  const long largeCap = 1;
  const long smallCap = 2;
  const long hitbus = 3;

  struct PixelFECommand{ //FE Command register
    uint8_t address;		       /* 5 bits */
    uint32_t command;            /* 32 bits */
    void serialize(SF::Archive &ar){
      ar & address & command;
    }
  };

  struct PixelFECalibFEI4{ 
    /* Sub-structure for calibration of injection-capacitors, VCAL-DAC and
       leakage current measurement */
    float cinjLo;
    float cinjHi;
    float vcalCoeff[4];
    float chargeCoeffClo;  
    float chargeCoeffChi;
    float chargeOffsetClo;
    float chargeOffsetChi;
    float monleakCoeff;
    void serialize(SF::Archive &ar){
      ar & cinjLo & cinjHi;
      for(int i=0;i<4;i++)ar & vcalCoeff[i];
      ar & chargeCoeffClo & chargeCoeffChi & chargeOffsetClo & chargeOffsetChi & monleakCoeff;
    }
  }; 

  struct PixelFEI4Trims{ //Trim DACs:
    uint8_t dacThresholdTrim[IPC_N_I4_PIXEL_COLUMNS][IPC_N_I4_PIXEL_ROWS];  /* 5 bits per pixel */
    uint8_t dacFeedbackTrim [IPC_N_I4_PIXEL_COLUMNS][IPC_N_I4_PIXEL_ROWS];  /* 4 bits per pixel */
    void serialize(SF::Archive &ar){
      for(int i=0;i<IPC_N_I4_PIXEL_COLUMNS;i++)
	for(int j=0;j<IPC_N_I4_PIXEL_ROWS;j++)
	  ar & dacThresholdTrim[i][j];
      for(int i=0;i<IPC_N_I4_PIXEL_COLUMNS;i++)
	for(int j=0;j<IPC_N_I4_PIXEL_ROWS;j++)
	  ar & dacFeedbackTrim[i][j];
    }
  } ; 
  
};

#endif
