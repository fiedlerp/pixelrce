#include "analysis/CrosstalkAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TF1.h"
#include "TStyle.h"


void CrosstalkAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"Crosstalk analysis"<<std::endl;
  unsigned int numval=scan->getLoopVarValues(0).size();
  gStyle->SetOptFit(10);
  gStyle->SetOptStat(0);
  std::map<int, TH1D*> histmap;
  TIter nextkey(gDirectory->GetListOfKeys()); // histograms
  TKey *key;
  while ((key=(TKey*)nextkey())) {
    file->cd();
    std::string hname(key->GetName());
    std::cmatch matches;
    std::regex re("_(\\d+)_Mean");
    std::regex re2("Mean");
    char name[128];
    char title[128];
    if(std::regex_search(hname.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      std::string chi2HistoName = std::regex_replace (hname, re2, "ChiSquare");
      std::string sigmaHistoName = std::regex_replace (hname, re2, "Sigma");
      TH2* histo = (TH2*)key->ReadObj();
      TH2* chi2Histo=(TH2*)gDirectory->Get(chi2HistoName.c_str());
      TH2* sigmahisto=(TH2*)gDirectory->Get(sigmaHistoName.c_str());
      assert(chi2Histo!=0);
      assert(sigmahisto!=0);
      int binsx=histo->GetNbinsX();
      int binsy=histo->GetNbinsY();
      sprintf(name, "thresh2d_Mod_%d", id);
      sprintf(title, "Thresholds Module %d at %s", id, findFieldName(cfg, id));
      TH2F* thresh2d=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      thresh2d->GetXaxis()->SetTitle("Column");
      thresh2d->GetYaxis()->SetTitle("Row");
      sprintf(name, "thresh1d_Mod_%d", id);
      sprintf(title, "Thresholds Module %d at %s", id, findFieldName(cfg, id));
      int nbins=binsx*binsy;
      TH1F* thresh1d=new TH1F(name, title, nbins, 0, (float)nbins);
      thresh1d->GetXaxis()->SetTitle("Channel");
      thresh1d->SetOption("p9");
      sprintf(name, "sigma1d_Mod_%d", id);
      sprintf(title, "Sigma Module %d at %s", id, findFieldName(cfg, id));
      TH1F* sigma1d=new TH1F(name, title, nbins, 0, (float)nbins);
      sigma1d->GetXaxis()->SetTitle("Channel");
      sigma1d->SetOption("p9");
      sprintf(name, "threshdist_Mod_%d", id);
      sprintf(title, "Threshold distribution Module %d at %s", id, findFieldName(cfg, id));
      TH1F* threshdist=new TH1F(name, title, 500, 20000, 60000);
      threshdist->GetXaxis()->SetTitle("Threshold");
      sprintf(name, "sigmadist_Mod_%d", id);
      sprintf(title, "Sigma distribution Module %d at %s", id, findFieldName(cfg, id));
      TH1F* sigmadist=new TH1F(name, title, 100, 0, 12000);
      sigmadist->GetXaxis()->SetTitle("Sigma");
      sprintf(name, "crosstalk2d_Mod_%d", id);
      sprintf(title, "Crosstalk for Last Scan Point, Module %d at %s", id, findFieldName(cfg, id));
      TH2F* crosstalk2d=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
      crosstalk2d->GetXaxis()->SetTitle("Column");
      crosstalk2d->GetYaxis()->SetTitle("Row");
      sprintf(name, "crosstalk1d_Mod_%d", id);
      sprintf(title, "Crosstalk for Last Scan Point, Module %d at %s", id, findFieldName(cfg, id));
      TH1F* crosstalk1d=new TH1F(name, title, nbins, 0, (float)nbins); 
      crosstalk1d->GetXaxis()->SetTitle("Channel");
      crosstalk1d->SetOption("p9");    
      
      TH1D* hits1d=new TH1D(Form("HitsPerBin_Mod_%d", id), 
			    Form("Hits per bin Mod %d at %s", id, findFieldName(cfg, id)), 
			    numval, -.5, (float)numval-.5) ;
      hits1d->GetXaxis()->SetTitle("Scan Point");


      for (int i=1;i<=histo->GetNbinsX();i++){
        for(int j=1;j<=histo->GetNbinsY();j++){
          thresh2d->SetBinContent(i,j,histo->GetBinContent(i,j));
          thresh1d->SetBinContent((i-1)*histo->GetNbinsY()+j, histo->GetBinContent(i,j));
          threshdist->Fill(histo->GetBinContent(i,j));
          sigma1d->SetBinContent((i-1)*histo->GetNbinsY()+j, sigmahisto->GetBinContent(i,j));
          sigmadist->Fill(sigmahisto->GetBinContent(i,j));
	}
      }

      TF1 gauss("gauss", "gaus");
      gauss.SetParameter(0, threshdist->GetMaximum());
      gauss.SetParameter(1, threshdist->GetMaximumBin()*threshdist->GetBinWidth(1));
      threshdist->Fit(&gauss,"q", "");



      int numdistbins=100;

      std::string finalhistoName = std::regex_replace (hname, re2, Form("Occupancy_Point_%03d", numval-1));
      TH2* finalOccHisto=(TH2*)gDirectory->Get(finalhistoName.c_str());
      if(finalOccHisto!=0){
	numdistbins = finalOccHisto->GetMaximum();
      }

      sprintf(name, "crosstalkdist_Mod_%d", id);
      sprintf(title, "Occupancy distribution for highest Scan Point, Module %d at %s", id, findFieldName(cfg, id));
      TH1F* crosstalkdist=new TH1F(name, title, numdistbins, -0.5, (float)numdistbins - 0.5);
      crosstalkdist->GetXaxis()->SetTitle("Occupancy for highest Scan Point");

      if(finalOccHisto==0){
	std::cout<<"Final Occupancy histogram not found. Won't fill crosstalk histos."<<std::endl;
      }
      else{

	for (int i=1;i<=finalOccHisto->GetNbinsX();i++){
	  for(int j=1;j<=finalOccHisto->GetNbinsY();j++){
	    crosstalk2d->SetBinContent(i,j,finalOccHisto->GetBinContent(i,j));
	    crosstalk1d->SetBinContent((i-1)*finalOccHisto->GetNbinsY()+j, finalOccHisto->GetBinContent(i,j));
	    crosstalkdist->Fill(finalOccHisto->GetBinContent(i,j));
	  }
	}

      }


      for(unsigned int k=0;k<numval;k++){

	std::string histoName = std::regex_replace (hname, re2, Form("Occupancy_Point_%03d", k));
	TH2* occHisto=(TH2*)gDirectory->Get(histoName.c_str());
	if(occHisto==0){
	  std::cout<<"No Occupancy histograms found. Won't fill 1-d hit histo."<<std::endl;
	  break;
	}
	hits1d->SetBinContent(k+1, occHisto->GetSumOfWeights());
      }

      anfile->cd();
      thresh2d->Write();
      thresh2d->SetDirectory(gDirectory);
      thresh1d->Write();
      thresh1d->SetDirectory(gDirectory);
      threshdist->Write();
      threshdist->SetDirectory(gDirectory);
      sigma1d->Write();
      sigma1d->SetDirectory(gDirectory);
      sigmadist->Write();
      sigmadist->SetDirectory(gDirectory);

      crosstalk2d->Write();
      crosstalk2d->SetDirectory(gDirectory);
      crosstalk1d->Write();
      crosstalk1d->SetDirectory(gDirectory);
      crosstalkdist->Write();
      crosstalkdist->SetDirectory(gDirectory);

      hits1d->Write();
      hits1d->SetDirectory(gDirectory);
    }
  }
  
}
