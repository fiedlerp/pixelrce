#include "analysis/NoiseAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>

namespace{
  const double threshold=0;
}
using namespace RCE;

void NoiseAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scn, int runno, ConfigGui* cfg[]){
  TIter nextkey(file->GetListOfKeys());
  TKey *key;
  std::regex re("_(\\d+)_NoiseOccupancy");
  std::regex re2("NoiseOccupancy");
  while ((key=(TKey*)nextkey())) {
    file->cd();
    std::string name(key->GetName());
    std::cmatch matches;
    if(std::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      std::string neHistoName = std::regex_replace (name, re2, "nEvents");
      TH2* histo = (TH2*)key->ReadObj();
      TH1* neHisto=(TH1*)gDirectory->Get(neHistoName.c_str());
      assert(neHisto!=0);
      double nev=(double)neHisto->GetBinContent(1);
      if(nev==0){
	std::cout<<"Number of events in "<<neHistoName<<" is 0"<<std::endl;
	continue;
      }
      TH2D* ahis=new TH2D(Form("norm_occ_mod_%d",id), Form("Occupancy Mod %d at %s",id, findFieldName(cfg, id)), 
	      		  histo->GetNbinsX(), histo->GetXaxis()->GetXmin(), histo->GetXaxis()->GetXmax(),
	      		  histo->GetNbinsY(), histo->GetYaxis()->GetXmin(), histo->GetYaxis()->GetXmax());
      ahis->GetXaxis()->SetTitle("Column");
      ahis->GetYaxis()->SetTitle("Row");
      TH2D* mhis=new TH2D(Form("mask_mod_%d",id), Form("Mask Mod %d at %s", id, findFieldName(cfg, id)), 
	      		  histo->GetNbinsX(), histo->GetXaxis()->GetXmin(), histo->GetXaxis()->GetXmax(),
	      		  histo->GetNbinsY(), histo->GetYaxis()->GetXmin(), histo->GetYaxis()->GetXmax());
      mhis->GetXaxis()->SetTitle("Column");
      mhis->GetYaxis()->SetTitle("Row");
      PixelConfig* confb=findConfig(cfg, id);
      bool isfei4=(confb->getType()!="FEI3");
      if(isfei4 && scn->clearMasks()==true)clearFEI4Masks(confb);
      for (int i=0;i<histo->GetNbinsX();i++){
	for(int j=0;j<histo->GetNbinsY();j++){
	  if(nev>0){
	    ahis->SetBinContent(i+1,j+1,(double)histo->GetBinContent(i+1, j+1)/nev);
	    if((scn->useAbsoluteNoiseThreshold()==true && (double)histo->GetBinContent(i+1, j+1)>scn->getNoiseThreshold())
	       || (scn->useAbsoluteNoiseThreshold()==false && ahis->GetBinContent(i+1, j+1)>scn->getNoiseThreshold())){
	      mhis->SetBinContent(i+1,j+1,1);
	      if(isfei4){
		confb->setFEMask(0,i,j, confb->getFEMask(0,i,j)&0xfe); //reset bit 0 (enable)
		confb->setFEMask(0,i,j, confb->getFEMask(0,i,j)|0x8); //reset bit 3 (hitbus)
	      }
	    }
	  }
	}
      }
      if(isfei4) writeConfig(anfile, runno);
      m_fw->writeMaskFile(Form("%sNoiseMask_Mod_%d", m_fw->getPath(anfile).c_str(), id), mhis);
      delete histo;
      anfile->cd();
      ahis->Write();
      ahis->SetDirectory(gDirectory);
      mhis->Write();
      mhis->SetDirectory(gDirectory);
    }
  }  
  if(configUpdate())writeTopFile(cfg, anfile, runno);
}

