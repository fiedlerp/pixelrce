#include "analysis/MultiTrigAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TF1.h"
#include "TStyle.h"


void MultiTrigAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"MultiTrig analysis"<<std::endl;
  unsigned int numval=scan->getLoopVarValues(0).size();
  const int totalScans=scan->getLoopVarValues(1).size();
  
  gStyle->SetOptFit(10);
  gStyle->SetOptStat(0);
  
  float xVals[totalScans];
  
  std::map<int, std::vector<TH1D*> > histoMeanScan;
  std::map<int, std::vector<TH1D*> > histoSigmaScan;
  std::map<int, std::vector<float> > meanVals;
  std::map<int, std::vector<float> > sigmaVals;
    
  //  float meanVals[totalScans][2];
  // float sigmaVals[totalScans][2];
  
  
  float xMin = scan->getLoopVarValues(1).at(0);
  float xMax = scan->getLoopVarValues(1).at(totalScans-1);
  if(xMax<xMin){
    float xtemp = xMax;
    xMax = xMin;
    xMin = xtemp;
  }
  
  int nScanBins = (xMax - xMin + 3);
    
  
  //loop over the different values of multitrig_interval
  for(int iScan = 0; iScan<totalScans; iScan++){
    file->cd(Form("loop1_%d",iScan));

    std::map<int, TH1D*> histmap;
    std::regex re0("_(\\d+)_Trig0_Mean");
    std::regex re1("_(\\d+)_Trig1_Mean");
    std::regex re2("Mean");
    
    int trigId=-1;
    TIter nextkey(gDirectory->GetListOfKeys()); // histograms
    TKey *key;
    
    while ((key=(TKey*)nextkey())) {
      std::string hname(key->GetName());
      std::cmatch matches;

      char name[128];
      char title[128];
      
      bool foundMatch=false;
      if(std::regex_search(hname.c_str(), matches, re0)){
	trigId=0;
	foundMatch=true;
      }
      else if(std::regex_search(hname.c_str(), matches, re1)){
	trigId=1;
	foundMatch=true;
      }

      if(foundMatch){

	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	//  std::cout<<"Match = "<<match.c_str()<<std::endl;
	// std::cout<<"name = "<<hname.c_str()<<std::endl;
	
	int id=strtol(match.c_str(),0,10);
	std::string chi2HistoName = std::regex_replace (hname, re2, "ChiSquare");
	std::string sigmaHistoName = std::regex_replace (hname, re2, "Sigma");
	TH2* histo = (TH2*)key->ReadObj();
	//std::cout<<"Looking for "<<chi2HistoName.c_str()<<std::endl;
	
	TH2* chi2Histo=(TH2*)gDirectory->Get(chi2HistoName.c_str());
	TH2* sigmahisto=(TH2*)gDirectory->Get(sigmaHistoName.c_str());
	assert(chi2Histo!=0);
	assert(sigmahisto!=0);
	int binsx=histo->GetNbinsX();
	int binsy=histo->GetNbinsY();
	sprintf(name, "thresh2d_%d_Mod_%d_Trig%d", iScan,id,trigId);
	sprintf(title, "Thresholds Module %d at %s Trig %d, Scan %d", id, findFieldName(cfg,id), trigId, iScan);
	TH2F* thresh2d=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
	thresh2d->GetXaxis()->SetTitle("Column");
	thresh2d->GetYaxis()->SetTitle("Row");
	sprintf(name, "thresh1d_%d_Mod_%d_Trig%d", iScan, id, trigId);
	sprintf(title, "Thresholds 2D Module %d at %s Trig %d, Scan %d", id, findFieldName(cfg,id), trigId, iScan);
	int nbins=binsx*binsy;
	TH1F* thresh1d=new TH1F(name, title, nbins, 0, (float)nbins); 
	thresh1d->GetXaxis()->SetTitle("Channel");
	thresh1d->SetOption("p9");
	sprintf(name, "sigma1d_%d_Mod_%d_Trig%d", iScan, id, trigId);
	sprintf(title, "Sigma Module %d at %s Trig %d, Scan %d", id, findFieldName(cfg,id), trigId, iScan);
	TH1F* sigma1d=new TH1F(name, title, nbins, 0, (float)nbins); 
	sigma1d->GetXaxis()->SetTitle("Channel");
	sigma1d->SetOption("p9");
	sprintf(name, "threshdist_%d_Mod_%d_Trig%d", iScan, id, trigId);
	sprintf(title, "Threshold distribution Module %d at %s Trig %d, Scan %d", id, findFieldName(cfg,id), trigId, iScan);
	TH1F* threshdist=new TH1F(name, title, 500, 1000, 6000); 
	threshdist->GetXaxis()->SetTitle("Threshold");
	sprintf(name, "sigmadist_%d_Mod_%d_Trig%d", iScan, id, trigId);
	sprintf(title, "Sigma distribution Module %d at %s Trig %d, Scan %d", id, findFieldName(cfg,id), trigId, iScan);
	TH1F* sigmadist=new TH1F(name, title, 100, 0, 500); 
	sigmadist->GetXaxis()->SetTitle("Sigma");
	sprintf(name, "BadPixels_%d_Mod_%d_Trig%d", iScan, id, trigId);
	sprintf(title, "Bad Pixels Module %d at %s Trig %d, Scan %d", id, findFieldName(cfg,id), trigId, iScan);
	TH2F* bad=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
	bad->GetXaxis()->SetTitle("Column");
	bad->GetYaxis()->SetTitle("Row");
	TH1D* hits1d=new TH1D(Form("HitsPerBin_%d_Mod_%d_Trig%d", iScan, id, trigId), Form("Hits per bin Mod %d at %s Trig %d, Scan %d", id, findFieldName(cfg,id), trigId, iScan), numval, -.5, (float)numval-.5) ;
	hits1d->GetXaxis()->SetTitle("Scan Point");
	
	for (int i=1;i<=histo->GetNbinsX();i++){
	  for(int j=1;j<=histo->GetNbinsY();j++){
	    thresh2d->SetBinContent(i,j,histo->GetBinContent(i,j));
	    thresh1d->SetBinContent((i-1)*histo->GetNbinsY()+j, histo->GetBinContent(i,j));
	    threshdist->Fill(histo->GetBinContent(i,j));
	    sigma1d->SetBinContent((i-1)*histo->GetNbinsY()+j, sigmahisto->GetBinContent(i,j));
	    sigmadist->Fill(sigmahisto->GetBinContent(i,j));
	    if(chi2Histo->GetBinContent(i,j)==0)bad->SetBinContent(i,j,1);
	  }
	}
	TF1 gauss("gauss", "gaus", 100, 10000);
	gauss.SetParameter(0, threshdist->GetMaximum());
	gauss.SetParameter(1, threshdist->GetMaximumBin()*threshdist->GetBinWidth(1));
	threshdist->Fit(&gauss,"q", "", 100, 10000);
	sigmadist->Fit("gaus","q");
	
	for(unsigned int k=0;k<numval;k++){
	  std::regex re2("Mean");
	  std::string histoName = std::regex_replace (hname, re2, Form("Occupancy_Point_%03d", k));
	  TH2* occHisto=(TH2*)gDirectory->Get(histoName.c_str());
	  if(occHisto==0){
	    std::cout<<"Looking for "<<histoName.c_str()<<std::endl;
	    std::cout<<"No Occupancy histograms found. Won't fill 1-d hit histo."<<std::endl;
	    break;
	  }
	  hits1d->SetBinContent(k+1, occHisto->GetSumOfWeights());
	}
	
	xVals[iScan] = scan->getLoopVarValues(1).at(iScan);
	

	if(histoMeanScan.find(id)==histoMeanScan.end()){

	  std::vector<TH1D*> temphistMean(2);
	  std::vector<TH1D*> temphistSigma(2);
	  	
	  for(int iTrig=0; iTrig<2; iTrig++){
	    temphistMean[iTrig] = new TH1D(Form("MeanScan_Mod%d_Trig%d",id,iTrig),Form("Threshold Mean versus Interval for Module %d, Trigger %d",id,iTrig+1), nScanBins,xMin-1,xMax+1);
	    temphistMean[iTrig]->GetXaxis()->SetTitle("Interval between 1st and 2nd inject");
	    temphistMean[iTrig]->GetYaxis()->SetTitle("Threshold Mean, Ne^{-}");
	    temphistMean[iTrig]->SetOption("P");
	    temphistMean[iTrig]->SetMarkerStyle(2);
	    temphistMean[iTrig]->SetMarkerSize(2);

	    temphistSigma[iTrig] = new TH1D(Form("SigmaScan_Mod%d_Trig%d",id,iTrig),Form("Threshold Sigma versus Interval for Module %d, Trigger %d",id,iTrig+1), nScanBins,xMin-1,xMax+1);
	    temphistSigma[iTrig]->GetXaxis()->SetTitle("Interval between 1st and 2nd inject");
	    temphistSigma[iTrig]->GetYaxis()->SetTitle("Threshold Sigma, Ne^{-}");
	    temphistSigma[iTrig]->SetOption("P");
	    temphistSigma[iTrig]->SetMarkerStyle(2);
	    temphistSigma[iTrig]->SetMarkerSize(2);
	    	    
	  }

	  histoMeanScan[id] = temphistMean;
	  histoSigmaScan[id] = temphistSigma;
	  std::vector<float> tempMeans(2*totalScans,0);
	  std::vector<float> tempSigmas(2*totalScans,0);
	  meanVals[id] = tempMeans;
	  sigmaVals[id] = tempSigmas;
	  
	}//end if id not in histoSigmaScan map
		
	
	TF1* meanGauss = threshdist->GetFunction("gauss");
	TF1* sigmaGauss = sigmadist->GetFunction("gaus");

	int myIndex = 2*iScan + trigId;
	if(meanGauss){
	  meanVals[id].at(myIndex) = meanGauss->GetParameter(1);
	}
	else{
	  meanVals[id].at(myIndex) = -1;
	}
	if(sigmaGauss){
	  sigmaVals[id].at(myIndex) = sigmaGauss->GetParameter(1);
	}
	else{
	  sigmaVals[id].at(myIndex) = -1;
	}
	
	std::cout<<"Trigger "<<trigId+1<<" , interval "<<xVals[iScan]<<" : threshold mean = "
		 <<meanVals[id].at(myIndex)<<" ; sigma = "<<sigmaVals[id].at(myIndex)<<std::endl;
	
	
	anfile->cd();
	thresh2d->Write();
	thresh2d->SetDirectory(gDirectory);
	//thresh1d->Write();
	//thresh1d->SetDirectory(gDirectory);
	threshdist->Write();
	threshdist->SetDirectory(gDirectory);
	//sigma1d->Write();
	//sigma1d->SetDirectory(gDirectory);
	sigmadist->Write();
	sigmadist->SetDirectory(gDirectory);
	//bad->Write();
	//bad->SetDirectory(gDirectory);
	hits1d->Write();
	hits1d->SetDirectory(gDirectory);
	
	file->cd(Form("loop1_%d",iScan));

      }//if foundmatch
    
    }//end loop over keys
    
  } //end loop over iScan
    
  for(std::map<int, std::vector<TH1D*> >::iterator it=histoMeanScan.begin();it!=histoMeanScan.end();it++){
    int id=it->first;
    
    for(int iScan = 0; iScan<totalScans; iScan++){
      
      int myIndex = 2*iScan;
      histoMeanScan[id].at(0)->Fill(xVals[iScan], meanVals[id].at(myIndex+0) );
      histoSigmaScan[id].at(0)->Fill(xVals[iScan], sigmaVals[id].at(myIndex+0) );
      histoMeanScan[id].at(1)->Fill(xVals[iScan], meanVals[id].at(myIndex+1) );
      histoSigmaScan[id].at(1)->Fill(xVals[iScan], sigmaVals[id].at(myIndex+1) );
      
    }
    
    anfile->cd();
    
    for(int iTrig=0; iTrig<2; iTrig++){
      histoMeanScan[id].at(iTrig)->Write();
      histoMeanScan[id].at(iTrig)->SetDirectory(gDirectory);
      histoSigmaScan[id].at(iTrig)->Write(); 
      histoSigmaScan[id].at(iTrig)->SetDirectory(gDirectory);
    }    
    
  } //end iterate over histoMeanScan
  
}

      
  
