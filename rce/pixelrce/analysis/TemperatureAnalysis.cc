#include "analysis/TemperatureAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>
#include <math.h>

using namespace RCE;

void TemperatureAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"Temperature analysis"<<std::endl;
  std::vector<float> loopVar=scan->getLoopVarValues(0);
  size_t numvals=loopVar.size();
  if(numvals<2){
    std::cout<<"Need at least two values instead of "<<numvals<<std::endl;
    return;
  }
  float a[2];
  for(int i=0;i<2;i++){
    if(loopVar[i]==1)a[i]=1;
    else if(loopVar[i]==2)a[i]=10;
    else if(loopVar[i]==3)a[i]=1000;
    else{
     a[i]=-1;
     std::cout<<"Bad setup for temperature scan"<<std::endl;
    }
  }
  float m=a[1]/a[0];
  if(m<1)m=1./m;
  std::cout<<"Factor is "<<m<<std::endl;
  TIter nextkey(file->GetListOfKeys());
  TKey *key;
  std::regex re("_(\\d+)_Voltage");
  std::vector<std::string> pos;
  std::vector<float> temps;
  std::vector<float> errs;
  while ((key=(TKey*)nextkey())) {
    std::string name(key->GetName());
    std::string title(key->GetTitle());
    std::cmatch matches;
    if(std::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      std::string match2=addPosition(name.c_str(), cfg).substr(0,5);
      pos.push_back(match2);
      TH1D* mhis=new TH1D(Form("Temperature_Mod_%d",id), Form("Temperature Mod %d at %s", id, findFieldName(cfg, id)), 1,0,1);
      mhis->GetYaxis()->SetTitle("Temperature (C)");
      TH1* histo = (TH1*)key->ReadObj();
      float val1=histo->GetBinContent(1);
      float err1=sqrt(histo->GetBinError(1));
      float val2=histo->GetBinContent(2);
      float err2=sqrt(histo->GetBinError(2));
      float volttoadc=1.013;
      float fact=1.6e-19/1.38e-23/logf(m)/1000./volttoadc;
      float temp=(val2-val1)*fact;
      if(temp<0)temp=-temp;
      temp-=273.15;
      float err=sqrt(err1*err1+err2*err2)*fact;
      mhis->SetBinContent(1,temp);
      mhis->SetBinError(1,err);
      std::cout<<"FE at "<<match2<<" has a temperature of "<<temp<<"+-"<<err<<" degrees Celsius."<<std::endl;
      temps.push_back(temp);
      errs.push_back(err);
      anfile->cd();
      mhis->Write();
      mhis->SetDirectory(gDirectory);
      delete histo;
    }
  }  
  TH1D* mhis[4];
  mhis[0]=new TH1D(Form("1-StavetempsA"), Form("Frontend Temperatures A side"), 16, 0, 16);
  mhis[1]=new TH1D(Form("1-StavetempsC"), Form("Frontend Temperatures C side"), 16, 0, 16);
  mhis[2]=new TH1D(Form("2-StavetempsA"), Form("Frontend Temperatures A side (2)"), 16, 0, 16);
  mhis[3]=new TH1D(Form("2-StavetempsC"), Form("Frontend Temperatures C side (2)"), 16, 0, 16);
  char position[10];
  for(int i=0;i<4;i++){ //half stave
    if(i%2==0)position[0]='A';
    else position[0]='C';
    mhis[i]->GetYaxis()->SetTitle("Temperature (C)");
    for(int j=1;j<=8;j++){ //module
      for(int k=1;k<=2;k++){ //FE
	sprintf(&position[1], "%d-%d", j,k);
	int bin=(j-1)*2+k;
	mhis[i]->GetXaxis()->SetBinLabel(bin,position);
	for(size_t l=0;l<pos.size();l++){
	  if(std::string(position)==pos[l].substr(1,4) && 
	     pos[l].substr(0,1)==Form("%d", i/2+1)){
	    mhis[i]->SetBinContent(bin, temps[l]);
	    mhis[i]->SetBinError(bin, errs[l]);
	  } 
	}
      }
    }
    anfile->cd();
    mhis[i]->Write();
    mhis[i]->SetDirectory(gDirectory);
  }
}

