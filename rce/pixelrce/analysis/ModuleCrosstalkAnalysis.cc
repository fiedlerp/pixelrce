#include "analysis/ModuleCrosstalkAnalysis.hh"
#include "server/PixScan.hh"
#include "server/ConfigGui.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>

using namespace RCE;

void ModuleCrosstalkAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"Module crosstalk analysis"<<std::endl;
  unsigned int numval=scan->getLoopVarValues(0).size();
  unsigned short trgmask=scan->getModuleTrgMask();
  file->cd();
  TIter nextkey(gDirectory->GetListOfKeys());
  
  TKey *key;
  std::regex re("_(\\d+)_Mean");
  while ((key=(TKey*)nextkey())) {
    std::string name(key->GetName());
    std::cmatch matches;
    if(std::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);

      int outlink = 99;      
      for(int i_conf=0;i_conf<ConfigGui::MAX_MODULES;i_conf++){
	if(cfg[i_conf]->isIncluded() && cfg[i_conf]->getId()==id){
	  outlink = cfg[i_conf]->getOutLink();  
	}
      }
      
      if(((1<<outlink)&trgmask)==0)continue;
      TH2* histo = (TH2*)key->ReadObj();
      TH2D* mhis=new TH2D(Form("Hits_Mod_%d",id), Form("All Hits Mod %d at %s", id, findFieldName(cfg, id)), 
	      		  histo->GetNbinsX(), histo->GetXaxis()->GetXmin(), histo->GetXaxis()->GetXmax(),
	      		  histo->GetNbinsY(), histo->GetYaxis()->GetXmin(), histo->GetYaxis()->GetXmax());
      mhis->GetXaxis()->SetTitle("Column");
      mhis->GetYaxis()->SetTitle("Row");
      TH1D* hits1d=new TH1D(Form("HitsPerBin_Mod_%d", id), 
			    Form("Hits per bin Mod %d at %s", id, findFieldName(cfg, id)), 
			    numval, -.5, (float)numval-.5) ;
      hits1d->GetXaxis()->SetTitle("Scan Point");
      
      for(unsigned int k=0;k<numval;k++){
	std::regex re2("Mean");
	std::string histoName = std::regex_replace (name, re2, Form("Occupancy_Point_%03d", k));
	TH2* occHisto=(TH2*)gDirectory->Get(histoName.c_str());
	assert(occHisto);
	mhis->Add(occHisto);
	hits1d->SetBinContent(k+1, occHisto->GetSumOfWeights());
      }
      anfile->cd();
      mhis->Write();
      mhis->SetDirectory(gDirectory);
      hits1d->Write();
      hits1d->SetDirectory(gDirectory);
      delete histo;
      file->cd(); //change back to this directory, because will need to access its histograms later
    }
  }  
}

