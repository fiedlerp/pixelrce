#include "analysis/GdacFastAnalysis.hh"
#include "server/PixScan.hh"
#include "server/ConfigGui.hh"
#include "config/FEI3/Module.hh"
#include "config/FEI4/Module.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TStyle.h"

void GdacFastAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  int repetitions = scan->getRepetitions();
  double targetEff = 0.5;
  TKey* key;
  char subdir[128];
  char name[128];
  char title[128];
  int binsx=0;
  size_t numvals=scan->getLoopVarValues(1).size();
  std::map<int, hdatagdacfast> *histomap=new std::map<int, hdatagdacfast>[numvals];
  double *val=new double[numvals];
  for (size_t i=0;i<numvals;i++){
    sprintf(subdir, "/loop2_0/loop1_%d", i);
    file->cd(subdir); // GDAC steps
    TIter nextkey(gDirectory->GetListOfKeys()); // histos
    while ((key=(TKey*)nextkey())) {
      std::cmatch matches;
      std::regex re("_(\\d+)_Occupancy_Point_000");
      if(std::regex_search(key->GetName(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int id=strtol(match.c_str(),0,10);
	TH2* histo = (TH2*)key->ReadObj();
	histo->SetMinimum(0); histo->SetMaximum(repetitions);
	TH1* gdachisto=(TH1*)gDirectory->Get(Form("GDAC_settings_Mod_%d_it_%d", id, i));
	assert(gdachisto);
	histomap[i][id].occupancy=histo;
	histomap[i][id].gdac=gdachisto;
	binsx=gdachisto->GetNbinsX();
      }
    }
  }
  for(std::map<int, hdatagdacfast>::iterator it=histomap[0].begin();it!=histomap[0].end();it++){
    int id=it->first;
    float pct = 100;
    sprintf(name, "BestOcc_Mod_%d", id);
    sprintf(title, "Best Occupancy Module %d at %s", id, findFieldName(cfg, id));
    TH1F* occBest=new TH1F(name, title, binsx, 0, binsx);
    occBest->SetMinimum(0); occBest->SetMaximum(pct);
    occBest->GetXaxis()->SetTitle("Chip");
    occBest->GetYaxis()->SetTitle("Occupancy[%]");
   
    sprintf(name, "BestGdac_Mod_%d", id);
    sprintf(title, "Best Gdacs Module %d at %s", id, findFieldName(cfg, id));
    TH1F* gdac=new TH1F(name, title, binsx, 0, binsx);
    gdac->GetXaxis()->SetTitle("Chip");
    gdac->GetYaxis()->SetTitle("Gdac");

    PixelConfig* confb=findConfig(cfg, id);

    int ncol, nrow, nchip, maxval, maxstage;
    std::string type=confb->getType();
    if (type=="FEI4A") {
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=256;
      maxstage=120;
    }
    else if (type=="FEI4B") {
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=256;
      maxstage=24;
    }
    else {
      ncol=18;
      nrow=160;
      nchip=16;
      maxval=32;
      maxstage=32;
    }
    int nstages = scan->getMaskStageSteps();
    int npixtotal = nchip*ncol*nrow;
    float npixused=float(nstages)/float(maxstage)*npixtotal;

    int nfe = binsx;
    for (int i = 0; i < nfe; i++){
      double bestval = 10*targetEff; // must be larger than 1 (with possibilty of double hit)
      int index = 0;
      for (size_t k = 0; k < numvals; k++) {
	int histfebins = (histomap[k][id].occupancy->GetNbinsX())/nfe;
	int nbinsy = histomap[k][id].occupancy->GetNbinsY();
	float meanEff = 0.0;
	for (int binx = histfebins*i+1; binx <= histfebins*(i+1); binx++) {
	  for (int biny = 1; biny <= nbinsy; biny++) {
	    meanEff += histomap[k][id].occupancy->GetBinContent(binx, biny) / repetitions;
	  }
	}
	val[k] = meanEff/npixused;
	if (fabs(val[k]-targetEff) < bestval) {
	  index = k;
	  bestval = fabs(val[k]-targetEff);
	}
      }
      occBest->SetBinContent(i+1, pct*val[index]);
      gdac->SetBinContent(i+1, histomap[index][id].gdac->GetBinContent(i+1));
      std::cout<<"Best GDAC for module " << id << " is " << histomap[index][id].gdac->GetBinContent(i+1) << std::endl;
      confb->setGDac(i, histomap[index][id].gdac->GetBinContent(i+1));
    }

    anfile->cd();

    occBest->Write();
    occBest->SetDirectory(gDirectory);
   
    gdac->Write();
    gdac->SetDirectory(gDirectory);

    if(scan->getScanType()==RCE::PixScan::GDAC_FAST_TUNE){
      std::vector<float> varValues=scan->getLoopVarValues(2);
      if(varValues.size()>=1){
	int val=(int)varValues[0];
	for (int chip=0;chip<nchip;chip++){
	  for (int col=0;col<ncol;col++){
	    for (int row=0;row<nrow;row++){
	      confb->setThresholdDac(chip, col, row, val);
	    }
	  }
	}
	
      }else{
	std::cout<<"No outer loop for GDAC tuning. Not setting up TDAC values"<<std::endl;
      }
    }
    writeConfig(anfile,runno);
  }
  if (configUpdate()) writeTopFile(cfg, anfile, runno);
  delete [] histomap;
  delete [] val;
}
