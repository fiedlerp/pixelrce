#include "analysis/T0Analysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TStyle.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TF1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>

namespace{
  const double threshold=0;
}
using namespace RCE;

void T0Analysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  unsigned int numval=scan->getLoopVarValues(0).size();
  gStyle->SetOptFit(10);
  gStyle->SetOptStat(0);
  std::cout<<"T0 analysis"<<std::endl;
  file->cd("loop1_0");
  TIter nextkey(gDirectory->GetListOfKeys());
  TKey *key;
  std::regex re("_(\\d+)_Mean");
  std::regex re2("Mean");
  while ((key=(TKey*)nextkey())) {
    file->cd("loop1_0");
    std::string name(key->GetName());
    std::cmatch matches;
    if(std::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      

      //   std::cout<<"match = "<<match.c_str()<<std::endl;
      int id=strtol(match.c_str(),0,10);

      //     std::cout<<"id = "<<id<<std::endl;
           
      std::string sigmaHistoName = std::regex_replace (name, re2, "Sigma");
    

      TH2* histo = (TH2*)key->ReadObj();
      TH2* sigmaHisto=(TH2*)gDirectory->Get(sigmaHistoName.c_str());
      assert(sigmaHisto);
      TH1D* hits1d[2];

      for (int iScan=0;iScan<2;iScan++){
	hits1d[iScan]=new TH1D(Form("HitsPerBin_%d_Mod_%d", iScan, id), 
			       Form("Hits per bin Mod %d at %s (Scan %d)"
				    , id, findFieldName(cfg, id), iScan), 
			       numval, -.5, (float)numval-.5) ;
	hits1d[iScan]->GetXaxis()->SetTitle("Scan Point");
      }

      fillHit1d(name, hits1d[0], numval);
      file->cd("loop1_1");
      fillHit1d(name, hits1d[1], numval);

      TH2* histo2 = (TH2*)gDirectory->Get(name.c_str());
      TH2* sigmaHisto2 = (TH2*)gDirectory->Get(sigmaHistoName.c_str());



      //Get histogram ranges
      float maxval[2] = {0,0};
      float minval[2] = {1e8, 1e8};
      float vals[4] = {0,0,0,0};

      for (int i=1;i<=histo->GetNbinsX();i++){
	for(int j=1;j<=histo->GetNbinsY();j++){

	  vals[0] = histo->GetBinContent(i, j);
	  vals[1] = histo2->GetBinContent(i, j);
	  vals[2] = sigmaHisto->GetBinContent(i, j);
	  vals[3] = sigmaHisto2->GetBinContent(i, j);

	  for(int kpar = 0; kpar<2; kpar++){
	    if(vals[kpar]>maxval[0]){
	      maxval[0] = vals[kpar];
	    }
	    if(vals[kpar]<minval[0]&&vals[kpar]>0){
	      minval[0] = vals[kpar];
	    }
	  }

	  for(int kpar = 2; kpar<4; kpar++){
	    if(vals[kpar]>maxval[1]){
	      maxval[1] = vals[kpar];
	    }
	    if(vals[kpar]<minval[1]&&vals[kpar]>0){
	      minval[1] = vals[kpar];
	    }   
	  }

	}
      }

      TH1D* t0mean[2];
      TH1D* t0sigma[2];

      int nGoodPixels = 0;
      float sumT0Diffs = 0;

      for(int iScan=0; iScan<2; iScan++){

	t0mean[iScan] =new TH1D(Form("T0_%d_Mod_%d",iScan,id), 
				Form("T0 for each pixel in Mod %d at %s (Scan %d)", 
				     id, findFieldName(cfg, id), iScan), 50, minval[0]-1, maxval[0]+1);
	t0sigma[iScan] =new TH1D(Form("Sigma_%d_Mod_%d",iScan,id), 
				 Form("T0 S-CURVE sigma for each pixel in Mod %d at %s (Scan %d)", 
				      id, findFieldName(cfg, id), iScan), 50, minval[1]-1, maxval[1]+1);
	

	t0mean[iScan]->GetXaxis()->SetTitle("t0 (strobe delay counts)");
	t0sigma[iScan]->GetXaxis()->SetTitle("t0 S-Curve sigma (strobe delay counts)");

      }

      
      


      for (int i=1;i<=histo->GetNbinsX();i++){
	for(int j=1;j<=histo->GetNbinsY();j++){

	  float val=0;

	  if(histo->GetBinContent(i, j)!=0 ){
	    val = histo->GetBinContent(i, j);
	    t0mean[0]->Fill(val);
	    
	    if(histo2->GetBinContent(i, j)!=0 ){
	      nGoodPixels++;
	      float val2 = histo2->GetBinContent(i, j);
	      sumT0Diffs += (val2 - val);
	    }

	  }
	  if(histo2->GetBinContent(i, j)!=0 ){
	    val = histo2->GetBinContent(i, j);
	    t0mean[1]->Fill(val);
	  }  

	  if(sigmaHisto->GetBinContent(i, j)!=0 ){
	    val = sigmaHisto->GetBinContent(i, j);
	    t0sigma[0]->Fill(val);
	  }
	  if(sigmaHisto2->GetBinContent(i, j)!=0 ){
	    val = sigmaHisto2->GetBinContent(i, j);
	    t0sigma[1]->Fill(val);
	  } 

	}

      }
      float avgt0diff = sumT0Diffs/nGoodPixels;

      float t0[2];
      for(int iScan=0; iScan<2; iScan++){

	t0mean[iScan]->Fit("gaus");
	t0[iScan] = t0mean[iScan]->GetFunction("gaus")->GetParameter(1);
	std::cout<<"T0 for module "<<id<<" for scan "<<iScan<<" is "<<t0[iScan]<<std::endl;
	
      }  

      assert(t0[1] > t0[0]);

      float convFactor = 25.6/(avgt0diff);  //one clock tick=25.6 ns
      
      std::cout<<"delay/(strobe-delay count) = "<<convFactor<<" (ns/count)"<<std::endl;

      
      TH1D* t0mean_ns[2];
      TH1D* t0sigma_ns[2];
      TH1D* t0diff_ns;
      

      for(int iScan=0; iScan<2; iScan++){

	t0mean_ns[iScan] = new TH1D(Form("T0_%d_Mod_%d_ns",iScan,id), 
				    Form("T0 (in ns) for each pixel in Mod %d at %s (Scan %d)", 
					 id, findFieldName(cfg, id), iScan), 50, minval[0]-1, maxval[0]+1);
	t0sigma_ns[iScan] = new TH1D(Form("Sigma_%d_Mod_%d_ns",iScan,id), 
				     Form("T0 S-CURVE sigma (in ns) for each pixel in Mod %d at %s (Scan %d)",
					  id, findFieldName(cfg, id), iScan), 50, minval[1]-1, maxval[1]+1);

	t0mean_ns[iScan]->GetXaxis()->SetTitle("t0 (ns)");
	t0sigma_ns[iScan]->GetXaxis()->SetTitle("t0 S-Curve sigma (ns)");
		
      }

      t0diff_ns =new TH1D(Form("T0_Diff_%d",id), 
			  Form("T0 difference (in ns) for each pixel in Mod %d at %s", 
			       id, findFieldName(cfg, id)), 50, 20., 30.);
      t0diff_ns->GetXaxis()->SetTitle("t0_2 - t0_1 (ns)");


      for (int i=1;i<=histo->GetNbinsX();i++){
	for(int j=1;j<=histo->GetNbinsY();j++){

	  float val=0;

	  if(histo->GetBinContent(i, j)!=0 ){
	    val = histo->GetBinContent(i, j)*convFactor;
	    t0mean_ns[0]->Fill(val);

	    if(histo2->GetBinContent(i, j)!=0){
	      float val2 = histo2->GetBinContent(i, j)*convFactor;
	      t0diff_ns->Fill(val2 - val);
	    }

	  }
	  if(histo2->GetBinContent(i, j)!=0 ){
	    val = histo2->GetBinContent(i, j)*convFactor;
	    t0mean_ns[1]->Fill(val);
	  }  

	  if(sigmaHisto->GetBinContent(i, j)!=0 ){
	    val = sigmaHisto->GetBinContent(i, j)*convFactor;
	    t0sigma_ns[0]->Fill(val);
	  }
	  if(sigmaHisto2->GetBinContent(i, j)!=0 ){
	    val = sigmaHisto2->GetBinContent(i, j)*convFactor;
	    t0sigma_ns[1]->Fill(val);
	  } 

	}

      }

   
      float t0_ns[2];

      for(int iScan=0; iScan<2; iScan++){
	t0mean_ns[iScan]->Fit("gaus");
	t0_ns[iScan] = t0mean_ns[iScan]->GetFunction("gaus")->GetParameter(1);
      }  

      float strobeDel = (t0_ns[1] + 5.0)/convFactor;
      int strobeDelInt = (int)(strobeDel+0.5);
      std::cout<<"t0 = "<<t0_ns[1]<<" ; strobe delay = "<<strobeDel<<" = "<<strobeDelInt<<std::endl;
      
      int trigLat = scan->getLoopVarValues(1).at(1);

      /*
      if(feglobalA){
	feglobalA->PlsrDelay=strobeDelInt;
	feglobalA->TrigLat=trigLat;
	writeFEI4Config(anfile, runno);
      }
      if(feglobalB){
	feglobalB->PlsrDelay=strobeDelInt;
	feglobalB->TrigLat=trigLat;
	writeFEI4Config(anfile, runno);
      }
      */
      

      delete histo;
      anfile->cd();

      for(int iScan=0; iScan<2; iScan++){

	t0mean[iScan]->Write();
	t0mean[iScan]->SetDirectory(gDirectory);	
	t0sigma[iScan]->Write();
	t0sigma[iScan]->SetDirectory(gDirectory);

	t0mean_ns[iScan]->Write();
	t0mean_ns[iScan]->SetDirectory(gDirectory);
	t0sigma_ns[iScan]->Write();
	t0sigma_ns[iScan]->SetDirectory(gDirectory);

	hits1d[iScan]->Write();
	hits1d[iScan]->SetDirectory(gDirectory);

      }
      t0diff_ns->Write();
      t0diff_ns->SetDirectory(gDirectory);
      

    }
  }  
  if(configUpdate())writeTopFile(cfg, anfile, runno);
}

void T0Analysis::fillHit1d(std::string &name, TH1* hits1d, unsigned numval){
for(unsigned int k=0;k<numval;k++){
  std::regex re2("Mean");
  std::string histoName = std::regex_replace (name, re2, Form("Occupancy_Point_%03d", k));
  TH2* occHisto=(TH2*)gDirectory->Get(histoName.c_str());
  if(occHisto==0){
    std::cout<<"No Occupancy histograms found. Won't fill 1-d hit histo."<<std::endl;
    break;
  }
  hits1d->SetBinContent(k+1, occHisto->GetSumOfWeights());
 }
}
