#include "analysis/Fdac_Analysis.hh"
#include "config/FEI3/Frontend.hh"
#include <regex>

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TKey.h>
#include <TStyle.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

void FdacAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  gStyle->SetOptFit(111);
  printf("Begin FDAC Analysis\n");
  float target = float(scan->getTotTargetValue());
  printf("Target Tot Value: %2.1f\n",target);
  std::vector<float> loopVar=scan->getLoopVarValues(0);
  size_t numvals=loopVar.size();
  //TH1F* iffhis = new TH1F("iff_v_module","Best IF Setting",128,0,127);
  //iffhis->GetXaxis()->SetTitle("Module");
  //iffhis->GetYaxis()->SetTitle("IF Setting");
  std::map<int, odata> *histomap=new std::map<int, odata>[numvals];
  TKey *key;
  int binsx=0, binsy=0;
  for (size_t i=0;i<numvals;i++){
    TIter nextkey(gDirectory->GetListOfKeys()); // FDAC settings
    while ((key=(TKey*)nextkey())) {
      std::string name(key->GetName());
      std::regex re(Form("_(\\d+)_Occupancy_Point_%03d", i));
      std::cmatch matches;
      if(std::regex_search(name.c_str(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int lnm=strtol(match.c_str(),0,10);
	histomap[i][lnm].occ = (TH2*)key->ReadObj();
	std::regex re2("Occupancy");
	std::string totHistoName = std::regex_replace (name, re2, "ToT");
	histomap[i][lnm].tot=(TH2*)gDirectory->Get(totHistoName.c_str());
	std::string tot2HistoName = std::regex_replace (name, re2, "ToT2");
	histomap[i][lnm].tot2=(TH2*)gDirectory->Get(tot2HistoName.c_str());
	assert(histomap[i][lnm].tot2);
	binsx=histomap[i][lnm].tot2->GetNbinsX();
	binsy=histomap[i][lnm].tot2->GetNbinsY();
      }
    }
  }
  for(std::map<int, odata>::iterator it=histomap[0].begin();it!=histomap[0].end();it++){ 
    int id=it->first;
    int max=16;
    if(findFEType(cfg, id)=="FEI3")max=64;
    PixelConfig *confb=findConfig(cfg, id);
    TH2D* fdachis = new TH2D(Form("FDAC_Mod_%i", id),
			     Form("Best FDAC Settings Mod %d at %s", id, findFieldName(cfg, id)),
			     binsx,0,binsx,binsy,0,binsy);
    fdachis->GetXaxis()->SetTitle("Column");
    fdachis->GetYaxis()->SetTitle("Row");
    TH2D* ahis = new TH2D(Form("ToT_MinDiff_Mod_%i", id),
			  Form("min(ToT - Target ToT) Mod %d at %s", id, findFieldName(cfg, id)),
			  binsx,0,binsx,binsy,0,binsy);
    ahis->GetXaxis()->SetTitle("Column");
    ahis->GetYaxis()->SetTitle("Row");
    TH2D* badpix = new TH2D(Form("BadPixels_Mod_%i", id), 
			    Form("Bad Pixel Map Mod %d at %s", id, findFieldName(cfg, id)),
			    binsx,0,binsx,binsy,0,binsy);
    badpix->GetXaxis()->SetTitle("Column");
    badpix->GetYaxis()->SetTitle("Row");
    TH1D* channel = new TH1D(Form("ToT_MinDiff_Channel_Mod_%i", id),
			     Form("min(ToT - Target ToT) Mod %d at %s", id, findFieldName(cfg, id)),
			     binsx*binsy,0,binsx*binsy);
    channel->GetXaxis()->SetTitle("Channel");
    channel->SetOption("p9");
    TH1D* dist = new TH1D(Form("ToT_MinDiff_Dist_Mod_%i", id),
			  Form("ToT - Target Mod %d at %s", id, findFieldName(cfg, id)),
			  100, -2, 2);
    dist->GetXaxis()->SetTitle("ToT");
    TH1D* totdist;
    if(findFEType(cfg, id)=="FEI3"){
      totdist = new TH1D(Form("ToT_Dist_Mod_%i", id),
			 Form("ToT Mod %d at %s", id, findFieldName(cfg, id)),
			 641, -.05, 64.05);
    }else{
      totdist = new TH1D(Form("ToT_Dist_Mod_%i", id),
			 Form("ToT Mod %d at %s", id, findFieldName(cfg, id)),
			 161, -.05, 16.05);
    }
    totdist->GetXaxis()->SetTitle("ToT");
    TH1F* tot1d=new TH1F(Form("tot1d_Mod_%d", id), 
			 Form("ToT Module %d at %s", id, findFieldName(cfg, id)), 
			 binsx*binsy, 0, (float)binsx*binsy); 
    tot1d->GetXaxis()->SetTitle("Channel");
    tot1d->SetMinimum(0);
    tot1d->SetMaximum(max);
    tot1d->SetOption("p9");
    TH2F* tot2d=new TH2F(Form("tot2d_Mod_%d", id), 
			 Form("ToT Module %d at %s", id, findFieldName(cfg, id)), 
			 binsx, 0, binsx, binsy, 0, binsy);
    tot2d->GetXaxis()->SetTitle("Column");
    tot2d->GetYaxis()->SetTitle("Row");
    tot2d->SetMinimum(0);
    tot2d->SetMaximum(max);
    int chan;
    for(int c = 1; c <= binsx; c++){
      for(int r = 1; r <= binsy ; r++){
	ahis->SetBinContent(c,r,-1.0);
	chan = r + (c-1)*binsy;
	channel->SetBinContent(chan,-1.0);
      }
    }
    for(int j = 1; j <= fdachis->GetNbinsX(); j++){
      for(int k = 1; k <= fdachis->GetNbinsY(); k++){
	int value = 0;
	float min = 999999.9;
	float diff= 999999.9;
	std::vector<float> vals;
	for(unsigned i = 0; i < numvals; i++){
	  //printf("Point %3i\n",i);
	  TH2* occ_histo=histomap[i][id].occ;
	  TH2* tot_histo=histomap[i][id].tot;
	  float nhits = occ_histo->GetBinContent(j,k);
	  float tot=0;
	  if(nhits != 0){
	    tot = float(tot_histo->GetBinContent(j,k)) / nhits;
	    if(fabs(tot - target) < min){
	      min = fabs(tot - target);
	      diff = (tot-target);
	      value = i;
	    }
	  }
	  vals.push_back(tot);
	}
	// interpolate
	int intval=int(loopVar[value]+.1);
	/*
	//below found value
	if(value!=0 && vals[value-1]!=vals[value] && loopVar[value]!=loopVar[value-1]){
	  float m=(vals[value]-vals[value-1])/(loopVar[value]-loopVar[value-1]);
	  float n=vals[value]-m*loopVar[value];
	  for (int i=int(loopVar[value-1]+1.1);i<int(loopVar[value]+0.1);i++){
	    if(fabs(m*i+n-target)<min){
	      min=fabs(m*i+n-target);
	      diff=m*i+n-target;
	      intval=i;
	    }
	  }
	}
	//above found value
	if((unsigned)value!=numvals-1 && vals[value+1]!=vals[value] && loopVar[value]!=loopVar[value+1]){
	  float m=(vals[value]-vals[value+1])/(loopVar[value]-loopVar[value+1]);
	  float n=vals[value]-m*loopVar[value];
	  for (int i=int(loopVar[value]+1.1);i<int(loopVar[value+1]+0.1);i++){
	    if(fabs(m*i+n-target)<min){
	      min=fabs(m*i+n-target);
	      diff=m*i+n-target;
	      intval=i;
	    }
	  }
	}
	*/
	if(min < 1000){
	  ahis->SetBinContent(j,k,min);
	  fdachis->SetBinContent(j,k,intval);
	  int chip=0;
	  int col=j-1;
	  int row=k-1;
	  if(confb->getType()=="FEI3"){
	    chip=(j-1)/FEI3::Frontend::N_COLS;
	    col=(j-1)%FEI3::Frontend::N_COLS;
	    row=k-1;
	  }
	  confb->setFeedbackDac(chip, col, row, intval);
	  chan = k + (j-1)*binsy;
	  if(min<max)channel->SetBinContent(chan,min);
	  dist->Fill(diff);
	  totdist->Fill(diff+target);
	  tot1d->SetBinContent(chan, diff+target);
	  tot2d->SetBinContent(j, k, diff+target);
	  if(min>1)badpix->SetBinContent(j,k,1);
	}
	else badpix->SetBinContent(j,k,1);
      }
    }
    dist->Fit("gaus");
    anfile->cd();
    fdachis->Write();
    fdachis->SetDirectory(gDirectory);
    ahis->Write();
    ahis->SetDirectory(gDirectory);
    badpix->Write();
    badpix->SetDirectory(gDirectory);
    channel->Write();
    channel->SetDirectory(gDirectory);
    dist->Write();
    dist->SetDirectory(gDirectory);
    totdist->Write();
    totdist->SetDirectory(gDirectory);
    tot1d->Write();
    tot1d->SetDirectory(gDirectory);
    tot2d->Write();
    tot2d->SetDirectory(gDirectory);
    m_fw->writeDacFile(Form("%sFdacs_Mod_%d", m_fw->getPath(anfile).c_str(), id), fdachis);
    writeConfig(anfile, runno);
  }
  if(configUpdate())writeTopFile(cfg, anfile, runno);
  delete [] histomap;
  printf("Done Analysis\n");
}

