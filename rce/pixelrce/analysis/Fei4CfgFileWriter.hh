#ifndef FEI4CFGFILEWRITER_HH
#define FEI4CFGFILEWRITER_HH

#include "analysis/CfgFileWriter.hh"

class Fei4CfgFileWriter: public CfgFileWriter{
public:
  Fei4CfgFileWriter(){}
  ~Fei4CfgFileWriter(){}
  void writeDacFile(const char* filename, TH2* his);
  void writeMaskFile(const char* filename, TH2* his);
};

#endif 
