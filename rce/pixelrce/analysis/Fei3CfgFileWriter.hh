#ifndef FEI3CFGFILEWRITER_HH
#define FEI3CFGFILEWRITER_HH

#include "analysis/CfgFileWriter.hh"

class Fei3CfgFileWriter: public CfgFileWriter{
public:
  Fei3CfgFileWriter(){}
  ~Fei3CfgFileWriter(){}
  void writeDacFile(const char* filename, TH2* his);
  void writeMaskFile(const char* filename, TH2* his);
};

#endif 
