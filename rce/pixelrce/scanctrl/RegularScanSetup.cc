#include "scanctrl/RegularScanSetup.hh"
#include "scanctrl/ScanLoop.hh"
#include <boost/property_tree/ptree.hpp>
#include "scanctrl/ActionFactory.hh"
#include "scanctrl/LoopAction.hh"
#include "scanctrl/EndOfLoopAction.hh"

#include <stdio.h>
#include <iostream>

int RegularScanSetup::setupLoops( NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af){
  int retval=0; 
  loop.clear();
  try{ //catch bad scan option parameters
    // Start with the trigger loop 
    int nEvents = scanOptions->get<int>("trigOpt.nEvents");
    ScanLoop* triggerloop=new ScanLoop("Triggerloop",nEvents);
    //    LoopAction* pause=af->createLoopAction("PAUSE");
    LoopAction* sendtrigger=af->createLoopAction("SEND_TRIGGER",scanOptions);
        //triggerloop->addLoopAction(pause);
    triggerloop->addLoopAction(sendtrigger);
    // loop is the nested loop object
    loop.addNewInnermostLoop(triggerloop);
    
    // The position of the mask stage loop is not 
    // explicitely specified by PixLib. Implicitely it's the innermost loop
    // if there is no other scan variable (nLoops==0) and next-to-innermost otherwise. 
    int nLoops = scanOptions->get<int>("nLoops");
    if(nLoops>0){
      //parameter loop is next
      int nPoints = scanOptions->get<int>("scanLoop_0.nPoints");
      ScanLoop *firstparloop=new ScanLoop("scanLoop_0",nPoints);
      //LoopAction* disable0=af->createLoopAction("DISABLE_TRIGGER");
      LoopAction* setuppar0=af->createLoopAction("SETUP_PARAM",&scanOptions->get_child("scanLoop_0"));
      LoopAction* chgbin0=af->createLoopAction("CHANGE_BIN");
      //LoopAction* linkmask0=af->createLoopAction("SETUP_CHANNELMASK");
      //      LoopAction* enable0=af->createLoopAction("ENABLE_TRIGGER");
      // End of loop action is deferred to mask stage
      //   firstparloop->addLoopAction(disable0);
      firstparloop->addLoopAction(setuppar0);
      firstparloop->addLoopAction(chgbin0);
      //firstparloop->addLoopAction(linkmask0);
    //  firstparloop->addLoopAction(enable0);
      loop.addNewOutermostLoop(firstparloop);
    }
    // now it's time for the mask stage loop
    int nMaskStages= scanOptions->get<int>("nMaskStages");
    ScanLoop* maskStageLoop = new ScanLoop("MaskStageLoop",nMaskStages);
    //LoopAction* disablem=af->createLoopAction("DISABLE_TRIGGER");
    LoopAction* maskaction=af->createLoopAction("SETUP_MASK", scanOptions);
    //LoopAction* linkmaskm=af->createLoopAction("SETUP_CHANNELMASK");
    //LoopAction* enablem=af->createLoopAction("ENABLE_TRIGGER");
    //maskStageLoop->addLoopAction(disablem);
    maskStageLoop->addLoopAction(maskaction);
    //maskStageLoop->addLoopAction(linkmaskm);
    //maskStageLoop->addLoopAction(enablem);
    // The end of loop action from the parameter loop above if there is one
    /* add calculation of ToT/BCID mean and sigma to end-of-loop sction */
    if(scanOptions->get<std::string>("DataProc")=="BCID" ||  scanOptions->get<std::string>("DataProc")=="TOT") {
      std::cout << "Add Mean/Sgima calculation as end-of-loop action" << std::endl;
      EndOfLoopAction* action=af->createEndOfLoopAction("CALCULATE_MEAN_SIGMA","CALCULATE_MEAN_SIGMA");
      if(action) maskStageLoop->addEndOfLoopAction(action);
    }


    if(nLoops>0){
      std::string action=scanOptions->get<std::string>("scanLoop_0.endofLoopAction.Action");
      std::string fitfunc=scanOptions->get<std::string>("scanLoop_0.endofLoopAction.fitFunction");
      EndOfLoopAction* fitaction=af->createEndOfLoopAction(action,fitfunc);
      /* protect against no loop action returned */
      if(fitaction)maskStageLoop->addEndOfLoopAction(fitaction);
    }
    loop.addNewOutermostLoop(maskStageLoop);
    // now add any additional loops
    char desc[128];
    for (int i=1;i<nLoops;i++){
      sprintf(desc,"scanLoop_%d",i);
      int nPoints = scanOptions->get<int>(std::string(desc)+".nPoints");
      ScanLoop *parloop=new ScanLoop(desc,nPoints);
      //  LoopAction* disable=af->createLoopAction("DISABLE_TRIGGER");
      LoopAction* setuppar=af->createLoopAction("SETUP_PARAM",&scanOptions->get_child(desc));
      LoopAction* chgbin=af->createLoopAction("CHANGE_BIN");
            //LoopAction* enable=af->createLoopAction("ENABLE_TRIGGER");
      //LoopAction* linkmask=af->createLoopAction("SETUP_CHANNELMASK");
      // End of loop action is deferred to mask stage
    //  parloop->addLoopAction(disable);
      parloop->addLoopAction(setuppar);
      parloop->addLoopAction(chgbin);
      //maskStageLoop->addLoopAction(linkmask);
    //  parloop->addLoopAction(enable);
      std::string action=scanOptions->get<std::string>(std::string(desc)+".endofLoopAction.Action");
      std::string fitfunc=scanOptions->get<std::string>(std::string(desc)+".endofLoopAction.fitFunction");
      EndOfLoopAction* fitaction=af->createEndOfLoopAction(action,fitfunc);
      if(fitaction)
	parloop->addEndOfLoopAction(fitaction);
      loop.addNewOutermostLoop(parloop);
    } 
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  //  loop.print();
  return retval;
}
