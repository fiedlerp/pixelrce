#ifndef ABSSCAN_HH
#define ABSSCAN_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include <iostream>
#include "scanctrl/NestedLoop.hh"
#include "config/ConfigIF.hh"

class AbsDataProcessor;

class AbsScan{

public:
  AbsScan(ConfigIF* cif): m_configIF(cif){};
  virtual ~AbsScan(){}
  virtual int configureScan(boost::property_tree::ptree* scanOptions)=0;
  virtual int pause()=0;
  virtual int resume()=0;
  virtual int abort()=0;
  virtual int startScan()=0;
  //virtual std::vector<Histo*> getHistos(const char* type, int module)=0;

protected:
  ConfigIF* m_configIF;
  //AbsDataProcessor* m_dataprocessor;
  NestedLoop m_loops;
};

#endif
