#ifndef COSMICMONITORINGGUIEMBDED_HH
#define COSMICMONITORINGGUIEMBDED_HH

#include "ConfigGui.hh"
#include "TGMdiMainFrame.h"
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include "TFile.h"
#include "TH2F.h"
#include "TGraph.h"
#include <TTree.h>
#include <string>
#include <map>
#include "TMutex.h"

#define MAX_RCES 8
#define MAXHIT 1000


class TGListTree;
class TGListTreeItem;
class TH1;
class TRootEmbeddedCanvas;
class IPCGuiCallback;
namespace eudaq {
class DetectorEvent;
}

struct EventInfo{
  ULong64_t timestamp;
  ULong64_t framenumber;
  Int_t triggeroffset;
  Int_t triggerinfo;
  Bool_t invalid;
  ULong64_t triggerTime;
};

class CosmicMonitoringGuiEmbedded: public TGVerticalFrame {
public:
  CosmicMonitoringGuiEmbedded(const char* filename, int nevt, const TGWindow *p,UInt_t w,UInt_t h,UInt_t options);
  CosmicMonitoringGuiEmbedded(const TGWindow *p,UInt_t w,UInt_t h,UInt_t options, const std::vector<ConfigGui*> config);
  virtual ~CosmicMonitoringGuiEmbedded();
  void setup(bool online, bool writeRootFile, const char* rootfile, bool writeTemps, const char* tempfile);
  void clearTree();
  void updateTree();
  void updateDisplay();
  void updateHisto();
  void updateGraph();
  void saveHistos(int runnum, const char* datadir);
  void saveScreenshot(int runnum, const char* datadir);
  void displayHisto(TGListTreeItem* item, int b);
  unsigned nextcurrent(unsigned char* p);
  unsigned getWord(unsigned char *p, int word);
  bool setupChannelsFromFile(const char* filename);
  bool addEvent(const eudaq::DetectorEvent* dev, bool refresh = false);
  bool readFile(const char* filename, int nevt);
  void addRce(unsigned int rceNum);
  void printModules(); 
  bool createLink(unsigned outlink, unsigned rceNum);
  void rebinTimestamps(double timestamp);
  double getHitsPerEvent(){return m_hitsperevent;}
  void closeRootFile();
  void closeTempFile();

private:

  void fillHistoTree();
  void init();
  void setupRootFile(const char* rootfile);

  enum Filemenu {LOAD, SAVE, QUIT};
  TGTextButton* *m_quit;
  TGListTree* m_tree;
  TH1 *m_histo;
  TGraph* m_graph;
  const std::vector<ConfigGui*> m_config;
  const char* m_filename; 
  TRootEmbeddedCanvas *m_canvas;
  std::string m_dir;
  bool m_delhisto;
  TGCanvas *m_tgc;

  //Currently setting the limit at 4 RCE's (with 8 modules each)
  TH2F* m_occ[ConfigGui::MAX_MODULES];
  TH2F* m_corxx[ConfigGui::MAX_MODULES-1];  
  TH2F* m_corxy[ConfigGui::MAX_MODULES-1];  
  TH2F* m_coryy[ConfigGui::MAX_MODULES-1];  
  TH2F* m_coryx[ConfigGui::MAX_MODULES-1];  
  TH1F* m_toth[ConfigGui::MAX_MODULES];
  TH1F* m_bx[ConfigGui::MAX_MODULES];
  TH1F* m_tdc[MAX_RCES];
  TH1F* m_hitbus[MAX_RCES][2][3];
  TH1F* m_timestamp[ConfigGui::MAX_MODULES];
  TH1F* m_projx[ConfigGui::MAX_MODULES];
  TH1F* m_projy[ConfigGui::MAX_MODULES];
  TH1F **m_afptdc[ConfigGui::MAX_MODULES];
  int m_corrindex[ConfigGui::MAX_MODULES];
  TH1F* m_tempsec[ConfigGui::MAX_MODULES];
  TGraph* m_tempmin[ConfigGui::MAX_MODULES];
  std::vector<TObject*> m_histlist;
  
  unsigned int m_indx;
  double m_noiserate;
  int m_noiseUpdateTime;

  std::map<int,int> m_linkmap;
  enum constants{MAX_RCE_NUM=100, MAX_LINKS=32};
  int **m_linkToIndex;
  std::vector<unsigned> m_outlinks;
  std::vector<unsigned> m_rceNums;
  std::vector<int> m_fetype;
  std::vector<unsigned int> m_rces;
  int m_nMods;
  int m_nRces;
  int m_nev;
  int m_nhit;
  int ntrg;
  int highesttot;
  double m_timeStampLimit;
  unsigned int m_ww, m_wh, m_www, m_hhh;
  int m_lastcount[ConfigGui::MAX_MODULES];
  int m_lasttime[ConfigGui::MAX_MODULES];
  double m_hitsperevent;
  int m_nScreenshots;
  TFile *m_evfile; 
  std::ofstream *m_tempfile;
  TTree **m_pltree, *m_evtree;
  Int_t *m_nhits; 
  Int_t **m_pixx, **m_pixy, **m_value, **m_timing, **m_hitincluster;
  Double_t **m_posx, **m_posy, **m_posz;
  EventInfo m_eventinfo;
  bool m_writeRootFile, m_logtemps;
  unsigned m_starttime_sec;
  TMutex m_graphmutex;
  
ClassDef (CosmicMonitoringGuiEmbedded,0)
};
#endif
