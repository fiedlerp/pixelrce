
#include "server/CosmicGui.hh"
#include "server/RCFController.hh"
#include "util/HistoManager.hh"
#include "TApplication.h"
#include <TROOT.h>
#include <TStyle.h>
#include <stdlib.h>
#include <getopt.h>

void print_usage() {
  std::cout << "usage CosmicGui" << std::endl;
  std::cout << "  --start,-s                 : Start run automatically when GUI comes up" <<std::endl;
  std::cout << "  --interface,-i <IP>        : IP of host the RCE connects to"<<std::endl;
  std::cout << "  --help,-h" <<std::endl;
  exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
  //
  bool start=false;
  std::string orbhost;
  int opt=0;
  static struct option long_options[] = {
    {"help",      no_argument,        0,  'h' },
    {"interface", required_argument,  0,  'i' },
    {"start",     no_argument,        0,  'd' },
    {0,           0,                  0,  0   }
  };
  int long_index =0;
  while ((opt = getopt_long(argc, argv,"i:sh", 
			    long_options, &long_index )) != -1) {
    switch (opt) {
    case 's' : start=true;
      break;
    case 'i' : orbhost=optarg;  
      break;
    case 'h':      
    default: print_usage(); 
    }
  }
  if(orbhost==""){
    print_usage();
  }

    //    ("interface,i", boost::program_options::value<std::string>(&orbhost)->required(), "IP address of the host interface that the RCE is connected to")
    //   ("start,s", boost::program_options::value<bool>(&start)->default_value(false), "Start run when GUI comes up")
    //    ("help,h", "produce help message");

  setenv("ORBHOST", orbhost.c_str(), 1);
  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  AbsController &acontroller(controller);
  new HistoManager(0);
  
  gROOT->SetStyle("Plain");
  TApplication theapp("app",&argc,argv);
  new CosmicGui(acontroller, start, gClient->GetRoot(),800, 735);
  theapp.Run();
  return 0;
}


