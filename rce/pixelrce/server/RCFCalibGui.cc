#include "server/CalibGui.hh"
#include "server/RCFController.hh"
#include "server/RCFHistoController.hh"
#include <boost/program_options.hpp>
#include <TROOT.h>
#include <TStyle.h>
#include "TApplication.h"
#include <iostream>

int main(int argc, char **argv){
  RCF::RcfInitDeinit rcfInit;
  AbsController* controller=new RCFController;
  AbsHistoController* hcontroller;
  try{
    hcontroller=new RCFHistoController;
  }catch(RCF::Exception ex){
    std::cout<<"***** Another GUI is already running. Exiting."<<std::endl;
    exit(0);
  }
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);

  TApplication theapp("app",&argc,argv);
  new CalibGui(controller, hcontroller, gClient->GetRoot(),800,735);
  theapp.Run();
  return 0;
}

