#include "server/GlobalConfigGui.hh"
#include "config/FEI4/FEI4AConfig.hh"
#include "config/FEI4/FEI4BConfig.hh"
#include "config/FEI3/ModuleConfig.hh"
#include "config/hitbus/HitbusConfig.hh"
#include "config/afp-hptdc/AFPHPTDCConfig.hh"
#include "server/ConfigGui.hh"
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <iostream>
#include <regex>
#include <sys/stat.h> 
#include <sstream>
#ifdef SQLDB
#include <mysql.h>
#endif

GlobalConfigGui::GlobalConfigGui(ConfigGui* cg[], const char* dffile, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options):
  TGVerticalFrame(p,w,h,options), m_config(cg), m_defaultfile(dffile){
  
  m_name=new TGLabel(this,"Global Module Configuration");
  AddFrame(m_name,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 10, 0));
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_name->SetTextFont(labelfont);
  
  TGHorizontalFrame *conf0 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf0,new TGLayoutHints(kLHintsExpandX ));
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-14-*-*-*-*-*-iso8859-1");
  m_update=new TGTextButton(conf0,"Update");
  m_update->SetFont(labelfont);
  m_update->SetMargins(5,10,5,5);
  m_update->Connect("Clicked()", "GlobalConfigGui", this,"update()");
  conf0->AddFrame(m_update,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  TGTextButton *sd=new TGTextButton(conf0,"Save");
  sd->Connect("Clicked()", "GlobalConfigGui", this,"saveDefault()");
  sd->SetFont(labelfont);
  sd->SetMargins(5,10,5,5);
  conf0->AddFrame(sd,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_save=new TGTextButton(conf0,"Save As");
  m_save->Connect("Clicked()", "GlobalConfigGui", this,"save()");
  m_save->SetFont(labelfont);
  m_save->SetMargins(5,10,5,5);
  conf0->AddFrame(m_save,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_load=new TGTextButton(conf0,"Load");
  m_load->Connect("Clicked()", "GlobalConfigGui", this,"load()");
  m_load->SetFont(labelfont);
  m_load->SetMargins(5,10,5,5);
  conf0->AddFrame(m_load,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  TGLabel *masklabel=new TGLabel(conf0,"Config Name:");
  FontStruct_t maskfont= gClient->GetFontByName("-adobe-helvetica-bold-r-*-*-12-*-*-*-*-*-iso8859-1");
  masklabel->SetTextFont(maskfont);
  conf0->AddFrame(masklabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_name=new TGLabel(conf0,"                                  ");
  conf0->AddFrame(m_name,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  masklabel=new TGLabel(conf0,"Key:");
  masklabel->SetTextFont(maskfont);
  conf0->AddFrame(masklabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 2, 0));
  m_key=new TGLabel(conf0,"           ");
  conf0->AddFrame(m_key,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));

  // Config directory

  FontStruct_t clabelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  FontStruct_t cmaskfont= gClient->GetFontByName("-adobe-helvetica-bold-r-*-*-12-*-*-*-*-*-iso8859-1");
  m_conf1 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(m_conf1,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *ddlabel=new TGLabel(m_conf1,"Config Root Dir:");
  m_conf1->AddFrame(ddlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 10, 0));
  ddlabel->SetTextFont(clabelfont);
  m_rootdirl=new TGLabel(m_conf1, " ");
  m_rootdirl->SetTextFont(cmaskfont);
  m_conf1->AddFrame(m_rootdirl,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 10, 0));
  TGTextButton *lddd=new TGTextButton(m_conf1,"Browse");
  lddd->Connect("Clicked()", "GlobalConfigGui", this,"guiSetRootDir()");
  lddd->SetFont(clabelfont);
  lddd->SetMargins(10,15,3,3);
  m_conf1->AddFrame(lddd,new TGLayoutHints(kLHintsCenterY | kLHintsRight ,2,2,5,5));
  TGTextButton *absp=new TGTextButton(m_conf1,"Abs Path");
  absp->Connect("Clicked()", "GlobalConfigGui", this,"setAbsPath()");
  absp->SetFont(clabelfont);
  absp->SetMargins(10,15,3,3);
  m_conf1->AddFrame(absp,new TGLayoutHints(kLHintsCenterY | kLHintsRight ,2,2,5,5));

  // Data directory

  m_conf2 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(m_conf2,new TGLayoutHints(kLHintsExpandX ));
  ddlabel=new TGLabel(m_conf2,"Data Dir:");
  m_conf2->AddFrame(ddlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 10, 0));
  ddlabel->SetTextFont(clabelfont);
  m_datadir=new TGLabel(m_conf2, " ");
  m_datadir->SetTextFont(cmaskfont);
  m_conf2->AddFrame(m_datadir,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 10, 0));
  lddd=new TGTextButton(m_conf2,"Browse");
  lddd->Connect("Clicked()", "GlobalConfigGui", this,"guiSetDataDir()");
  lddd->SetFont(clabelfont);
  lddd->SetMargins(10,15,3, 3);
  m_conf2->AddFrame(lddd,new TGLayoutHints(kLHintsCenterY | kLHintsRight ,2,2,5,5));

  // Run Number
  TGHorizontalFrame *datapanel2 = new TGHorizontalFrame(this, 2, 2, kSunkenFrame);
  AddFrame(datapanel2,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *runlabel=new TGLabel(datapanel2,"Run Number:");
  runlabel->SetTextFont(clabelfont);
  datapanel2->AddFrame(runlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 10, 0));
  FontStruct_t labelfont2;
  labelfont2 = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-24-*-*-*-*-*-iso8859-1");
  m_runno=new TGNumberEntry(datapanel2,0,5,-1,TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative);
  m_runno->GetNumberEntry()->SetFont(labelfont2);
  m_runno->SetHeight(30);
  m_runno->SetWidth(140);
  //reset RunNum if SQLDB
#ifdef SQLDB
  m_runno->SetIntNumber(maxRunNum());
  m_runno->SetState(false);
#endif 
  //  m_name->Connect("TextChanged(const char*)", "CalibGui", this,"setName(const char*)");
  datapanel2->AddFrame(m_runno,new TGLayoutHints(kLHintsCenterY |kLHintsCenterX, 5, 5, 5, 5)); 

  gClient->GetColorByName("red", m_red);
  char* cfgdir=getenv("CALIBGUI_DEFAULT");
  if(cfgdir==0) m_homedir=std::string(getenv("HOME"));
  else m_homedir=std::string(cfgdir);

}


GlobalConfigGui::~GlobalConfigGui(){
  Cleanup();
}

void GlobalConfigGui::guiSetDataDir(){
  std::string dirs=setDir();
  if(dirs!="")setDataDir(dirs.c_str());
}

void GlobalConfigGui::setDataDir(const char* dd){
  m_datadir->SetText(dd);
  m_conf2->Layout();
  m_ddir=dd;
}

void GlobalConfigGui::updateText(TGLabel* label, const char* newtext){
  //unsigned len=strlen(label->GetText()->GetString());
  label->SetText(newtext);
  //if (strlen(newtext)>len)Layout();
  Layout();
}
void GlobalConfigGui::rceDoesNotExist(int rce){
    new TGMsgBox(gClient->GetRoot(), 0, "Attention", 
		 Form("RCE %d specified in config does not exist.", rce), 
		 kMBIconExclamation, kMBOk); 
}
  

void GlobalConfigGui::enableControls(bool on){
  #ifndef SQLDB
  m_runno->SetState(on);
  #endif
  m_save->SetEnabled(on);
  m_load->SetEnabled(on);
  m_update->SetEnabled(on);
}
  
void GlobalConfigGui::print(std::ostream &os){
}

void GlobalConfigGui::updateAvailable(bool on){
  if(on)m_update->SetTextColor(m_red);
  else m_update->SetTextColor(0);
  m_upd=on;
  Layout();
}

void GlobalConfigGui::save(){
  const char *filetypes[] = { "Config files",     "*.cfg",
			      "JSON Config files", "*.json",
                              "All files",     "*",
			      0,               0 };
  bool sth=false;
  for(int i=0;i<ConfigGui::MAX_MODULES;i++)if(m_config[i]->isIncluded())sth=true;
  if(sth==false){
    std::cout<<"Nothing to save."<<std::endl;
    return;
  }
  TGFileInfo fileinfo;
  if(m_path!="")
    fileinfo.fIniDir=StrDup(m_path.c_str());
  else
    fileinfo.fIniDir=StrDup(m_rootdir.c_str());
  fileinfo.fFileTypes = filetypes;
  fileinfo.fFilename=0;
  char newconfigname[512];
  std::string oldname=m_name->GetText()->Data();
  if(oldname!="                                  " && oldname!="Unknown"){
    sprintf(newconfigname, "%s__%d.cfg", oldname.c_str(),getRunNumber());
    fileinfo.fFilename=StrDup(newconfigname);
  }
  new TGFileDialog(gClient->GetRoot(), 0, kFDSave, &fileinfo);
  if(!fileinfo.fFilename){
    printf("Scheisse\n");
    return;
  }
  m_filename=fileinfo.fFilename;
  if(m_filename.size()<4 || m_filename.substr(m_filename.size()-4,4)!=".cfg")m_filename+=".cfg";
  setupDisplay();
  saveConfig();
}
void GlobalConfigGui::saveConfig(){
  copyConfig(m_filename.c_str());
}
void GlobalConfigGui::copyConfig(const char *filename){
  std::ofstream of(filename);
  for(int i=0;i<ConfigGui::MAX_MODULES;i++){
    m_config[i]->writeConfig(of);
  }
}
      


void GlobalConfigGui::load(){
  const char *filetypes[] = { "Config files",     "*.cfg",
                              "All files",     "*",
			      0,               0 };
  TGFileInfo fileinfo;
  if(std::string(m_name->GetText()->Data())!="Default" && m_path!="")
    fileinfo.fIniDir=StrDup(m_path.c_str());
  else
    fileinfo.fIniDir=StrDup(m_rootdir.c_str());
  fileinfo.fFileTypes = filetypes;
  fileinfo.fFilename=StrDup(m_cfgpluskey.c_str());
  new TGFileDialog(gClient->GetRoot(), 0, kFDOpen, &fileinfo);
  if(!fileinfo.fFilename){
    printf("Scheisse\n");
    return;
  }
  m_filename=fileinfo.fFilename;
  setupDisplay();
  loadConfig();
}

void GlobalConfigGui::load(const char* filename){
  m_filename=filename;
  setupDisplay();
  loadConfig();
}

void GlobalConfigGui::loadConfig(){
  // Attempt to get the file attributes
  struct stat stFileInfo;
  int intStat;
  intStat = stat(m_filename.c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist}
    m_filename="None";
    setupDisplay();
    return;
  }
  std::ifstream cfgFile(m_filename.c_str());
  std::string filename;
  for(int i=0;i<ConfigGui::MAX_MODULES;i++){
    m_config[i]->readConfig(cfgFile);
  }
}

void GlobalConfigGui::update(){
  if(m_upd==false)return;
  // Attempt to get the file attributes
  struct stat stFileInfo;
  int intStat;
  char fname[512];
  sprintf(fname, "%stop/config__%d.cfg", m_configdir.c_str(), getRunNumber());
  intStat = stat(fname,&stFileInfo);
  if(intStat != 0) { //File does not exist}
    std::cout<<"There is no top level config file for this run."<<std::endl;
    return;
  }
  const char *filetypes[] = { "Config files",     "*.cfg",
                              "All files",     "*",
			      0,               0 };
  std::string oldname=m_name->GetText()->Data();
  if(oldname!="                                  " && oldname!="Unknown")
  {
    m_filename=m_path+oldname+Form("__%d.cfg",getRunNumber()); 
  }
  else{
    TGFileInfo fileinfo;
    fileinfo.fIniDir=StrDup(m_rootdir.c_str());
    fileinfo.fFileTypes = filetypes;
    fileinfo.fFilename=0;
    new TGFileDialog(gClient->GetRoot(), 0, kFDSave, &fileinfo);
    if(!fileinfo.fFilename){
      printf("Scheisse\n");
      return;
    }
    m_filename=fileinfo.fFilename;
    if(m_filename.size()<4 || m_filename.substr(m_filename.size()-4,4)!=".cfg")m_filename+=".cfg";
  }
  updateAvailable(false);
  setupDisplay();
  std::ifstream oldfile(fname);
  std::ofstream newfile(m_filename.c_str());
  int chan;
  std::string filename, oldfilename, oldfilenamenopath;
  int ind=0;
  while (oldfile.good()){
    oldfile>>oldfilenamenopath;
    if(oldfilenamenopath[0]!='/')oldfilename=m_rootdir+oldfilenamenopath; //relative path
    else oldfilename=oldfilenamenopath;
    filename=oldfilename;
    if(oldfile.eof())break;
    std::regex re("configUpdate");
    bool docopy=false;
    std::string path=m_config[ind]->getPath();
    if(path.substr(0,m_rootdir.size())==m_rootdir)path=path.substr(m_rootdir.size());
    if(std::regex_search(filename.c_str(), re)){
      filename.replace(0,m_configdir.size(), path);
      docopy=true;
      newfile<<filename<<std::endl;
    }else{
      newfile<<oldfilenamenopath<<std::endl;
    }
    for(int i=0;i<5;i++){
      oldfile>>chan;
      newfile<<chan<<std::endl;
    }
    if(docopy==true){
      PixelConfig *cf;
      if(m_config[ind]->getType()=="FEI3"){
        cf=new ModuleConfig(oldfilename);
      }else if(m_config[ind]->getType()=="FEI4A"){
        cf=new FEI4AConfig(oldfilename);
      }else if(m_config[ind]->getType()=="FEI4B"){
        cf=new FEI4BConfig(oldfilename);
      }else if(m_config[ind]->getType()=="Hitbus"){
        cf=new HitbusConfig(oldfilename);
      }else if(m_config[ind]->getType()=="Hptdc"){
        cf=new AFPHPTDCConfig(oldfilename);
      }
      char key[32];
      sprintf(key, "%d", getRunNumber());
      cf->writeModuleConfig(m_config[ind]->getPath(),
			    m_config[ind]->getConfdir(), 
			    m_config[ind]->getConfigName(),
			    key);
      delete cf;
    }
    ind++;
  }
  newfile.close();
  loadConfig();
}

void GlobalConfigGui::setConfigDir(const char* name){
  m_configdir=name;
  mkdir((m_configdir+"top").c_str(), 0777);
}

void GlobalConfigGui::saveDefault(){
  std::ofstream of((m_homedir+m_defaultfile).c_str());
  writeGuiConfig(of);
  of.close();
}

void GlobalConfigGui::writeGuiConfig(std::ofstream &ofile){
  if(m_filename=="None")
    m_filename=std::string(getenv("HOME"))+"/"+"Default.cfg";
  ofile<<getRunNumber()<<std::endl;
  ofile<<m_filename<<std::endl;
  ofile<<m_ddir<<std::endl;
  ofile<<m_rootdir<<std::endl;
  saveConfig();
}

void GlobalConfigGui::readGuiConfig(std::ifstream &ifile){
  int runno;
  ifile>>runno;
  setRunNumber(runno);
  std::string filename;
  ifile>>m_filename;
  std::string datadir;
  ifile>>datadir;
  if(datadir=="")datadir=m_homedir+"/calibData";
  setDataDir(datadir.c_str());
  std::string configdir;
  ifile>>configdir;
  setRootDir(configdir.c_str());
  setupDisplay();
  loadConfig();
}

void GlobalConfigGui::setupDisplay(){
  std::cmatch matches;
  //std::string res="/([^/]*)\\.cfg$"; // parse what's hopefully the config file name
  std::regex re;
  std::string res("(^.*\\/)(.+)\\.cfg$");
  re=res;
  if(std::regex_search(m_filename.c_str(), matches, re)){
    if(matches.size()>2){
      m_path=std::string(matches[1].first, matches[1].second);
      m_cfgpluskey=std::string(matches[2].first, matches[2].second);
      res="(.*)__([0-9]+)$"; // parse what's hopefully the key
      re=res;
      if(std::regex_search(m_cfgpluskey.c_str(), matches, re)){
	if(matches.size()>2){
	  std::string match1(matches[1].first, matches[1].second);
	  std::string match2(matches[2].first, matches[2].second);
	  updateText(m_name,match1.c_str());
	  updateText(m_key,match2.c_str());
	}else{
	  updateText(m_name,m_cfgpluskey.c_str());
	  updateText(m_key,"");
	}
      }else{
	updateText(m_name,m_cfgpluskey.c_str());
	updateText(m_key,"");
      }
    }else{
      updateText(m_name,"Unknown");
      updateText(m_key,"Unknown");
      m_path="";
    }
  }else{
    updateText(m_name,"Unknown");
    updateText(m_key,"Unknown");
    m_path="";
  }
}
void GlobalConfigGui::filePrompt()//Prompt for name of config file
{
    const char *filetypes[] = { "Config files",     "*.cfg",
                              "All files",     "*",
			      0,               0 };
    TGFileInfo fileinfo;
    fileinfo.fIniDir=0;
    fileinfo.fFileTypes = filetypes;
    fileinfo.fFilename=0;
    new TGFileDialog(gClient->GetRoot(), 0, kFDSave, &fileinfo);
    m_filename=fileinfo.fFilename;
    if(m_filename.size()<4 || m_filename.substr(m_filename.size()-4,4)!=".cfg")m_filename+=".cfg";
    setupDisplay();
    std::cout << "File name " << m_filename << std::endl;
}
bool GlobalConfigGui::oldFileLoaded()
{
	  std::string oldname=m_name->GetText()->Data();
	
  	return oldname!="                                  " && oldname!="Unknown";
}

std::string GlobalConfigGui::setDir(){
  const char *filetypes[] = { "Directory", "*/", 0,               0 };
  TGFileInfo fileinfo;
  fileinfo.fIniDir=0;
  //fileinfo.fIniDir=StrDup(m_path.c_str());
  fileinfo.fFileTypes = filetypes;
  //fileinfo.fFilename=StrDup(m_cfgpluskey.c_str());
  fileinfo.fFilename=0;
  new TGFileDialog(gClient->GetRoot(), 0, kFDOpen, &fileinfo);
  if(!fileinfo.fFilename){
    printf("Scheisse\n");
    return "";
  }
  std::string datadir;
  struct stat stFileInfo;
  int intStat;
  // Root TGFileinfo is a mess, have to try multiple options
  intStat = stat(fileinfo.fFilename,&stFileInfo);
  if(intStat != 0) { //File does not exist
    intStat = stat(fileinfo.fIniDir,&stFileInfo);
    if(intStat !=0) { // File does not exist
      std::cout<<"Directory does not exist"<<std::endl;
    }else{
      datadir=fileinfo.fIniDir;
    }
  }else{
    datadir=fileinfo.fFilename;
  }
  return datadir;
}

void GlobalConfigGui::guiSetRootDir(){
  std::string dirs=setDir();
  if(dirs!="") setRootDir(dirs.c_str());
}
  

void GlobalConfigGui::setRootDir(const char* dd){
  m_rootdirl->SetText(dd);
  m_conf1->Layout();
  m_rootdir=dd;
  if(m_rootdir.size()>0 && m_rootdir[m_rootdir.size()-1]!='/')m_rootdir+="/";
  m_config[0]->setRootDir(m_rootdir); //static function
}
void GlobalConfigGui::setAbsPath(){
  m_rootdirl->SetText("");
  m_conf1->Layout();
  m_rootdir="";
  m_config[0]->setRootDir(m_rootdir); //static function
}

// Does not increment anymore, increment is happening in maxRunNum
// just being lazy
int GlobalConfigGui::incrementRunNumber(){
  int newrunno=maxRunNum();
  m_runno->SetIntNumber(newrunno);
  return newrunno;
}

int GlobalConfigGui::maxRunNum()
{
	#ifdef SQLDB
	MYSQL *conn;
  	MYSQL_RES *result;
  	MYSQL_ROW row;
        conn = mysql_init(NULL);
	std::string dbName="RCECalibRuns";
	#ifdef SQLDEBUG
		dbName="RCECalibRunsDev";
	#endif
	if(mysql_real_connect(conn, "pixiblsr1daq1", "DBWriter", "W#1ter", dbName.c_str(), 0, NULL, 0)==NULL)
	{
				
		std::cout << "SQL connection to " << dbName << " failed!! Nothing will be inserted!" <<std::endl;
		//m_primList->updateStatus("Failed to connect to SQL database!");
		return	m_runno->GetIntNumber();
	}
	//mysql_query(conn, "SELECT max(RunNum) FROM CalibRunLog");
	mysql_query(conn, "INSERT INTO CalibRunLog (RunNum) VALUES(NULL);");
	mysql_query(conn, "SELECT LAST_INSERT_ID();");
  	result = mysql_store_result(conn);
	row= mysql_fetch_row(result);
	int maxNum;
	std::stringstream(row[0]) >> maxNum;
        mysql_free_result(result);
        mysql_close(conn);
	return maxNum;
	#endif
	return m_runno->GetIntNumber()+1;
}
