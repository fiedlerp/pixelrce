#include "server/ConfigGui.hh"
#include "config/PixelConfig.hh"
#include "config/PixelConfigFactory.hh"
#include <regex>
#include "util/exceptions.hh"
#include <TGFileDialog.h>
#include <TStyle.h>
#include <regex>
#include <iostream>
#include "server/FEI4MaskMaker.hh"

std::string ConfigGui::m_rootdir="";

ConfigGui::ConfigGui(const char* name, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options, const char* prefix):
  TGVerticalFrame(p,w,h,options), m_modName(name), m_valid(false), m_cfg(0), m_filename("None"), m_id(-1) {
  std::string namelabel(prefix);
  namelabel+=name;
  m_name=new TGLabel(this,namelabel.c_str());
  AddFrame(m_name,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 2, 2, 0, 0));
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_name->SetTextFont(labelfont);

  TGHorizontalFrame *confnl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(confnl,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *cfg=new TGLabel(confnl,"File:");
  confnl->AddFrame(cfg,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_configname=new TGLabel(confnl,"None");
  confnl->AddFrame(m_configname,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  //TGLabel *cfgkey=new TGLabel(confnl,"-");
  //confnl->AddFrame(cfgkey,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_key=new TGLabel(confnl,"");
  confnl->AddFrame(m_key,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *confdnl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(confdnl,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *cfgd=new TGLabel(confdnl,"Dir:");
  confdnl->AddFrame(cfgd,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_confdir=new TGLabel(confdnl,"None");
  confdnl->AddFrame(m_confdir,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));

  m_configtype=new TGLabel(confdnl,"     ");
  confdnl->AddFrame(m_configtype,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));
  TGLabel *cfgtype=new TGLabel(confdnl,"Type:");
  confdnl->AddFrame(cfgtype,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *confidl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(confidl,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *cfgid=new TGLabel(confidl,"FEID:");
  confidl->AddFrame(cfgid,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_idl=new TGLabel(confidl,"-1");
  confidl->AddFrame(m_idl,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_modid=new TGLabel(confidl,"");
  confidl->AddFrame(m_modid,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));
  TGLabel *modid=new TGLabel(confidl,"ModID:");
  confidl->AddFrame(modid,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *buttonl = new TGHorizontalFrame(this, 2,2 );
  AddFrame(buttonl,new TGLayoutHints(kLHintsExpandX ));
  m_choosebutton=new TGTextButton(buttonl,"Choose");
  buttonl->AddFrame(m_choosebutton,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  m_choosebutton->Connect("Clicked()", "ConfigGui", this, "openFileWindow()");
  TGTextButton *reload=new TGTextButton(buttonl,"Reload");
  buttonl->AddFrame(reload,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  reload->Connect("Clicked()", "ConfigGui", this, "setConfig()");
  m_maskbutton=new TGTextButton(buttonl,"Edit");
  buttonl->AddFrame(m_maskbutton,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  m_maskbutton->Connect("Clicked()", "ConfigGui", this, "openMaskEditor()");

  TGHorizontalFrame *conf2 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf2,new TGLayoutHints(kLHintsExpandX ));


  m_configvalid=new TGCheckButton(conf2,"Valid");
  //  m_configvalid->SetEnabled(false);
  m_configvalid->SetOn(false);
  conf2->AddFrame(m_configvalid,new TGLayoutHints(kLHintsCenterY | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  m_configvalid->Connect("Toggled(Bool_t)", "ConfigGui", this, "dummy(Bool_t)");

  m_inlink=new TGNumberEntry(conf2, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 28);
  conf2->AddFrame(m_inlink,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel2=new TGLabel(conf2,"Inlink:");
  conf2->AddFrame(linklabel2,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));

  TGHorizontalFrame *conf1 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf1,new TGLayoutHints(kLHintsExpandX ));


  m_included=new TGCheckButton(conf1,"Included");
  m_included->SetOn(false);
  conf1->AddFrame(m_included,new TGLayoutHints(kLHintsCenterY | kLHintsLeft | kLHintsExpandX ,2,2,0,0));
  m_included->Connect("Toggled(Bool_t)", "ConfigGui", this, "setIncluded(Bool_t)");


  m_outlink=new TGNumberEntry(conf1, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 28);
  conf1->AddFrame(m_outlink,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel=new TGLabel(conf1,"Outlink:");
  conf1->AddFrame(linklabel,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));


  TGHorizontalFrame *conf3 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf3,new TGLayoutHints(kLHintsExpandX ));

  //  TGHorizontalFrame *conf4 = new TGHorizontalFrame(this, 2,2 );
    //AddFrame(conf4,new TGLayoutHints(kLHintsExpandX ));
  m_phase=new TGNumberEntry(conf3, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 255);
  conf3->AddFrame(m_phase,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel4=new TGLabel(conf3,"Phase:");
  conf3->AddFrame(linklabel4,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));

  m_rce=new TGNumberEntry(conf3, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 99);
  conf3->AddFrame(m_rce,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 0, 0));
  TGLabel *linklabel3=new TGLabel(conf3,"Rce:");
  conf3->AddFrame(linklabel3,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 0, 0));

}

ConfigGui::~ConfigGui(){
  Cleanup();
}

void ConfigGui::openFileWindow(){
  const char *filetypes[] = { "Config files",     "*.cfg",
			      "JSON Config Files", "*.json",
                              "All files",     "*",
			      0,               0 };
  std::cmatch matches;
  std::regex re("(^.*\\/)(.+\\.cfg)");
  std::string path(m_rootdir);
  std::string filename;
  if(std::regex_search(m_filename.c_str(), matches, re)){
    if(matches.size()>2){
      path=std::string(matches[1].first, matches[1].second);
      filename=std::string(matches[2].first, matches[2].second);
    }
  }
  TGFileInfo fileinfo;
  fileinfo.fIniDir=StrDup(path.c_str());
  fileinfo.fFilename=StrDup(filename.c_str());
  fileinfo.fFileTypes = filetypes;
  new TGFileDialog(gClient->GetRoot(), 0, kFDOpen, &fileinfo);
  if(!fileinfo.fFilename){
    printf("Scheisse\n");
    return;
  }
  m_filename=fileinfo.fFilename;
  setConfig();
}

void ConfigGui::openMaskEditor(){
  if(isValid() && m_cfg->getEnableMaskFile()!=""){
    std::cout<<"File "<<m_cfg->getEnableMaskFile()<<std::endl;
    std::cout<<"HB File "<<m_cfg->getHitbusMaskFile()<<std::endl;
    gStyle->SetOptStat(0);
    FEI4MaskMaker* mm=new FEI4MaskMaker(m_cfg->getEnableMaskFile(),
		      m_cfg->getHitbusMaskFile(),
		      gClient->GetRoot(),FrameXSize,FrameYSize);
    mm->Connect("Reload()", "ConfigGui", this, "setConfig()");
  }else{
    std::cout<<"This configuration has no mask file associated with it"<<std::endl;
  }
}

void ConfigGui::setId(int id){
  m_id=id; 
  char a[128];
  sprintf(a, "%d", id);
  updateText(m_idl, a);
  setModuleName();
}

void ConfigGui::setModuleName(){
  char modname[128];
  sprintf(modname,"RCE%d_module_%d",getRce(), getId());
  m_modulename=modname;
}

void ConfigGui::setConfig(){
  delete m_cfg;
  if(m_filename.substr(m_filename.size()-4)=="None"){
    m_cfg=0;
    return;
  }
  PixelConfigFactory pcf;
  try{
    m_cfg=pcf.createConfig(m_filename.c_str());
  }catch(rcecalib::Config_File_Error &err){
    std::cout<<err.what()<<std::endl;
    resetConfig();
    return;
  }
  setId(m_cfg->getId());
  setType(m_cfg->getType());
  updateText(m_modid, m_cfg->getName());
  setValid(true);
  setIncluded(true);
  std::cmatch matches;
  std::string res="/([^/]*)\\.cfg$"; // parse what's hopefully the config file name
  std::regex re;
  re=res;
  if(std::regex_search(m_filename.c_str(), matches, re)){
    if(matches.size()>1){
      std::string match(matches[1].first, matches[1].second);
      res="(.*)__([0-9]+)$"; // parse what's hopefully the key
      re=res;
      if(std::regex_search(match.c_str(), matches, re)){
	if(matches.size()>2){
	  std::string match1(matches[1].first, matches[1].second);
	  std::string match2(matches[2].first, matches[2].second);
	  updateText(m_configname,match1.c_str());
	  updateText(m_key,match2.c_str());
	}else{
	  updateText(m_configname,match.c_str());
	  updateText(m_key,"");
	}
      }else{
	updateText(m_configname,match.c_str());
	updateText(m_key,"");
      }
    }else{
      updateText(m_configname,"None");
      updateText(m_key,"");
    }
  }else{
    updateText(m_configname,"None");
    updateText(m_key,"");
  }
  res="/([^/]*)/[^/]*/*[^/]*$"; // parse what's hopefully the module name
  re=res;
  if(std::regex_search(m_filename.c_str(), matches, re)){
    if(matches.size()>1){
      std::string match(matches[1].first, matches[1].second);
      updateText(m_confdir, match.c_str());
      m_path=m_filename.substr(0,matches.position()+1);
    }else{
      updateText(m_confdir, "None");
    }
  }else{
    updateText(m_confdir, "None");
  }
  Layout();
}
void ConfigGui::resetConfig(){
  setIncluded(false);
  setValid(false);
  setType("");
  setInLink(0);
  setOutLink(0);
  setRce(0);
  updateText(m_configname,"None");
  updateText(m_modid, "None");
  m_filename="None";
  setId(-1);
}  
void ConfigGui::updateText(TGLabel* label, const char* newtext){
  unsigned len=strlen(label->GetText()->GetString());
  label->SetText(newtext);
  if (strlen(newtext)>len)Layout();
}

void ConfigGui::setIncluded(bool on){
  if(m_configvalid->IsOn())
    m_included->SetOn(on);
  else 
    m_included->SetOn(false);
}

void ConfigGui::setValid(bool on){
  m_configvalid->SetOn(on);
  m_valid=on;
}
void ConfigGui::setType(const char* type){
  updateText(m_configtype, type);
}
void ConfigGui::dummy(bool on){
  // disable clicking
  m_configvalid->SetOn(m_valid);
}

bool ConfigGui::isValid(){
  return m_valid;
}

const std::string ConfigGui::getType(){
  return std::string(m_configtype->GetText()->Data());
}

bool ConfigGui::isIncluded(){
  return m_included->IsOn() || m_included->IsDisabledAndSelected();
}
void ConfigGui::enableControls(bool on){
  m_choosebutton->SetEnabled(on);
  m_maskbutton->SetEnabled(on);
  if((m_included->GetState()==kButtonDisabled)==on)
    m_included->SetEnabled(on);
  m_inlink->SetState(on);
  m_outlink->SetState(on);
  m_rce->SetState(on);
  if((m_configvalid->GetState()==kButtonDisabled)==on)
    m_configvalid->SetEnabled(on);
  m_phase->SetState(on);
}
  
void ConfigGui::writeConfig(std::ofstream &ofile){
  copyConfig(ofile, m_filename.c_str());
}

void ConfigGui::copyConfig(std::ofstream &ofile, const char* filename){
  std::string fn(filename);
  if(fn.substr(0,m_rootdir.size())==m_rootdir){
    ofile<<fn.substr(m_rootdir.size())<<std::endl;
  }else{
    ofile<<filename<<std::endl;
  }
  ofile<<isIncluded()<<std::endl;
  ofile<<getInLink()<<std::endl;
  ofile<<getOutLink()<<std::endl;
  ofile<<getRce()<<std::endl;
  ofile<<getPhase()<<std::endl;
}
void ConfigGui::readConfig(std::ifstream &ifile){
  ifile>>m_filename;
  if(m_filename[0]!='/')m_filename=m_rootdir+m_filename; //relative path
  setConfig();
  bool incl;
  ifile>>incl;
  setIncluded(incl);
  int link;
  ifile >> link;
  setInLink(link);
  ifile >> link;
  setOutLink(link);
  ifile >> link;
  setRce(link);
  ifile >> link;
  setPhase(link);
}
void ConfigGui::copyConfig(const char* dirname){
  m_cfg->writeModuleConfig(dirname, getConfdir(), getConfigName(), getKey());
}
    
