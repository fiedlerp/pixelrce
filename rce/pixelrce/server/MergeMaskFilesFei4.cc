#include <iostream>
#include <fstream>
#include <TH2D.h>
#include "analysis/Fei4CfgFileWriter.hh"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string.hpp>
const unsigned int NROWS=336;
const unsigned int NCOLS=80;

void fillMask(TH2D& histo, const char* filename){
  std::string inpline;
  unsigned short row=0;
  std::ifstream maskfile(filename);
  while(true){
    getline(maskfile, inpline);
      if(maskfile.eof())break;
         boost::trim(inpline);   
          if(inpline.size()!=0 && inpline[0]!='#'){ //remove comment lines and empty lines
	    //	    // if(inpline.find("#")==0) continue;
	    //    if(inpline.length()==0) continue;

	std::vector<std::string> splitVec; 
	split( splitVec, inpline, boost::is_any_of(" -"), boost::token_compress_on ); 
	if(splitVec.size()!=17){
	  std::cout<<"Bad input line "<<inpline<<std::endl;
	  continue;
	}
	int r=strtol(splitVec[0].c_str(),0,10);
	assert(r==++row);//check and increment row number
	std::string oneline;
	for(int i=1;i<17;i++)oneline+=splitVec[i]; //concatenate everyting.
	assert(oneline.size()==NCOLS);
	for(unsigned int i=0;i<NCOLS;i++){
	  if(oneline[i]=='0')histo.SetBinContent(i+1, row, 1);
	}
	}
  }
  assert(row==NROWS);
}


int main(int argc, char **argv){
  if(argc<4){
    std::cout<<"Usage: mergeMasksFei4 outfile maskfile1 maskfile2 ..."<<std::endl;
    exit(0);
  }
  TH2D mask("mask","mask", NCOLS, 0, NCOLS, NROWS, 0, NROWS);
  for(int i=2;i<argc;i++){
    fillMask(mask, argv[i]);
  }
  Fei4CfgFileWriter fw;
  fw.writeMaskFile(argv[1], &mask);
}
