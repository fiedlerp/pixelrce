#ifndef PIXSCAN_HH
#define PIXSCAN_HH

#include "ScanOptions.hh"
#include "Callback.hh"
#include "PixScanBase.h"
using namespace ipc;


#include <vector>
#include <list>
#include <map>
#include <string>
#include <assert.h>
#include <sys/time.h>

namespace RCE {

  class PixScan : public PixLib::PixScanBase{


private:
  // Global scan config
  // RCE specific
  std::string m_name;
  std::string m_scanType;
  std::string m_triggerType;
  std::map<std::string, std::string> m_formatterType;
  std::string m_receiver;
  std::string m_dataHandler;
  std::string m_dataProc;
  std::string m_analysisType;
  ipc::Priority m_callbackPriority;
  unsigned m_timeout_seconds;
  unsigned m_timeout_nanoseconds;
  std::vector<std::string>m_histoNames;
  int m_eventInterval;
  bool m_oneByOne;
  int m_firstStage;
  int m_stepStage;
  int m_LVL1Latency_Secondary;
  unsigned short m_moduleTrgMask;
  bool m_mixedTrigger;
  bool m_setupThreshold;
  unsigned short m_threshold;
  std::string m_fitfun;
  bool m_verifyConfig;
  unsigned short m_triggerMask;
  bool m_triggerDataOn;
  int m_injectForTrigger;  
  unsigned short m_deadtime;
  unsigned short m_hitbusConfig;
  bool m_clearMasks;
  bool m_protectFifo;
  int m_runnumber;
  bool m_overrideLatency;
  int m_allowedTimeouts;
  unsigned m_nTriggers;
  double m_noiseThreshold;
  bool m_useAbsoluteNoiseThreshold;
  bool m_useVcal;
    
    
  std::map<std::string, int> m_histogramTypes;
  std::map<std::string, int> m_scanTypes;
  void prepareIndexes(HistogramType type, unsigned int mod, int ix2, int ix1, int ix0);

public:
  //! Constructors
    PixScan() {};
    PixScan(ScanType presetName, FEflavour feFlavour);
    PixScan(PixLib::PixScanBase &base):PixLib::PixScanBase(base){};

  //! Destructor
  ~PixScan();


  //! Load predefined configurations

    // setup RCE specific fields
    bool setupRCE(ScanType presetName, FEflavour feFlavour);
    bool presetRCE(ScanType presetName, FEflavour feFlavour);

    void resetScan();
    
  void convertScanConfig(ipc::ScanOptions& scanPar);
  void dump(std::ostream &os, const ipc::ScanOptions& scanPar);
  void toJSON(const ipc::ScanOptions& scanPar);  

  //! Scan attributes
  void setFirstStage(int firstStage){
  	m_firstStage=firstStage;
  }
  int getFirstStage(){
	return m_firstStage;
  }
  void setStepStage(int stepStage){
  	m_stepStage=stepStage;
  }
  int getStepStage(){
	return m_stepStage;
  }
  void setSetupThreshold(bool setupThreshold) {
  	m_setupThreshold=setupThreshold;
  }
  bool getSetupThreshold() {
	return m_setupThreshold;
  }
  void setThreshold(unsigned short threshold){
	m_threshold=threshold;
  }
  unsigned short getThreshold(){
	return m_threshold;
  }
  void setTimeoutSeconds(unsigned timeoutSeconds){
  	m_timeout_seconds=timeoutSeconds;
  }
  unsigned getTimeoutSeconds(){
	return m_timeout_seconds;
  }

  void setName(const char* name){
    m_name=name;
  }
  std::string getName(){
    return m_name;
  }
  void addHistoName(const char* name){
    m_histoNames.push_back(name);
  }
  std::vector<std::string>& getHistoNames(){
    return m_histoNames;
  }
  unsigned short getModuleTrgMask(){
    return m_moduleTrgMask;
  }
  void setModuleTrgMask (unsigned short modTrgMask){
  	m_moduleTrgMask=modTrgMask;
  }
  const char* getAnalysisType(){
    return m_analysisType.c_str();
  }
  ipc::Priority getCallbackPriority(){
     return m_callbackPriority;
  }
  const char* getScanTypeName(){
    return m_scanType.c_str();
  }
      
  const char* getTriggerType(){
    return m_triggerType.c_str();
  }
  const char* getFormatterType(const char* type){
    return m_formatterType[type].c_str();
  }
  const bool verifyConfig(){
    return m_verifyConfig;
  }
  void setTriggerMask(unsigned short mask){
    m_triggerMask=mask;
  }
  void setEventInterval(unsigned val){
    m_eventInterval=val;
  }
  void setDeadtime(unsigned val){
    m_deadtime=val;
  }
  void setHitbusConfig(unsigned short val){
    m_hitbusConfig=val;
  }
  unsigned short getHitbusConfig(){
    return m_hitbusConfig;
  }
  bool clearMasks(){
    return m_clearMasks;
  }
  void setClearMasks(bool on){
    m_clearMasks=on;
  }
  bool protectFifo(){
    return m_protectFifo;
  }
  void setRunNumber(int runnum){
    m_runnumber=runnum;
  }
  int getRunNumber(){
    return m_runnumber;
  }
  double getNoiseThreshold(){
    return m_noiseThreshold;
  }
  bool useAbsoluteNoiseThreshold(){
    return m_useAbsoluteNoiseThreshold;
  }
  void setSecondaryLatency(int lat){
    m_LVL1Latency_Secondary=lat;
  }
  int getSecondaryLatency(){
    return m_LVL1Latency_Secondary;
  }
  //! Scan coontrol
 
  void initConfig();
  bool loop(int index);
  int scanIndex(int index);
  void next(int index);
  void terminate(int index);
  bool newMaskStep();
  bool newScanStep();


  };

}
#endif

 

