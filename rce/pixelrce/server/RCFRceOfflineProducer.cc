#include "server/RceOfflineProducer.hh"
#include "server/RCFController.hh"
#include "util/HistoManager.hh"
#include <netdb.h>
#include <getopt.h>

void print_usage() {
  std::cout << "usage rceOfflineProducer" << std::endl;
  std::cout << "  --rce,-r <rce_number>      : RCE to connect to" <<std::endl;
  std::cout << "  --interface,-i <IP>        : IP of host the RCE connects to"<<std::endl;
  std::cout << "  --runcontrol,-d <hostname> : Runcontrol hostname"<<std::endl;
  std::cout << "  --help,-h" <<std::endl;
  exit(EXIT_FAILURE);
}

int main ( int argc, char *argv[] )
{
  int opt=0;
  int rce=-1;
  std::string hostname="";
  std::string orbhost="";
  static struct option long_options[] = {
    {"rce",       required_argument,  0,  'r' },
    {"help",      no_argument,        0,  'h' },
    {"interface", required_argument,  0,  'i' },
    {"hostname",  required_argument,  0,  'd' },
    {0,           0,                  0,  0   }
  };
  int long_index =0;
  while ((opt = getopt_long(argc, argv,"r:i:d:h", 
			    long_options, &long_index )) != -1) {
    switch (opt) {
    case 'r' : rce = atoi(optarg);
      break;
    case 'd' : hostname=optarg;
      break;
    case 'i' : orbhost=optarg;  
      break;
    case 'h':      
    default: print_usage(); 
    }
  }

  if(rce<0 || orbhost=="" || hostname == "")  {
    print_usage();
  }
 
  setenv("ORBHOST", orbhost.c_str(), 1);
  std::string rchost;
  hostent * ipAddrContainer = gethostbyname(hostname.c_str());
  if (ipAddrContainer != 0) {
    int nBytes;
    if (ipAddrContainer->h_addrtype == AF_INET) nBytes = 4;
    else if (ipAddrContainer->h_addrtype == AF_INET6) nBytes = 6;
    else {
      std::cout << "Unrecognized IP address type. Run not started." 
		<< std::endl;
      exit(0);
    }
    std::stringstream ss("tcp://");
    ss << "tcp://"; 
    for (int i = 0, curVal; i < nBytes; i++)
      {
	curVal = static_cast<int>(ipAddrContainer->h_addr[i]);
	if (curVal < 0) curVal += 256;
	ss << curVal;
	if (i != nBytes - 1) ss << ".";
      }
    ss<<":44000";
    rchost=ss.str();
  }else{
    std::cout<<"Bad IP address. Exiting."<<std::endl;
    exit(0);
  }
  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  new HistoManager(0);
  RceOfflineProducer producer("RceOfflineProducer", rchost.c_str(), &controller, rce);
  sleep(100000000);
}
