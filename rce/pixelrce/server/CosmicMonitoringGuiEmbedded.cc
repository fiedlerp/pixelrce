#include "server/CosmicMonitoringGuiEmbedded.hh"
#include "server/ConfigGui.hh"
#include "TApplication.h"
#include "TGMsgBox.h"
#include "TH1.h"
#include "TGIcon.h"
#include "TGMenu.h"
#include "TCanvas.h"
#include "TGCanvas.h"
#include "TGListTree.h"
#include "TRootEmbeddedCanvas.h"
#include <TGFileDialog.h>
#include <TROOT.h>
#include <TStyle.h>
#include <assert.h>
#include <math.h>
#include <iostream>
#include <time.h>
#include <sys/stat.h> 
#include <time.h> 
#include <fstream>
#include <list>
#include <pthread.h>
#include <vector>
#include "eudaq/Event.hh"
#include "config/FormattedRecord.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/FileSerializer.hh"
#include <memory>

int hitbusEdge(unsigned word, int j){
  for (int i=9;i>=0;i--){
    if((word&(1<<((2-j)+3*i)))!=0){
      return 9-i;
    }
  }
  return -1;
}

CosmicMonitoringGuiEmbedded::~CosmicMonitoringGuiEmbedded(){
  Cleanup();
  for(int i=0;i<MAX_RCE_NUM;i++)delete [] m_linkToIndex[i];
  delete [] m_linkToIndex;
}

void CosmicMonitoringGuiEmbedded::init(){
  TGVerticalFrame* datapanel=new TGVerticalFrame(this,1,1, kSunkenFrame);
  // scan panel
  TGHorizontalFrame *datapanel5 = new TGHorizontalFrame(datapanel, 2, 2, kSunkenFrame);
  datapanel->AddFrame(datapanel5,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  AddFrame(datapanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  TGVerticalFrame *treepanel = new TGVerticalFrame(datapanel5, 150, 2, kSunkenFrame);
  datapanel5->AddFrame(treepanel,new TGLayoutHints(kLHintsExpandY));
  TGVerticalFrame *plotpanel = new TGVerticalFrame(datapanel5, 2, 2, kSunkenFrame);
  datapanel5->AddFrame(plotpanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  m_tgc=new TGCanvas(treepanel, 300,100);
  //TGViewPort* vp=tgc->GetViewPort();
  m_tree=new TGListTree(m_tgc, kHorizontalFrame);
  m_tree->Connect("Clicked(TGListTreeItem*, Int_t)","CosmicMonitoringGuiEmbedded", this, "displayHisto(TGListTreeItem*, Int_t)");
  //tree->AddItem(0,"/");
  treepanel->AddFrame(m_tgc,new TGLayoutHints(kLHintsExpandY));
  m_canvas=new TRootEmbeddedCanvas("Canvas",plotpanel,100,100);
  m_canvas->GetCanvas()->GetPad(0)->SetRightMargin(0.15);
  m_www=0; m_hhh=0;
  gStyle->SetPalette(1);
  gStyle->SetOptStat(10);
  plotpanel->AddFrame(m_canvas,new TGLayoutHints(kLHintsExpandY|kLHintsExpandX));
  m_linkToIndex=new int*[MAX_RCE_NUM];
  for(int i=0;i<MAX_RCE_NUM;i++)m_linkToIndex[i]=0;
  
}    


void CosmicMonitoringGuiEmbedded::setup(bool online, bool writeRootFile, const char* rootfile,
					bool writeTemps, const char* tempfile){

  std::cout<<"Setting up monitoring histograms"<<std::endl;
  m_writeRootFile=writeRootFile;
  m_logtemps=writeTemps;
  if(m_nMods>0){
    delete [] m_nhits;
    for (int i=0;i<m_nMods;i++){
      delete [] m_pixx[i];
      delete [] m_pixy[i];
      delete [] m_value[i];
      delete [] m_timing[i];
      delete [] m_hitincluster[i];
      delete [] m_posx[i];
      delete [] m_posy[i];
      delete [] m_posz[i];
    }
    delete [] m_pixx;
    delete [] m_pixy;
    delete [] m_value;
    delete [] m_timing;
    delete [] m_hitincluster;
    delete [] m_posx;
    delete [] m_posy;
    delete [] m_posz;
  }
  m_starttime_sec=(unsigned)time(0);
  m_nMods = 0;
  m_nRces = 0;
  m_linkmap.clear();
  for(int i=0;i<MAX_RCE_NUM;i++){
    delete [] m_linkToIndex[i];
    m_linkToIndex[i]=0;
  }
  m_outlinks.clear();
  m_rceNums.clear();
  m_rces.clear();
  m_fetype.clear();
  m_timeStampLimit=5.0;
  m_nev=0;
  m_hitsperevent=0;
  m_nScreenshots=0;
  m_histo=0;
  m_graph=0;
  for (int i=0;i<MAX_RCES;i++){
    m_tdc[i]=0;
  for(int k=0;k<2;k++)for(int l=0;l<3;l++)m_hitbus[i][k][l]=0;
  }
  for(int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(i!=ConfigGui::MAX_MODULES-1){
      m_corxx[i]=0;
      m_corxy[i]=0;
      m_coryy[i]=0;
      m_coryx[i]=0;
    }
    m_tempsec[i]=0;
    m_tempmin[i]=0;
    m_occ[i]=0;
    m_toth[i]=0;
    m_bx[i]=0;
    m_afptdc[i]=0;
    m_timestamp[i]=0;
    m_projx[i]=0;
    m_projy[i]=0;
    m_corrindex[i]=-1;
  }
  
  if(online){

    for (unsigned int i=0; i<m_config.size(); i++){
      if(m_config[i]->isIncluded()){
	//valid=true;
	m_lastcount[i]=0;
	unsigned outlink=m_config[i]->getOutLink();
	unsigned rceNum=m_config[i]->getRce();
	addRce(rceNum);

	std::cout<<"\tModule "<<i<<": outlink "<<outlink<<" , RCE "<<rceNum<<std::endl;
	createLink(outlink,rceNum); //this increments m_nMods
	
	if(m_config[i]->getType()=="FEI4A" || m_config[i]->getType()=="FEI4B"){
	  m_fetype.push_back(4); 
	}else if(m_config[i]->getType()=="FEI3"){
	  m_fetype.push_back(3);
	}else if(m_config[i]->getType()=="Hitbus"){
	  m_fetype.push_back(9);
	}else if(m_config[i]->getType()=="HPTDC"){
	  m_fetype.push_back(11);
	}
	else {
	  std::cout<<"Unknown FE type"<<std::endl;
          assert(0);
	}
      }
    }
    
  }
  else{ //offline/standalone executable

    std::string chanDefName = "cosmicChannelDefs.txt";
    std::ifstream cha(chanDefName.data());
    if (!cha.is_open()){

      std::cout << "Cannot find "<<chanDefName<<std::endl;
      bool success =  setupChannelsFromFile(m_filename);
      
      if(!success){
	std::cout<<"Couldn't read "<<m_filename<<" ; instead setting default values for channels"<<std::endl;
	
	createLink(11,0);
	createLink(12,0);
	createLink(13,0);
	createLink(14,0);
	createLink(0,0);
	
	m_fetype.push_back(3);
	m_fetype.push_back(3);
	m_fetype.push_back(3);
	m_fetype.push_back(3);
	m_fetype.push_back(4);
	
	addRce(0);
      }
      
    }
    else{
      
      //Read in channel definitions from text file (for standalone cosmicMonitoringGui only)
      std::cout<<"Reading in channel definitions from "<<chanDefName<<std::endl;
      int upl, rce, fet;
      while(!cha.eof()){
	cha>>upl>>rce>>fet;

	createLink(upl,rce);
	addRce(rce);
	m_fetype.push_back(fet);
	
	std::cout<<"Module "<<(m_nMods-1)<<" :  uplink "<<upl<<" ; RCE "<<rce<<" ; FEI"<<fet<<std::endl;
	
      }
      
    }
    
  } //end if(!online)
  
  // These are needed even when no root file is written.
  m_nhits=new Int_t[m_nMods];
  m_pixx=new Int_t*[m_nMods];
  m_pixy=new Int_t*[m_nMods];
  m_value=new Int_t*[m_nMods];
  m_timing=new Int_t*[m_nMods];
  m_hitincluster=new Int_t*[m_nMods];
  m_posx=new Double_t*[m_nMods];
  m_posy=new Double_t*[m_nMods];
  m_posz=new Double_t*[m_nMods];
  for (int i=0;i<m_nMods;i++){
     m_pixx[i]=new Int_t[MAXHIT];
     m_pixy[i]=new Int_t[MAXHIT];
     m_value[i]=new Int_t[MAXHIT];
     m_timing[i]=new Int_t[MAXHIT];
     m_hitincluster[i]=new Int_t[MAXHIT];
     m_posx[i]=new Double_t[MAXHIT];
     m_posy[i]=new Double_t[MAXHIT];
     m_posz[i]=new Double_t[MAXHIT];
  }
  for (int i=0;i<m_nMods;i++){
    for(int j=0;j<MAXHIT;j++){
       m_hitincluster[i][j]=0;
       m_posx[i][j]=0;
       m_posy[i][j]=0;
       m_posz[i][j]=0;
    }
  }
  fillHistoTree();
  if(writeRootFile==true)setupRootFile(rootfile);
  if(writeTemps==true)m_tempfile=new std::ofstream(tempfile);
  
}

CosmicMonitoringGuiEmbedded::CosmicMonitoringGuiEmbedded(const TGWindow *p,UInt_t w,UInt_t h,UInt_t options, const std::vector<ConfigGui*> config)
  : TGVerticalFrame(p,w,h,options), m_config(config), m_nMods(0), m_evfile(0) {
  init();
}

CosmicMonitoringGuiEmbedded::CosmicMonitoringGuiEmbedded(const char* filename, int nevt, const TGWindow *p,UInt_t w,UInt_t h,UInt_t options)
  : TGVerticalFrame(p,w,h,options), m_filename(filename),m_nMods(0), m_evfile(0) {
  
  init();
  setup(false, false, 0, false, 0);  //online==false
  
  readFile(m_filename, nevt);
//  TFile a("out.root","recreate");
//  tdc->Write();
 // a.Close();
}

void CosmicMonitoringGuiEmbedded::clearTree(){
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  if(root) {m_tree->DeleteChildren(root);
    root->SetOpen(false);
    updateTree();
  }
}

void CosmicMonitoringGuiEmbedded::updateTree(){
  int x=m_tgc->GetViewPort()->GetX();
  int y=m_tgc->GetViewPort()->GetY();
  int w=m_tgc->GetViewPort()->GetWidth();
  int h=m_tgc->GetViewPort()->GetHeight();
  m_tree->DrawRegion(x,y,w,h);
}

void CosmicMonitoringGuiEmbedded::updateHisto(){
  m_histo->SetMinimum(0);
  if(m_histo->GetDimension()==1){
    m_canvas->GetCanvas()->SetCanvasSize(m_ww, m_wh);
    if(std::string(m_histo->GetName()).substr(0,4)=="proj")gStyle->SetOptStat(1110);
    else gStyle->SetOptStat(10);
    m_histo->Draw();
  }else{
    gStyle->SetOptStat(10);
    int nx=m_histo->GetNbinsX();
    int wh=0;
    if(nx==144){ //FEI3
      wh=int(m_ww*0.944/0.4/144*320*0.05);
    }else{ //FEI4
      wh=int(m_ww*0.944/0.25/80*336*0.05);
    }
    m_canvas->GetCanvas()->SetCanvasSize(m_ww, wh);
    m_histo->Draw("colz");
  }
}  

void CosmicMonitoringGuiEmbedded::updateGraph(){
  m_canvas->GetCanvas()->SetCanvasSize(m_ww, m_wh);
  m_graph->Draw("AP");
}

void CosmicMonitoringGuiEmbedded::fillHistoTree(){
    
  char name[128], title[128];
  
  clearTree();
  for (size_t i=0;i<m_histlist.size();i++){
    delete m_histlist[i];
  }
  m_histlist.clear();
  for (int i=0;i<m_nMods;i++){
    if(m_fetype[i]==4){
      sprintf(name,"tempmin%d",i);
      sprintf(title,"Temperature by minute Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]);
      m_tempmin[i]=new TGraph;
      m_tempmin[i]->SetMinimum(-40);
      m_tempmin[i]->SetMarkerStyle(22);
      m_tempmin[i]->SetName(name);
      m_tempmin[i]->SetTitle(title);
      m_tempmin[i]->Expand(1000);
      m_histlist.push_back(m_tempmin[i]);
      sprintf(name,"tempsec%d",i);
      sprintf(title,"Temperature 60 sec Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]);
      m_tempsec[i]=new TH1F(name, title, 60,0,60);
      m_tempsec[i]->SetOption("p");
      m_tempsec[i]->SetMarkerStyle(22);
      m_histlist.push_back(m_tempsec[i]);
      m_lasttime[i]=0;
    }else{
      m_tempmin[i]=0;
      m_tempsec[i]=0;
    }
    sprintf(name,"occ%d",i);
    sprintf(title,"Occupancy Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]);
    if(m_fetype[i]==4){
      m_occ[i]=new TH2F(name,title, 80,0,80,336,0, 336);	//FEI4
      m_histlist.push_back(m_occ[i]);
      m_occ[i]->SetMinimum(0);
    } else if(m_fetype[i]==3) {
      m_occ[i]=new TH2F(name,title, 8*18,0,8*18,2*160,0, 2*160);	
      m_histlist.push_back(m_occ[i]);
      m_occ[i]->SetMinimum(0);
    }else{
      m_occ[i]=0;
    }
    if(m_fetype[i]==4){
      m_projx[i]=new TH1F(Form("projx%d%d",m_outlinks[i],m_rceNums[i]),
			  Form("X projection Mod %d RCE %d", m_outlinks[i],m_rceNums[i]),
			  80,0,20);
      m_projx[i]->GetXaxis()->SetTitle("x (mm)");
      m_projy[i]=new TH1F(Form("projy%d%d",m_outlinks[i],m_rceNums[i]),
			  Form("Y projection Mod %d RCE %d", m_outlinks[i],m_rceNums[i]),
			  336,0,16.8);
      m_projy[i]->GetXaxis()->SetTitle("y (mm)");
      m_histlist.push_back(m_projx[i]);
      m_histlist.push_back(m_projy[i]);
    } else if(m_fetype[i]==3) {
      m_projx[i]=new TH1F(Form("projx%d%d",m_outlinks[i],m_rceNums[i]),
			  Form("X projection Mod %d RCE %d", m_outlinks[i],m_rceNums[i]),
			  144,0,57.6);
      m_projx[i]->GetXaxis()->SetTitle("x (mm)");
      m_projy[i]=new TH1F(Form("projy%d%d",m_outlinks[i],m_rceNums[i]),
			  Form("Y projection Mod %d RCE %d", m_outlinks[i],m_rceNums[i]),
			  320,0,16);
      m_projy[i]->GetXaxis()->SetTitle("y (mm)");
      m_histlist.push_back(m_projx[i]);
      m_histlist.push_back(m_projy[i]);
    }else{
      m_projx[i]=0;
      m_projy[i]=0;
    }
    sprintf(name,"tot%d",i);
    sprintf(title,"ToT Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]);
    if(m_fetype[i]==4){
      m_toth[i]=new TH1F(name,title, 60,-.5,59.5);
      m_histlist.push_back(m_toth[i]);
    } else if(m_fetype[i]==3) {
      m_toth[i]=new TH1F(name,title, 256,-.5,255.5);
      m_histlist.push_back(m_toth[i]);
    } else
      m_toth[i]=0;
    
    if(m_fetype[i]==3 || m_fetype[i]==4){
      sprintf(title,"Timing Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]);
      sprintf(name,"bx%d",i);
      m_bx[i]=new TH1F(name,title, 16,-.5,15.5);
      m_histlist.push_back(m_bx[i]);
    
      //      sprintf(title,"ToT Vs Timestamp Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]);
            //sprintf(name,"tstamp%d",i);
            //m_timestamp[i]=new TH1F(name, title, 5000,0.0,m_timeStampLimit);
            //m_histlist.push_back(m_timestamp[i]);
      m_timestamp[i]=0;
    }else{
      m_bx[i]=0;
      m_timestamp[i]=0;
    }
    if(m_fetype[i]==11){ // AFP HPTDC board
      m_afptdc[i]=new TH1F*[12];
      for(int j=0;j<12;j++){
	sprintf(title,"TDC RCE%d-Board%d-Channel%d",m_rceNums[i],m_outlinks[i],j);
	sprintf(name,"bx%d_%d",i,j);
	m_afptdc[i][j]=new TH1F(name,title, 1024,-.5,1023.5);
	m_histlist.push_back(m_afptdc[i][j]);
      }
    }else{
      m_afptdc[i]=0;
    }


    if(m_fetype[i]==3 || m_fetype[i]==4){
      int j=i+1;
      if(j==m_nMods) j=0;  //wrap around
      while(m_fetype[j]!=3 && m_fetype[j]!=4){
	if(i==j)break;
	j=j+1;
	if(j==m_nMods)j=0; //wrap around
      }
      if(i!=j  && m_corrindex[j]!=i){

	int nX[2] = {8*18, 80};
	int nY[2] = {2*160, 336};
	int idxI =0, idxJ = 0;
	
	if(m_fetype[i]==4){
	  idxI = 1;
	}
	if(m_fetype[j]==4){
	  idxJ = 1;
	}
	sprintf(title,"Col-Col Correlation Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
	sprintf(name,"corxx%d%d",i,j);
	m_corxx[i]=new TH2F(name,title, nX[idxI],0,nX[idxI],nX[idxJ],0,nX[idxJ]);
	
	sprintf(title,"Col-Row Correlation Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
	sprintf(name,"corxy%d%d",i,j);
	m_corxy[i]=new TH2F(name,title, nX[idxI],0,nX[idxI],nY[idxJ],0,nY[idxJ]);
	
	sprintf(title,"Row-Row Correlation Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
	sprintf(name,"coryy%d%d",i,j);
	m_coryy[i]=new TH2F(name,title, nY[idxI],0,nY[idxI],nY[idxJ],0,nY[idxJ]);

	sprintf(title,"Row-Col Correlation Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
	sprintf(name,"coryx%d%d",i,j);
	m_coryx[i]=new TH2F(name,title, nY[idxI],0,nY[idxI],nX[idxJ],0,nX[idxJ]);

        m_histlist.push_back(m_corxx[i]);
	m_histlist.push_back(m_corxy[i]);
	m_histlist.push_back(m_coryy[i]);
	m_histlist.push_back(m_coryx[i]);
	m_corrindex[i]=j;
      }else{
	m_corxx[i]=0;
	m_corxy[i]=0;
	m_coryy[i]=0;
	m_coryx[i]=0;
      }
    }
  }
      
  for (int i=0;i<m_nRces;i++){
    sprintf(title,"TDC for RCE %d",m_rces[i]);
    sprintf(name,"tdc%d",i);
    m_tdc[i]=new TH1F(name, title, 64,-.5,63.5);
    m_histlist.push_back(m_tdc[i]);
    for (int j=0;j<m_nMods;j++){
      if(m_fetype[j]==9){
	for(int k=0;k<2;k++){
	  char tel[8];
	  if(k==0)sprintf(tel,"A");
	  else sprintf(tel,"B");
	  for (int l=0;l<3;l++){
	    sprintf(title,"Timing hitbus data RCE %d telescope %s FE %d", i, tel, l+1);
	    sprintf(name, "hbtiming_%d_%s_%d", i, tel, l+1);
	    m_hitbus[i][k][l]=new TH1F(name, title, 10,0,10);
	    m_histlist.push_back(m_hitbus[i][k][l]);
	  }
	}
	break;
      }
    }
  }  

  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  if (!root) root = m_tree->AddItem(0,"Histos");
  TGListTreeItem* occr=m_tree->AddItem(root,"Occupancy");
  TGListTreeItem* prxr=m_tree->AddItem(root,"Projection X");
  TGListTreeItem* pryr=m_tree->AddItem(root,"Projection Y");
  TGListTreeItem* timingr=m_tree->AddItem(root,"Timing");
  TGListTreeItem* totr=m_tree->AddItem(root,"ToT");
  TGListTreeItem* corr=m_tree->AddItem(root,"Correlation");
  TGListTreeItem* corrmods[m_nMods];
  for (int i=0;i<m_nMods;i++){
    sprintf(name,"Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]); 
    corrmods[i]=m_tree->AddItem(corr, name);
  }
  TGListTreeItem* tempr=m_tree->AddItem(root,"Temperatures");
  TGListTreeItem* tdcr=m_tree->AddItem(root,"TDC");
  //  TGListTreeItem* tstampr=m_tree->AddItem(root,"Timestamp");
  TGListTreeItem* hitbusr=m_tree->AddItem(root,"Hitbus Timing");
  TGListTreeItem* afpr=m_tree->AddItem(root,"AFP TDC");
  const TGPicture *thp = gClient->GetPicture("h1_t.xpm");
  
  for (int i=0;i<m_nRces;i++){
    char mtit[128];
    sprintf(mtit,"TDC for RCE %d",m_rces[i]);
    m_tree->AddItem(tdcr,mtit, m_tdc[i], thp, thp);
  }  

  for (int i=0;i<m_nRces;i++){
    char mtit[128];
    if(m_hitbus[i][0][0]!=0){
      for(int k=0;k<2;k++){
	char tel[8];
	if(k==0)sprintf(tel,"A");
	else sprintf(tel,"B");
	for(int l=0;l<3;l++){
	  sprintf(mtit, "RCE %d Telescope %s FE %d", i, tel, l+1);
	  m_tree->AddItem(hitbusr, mtit, m_hitbus[i][k][l], thp, thp);
	}
      }
    }
  }
	

  for(int i=0;i<m_nMods;i++){
    char mtit[128];
    sprintf(mtit,"Mod%d-RCE%d",m_outlinks[i],m_rceNums[i]);
    if(m_tempsec[i]!=0)m_tree->AddItem(tempr,Form("%s-sec", mtit),m_tempsec[i], thp, thp);
    if(m_tempmin[i]!=0)m_tree->AddItem(tempr,Form("%s-min", mtit),m_tempmin[i], thp, thp);
    if(m_occ[i]!=0)m_tree->AddItem(occr,mtit,m_occ[i], thp, thp);
    if(m_projx[i]!=0)m_tree->AddItem(prxr,mtit,m_projx[i], thp, thp);
    if(m_projy[i]!=0)m_tree->AddItem(pryr,mtit,m_projy[i], thp, thp);
    if(m_toth[i]!=0)m_tree->AddItem(totr,mtit,m_toth[i], thp, thp);
    if(m_bx[i]!=0)m_tree->AddItem(timingr,mtit, m_bx[i], thp, thp);
    //if(m_timestamp[i]!=0)m_tree->AddItem(tstampr,mtit, m_timestamp[i], thp, thp);
    if(m_afptdc[i]!=0){
      TGListTreeItem* mfpr=m_tree->AddItem(afpr,mtit);
      for(int j=0;j<12;j++){
	sprintf(mtit, "Ch%d", j);
	m_tree->AddItem(mfpr, mtit, m_afptdc[i][j], thp, thp); 
      }
    }
    if(m_corxx[i]!=0){
      int j=m_corrindex[i];
      sprintf(mtit,"Col-Col Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
      m_tree->AddItem(corrmods[i],mtit, m_corxx[i], thp, thp);
      sprintf(mtit,"Col-Row Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
      m_tree->AddItem(corrmods[i],mtit, m_corxy[i], thp, thp);
      sprintf(mtit,"Row-Row Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
      m_tree->AddItem(corrmods[i],mtit, m_coryy[i], thp, thp);
      sprintf(mtit,"Row-Col Mod%d-RCE%d x Mod%d-RCE%d",m_outlinks[i],m_rceNums[i],m_outlinks[j],m_rceNums[j]);
      m_tree->AddItem(corrmods[i],mtit, m_coryx[i], thp, thp);
    }
  }

  root->SetOpen(true);
  timingr->SetOpen(true);
  totr->SetOpen(true);
  occr->SetOpen(true);
  tempr->SetOpen(true);
  prxr->SetOpen(true);
  pryr->SetOpen(true);
  corr->SetOpen(true);
  afpr->SetOpen(true);
  for (int i=0;i<m_nMods;i++){
	corrmods[i]->SetOpen(true);
  }
}

void CosmicMonitoringGuiEmbedded::displayHisto(TGListTreeItem* item, int b){
  TGListTreeItem *parent=item->GetParent();
  if(parent==0)return;
  if(std::string(parent->GetText())=="Histos")return;
  if(std::string(parent->GetText())=="Correlation")return;
  if(std::string(parent->GetText())=="AFP TDC")return;
  TObject *t=(TObject*)item->GetUserData();
  if(t->InheritsFrom("TH1")){
    m_histo=(TH1*)item->GetUserData();
    m_graph=0;
  }else if(t->InheritsFrom("TGraph")){
    m_graph=(TGraph*)item->GetUserData();
    m_histo=0;
  }
  updateDisplay();
}

void CosmicMonitoringGuiEmbedded::updateDisplay(){
  if(m_www!=GetWidth()||
     m_hhh!=GetHeight()){
    m_www=GetWidth();
    m_hhh=GetHeight();
    m_ww=m_canvas->GetCanvas()->GetWw();
    m_wh=m_canvas->GetCanvas()->GetWh();
  }
  m_canvas->GetCanvas()->Clear("");
  m_canvas->GetCanvas()->Update();
  if(m_histo!=0) updateHisto();
  else if(m_graph!=0){
    m_graphmutex.Lock(); //ROOT crashes if the graph is updated between Draw() and canvas Update()
    updateGraph();
  }
  m_canvas->GetCanvas()->Update();
  if(m_graph!=0)m_graphmutex.UnLock();
}

unsigned CosmicMonitoringGuiEmbedded::nextcurrent(unsigned char* p){
  unsigned current=*(unsigned*)&p[m_indx];
  //unsigned current=(p[m_indx]<<24) | (p[m_indx+1]<<16) | (p[m_indx+2]<<8) | p[m_indx+3];
  m_indx+=4;
  return current;
}
unsigned CosmicMonitoringGuiEmbedded::getWord(unsigned char *p, int word){
  return *(unsigned*)&p[word*4];
  //return (p[word*4]<<24) | (p[word*4+1]<<16) | (p[word*4+2]<<8) | p[word*4+3];
}


bool CosmicMonitoringGuiEmbedded::setupChannelsFromFile(const char* filename)
{
  std::cout<<"Setting up channels based on first event in file "<<filename<<std::endl;
  eudaq::FileDeserializer fs(filename);
  bool itWorked=false;

  if(fs.HasData()){
    
    eudaq::DetectorEvent* dev=(eudaq::DetectorEvent*)eudaq::EventFactory::Create(fs);
    dev=(eudaq::DetectorEvent*)eudaq::EventFactory::Create(fs); //get the second event in file (first event is just a file header)
    
    if(dev->IsEORE() || dev->IsBORE()) return false;
    eudaq::RawDataEvent::data_t pixblock;

    int rce=-1;
    int link=-1;

    for(unsigned k=0;k<dev->NumEvents();k++){
      
      eudaq::RawDataEvent* rev=(eudaq::RawDataEvent*)dev->GetEvent(k);
      
      int totalBlocks = rev->NumBlocks();
      int currentBlock = 0;
      
      if(rev->GetSubType()=="APIX-CT"){
	eudaq::RawDataEvent::data_t block0=rev->GetBlock(0);
	
	size_t blockSize = block0.size();
	const unsigned tdcSize = 10; //number of 4-byte words of TDC data per RCE
	
	int nTDC = blockSize/(4*tdcSize);
	
	for(int iTDC=0; iTDC<nTDC; iTDC++){
	  int firstWord = iTDC*tdcSize;
	  unsigned rceNum=getWord(&block0[0],firstWord+0);
	  rceNum = rceNum&0xff;
	  addRce(rceNum);
	}
	currentBlock = 1;
	
      }else if(rev->GetSubType()=="CTEL"){
	currentBlock = 0;
	totalBlocks = 1;
      }else{
	//std::cout<<"Unknown data block. Skipping."<<std::endl;
	continue;
      }
      
      
      while(currentBlock<totalBlocks){
	
	pixblock=rev->GetBlock(currentBlock);
	m_indx=0;

	while (m_indx<pixblock.size()){
	  unsigned currentu=nextcurrent(&pixblock[0]);

	  FormattedRecord current(currentu);
	  if(current.isHeader()){
	    link=current.getLink();
	    bool newlink = createLink(link,rce);
	    if(newlink){
	      m_fetype.push_back(4);  //hard-code FEI4 for now
	    }
	  }
	  else if(current.isHeaderTwo()){
	    rce = current.getRCE();
	  }
	  else{
	    continue;
	  }

	}
	currentBlock++;
      } //end loop over all the pixel blocks in event

    } //end loop over all the 'events'
    
    itWorked=true;
  }
  return itWorked;

}


bool CosmicMonitoringGuiEmbedded::readFile(const char* filename,int nevt)
{
  //This reads in a file in the EUDET format

  std::cout<<"Reading file "<<filename<<std::endl;

  m_nev = ntrg = highesttot = 0;
  eudaq::FileDeserializer fs(filename);
  while(fs.HasData()){
    eudaq::DetectorEvent* dev=(eudaq::DetectorEvent*)eudaq::EventFactory::Create(fs);
    if(m_nev>nevt)break;
    //if(nev==10)break;
    //  dev->Print(std::cout);
    addEvent(dev);
  }
  std::cout<<"\n----------Summary---------------------"<<std::endl;
  std::cout<<"Highest ToT: "<<highesttot<<std::endl;
  //need to subtract 2 from m_nev because of the end-of-file and begin-of-file
  std::cout<<(m_nev-2)<<" events. "<<ntrg<<" triggers, summed over all frontends."<<std::endl;

  return true;
}
bool CosmicMonitoringGuiEmbedded::addEvent(const eudaq::DetectorEvent* dev, bool refresh){
  eudaq::RawDataEvent::data_t pixblock;
  if(dev->IsEORE() || dev->IsBORE()) return false;
  //  std::cout<<std::endl<<"Adding a new event!!!"<<std::endl;

  int l1id=-1;
  int link=-1;
  int bxid=0;
  int rce=-1;
  int firstbxid=-1;
  int oldrce=-1;
  int oldbxid[MAX_LINKS];
  std::vector<int> modtot;
  for(int i=0;i<m_nMods;i++){
    modtot.push_back(0);
    m_nhits[i]=0;
    oldbxid[m_outlinks[i]]=-1;
  }
  
  //std::vector<double> timeStamps;
  //for(int i=0; i<m_nRces; i++){
    //timeStamps.push_back(0);
  //}
    
  
  for(unsigned k=0;k<dev->NumEvents();k++){
    eudaq::RawDataEvent* rev=(eudaq::RawDataEvent*)dev->GetEvent(k);

    int totalBlocks = rev->NumBlocks();
    int currentBlock = 0;

    if(rev->GetSubType()=="APIX-CT"){
      m_nev++;
      if((m_nev-1)%10000==0){
	std::cout<<"Event "<<(m_nev-1)<<std::endl;
	if(m_nev>1){
	  for (int i=0;i<m_nMods;i++){
	    if(m_occ[i]==0)continue;
	    float rate=0;
	    if(m_occ[i]->GetEntries()-m_lastcount[i]>0)rate=(m_occ[i]->GetEntries()-m_lastcount[i])/1000;
	    std::cout<<"Hits per event for "<<m_rceNums[i]<<" "<<m_outlinks[i]<<": "<<rate <<std::endl;
	    m_lastcount[i]=m_occ[i]->GetEntries();
	    if((m_nMods>1 && i==1)||m_nMods==1)m_hitsperevent=rate;
	  }
	}
      }
      //  std::cout<<"event "<<m_nev<<" contains "<<dev->NumEvents()<<" 'events' "<<std::endl;

      eudaq::RawDataEvent::data_t block0=rev->GetBlock(0);
      
      size_t blockSize = block0.size();
      const unsigned tdcSize = 10; //number of 4-byte words of TDC data per RCE
      
      int nTDC = blockSize/(4*tdcSize); 
      
      //for(unsigned int i=0;i<(blockSize/4);i++){
      // std::cout<<std::hex<<getWord(&block0[0],i)<<std::dec<<std::endl;
      //}
      
      for(int iTDC=0; iTDC<nTDC; iTDC++){
	
	int firstWord = iTDC*tdcSize;
	
	unsigned rceNum=getWord(&block0[0],firstWord+0);
	rceNum = rceNum&0xff;
	
	unsigned counter1=getWord(&block0[0],firstWord+3);
	unsigned counter2=getWord(&block0[0],firstWord+4);
	unsigned long long counter=counter2;
	counter=(counter<<32) | counter1;
	//printf("%llx\n",counter);
	int bitpos=-1;
	for (int j=0;j<64;j++){
	  if(((counter>>j)&1)==0){
	    bitpos=j;
	    break;
	  }
	} 
	//	if(bitpos!=-1)std::cout<<"Phase is " <<bitpos*.4<<" ns"<<std::endl;
	
	for(int iRce=0; iRce<m_nRces; iRce++){
	  if(m_rces[iRce]==rceNum){
	    if(bitpos!=-1)m_tdc[iRce]->Fill(bitpos);
	    break;
	  }
	}

	for(int iRce=0; iRce<m_nRces; iRce++){
	  if(m_rces[iRce]==rceNum){
	    if(m_hitbus[iRce][0][0]!=0){
	      for(int k=0;k<2;k++){
		for (int l=0;l<3;l++){
		  int edge=hitbusEdge(k==0?counter1 : counter2,l);
		  if(edge!=-1)m_hitbus[iRce][k][l]->Fill(edge);
		}
	      }
	    }
	    break;
	  }
	}
	

	
	unsigned trgtime1=getWord(&block0[0],firstWord+5);
	unsigned trgtime2=getWord(&block0[0],firstWord+6);
	unsigned deadtime1=getWord(&block0[0],firstWord+7);
	unsigned deadtime2=getWord(&block0[0],firstWord+8);
	unsigned long long trgtime=trgtime1;
	trgtime=(trgtime<<32) | trgtime2;
	unsigned long long deadtime=deadtime1;
	deadtime=(deadtime<<32) | deadtime2;
	//unsigned hitbusword=(getWord(&block0[0],firstWord+9)&0xf000000)>>24;
	//std::cout<<"Trigtime "<<trgtime<<std::endl;
	//std::cout<<"Deadtime "<<deadtime<<std::endl;
	//double timeStampSeconds = trgtime * 25.6e-9;
	//std::cout<<"event "<<m_nev<<" on RCE "<<rceNum<<"\t\t Time stamp: "<<timeStampSeconds<<" sec."<<std::endl;
	//rebinTimestamps(timeStampSeconds);
	//	unsigned eudaqtrg=getWord(&block0[0],firstWord+9);
	//        std::cout<<"Eudaq trigger word: "<<eudaqtrg<<std::endl;
	//for(int iRce=0; iRce<m_nRces; iRce++){
          //if(m_rces[iRce]==rceNum){
	    //timeStamps[iRce] = timeStampSeconds;
          //}
        //}
	
	if(m_writeRootFile==true && iTDC==0){
	  m_eventinfo.timestamp=time(NULL);
	  m_eventinfo.framenumber=m_nev;
	  m_eventinfo.triggerTime=trgtime;
	  m_evtree->Fill();
	}
      } //end loop over the different RCE's in the TDC block
      
      currentBlock = 1;
	
    }else if(rev->GetSubType()=="CTEL"){
      currentBlock = 0;
      totalBlocks = 1;
    }else if(rev->GetSubType()=="TEMP"){
      // Temperature readout
      pixblock=rev->GetBlock(0);
      unsigned rce=getWord(&pixblock[0],0);
      unsigned char *chanmap=(unsigned char*)&pixblock[4];
      unsigned short *adc=(unsigned short*)&pixblock[12];
      unsigned timenow=(unsigned)time(0);
      int timediffsec=timenow-m_starttime_sec;
      for(int i=0;i<8;i++){
	float res=0;
	int chan=chanmap[i]; // remapped to outlinks.
	int mod = m_linkToIndex[rce][chan];
	if(mod==-1 || m_tempsec[mod]==0)continue;
	float reading=float(adc[i]&0xfff);
	if(reading==0)res=0;
	else res=(4095./reading-1)*10000-100;
	float temperature=-273;
	float b=3435;
	if(res!=0)temperature=b*298.15/(b+log(res/10000)*298.15)-273.15;
	if(timediffsec%60<m_lasttime[mod]){
	  float newval=0;
	  if(m_tempsec[mod]->GetEntries()>0)newval=m_tempsec[mod]->GetSumOfWeights()/m_tempsec[mod]->GetEntries();
	  m_graphmutex.Lock();
	  m_tempmin[mod]->SetPoint(m_tempmin[mod]->GetN(), (float)timediffsec/60, newval);
	  m_graphmutex.UnLock();
	  m_tempsec[mod]->Reset();
	}
	m_lasttime[mod]=timediffsec%60;
	if(m_tempsec[mod]->GetBinContent(timediffsec%60)==0){ //sometimes 2 readings in the same bin
	  m_tempsec[mod]->SetBinContent(timediffsec%60, temperature);
	  if(m_logtemps==true)*m_tempfile<<timenow<<" "<<rce<<" "<<chan<<" "<<temperature<<std::endl;
	}
      }
      if (refresh) updateDisplay();
      return true;
    }else{
      //std::cout<<"Unknown data block. Skipping."<<std::endl;
      continue;
    }
    
    while(currentBlock<totalBlocks){
      
      pixblock=rev->GetBlock(currentBlock);    
      m_indx=0;
      
      while (m_indx<pixblock.size()){
	unsigned currentu=nextcurrent(&pixblock[0]);
	
	FormattedRecord current(currentu);
	if(current.isHeader()){
	  ntrg++; //really, this is the total number of triggers summed over all the modules
	  link=current.getLink();
	  int l1id_temp=current.getL1id()&0xf;
	  bxid=current.getBxid()&0xff;
	  if(l1id==-1){
	    l1id = l1id_temp;
	  } else{
	    if(l1id_temp != l1id){
	      std::cout<<"Not all data in event has same l1id!! FATAL ERROR!"<<std::endl;
	      assert(0);
	    }
	  }
	  if(firstbxid==-1){
	    firstbxid = bxid;
	  }
	  
	  if( oldbxid[link]!=-1 ){
	    int bxid_expected = oldbxid[link] + 1;
	    if(bxid_expected==256){
	      bxid_expected=0; 
	    }
	    if(bxid!=bxid_expected){
	      std::cout<<"WARNING:  RCE "<<rce<<" link "<<link<<" has bxid = "<<bxid;
	      std::cout<<" ; expected bxid = "<<bxid_expected<<std::endl;
	    }
	  } else{
	    //this is the first data from this rce/link in this event.
	    if(bxid!=firstbxid){
	      std::cout<<"ERROR:  not all the modules in RCE "<<rce<<" in this event have the ";
	      std::cout<<"same initial bxid.  FATAL ERROR."<<std::endl;
	      assert(0);
	    }
	    
	  }
	  oldbxid[link] = bxid;
	}
	else if(current.isHeaderTwo()){
	  rce = current.getRCE();
	  if(rce != oldrce){
	    oldrce=rce;
	    for(int i=0;i<m_nMods;i++)oldbxid[m_outlinks[i]]=-1;
	    firstbxid=-1;  //Modules from different rce's can have different
	                      //bxid's, even if they have the same l1id
	  }

	}else if(current.isData()){
	  m_nhit++;
	  int mod = m_linkToIndex[rce][link];
	  int chip=0;
	  int tot=0;
	  int col=0;
	  int row=0;
	  int x=0;
	  int y=0;
	  int diffbx=0;
	  if(m_fetype[mod]==3 || m_fetype[mod]==4){
	    chip=current.getFE();
	    tot=current.getToT();
	    if(tot>highesttot)highesttot=tot;
	    col=current.getCol();
	    row=current.getRow();
	    diffbx=bxid-firstbxid;
	    if (diffbx<0)diffbx+=256;
	    if (chip<8){
	      x=18*chip+col;
	      y=row;
	    } else {
	      x=(15-chip)*18+17-col;
	      y= 319-row;
	    }
	    if(m_occ[mod]!=0){
	      m_occ[mod]->Fill(x,y);
	      modtot[mod]+=tot;
	      m_bx[mod]->Fill(diffbx);
	    }
	    if(m_projx[mod]!=0){
	      int nx=m_projx[mod]->GetNbinsX();
	      double scale=0.25;
	      if(nx==144)scale=0.4;
	      m_projx[mod]->Fill(x*scale+scale/2);
	      m_projy[mod]->Fill(0.05*y+0.025);
	    }
	  }else if (m_fetype[mod]==11){ //AFP HPTDC
	    unsigned word=current.getWord();
	    unsigned tdcid=(word>>24)&0xf;
	    if (tdcid>0xc || tdcid<0xa){ //TDC ids are a, b, c
	      std::cout<<"Bad TDC id"<<std::endl;
	    }else{
	      unsigned chan=(word>>22)&0x3;
	      x=(tdcid-0xa)*4+chan;
	      y=(word>>28)&0x1;
	      tot=word&0x1fffff; //10 bits
	      if(m_afptdc[mod]!=0){
	        m_afptdc[mod][x]->Fill(tot&0x3ff);
	      }
	    } 
	  }
	  if(m_nhits[mod]<MAXHIT){
	    m_pixx[mod][m_nhits[mod]]=x;
	    m_pixy[mod][m_nhits[mod]]=y;
	    m_value[mod][m_nhits[mod]]=tot;
	    m_timing[mod][m_nhits[mod]]=diffbx;
	    m_nhits[mod]++;
	  }else{
	    std::cout<<"MORE THAN "<<MAXHIT<<" hits in module RCE "<<m_rceNums[mod]<<" outlink "<<m_outlinks[mod]<<std::endl;
	  }
	}

      }//end loop over data in the pixel block
    
      currentBlock++;
    } //end loop over all the pixel blocks in event
    
  } //end loop over all the events

  
  for(int i=0;i<m_nMods;i++){
    
    if(m_toth[i]!=0 && modtot[i]!=0)m_toth[i]->Fill(modtot[i]);
    
    //double theTimeStamp=0;
    //unsigned rceNum = m_rceNums[i];
    
    //for(int iRce=0; iRce<m_nRces; iRce++){
      //if(m_rces[iRce]==rceNum){
	////std::cout<<"getting timestamp for iRce = "<<iRce<<std::endl;
	//theTimeStamp = timeStamps[iRce];
      //}
    //}
    
    //if(modtot[i]>100){
      // std::cout<<"filling stamp for mod "<<i<<" with "<<theTimeStamp<<std::endl;
      //std::cout<<m_timestamp[i]<<std::endl;
    //if(m_timestamp[i]!=0)m_timestamp[i]->Fill(theTimeStamp,modtot[i]);
      //}
    
    //if(modtot[i]>100){
      //std::cout<<"Noisy hit on event "<<m_nev<<", timestamp "<<theTimeStamp<<" s, with ToT on mod "<<i<<" = "<<modtot[i]<<std::endl;
    //}

    if(m_corxx[i]!=0){
      int j=m_corrindex[i];
      for(int iHit=0; iHit<m_nhits[i]; iHit++){
	for(int jHit=0; jHit<m_nhits[j]; jHit++){
	  
	  int x1 = m_pixx[i][iHit];
	  int y1 = m_pixy[i][iHit];
	  int x2 = m_pixx[j][jHit];
	  int y2 = m_pixy[j][jHit];
	  
	  m_corxx[i]->Fill(x1,x2);
	  m_corxy[i]->Fill(x1,y2);
	  m_coryy[i]->Fill(y1,y2);
	  m_coryx[i]->Fill(y1,x2);
         
	} 
      }
    }
    if(m_writeRootFile==true){
      m_pltree[i]->Fill();
    }
  }
    
  if (refresh) updateDisplay();
  
  return true;
  
}

void CosmicMonitoringGuiEmbedded::addRce(unsigned int rceNum){
  
  int rceSize = m_rces.size();
  bool newRce=true;
  for(int iRce=0; iRce<rceSize; iRce++){
    if(m_rces[iRce]==rceNum){
      newRce=false;
      break;
    }
  }
  if(newRce){
    std::cout<<"Adding rce "<<rceNum<<std::endl;
    m_rces.push_back(rceNum);
  }
  m_nRces=m_rces.size();

}

void CosmicMonitoringGuiEmbedded::printModules(){

  for (unsigned int i=0; i<m_config.size(); i++){
    
    if(m_config[i]->isIncluded()){
      //valid=true;
      unsigned outlink=m_config[i]->getOutLink();
      unsigned rce=m_config[i]->getRce();
      std::cout<<"Module "<<i<<" :   rce = "<<rce<<", outlink = "<<outlink<<std::endl;
    }
  }

}

bool CosmicMonitoringGuiEmbedded::createLink(unsigned outlink, unsigned rceNum){
  bool newLink=false;

  if(m_linkmap.find(outlink + 1000*rceNum) != m_linkmap.end()){
    //std::cout<<"Already have link "<<outlink<<" rce "<<rceNum<<std::endl;
  }
  else{
    std::cout<<"Making link "<<outlink<<" rce "<<rceNum<<std::endl;
    
    m_linkmap[outlink + 1000*rceNum] = m_nMods;
    if(m_linkToIndex[rceNum]==0){
      m_linkToIndex[rceNum]=new int[MAX_LINKS];
      for(int i=0;i<MAX_LINKS;i++)m_linkToIndex[rceNum][i]=-1;
    }
    m_linkToIndex[rceNum][outlink]=m_nMods;
    m_outlinks.push_back(outlink);
    m_rceNums.push_back(rceNum);
    m_nMods++;
    newLink=true;  
  }

  return newLink;
  
}

void CosmicMonitoringGuiEmbedded::saveHistos(int runnum, const char* datadir){
  
  char filename[512];
  sprintf(filename, "%s/cosmic_%06d/monitoringHistograms.root", datadir, runnum);
  TFile histofile(filename, "recreate");
  for (int i=0;i<m_nMods;i++){
    if (m_tempmin[i] != 0) m_tempmin[i]->Write();
    if (m_tempsec[i] != 0) m_tempsec[i]->Write();
    if (m_occ[i] != 0) m_occ[i]->Write();
    if (m_toth[i] != 0) m_toth[i]->Write();
    if (m_bx[i] != 0) m_bx[i]->Write();
    //if (m_timestamp[i]!=0) m_timestamp[i]->Write();
    if (m_corxx[i] != 0) m_corxx[i]->Write();
    if (m_corxy[i] != 0) m_corxy[i]->Write();
    if (m_coryy[i] != 0) m_coryy[i]->Write();
    if (m_coryx[i] != 0) m_coryx[i]->Write();
    if (m_projx[i] != 0) m_projx[i]->Write();
    if (m_projy[i] != 0) m_projy[i]->Write();
    if (m_afptdc[i]!=0){
      for (int j=0;j<12;j++){
	m_afptdc[i][j]->Write();
      }
    }
  }
  for(int i=0; i<m_nRces; i++){
    if (m_tdc[i]!=0) m_tdc[i]->Write();    
    for (int k=0;k<2;k++){
      for (int l=0;l<3;l++){
	if(m_hitbus[i][k][l]!=0)m_hitbus[i][k][l]->Write();
      }
    }
  }
}

void CosmicMonitoringGuiEmbedded::saveScreenshot(int runnum, const char* datadir){
  
  char filename[512];
  sprintf(filename, "%s/cosmic_%06d/screenshot_%d.png", datadir, runnum, m_nScreenshots++);
  std::cout<<filename<<std::endl;
  m_canvas->SaveAs(filename);
}
 
void CosmicMonitoringGuiEmbedded::rebinTimestamps(double timeStamp){
  
  while( (timeStamp + 1) > m_timeStampLimit ){

    m_timeStampLimit = m_timeStampLimit*2;

    //std::cout<<"Timestamp limit changed to "<<m_timeStampLimit<<std::endl;

    for(int imod=0; imod<m_nMods; imod++){
      if(m_timestamp[imod]!=0){
	const int nbins =  m_timestamp[imod]->GetNbinsX();
	double xmin = m_timestamp[imod]->GetBinLowEdge(1);
	double xmax = m_timestamp[imod]->GetBinLowEdge(nbins+1);
	double newxmax = 2*(xmax-xmin) + xmin;
	
	double yvals[nbins];
	
	//std::cout<<"Originally:"<<std::endl;
	//for(int jbin=0; jbin < nbins; jbin++){
	//	std::cout<<"jbin+1: "<<jbin+1<<"   = "<<m_timestamp[imod]->GetBinContent(jbin+1)<<std::endl;
	// }
	
	
	for(int jbin=0; jbin < nbins; jbin++){
	  yvals[jbin] = 0;
	}
	for(int jbin=0; jbin < (nbins/2); jbin++){
	  int bin1 = jbin*2 + 1;
	  yvals[jbin] = m_timestamp[imod]->GetBinContent(bin1) +  m_timestamp[imod]->GetBinContent(bin1+1);
	}
	
	m_timestamp[imod]->SetBins(nbins,xmin,newxmax);
	
	for(int jbin=0; jbin < nbins; jbin++){
	  m_timestamp[imod]->SetBinContent(jbin+1,yvals[jbin]);
	}

      //std::cout<<"Now:"<<std::endl;
      // for(int jbin=0; jbin < nbins; jbin++){
      //	std::cout<<"jbin+1: "<<jbin+1<<"  = "<<m_timestamp[imod]->GetBinContent(jbin+1)<<std::endl;
      // }

      }  
    }


  }

}

void CosmicMonitoringGuiEmbedded::setupRootFile(const char* rootfile){
  m_evfile=new TFile(rootfile, "recreate");
  m_pltree=new TTree*[m_nMods];
  for (int i=0;i<m_nMods;i++){
     char name[128];
     sprintf(name, "Plane%d", i);
     m_evfile->cd();
     m_evfile->mkdir(name);
     m_evfile->cd(name);
     m_pltree[i]=new TTree("Hits", "Hits"); 
     m_pltree[i]->Branch("NHits", &m_nhits[i], "NHits/I");
     m_pltree[i]->Branch("PixX", m_pixx[i], "HitPixX[NHits]/I");
     m_pltree[i]->Branch("PixY", m_pixy[i], "HitPixY[NHits]/I");
     m_pltree[i]->Branch("Value", m_value[i], "HitValue[NHits]/I");
     m_pltree[i]->Branch("Timing", m_timing[i], "HitTiming[NHits]/I");
     m_pltree[i]->Branch("HitInCluster", m_hitincluster[i], "HitInCluster[NHits]/I");
     m_pltree[i]->Branch("PosX", m_posx[i], "HitPosX[NHits]/D");
     m_pltree[i]->Branch("PosY", m_posy[i], "HitPosY[NHits]/D");
     m_pltree[i]->Branch("PosZ", m_posz[i], "HitPosZ[NHits]/D");
  }
  m_evfile->cd();
  m_evtree=new TTree("Event", "Event information"); 
  m_evtree->Branch("TimeStamp", &m_eventinfo.timestamp, "TimeStamp/l");
  m_evtree->Branch("FrameNumber", &m_eventinfo.framenumber, "FrameNumber/l");
  m_evtree->Branch("TriggerOffset", &m_eventinfo.triggeroffset, "TriggerOffset/I");
  m_evtree->Branch("TriggerInfo", &m_eventinfo.triggerinfo, "TriggerInfo/I");
  m_evtree->Branch("Invalid", &m_eventinfo.invalid, "Invalid/O");
  m_evtree->Branch("TriggerTime", &m_eventinfo.triggerTime, "TriggerTime/l");
  m_evfile->Write();
  
}
void CosmicMonitoringGuiEmbedded::closeRootFile(){
  usleep(500000);
  m_evfile->Write();
  m_evfile->Close();
  m_evfile->Delete();
  delete [] m_pltree;
}
void CosmicMonitoringGuiEmbedded::closeTempFile(){
  m_tempfile->close();
  delete m_evfile;
} 
