#ifndef DELAYSCANDATAPROC_HH
#define DELAYSCANDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include <vector>
#include "util/RceHisto1d.cc"

class DelayScanDataProc: public AbsDataProc{
public:
  DelayScanDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~DelayScanDataProc();
  int processData(unsigned link, unsigned *data, int size);
private:
  std::vector<std::vector<RceHisto1d<short, short>*> > m_histo;

};

#endif
