#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/MonleakDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "config/FEI4/FEI4BRecord.hh"
#include "config/FEI4/PixelRegister.hh"
using namespace FEI4;

int MonleakDataProc::fit(std::string fitfun) {
  std::cout << "Running: " << fitfun << std::endl;
  if(fitfun=="NORMALIZE") {
    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      for(int j=0;j<m_nPoints;j++){
	//	float n=(float)(*m_histo_occ[module])(j);
	std::cout<<"Fit function currently not working, will need to change eventually to work with 2d histos"<<std::endl;
	//	float n=1;
	//	float mean=0;

	/*
	if(n>0)mean=(float)(*m_histo_adc[module])(j)/n;
	m_histo_norm[module]->set(j, mean);
	float err=0;
	if(n>0)err=((*m_histo_adc2[module])(j)/n-mean*mean)/n; //error^2 on mean
	if(err<0)err=0; //prevent rounding errors
	m_histo_norm[module]->setBinError(j, err);
	//	  std::cout<<"Bin content is "<<(*m_histo[module][i])(j)<<std::endl;
	//std::cout<<"Bin error is "<<m_histo[module][i]->getBinError(j)<<std::endl;
	*/

      }
    }
  }
  return 0;
}

int MonleakDataProc::processData(unsigned link, unsigned *buffer, int buflen){
  //m_timer.Start();
  if(buflen==0)return 0;
  int nL1A=0;
  int module=m_linkToIndex[link];
  unsigned char* bytepointer=(unsigned char*)buffer;
  unsigned char* last=bytepointer+buflen*sizeof(unsigned)-3;
  FEI4::FEI4BRecord rec;
  while(bytepointer<=last){
    rec.setRecord(bytepointer); 
    if(rec.isData()){ //includes empty record type
      //should not happen
    } else if(rec.isDataHeader()){
      //should not happen, either
    }else if(rec.isServiceRecord()){
      //no service records
    }else if(rec.isValueRecord()){ //val rec without addr rec
      int value=rec.getValue()>>4;
      
      //std::cout<<"Link "<<link<<" value record! Value="<<std::hex<<rec.getValue()<<std::dec<<std::endl;
      
      fillHistogram(m_histo_occ[module], m_currentMaskStage, 1);
      fillHistogram(m_histo_adc[module], m_currentMaskStage, value);
      fillHistogram(m_histo_adc2[module], m_currentMaskStage, value*value);
      
      nL1A++;
    }else if(rec.isAddressRecord()){ // address record
      // Ignore address records
      //std::cout<<"FE "<<m_info[module].getId()<<": Address record for ";
      //if(rec.isGlobal())std::cout<<" global register ";
      //else std::cout<<" shift register ";
      //std::cout<<rec.getAddress()<<std::endl;
      //std::cout<<"This should not happen."<<std::endl;
    }else{
      std::cout<<"FE "<<m_info[module].getId()<<": Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
      //return FEI4::FEI4ARecord::BadRecord;
      return 0;
    }
    bytepointer+=3;
  }
  //m_timer.Stop();
  return nL1A;
}

MonleakDataProc::MonleakDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif),m_fit(0) {
  std::cout<<"Monleak Data Proc"<<std::endl;
  try{ //catch bad scan option parameters
    m_nLoops = scanOptions->get<int>("nLoops");
    /* there is at least one parameter loop */
    m_nPoints=1;
    if(m_nLoops>0){         
      m_nPoints=scanOptions->get<int>("scanLoop_0.nPoints");
      for(int i=0;i<m_nPoints;i++) {
	char pointname[10];
	sprintf(pointname,"P_%d",i);
	int vcal=scanOptions->get<int>(std::string("scanLoop_0.dataPoints.")+pointname);
	//std::cout << "point vcal " << vcal << std::endl;
	m_vcal.push_back(vcal);
      }
    }
    m_nTrigger=scanOptions->get<int>("trigOpt.nEvents");
    
    m_maskstageType=scanOptions->get<std::string>("maskStages");  //this is m_maskStageTotalSteps from PixScanBase_presetRCE.h
    
    std::cout<<"Initializing MonleakDataProc with maskstagetype = "<<m_maskstageType<<std::endl;


    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      char name[128];
      char title[128];
      
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      
      RceHisto2d<int, int> *histoOcc;
      RceHisto2d<int, int> *histoAdc;
      RceHisto2d<int, int> *histoAdc2;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      
      /* retrieve scan points - Vcal steps in this case */
      sprintf(name, "Mod_%d_nEvents", moduleId);
      sprintf(title, "Module %d at %s Number of events", moduleId, moduleName.c_str());     
      histoOcc=new RceHisto2d<int, int>(name,title,rows,0,rows,cols,0,cols, true);
      
      sprintf(name, "Mod_%d_ADC", moduleId);
      sprintf(title, "Module %d at %s ADC", moduleId, moduleName.c_str());
      histoAdc=new RceHisto2d<int, int>(name,title,rows,0,rows,cols,0,cols, true);
      
      sprintf(name, "Mod_%d_ADC2", moduleId);
      sprintf(title, "Module %d at %s ADC^2", moduleId, moduleName.c_str());
      histoAdc2=new RceHisto2d<int, int>(name,title,rows,0,rows,cols,0,cols, true);

      if(m_info[module].getNFrontends()==1){
	histoOcc->setAxisTitle(1,"Column");
	histoAdc->setAxisTitle(1,"Column");
	histoAdc2->setAxisTitle(1,"Column");
      }
      else{
	histoOcc->setAxisTitle(1,"FE*N_COL+Column");
	histoAdc->setAxisTitle(1,"FE*N_COL+Column");
	histoAdc2->setAxisTitle(1,"FE*N_COL+Column");
      }
      histoOcc->setAxisTitle(0, "Row");
      histoAdc->setAxisTitle(0, "Row");
      histoAdc2->setAxisTitle(0, "Row");
      m_histo_occ.push_back(histoOcc);
      m_histo_adc.push_back(histoAdc);
      m_histo_adc2.push_back(histoAdc2);
      
      sprintf(name, "Mod_%d_Voltage", moduleId);
      sprintf(title, "Module %d at %s Voltage", moduleId, moduleName.c_str());
      m_histo_norm.push_back(new RceHisto1d<float, float>(name, title, m_nPoints, 0, m_nPoints, true));
      m_histo_norm.back()->setAxisTitle(0, "Set point");
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
}

MonleakDataProc::~MonleakDataProc(){
  for (size_t module=0;module<m_histo_occ.size();module++){
    delete m_histo_occ[module];
    delete m_histo_adc[module];
    delete m_histo_adc2[module];
    delete m_histo_norm[module];
  }

}


int MonleakDataProc::fillHistogram(RceHisto2d<int, int>* hist, int maskStage, int value){

  if(m_maskstageType=="FEI4_26880"){
    int row = maskStage%PixelRegister::N_ROWS;
    int col = maskStage/PixelRegister::N_ROWS;

    //now I need to write code to translate maskStage into row and col
    //std::cout<<"I'm filling histogram row,col = "<<row<<" , "<<col<<" with val = "<<value<<std::endl;
    hist->fill(row,col,value);

  }
  else{
    return 0;
  }
  
  return 1;
  
}

