#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/HitorDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"


int HitorDataProc::processData(unsigned link, unsigned *data, int size){
  //    std::cout<<"Process data"<<std::endl;
  int module=m_linkToIndex[link]; 
  int col=m_currentMaskStage/m_info[module].getNRows();
  int row=m_currentMaskStage%m_info[module].getNRows();
  m_histo_occ[module][0]->increment(col,row);
  return 0;
}

HitorDataProc::HitorDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif){
  std::cout<<"Hitor Data Proc"<<std::endl;
  try{ //catch bad scan option parameters

    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      std::vector<RceHisto2d<short, short>* > vh;
      m_histo_occ.push_back(vh);
      char name[128];
      char title[128];
      RceHisto2d<short, short> *histo;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      std::cout<<"Creating Hitor histograms."<<std::endl;
      sprintf(title,"OCCUPANCY Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Hitor", moduleId);
      histo=new RceHisto2d<short, short>(name,title,cols,0,cols,rows,0,rows);
      if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
      else histo->setAxisTitle(0,"FE*N_COL+Column");
      histo->setAxisTitle(1, "Row");
      m_histo_occ[module].push_back(histo);
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
}

HitorDataProc::~HitorDataProc(){
  for (size_t module=0;module<m_histo_occ.size();module++)
    for(size_t i=0;i<m_histo_occ[module].size();i++)delete m_histo_occ[module][i];

}
  

