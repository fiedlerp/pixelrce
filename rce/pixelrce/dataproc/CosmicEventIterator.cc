
#include "dataproc/CosmicEventIterator.hh"
#include "dataproc/CosmicEvent.hh"
#include <iostream>

namespace{
  int badEventThreshold = 3;
}

CosmicEventIterator::CosmicEventIterator(bool monitor, eudaq::FileSerializer* fs, std::ofstream *pfile, unsigned runno, int nMod, std::vector<int> rces)
  : m_runNo(runno), m_nMod(nMod), m_rces(rces), m_monitor(monitor), m_file(fs), m_pfile(pfile),
    m_99(true), m_101(false), m_badEvents(0), m_nEvents(1){
  for(int i=0;i<m_nMod;i++) {
    m_modit.push_back(std::list<CosmicEvent*>::iterator());
  }

  m_nRce = m_rces.size();
  for(int i=0;i<m_nRce;i++) {
    m_tdcit.push_back(std::list<CosmicEvent*>::iterator());
  }

  m_refevent=new CosmicEvent(m_runNo,1,m_nMod,m_rces); // 1 is the first L1id
  m_events.push_back(m_refevent);
  set(m_events.begin());
  CosmicEvent::synch(false);
}

CosmicEventIterator::~CosmicEventIterator(){
  if((*(m_events.begin()))!=m_refevent)delete m_refevent;
  std::list<CosmicEvent*>::iterator it;
  for(it=m_events.begin();it!=m_events.end();it++){
    delete (*it);
  }
}

void CosmicEventIterator::resynch(){
  std::cout<<"CosmicEventIterator::resynch()"<<std::endl;
  CosmicEvent::synch(true);
  if((*(m_events.begin()))!=m_refevent){
    delete m_refevent;
    std::cout<<"First event is not refevent"<<std::endl;
  }
  std::list<CosmicEvent*>::iterator it;
  int i=0;
  for(it=m_events.begin();it!=m_events.end();it++){
    std::cout<<"Tossing event with L1A id="<<(*it)->getL1A()<<std::endl;
    //(*it)->print();
    delete (*it);
    i++;
  }
  std::cout<<"CosmicEventIterator: Tossed "<<i<<" events."<<std::endl;
  m_events.clear();
  m_refevent=new CosmicEvent(m_runNo,1,m_nMod,m_rces); // 1 is the first L1id
  m_events.push_back(m_refevent);
  set(m_events.begin());
}
inline void CosmicEventIterator::set(std::list<CosmicEvent*>::iterator it){
  for(int i=0;i<m_nRce;i++)m_tdcit[i]=it;
  for(int i=0;i<m_nMod;i++)m_modit[i]=it;
}

bool CosmicEventIterator::setTdcData(unsigned *data, int rceIndex){
  //return false: indicates that resynching is necessary

  unsigned current=data[0]>>16; // l1id
  //unsigned bxid=current&0xff;
  unsigned l1id=(current>>8)&0xf;
  current|=0x200a0000; //link 10 and header word marker
  unsigned l1a=(*m_tdcit[rceIndex])->getL1A();
  if(l1a!=l1id){
    std::cout<<"TDC Event missing. Got L1id "<<l1id<<", was expecting "<<l1a<<std::endl;
    return false;
  }

  (*m_tdcit[rceIndex])->setTdcData(current, &data[0], rceIndex);
  bool success=checkForNewEvent(l1id,m_tdcit[rceIndex]);
  if(success==false)return false;
  m_tdcit[rceIndex]++;
  return checkForWrite();
}

bool CosmicEventIterator::addPixelData(int mod, unsigned l1id, int bxfirst, int bxlast, int ntrg, int rceIndex, 
				       bool isdut, unsigned* data, unsigned size){
  CosmicEvent *ev=*m_modit[mod];
  unsigned l1a=ev->getL1A();
  if(l1a!=l1id){
    if((l1id==0&&l1a==15) || (l1id==l1a+1)){
      bool success=checkForNewEvent(l1a,m_modit[mod]);
      if(success==false){
	std::cout<<"bad check"<<std::endl;
	return false;
      }
      m_modit[mod]++;
      ev=*m_modit[mod];
      if(checkForWrite()==false)return false;
    }else{
      std::cout<<"Module "<<mod<<" Event missing. Got L1id "<<l1id<<", was expecting "<<l1a<<std::endl;
      return false;
    }
  }
  if(ntrg>0)ev->incrementTrigger(mod, ntrg, bxfirst, bxlast, rceIndex);
  if(isdut==true)ev->appendDutData(data,size,rceIndex);
  else ev->appendCtelData(data,size);
  return true;
}
void CosmicEventIterator::flushEvents(){
  int eventswritten=0;
  int badevents=0;
  while(!m_events.empty()){
    CosmicEvent* ev=m_events.front();
    m_events.pop_front();
    if(ev->consistent(m_refevent)){
      std::cout<<"Flushing event with L1id="<<ev->getL1A()<<std::endl;
      ev->writeEvent(m_monitor, m_file, m_pfile, m_nEvents);
      eventswritten++;
    }else{
      badevents++;
    }
    if(ev!=m_refevent)delete ev;
  }
  std::cout<<"Wrote "<<eventswritten<<" events and discarded "<<badevents<<std::endl;
}
    
inline bool CosmicEventIterator::checkForWrite(){
  bool retval=true;
  while(!m_events.empty() && inUse(m_events.begin())==false){
    CosmicEvent* ev=m_events.front();
    m_events.pop_front();
    if(ev->consistent(m_refevent)){
      //ev->print();
      ev->writeEvent(m_monitor, m_file, m_pfile, m_nEvents);
      m_badEvents=0;
    }else{ //inconsistent event.
      m_badEvents++;
      if(m_badEvents>badEventThreshold){
	m_badEvents=0;
	retval=false;
	ev->print();
	std::cout<<"Bad consistency for more than "<<badEventThreshold<<" events in a row. Resynchronizing"<<std::endl;
      } else{
	ev->print();
	std::cout<<"Bad consistency for "<<m_badEvents<<" events in a row. Tossing event."<<std::endl;
      }
    }
    if(ev!=m_refevent)delete ev;
  }
  return retval;
} 

inline bool CosmicEventIterator::inUse(std::list<CosmicEvent*>::iterator it){
  
  for (int i=0;i<m_nRce;i++){
    if(m_tdcit[i]==it){
      return true;
    }
  }
  for (int i=0;i<m_nMod;i++){
    if(m_modit[i]==it){
      return true;
    }
  }
  return false;
}

inline bool CosmicEventIterator::checkForNewEvent(unsigned l1a, std::list<CosmicEvent*>::iterator it){
  //return false only if we need to create new event AND we don't have room to store it
  it++;
  if(it==m_events.end()){ // need new event
    unsigned nextl1a= l1a==15 ? 0 : l1a+1;
    m_events.push_back(new CosmicEvent(m_runNo, nextl1a, m_nMod, m_rces));
    if(m_events.size()==1001){
      std::cout<<"Now exceeding 1000 events in list. Resynching."<<std::endl;
      return false;
    }
  }
  return true;
}
