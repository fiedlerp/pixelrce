#include <boost/property_tree/ptree.hpp>
#include <regex>
#include <boost/algorithm/string.hpp>
#include "dataproc/CosmicDataProc.hh"
#include "dataproc/Channeldefs.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "dataproc/DataProcFactory.hh"
#include "dataproc/CosmicEvent.hh"
#include "dataproc/CosmicEventIterator.hh"
#include "dataproc/Channeldefs.hh"
#include "eudaq/Event.hh"
#include "eudaq/DataSender.hh"
#include "eudaq/DataSenderIF.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/RawDataEvent.hh"

#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <memory>

int CosmicDataProc::fit(std::string command){
  if(command=="CloseFile"){
    std::cout<<"Number of fragments processed: "<<m_nfrag<<std::endl;
    if(m_file!=0){
      sleep(1); // wait for any left over data to trickle in
      eudaq::DetectorEvent dev(m_runNo,m_nEvents,0);
      dev.SetFlags(eudaq::Event::FLAG_EORE);
      eudaq::RawDataEvent *revc=new eudaq::RawDataEvent(eudaq::RawDataEvent::EORE("CTEL",m_runNo,m_nEvents));
      std::shared_ptr<eudaq::Event> cpc(revc);
      dev.AddEvent(cpc);
      eudaq::RawDataEvent *revd=new eudaq::RawDataEvent(eudaq::RawDataEvent::EORE("APIX-CT",m_runNo,m_nEvents));
      std::shared_ptr<eudaq::Event> cpd(revd);
      dev.AddEvent(cpd);
      dev.Serialize(*m_file);
      delete m_file;
      m_file=0;
    }
    //    m_file.close();
    std::cout<<"close file called"<<std::endl;
  }
  return 0;
}

int CosmicDataProc::processData(unsigned rlink, unsigned *data, int size){
  m_nfrag++;
  if((m_nfrag&0xffff)==0)usleep(1); //give external commands a chance to execute from time to time
  int link=rlink&LINKMASK;
  bool marker=(rlink>>MARKERPOS)&0x1;
  m_hits=0;
  //  std::cout<<"DataProc Link "<<link<<" size "<<size<<std::endl;
  // if synching throw away fragments until we find the marker
  if(m_linksynch[link]==true) {
    if (marker==false){
      if(m_print>0){
	if(link==TDCREADOUT)std::cout<<"Ignoring TDCREADOUT L1A: "<<(data[1]>>24)<<std::endl;
	else {
	  FormattedRecord fr(data[0]);
	  std::cout<<"Ignoring Link "<<link<<" L1A: "<<fr.getL1id()<<" bxid: "<<fr.getBxid()<<std::endl;
	}
	m_print--;
      }
      m_tossed[link]++;
      return 0;
    }
    m_linksynch[link]=false;
    //if(marker)std::cout<<"Markerevent"<<std::endl;
    std::cout<<"Tossed "<<m_tossed[link]<<" fragments for link "<<link<<std::endl;
  }
  if (link==TDCREADOUT){
     // unsigned trgtime1=data[3];
     // unsigned trgtime2=data[4];
     // unsigned long long trgtime=trgtime1;
     // trgtime=(trgtime<<32) | trgtime2;

     // std::cout<<"Trigger time: "<<std::hex<<trgtime<<std::dec<<std::endl;
    // std::cout<<"Link 10 L1id "<<((data[0]>>24)&0xf)<<std::endl;
    //This doesn't work with multiple RCE's yet--BL
    bool success=m_iterator->setTdcData(data,0);
    if(success==false){
      resynch();
      return 0;
    }
  }else{
    int module=m_linkToIndex[link]; //map link onto module index
    unsigned int l1id=99;
    int ntrg=0;
    int bxfirst=-666;
    int bxlast=-555;
    int start=0;
    do{ //possibly we must split the data because it belongs to 2 triggers. 
      //if(start!=0)std::cout<<"Newstart "<<start<<std::endl;
      int newstart=processModuleData(&data[start],size-start,link, l1id,bxfirst,bxlast,ntrg);
      //if(m_hits>0) std::cout<<"Link "<<link<<" had "<<m_hits<<" hits."<<std::endl;
      // std::cout<<"Data from module "<<module<<" link="<<link<<" with l1id="<<l1id<<" ntrg="<<ntrg<<std::endl;
      bool isdut=((link<9) || (m_file==0)); //all data is DUT when running as a producer (m_file==0)

      //This doesn't work with multiple RCE's yet--BL
      bool success=m_iterator->addPixelData(module,l1id, bxfirst, bxlast, ntrg, 0, isdut,&data[start],newstart);
      if(success==false){
	resynch();
	return 0;
      }
      start+=newstart;
    }while(start<size);
  }
  return 0;
}

int CosmicDataProc::processModuleData(unsigned* udata,int size, int link, unsigned& l1id, int& bxfirst, int &bxlast, int &ntrg){
  FormattedRecord* data=(FormattedRecord*)udata;
  if(!data[0].isHeader()){
    std::cout<<"Module data not starting with 0x2xxxxxxx"<<std::endl;
    return -1;
  }
  //int bx;
  ntrg=1;
  data[0].setLink(link);
  bxfirst=data[0].getBxid()&0xff;
  bxlast=bxfirst;
  l1id=data[0].getL1id()&0xf;
  //std::cout<<"Link "<<link<<" l1a "<<l1id<<" bx "<<bx<<std::endl;
  unsigned l1a;
  for (int i=1;i<size;i++){
    if (data[i].isHeader()){
      data[i].setLink(link);
      l1a=data[i].getL1id()&0xf;
      bxlast=data[i].getBxid();
      //std::cout<<"Link "<<link<<" l1a "<<l1a<<" bx "<<bx<<std::endl;
      if(l1id!=l1a)return i;
      ntrg++;
        }
    //}else if(data[i]&0x80000000){
         //       //m_hits++;
         //     int chip=(data[i]>>24)&0xf;
         //     int tot=(data[i]>>16)&0xff;
         //     int col=(data[i]>>8)&0x1f;
         //     int row=data[i]&0xff;  	   
         //     int tr=bx-bxfirst;
         //     if (tr<0)tr+=256;
      //if(row<156 && tot > 6)
         //     std::cout<<"Link "<<link<<" Trigger "<<tr<<" Chip "<<chip<<" row "<<row<<" col "<<col<<" tot "<<tot<<std::endl;
         //     }
  }
  return size;
}
      

void CosmicDataProc::resynch(){
  std::cout<<"Resynchronizing"<<std::endl;
  int serstat;
  serstat=m_configIF->sendHWcommand(4);//pause run
  assert(serstat==0);
  // set synch flags for everybody
  for (int i=0;i<16;i++)m_linksynch[i]=true;
  for (int i=0;i<16;i++)m_tossed[i]=0;
  m_print=0;
  m_configIF->resetCountersHW();//send ECR and BCR
  m_iterator->resynch();
  serstat=m_configIF->sendHWcommand(7);//resume run and post marker
  assert(serstat==0);
}

CosmicDataProc::CosmicDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif) {
  std::cout<<"Start CosmicDataProc Constructor"<<std::endl;
  try{ //catch bad scan option parameters
    m_nModules=m_configIF->getNmodules();

  for (int i=0;i<16;i++)m_linksynch[i]=false;
    m_nfrag=0;
    m_print=0;
    m_nL1AperEv=scanOptions->get<int>("trigOpt.nL1AperEvent");
    std::string name=scanOptions->get<std::string>("Name");
    std::cout<<"CosmicDataProc name "<<name<<std::endl;
    m_runNo=0;

    std::string prefix("CosmicGui");
    if (name.compare(0, prefix.size(), prefix) == 0) 
    {
      // CosmicGui asks to send back over TCP
      // parse string of form: CosmicGui|01168|tcp://127.56.2.6:4500
      std::vector<std::string> strs;
      boost::split(strs, name, boost::is_any_of("|"));
      m_runNo=atoi(strs[1].c_str());
      name = strs[2];

      std::cout<<"TCP to " << name << " instead of file"<<std::endl;
      m_file = 0;
      eudaq::DataSender * ds = new eudaq::DataSender(std::string("DataSender"), std::string("CosmicGuiDataSender"));
      DataSenderIF::setDataSender(ds);
      try
      {
        ds->Connect(name);
      }
      catch (const std::exception & ex)
      {
        // Need to do sensible things here, maybe catch different types of exceptions.
        std::cout << "DataSender could not connect. Exception says: \n  " << ex.what() << std::endl;
      }

    }
    else
    {
      std::cmatch matches;
      std::regex re("(\\d+)");
      if(std::regex_search(name.c_str(), matches, re)){
        if(matches.size()>1){
          std::string match(matches[1].first, matches[1].second);
          m_runNo=strtoul(match.c_str(),0,10);
        }
      }

      std::regex r1("TCP");
      if(std::regex_search(name,r1)==0)
      {
        // Write file to NFS 
        std::string path="/nfs/cosmicData/";
        std::string fullpath=path+name+".raw";
        //    m_file.open(fullpath.c_str());
        m_file=new eudaq::FileSerializer(fullpath.c_str());
        eudaq::DetectorEvent dev(m_runNo,0,0);
        dev.SetFlags(eudaq::Event::FLAG_BORE);
        dev.SetTag("CONFIG", "Name = Test");
        eudaq::RawDataEvent *revc=new eudaq::RawDataEvent(eudaq::RawDataEvent::BORE("CTEL",m_runNo));
	std::shared_ptr<eudaq::Event> cpc(revc);
	dev.AddEvent(cpc);
        eudaq::RawDataEvent *revd=new eudaq::RawDataEvent(eudaq::RawDataEvent::BORE("APIX-CT",m_runNo));
	std::shared_ptr<eudaq::Event> cpd(revd);
	dev.AddEvent(cpd);
        dev.Serialize(*m_file);
      }
      else
      {
        // Don't write to file. Send events to Eudaq instead.
        m_file=0;
      }

      
    }
    std::cout<<"Run number "<<m_runNo<<std::endl;
  
    // create the event iterator class which does all the work
    //This doesn't work with multiple RCE's yet--BL
    std::vector<int> rces;
    rces.push_back(0);
    m_iterator=new CosmicEventIterator(false, m_file, 0, m_runNo,m_nModules,rces);
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
}

CosmicDataProc::~CosmicDataProc(){
  
  delete m_file;
  delete m_iterator;
}

unsigned CosmicDataProc::nEvents(){
  return m_iterator->nEvents();
}
