#include "dataproc/PgpCosmicNwReceiver.hh"
#include "dataproc/Channeldefs.hh"
#include "HW/RCDImasterL.hh"
#include "eudaq/DataSender.hh"
#include "eudaq/DataSenderIF.hh"
#include <regex>
#include <boost/algorithm/string.hpp>
//#include "rce/service/Cache.hh"
using namespace PgpTrans;

#include <iostream>
#include "HW/Headers.hh"
#include <stdlib.h>
#include <boost/property_tree/ptree.hpp>
#include "HW/SerialIF.hh"
#include "config/FEI3/FECommands.hh"
#include "util/RceName.hh"



PgpCosmicNwReceiver::PgpCosmicNwReceiver(AbsDataHandler* handler, boost::property_tree::ptree* scanOptions)
  :AbsReceiver(handler),Receiver(), m_print(0), m_abs_sec(0),
   m_bufferssend(0), m_senddiff(0){
  m_bad=0;
  for (int i=0;i<32;i++)m_linksynch[i]=false;
  RCDImaster::instance()->setReceiver(this);
  std::cout<<"Created pgp cosmic network receiver"<<std::endl;
  m_counter=0;
  std::string name=scanOptions->get<std::string>("Name");
  std::cout<<"CosmicDataProc name "<<name<<std::endl;
  std::string prefix("CosmicGui");
  assert(name.compare(0, prefix.size(), prefix) == 0) ;
  // CosmicGui asks to send back over TCP
  // parse string of form: CosmicGui|01168|tcp://127.56.2.6:4500
  std::vector<std::string> strs;
  boost::split(strs, name, boost::is_any_of("|"));
  unsigned runNo=atoi(strs[1].c_str());
  DataSenderIF::OnStart(runNo);
  name = strs[2];
  std::cout<<"TCP to " << name <<std::endl;
  m_ds = new eudaq::DataSender(std::string("DataSender"), std::string("CosmicGuiDataSender"));
  DataSenderIF::setDataSender(m_ds);
  try
    {
      m_ds->Connect(name);
    }
  catch (const std::exception & ex)
    {
      // Need to do sensible things here, maybe catch different types of exceptions.
      std::cout << "DataSender could not connect. Exception says: \n  " << ex.what() << std::endl;
    }
  m_sendbuffer=new unsigned char[1024*1024];
  m_bufferpointer=0;
}
PgpCosmicNwReceiver::~PgpCosmicNwReceiver(){
    std::cout<<"Received "<<std::dec<<m_counter<<" buffers in previous run."<<std::endl;
    std::cout<<"Sent "<<std::dec<<m_bufferssend<<" buffers in previous run."<<std::endl;
    delete m_ds;
    delete [] m_sendbuffer;
  }
void PgpCosmicNwReceiver::receive(PgpTrans::PgpData *pgpdata){
  int link=pgpdata->header[2];
  if(link==PGPACK){
    if(m_bufferpointer>0){ //sweep
      DataSenderIF::SendEvent(m_sendbuffer, m_bufferpointer);
      m_abs_sec=(unsigned int)time(0);
      m_bufferpointer=0;
    }
    return;
  }
  m_counter++;
  if((m_counter&0xffff)==0)usleep(1); //give external commands a chance to execute from time to time
  unsigned size=pgpdata->payloadSize;
  unsigned* data=pgpdata->payload;
  //std::cout<<"Link "<<link<<" Size ="<<size<<std::endl;
  bool marker=pgpdata->header[6]&0x80;
  if(m_linksynch[link]==true) {
    if (marker==false){
      if(m_print>0){
	if(link==TDCREADOUT)std::cout<<"Ignoring TDC link L1A: "<<(data[1]>>24)<<std::endl;
	else {
	  std::cout<<"Ignoring Link "<<link<<std::endl;
	}
	m_print--;
      }
      m_tossed[link]++;
      return ;
    }
    m_linksynch[link]=false;
    //if(marker)std::cout<<"Markerevent"<<std::endl;
    std::cout<<"Tossed "<<m_tossed[link]<<" fragments for link "<<link<<std::endl;
  }
  if(marker){
    link|=1<<MARKERPOS;
  }

  int rce = RceName::getRceNumber();
  rce=rce&RCEMASK;
  //std::cout<<"link without rce = "<<link<<std::endl;
  link |= (rce << RCEPOS);
  //std::cout<<"link with rce = "<<link<<std::endl;
  
  //link is a 16-bit number with the following format:
  //xxxxrrrr rrrmllll
  //where the x's are currently unused, rrrrrrr is the rce number,
  //m is a marker (used for resynching), and llll is the outlink number

  //std::cout<<"Payloadsize "<<payloadSize<<std::endl;
  unsigned short* se=(unsigned short*)&m_sendbuffer[m_bufferpointer];
  int bytesize=size*sizeof(unsigned);
  se[0]=bytesize;
  se[1]=link;
#ifdef __rtems__ //swap
  se[0]=se[0]<<8|se[0]>>8;
  se[1]=se[1]<<8|se[1]>>8;
#endif
  memcpy(&m_sendbuffer[m_bufferpointer+4], data, bytesize);
  m_bufferpointer+=bytesize+4;
  unsigned abs_sec=(int)time(0);
  if(m_bufferpointer>9000 || abs_sec>m_abs_sec){ //send data if buffer full or timeout or link==9(sweep)
    DataSenderIF::SendEvent(m_sendbuffer, m_bufferpointer);
    m_abs_sec=abs_sec;
    m_bufferpointer=0;
  }
}
void PgpCosmicNwReceiver::resynch(){
  std::cout<<"Resynchronizing"<<std::endl;
  int serstat;
  serstat=SerialIF::sendCommand(4);//pause run
  assert(serstat==0);
  usleep(100000);
  // set synch flags for everybody
  for (int i=0;i<32;i++)m_linksynch[i]=true;
  for (int i=0;i<32;i++)m_tossed[i]=0;
  m_print=0;
  //ECR/BCR
  BitStream *bs=new BitStream;
  BitStreamUtils::prependZeros(bs);
  FEI3::FECommands::sendECR(bs);
  FEI3::FECommands::sendBCR(bs);
  SerialIF::send(bs,SerialIF::WAITFORDATA);
  delete bs;
  //clear send buffer
  m_abs_sec=(unsigned int)time(0);
  m_bufferpointer=0;
  serstat=SerialIF::sendCommand(7);//resume run and post marker
  assert(serstat==0);
  std::cout<<"Resumed"<<std::endl;
}
