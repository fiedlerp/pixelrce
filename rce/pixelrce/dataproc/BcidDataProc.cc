#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/BcidDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "dataproc/fit/FitFactory.cc"
#include "dataproc/fit/CalculateMeanSigma.cc"


int BcidDataProc::fit(std::string fitfun) {
  std::cout << "Running: " << fitfun << std::endl;
  if(fitfun=="CALCULATE_MEAN_SIGMA" && m_nPoints!=0) {
    calculateMeanSigma(m_histo_occ, m_histo_bcid, m_histo_bcid2, m_histo_bcid_mean, m_histo_bcid_sigma); 
  }
  return 0;
}

int BcidDataProc::processData(unsigned link, unsigned *data, int size){
  //    std::cout<<"Process data"<<std::endl;
  for (int i=0;i<size;i++){
    int module=m_linkToIndex[link]; // will be different when parser is fully there
    FormattedRecord current(data[i]);
    if (current.isHeader()){
      l1id = current.getL1id();
      bcid = current.getBxid();
      //printf("bcid : %x \n", bcid);
      //printf("l1id : %x \n", l1id);
      if (l1id != l1id_last)
	{
	  l1id_last = l1id;
	  bcid_ref  = bcid;
	}
      bcid = bcid-bcid_ref;
      // printf("bcidafter : %x \n", bcid);
    }
    if (current.isData()){
      unsigned int chip=current.getFE();
      //unsigned int tot=current.getToT();
      unsigned int col=current.getCol();
      unsigned int row=current.getRow();
      //printf("Hit col=%d row=%d tot=%d\n",col, row, tot);
      m_histo_occ[module][m_currentBin]->increment(chip*m_info[module].getNColumns()+col,row);
      m_histo_bcid[module][m_currentBin]->fill(chip*m_info[module].getNColumns()+col,row,(unsigned int)bcid);
      m_histo_bcid2[module][m_currentBin]->fill(chip*m_info[module].getNColumns()+col,row,(unsigned int)bcid*bcid);
      //printf("bcidafter : %x \n", bcid);
    }
  }
  return 0;
}

BcidDataProc::BcidDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif) {
  try{ //catch bad scan option parameters
    m_nLoops = scanOptions->get<int>("nLoops");
    /* there is at least one parameter loop */
    /* TODO: fix in scan control */
    m_nPoints=1;
    if(m_nLoops>0){         
      m_nPoints=scanOptions->get<int>("scanLoop_0.nPoints");
      for(int i=0;i<m_nPoints;i++) {
	char pointname[10];
	sprintf(pointname,"P_%d",i);
	int vcal=scanOptions->get<int>(std::string("scanLoop_0.dataPoints.")+pointname);
	//std::cout << "point vcal " << vcal << std::endl;
	m_vcal.push_back(vcal);
      }
    }
    m_nTrigger=scanOptions->get<int>("trigOpt.nEvents");
    bcid_ref  = 0;
    l1id_last = 0;
    bcid = 0;
    l1id = 0; 

    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      std::vector<RceHisto2d<short, short>* > vh;
      std::vector<RceHisto2d<char, char>* > vhc;
      m_histo_occ.push_back(vhc);
      m_histo_bcid.push_back(vh);
      m_histo_bcid2.push_back(vh);
      m_histo_bcid_sigma.push_back(vh);
      m_histo_bcid_mean.push_back(vh);
      char name[128];
      char title[128];
      RceHisto2d<short, short> *histo;
      RceHisto2d<char, char> *histoc;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      /* retrieve scan points - Vcal steps in this case */
      for (int point=0;point<m_nPoints;point++){
	sprintf(title,"OCCUPANCY Mod %d at %s", moduleId, moduleName.c_str());
	sprintf(name,"Mod_%d_Occupancy_Point_%03d", moduleId,point);
	histoc=new RceHisto2d<char, char>(name,title,cols,0,cols,rows,0,rows);
	if(m_info[module].getNFrontends()==1)histoc->setAxisTitle(0,"Column");
	else histoc->setAxisTitle(0,"FE*N_COL+Column");
	histoc->setAxisTitle(1, "Row");
	m_histo_occ[module].push_back(histoc);
	sprintf(title,"BCID Mod %d at %s", moduleId, moduleName.c_str());
	sprintf(name,"Mod_%d_BCID_Point_%03d", moduleId,point);
	histo=new RceHisto2d<short, short>(name,title,cols,0,cols,rows,0,rows);
	if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
	else histo->setAxisTitle(0,"FE*N_COL+Column");
	histo->setAxisTitle(1, "Row");
	m_histo_bcid[module].push_back(histo);
	
	sprintf(title,"BCID2 Mod %d at %s", moduleId, moduleName.c_str());
	sprintf(name,"Mod_%d_BCID2_Point_%03d", moduleId,point);
	histo=new RceHisto2d<short, short>(name,title,cols,0,cols,rows,0,rows);
	if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
	else histo->setAxisTitle(0,"FE*N_COL+Column");
	histo->setAxisTitle(1, "Row");
	m_histo_bcid2[module].push_back(histo);
	
	sprintf(title,"BCID_MEAN Mod %d at %s", moduleId, moduleName.c_str());
	sprintf(name,"Mod_%d_BCIDmean_Point_%03d",  moduleId,point);
	histo=new RceHisto2d<short, short>(name,title,cols,0,cols,rows,0,rows);
	if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
	else histo->setAxisTitle(0,"FE*N_COL+Column");
	histo->setAxisTitle(1, "Row");
	m_histo_bcid_mean[module].push_back(histo);
	
	sprintf(title,"BCID_SIGMA Mod %d at %s", moduleId, moduleName.c_str());
	sprintf(name,"Mod_%d_BCIDsigma_Point_%03d", moduleId,point);
	histo=new RceHisto2d<short, short>(name,title,cols,0,cols,rows,0,rows);
	if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
	else histo->setAxisTitle(0,"FE*N_COL+Column");
	histo->setAxisTitle(1, "Row");
	m_histo_bcid_sigma[module].push_back(histo);
      }
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
}

BcidDataProc::~BcidDataProc(){
  for (size_t module=0;module<m_histo_bcid.size();module++)
    for(size_t i=0;i<m_histo_bcid[module].size();i++)delete m_histo_bcid[module][i];
  for (size_t module=0;module<m_histo_bcid2.size();module++)
    for(size_t i=0;i<m_histo_bcid2[module].size();i++)delete m_histo_bcid2[module][i];
  for (size_t module=0;module<m_histo_bcid_sigma.size();module++)
    for(size_t i=0;i<m_histo_bcid_sigma[module].size();i++)delete m_histo_bcid_sigma[module][i];
  for (size_t module=0;module<m_histo_bcid_mean.size();module++)
    for(size_t i=0;i<m_histo_bcid_mean[module].size();i++)delete m_histo_bcid_mean[module][i];
  for (size_t module=0;module<m_histo_occ.size();module++)
    for(size_t i=0;i<m_histo_occ[module].size();i++)delete m_histo_occ[module][i];

}
  

