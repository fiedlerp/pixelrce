#ifndef HITORDATAPROC_HH
#define HITORDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/fit/AbsFit.hh"
#include <vector>
#include "util/RceHisto2d.cc"

class HitorDataProc: public AbsDataProc{
public:
  HitorDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~HitorDataProc();
  int processData(unsigned link, unsigned *data, int size);

protected:

  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_occ;
};

#endif
