#ifndef MEASUREMENTDATAPROC_HH
#define MEASUREMENTDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/fit/AbsFit.hh"
#include <vector>
#include "util/RceHisto1d.cc"

class MeasurementDataProc: public AbsDataProc{
public:
  MeasurementDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~MeasurementDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:

  RceHisto1d<float, float>* m_histo;
  std::vector<int> m_vcal;
  float *m_err;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;
  int m_nMaskStages;
};

#endif
