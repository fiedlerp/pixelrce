#include "dataproc/ModuleCrosstalkRawDataHandler.hh"
#include "dataproc/AbsDataProc.hh"
#include "config/AbsFormatter.hh"
#include "config/ConfigIF.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include "util/DataCond.hh"

ModuleCrosstalkRawDataHandler::ModuleCrosstalkRawDataHandler(AbsDataProc* dataproc, 
					       DataCond& datacond,
					       ConfigIF* cif, 
					       boost::property_tree::ptree* scanOptions)
  :AbsDataHandler(dataproc, datacond, cif){
  std::cout<<"Regular raw data handler"<<std::endl;
  m_nL1AperEv=scanOptions->get<int>("trigOpt.nL1AperEvent");
  int ntrigpergroup=scanOptions->get<int>("trigOpt.nTriggersPerGroup");
  if(ntrigpergroup!=0)m_nL1AperEv*=ntrigpergroup;
  std::cout<<"Number of expected triggers is "<<m_nL1AperEv<<std::endl;
  int maxlink=0;
  m_nModules=m_configIF->getNmodules();
  m_L1Acounters=new int[m_nModules];
  m_outlinkMask=new int[m_nModules];
  m_moduleTrgMask=scanOptions->get<int>("trigOpt.moduleTrgMask");
  resetL1counters();
  for (int i=0;i<m_nModules;i++){
    m_outlinkMask[i]=1<<m_configIF->getModuleInfo(i).getOutLink();
    if(m_configIF->getModuleInfo(i).getOutLink()>maxlink)maxlink=m_configIF->getModuleInfo(i).getOutLink();
  }
  maxlink++;
  m_linkToIndex=new int[maxlink];
  for (unsigned int i=0;i<m_configIF->getNmodules();i++){
    m_linkToIndex[m_configIF->getModuleInfo(i).getOutLink()]=i;
  }
  //m_timer.Reset();
  m_counter=0;
}

ModuleCrosstalkRawDataHandler::~ModuleCrosstalkRawDataHandler(){
  delete [] m_L1Acounters;
  delete [] m_outlinkMask;
  delete [] m_linkToIndex;
  // m_timer.Print("ModuleCrosstalkRawDataHandler");
}

void ModuleCrosstalkRawDataHandler::timeoutOccurred(){
  resetL1counters();
}

void ModuleCrosstalkRawDataHandler::resetL1counters(){
  for (int i=0;i<m_nModules;i++){
    m_L1Acounters[i]=0;
  }
}

void ModuleCrosstalkRawDataHandler::handle(unsigned link, unsigned *data, int size){
  // nL1A contains the number of L1A in the data chunk
  //std::cout<<"Data from Link "<<link<<std::endl;
  //m_timer.Start();
  int nL1A= m_dataProc->processData(link, data, size);
  //m_timer.Stop();
  //std::cout<<nL1A<<" events."<<std::endl;
    //  std::cout<<"Expecting "<<m_nL1AperEv<<std::endl;
  //for (int i=size-4;i<size;i++)std::cout<<std::hex<<data[i]<<std::endl;
  m_L1Acounters[m_linkToIndex[link]]+=nL1A;
  //  std::cout<<m_L1Acounters[m_linkToIndex[link]]<<" events so far."<<std::endl;
  bool done=true;
  //check if the event is complete.
  for (int i=0;i<m_nModules;i++){
    //if(m_L1Acounters[i]>m_nL1AperEv)std::cout<<m_L1Acounters[i]<<" L1Atriggers"<<std::endl;
    if(m_outlinkMask[i]&m_moduleTrgMask)continue;
    if (m_L1Acounters[i]<m_nL1AperEv){
      done=false;
      break;
    }
  }
  if(done){
    //std::cout<<"Restarting scan "<<std::endl;
    resetL1counters();
    // Next trigger
    rce_mutex_lock pl( m_dataCond.mutex );
    if(m_dataCond.waitingForData==true)m_dataCond.cond.signal();
    else std::cout<<"Scan was not waiting for data!"<<std::endl;
  }
}
