#ifndef ABSDATAHANDLER_HH
#define ABSDATAHANDLER_HH

#include <vector>
#include <assert.h>
#include "util/DataCond.hh"

class AbsDataProc;
class ConfigIF;

class AbsDataHandler{
public:
  AbsDataHandler(AbsDataProc* dataproc, DataCond& datacond, ConfigIF* cif)
    :m_dataProc(dataproc), m_dataCond(datacond), m_configIF(cif){}
  virtual ~AbsDataHandler(){};
  virtual void handle(unsigned link, unsigned* data, int size)=0;
  virtual void timeoutOccurred(){}
protected:
  AbsDataProc* m_dataProc;
  DataCond& m_dataCond;
  ConfigIF* m_configIF;
};
#endif
