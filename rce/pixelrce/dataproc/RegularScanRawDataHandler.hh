#ifndef REGULARSCANRAWDATAHANDLER_HH
#define REGULARSCANRAWDATAHANDLER_HH

#include <assert.h>

#include "dataproc/AbsDataHandler.hh"
#include <boost/property_tree/ptree_fwd.hpp>


class AbsFormatter;
class DataCond;

class RegularScanRawDataHandler: public AbsDataHandler{
public:
  RegularScanRawDataHandler(AbsDataProc* dataproc, DataCond& datacond, ConfigIF* cif, boost::property_tree::ptree* scanOptions);
  virtual ~RegularScanRawDataHandler();
  inline void handle(unsigned link, unsigned* data, int size);
  void timeoutOccurred();
  inline void resetL1counters();
protected:
  int m_nL1AperEv;
  int* m_L1Acounters;
  int m_nModules;
  int *m_linkToIndex;
  int m_counter;
};
#endif
