#include "dataproc/fit/FitScurveLikelihoodFastInt.hh"
#include "dataproc/fit/FitData.hh"
#include "config/ConfigIF.hh"
#include <iostream>
#include <stdio.h>

template<typename TP, typename TE>
FitScurveLikelihoodFastInt<TP, TE>::FitScurveLikelihoodFastInt(ConfigIF *cif,std::vector<std::vector<RceHisto2d<TP, TE>*> > &histo,std::vector<int> &vcal,int nTrigger,const char* name):AbsFit(cif,vcal,nTrigger),m_histo(histo),m_name(name) {
  initFit(&m_fit);
}

template<typename TP, typename TE>
FitScurveLikelihoodFastInt<TP, TE>::~FitScurveLikelihoodFastInt(){
  for(unsigned int module=0;module<m_fithisto.size();module++) {
    for(size_t i=0;i<m_fithisto[module].size();i++){
      delete m_fithisto[module][i];
    }
  }
}
      

template<typename TP, typename TE>
int FitScurveLikelihoodFastInt<TP, TE>::doFit(int fitopt) {
 FitData pfdi,pfdo;
  GaussianCurve gc;
#if 0
  clock_t starttime,endtime;
  starttime= RceSvc::ticks();
#endif
  int nBins=m_vcal.size();
  if(!nBins) return -1;
  m_x=new UINT32[nBins];
  m_y=new UINT8[nBins];
 
  for(unsigned int module=0;module<m_config->getNmodules();module++) {
    int nRows= m_config->getModuleInfo(module).getNRows();
    int nCols=m_config->getModuleInfo(module).getNColumns();
    //int nHistos=m_histo[module].size();
    std::cout << "nTrigger " << m_nTrigger << std::endl;
    for(int chip=0;chip<m_config->getModuleInfo(module).getNFrontends();chip++) {
      if((fitopt&NOCONV)==0)  {
	/* apply dac to electron calibration */
	for(int bin=0;bin<nBins;bin++) {
	  /* for now we use float here */
	  m_x[bin]=(UINT32)(m_config->dacToElectrons(module,chip,m_vcal[bin])*10);
	}
      } else {
	for(int bin=0;bin<nBins;bin++) {
	  /* for now we use float here */
	  m_x[bin]=m_vcal[bin]*10;
	}
      }
      for(int col=0;col<nCols;col++) {
	for(int row=0;row<nRows;row++) {
	  // extract S-Curve
	  for(int bin=0;bin<nBins;bin++) {
	    m_y[bin]=(UINT8)(*m_histo[module][bin])(row, chip*nCols+col);
	  }
	  pfdi.n=nBins;
	  pfdi.x=&m_x[0]; 
	  pfdi.y=&m_y[0];

	  int goodBins = extractGoodData(&pfdi,&pfdo,m_nTrigger);
	  int maxVal = m_nTrigger;

	  if(goodBins<2){
	    if(fitopt&XTALK){
	      goodBins = roughFitRange(&pfdi,&pfdo,maxVal);
	    }
	  }
	  if(goodBins<2){
	    continue;
	  }
		
	  if(goodBins<5){
	    m_fit.curve=&gc;
	    m_fit.maxIters=100;
	    int result = sGuess(&pfdo,&m_fit);
	    
	    if(!result) {

	      int lastbin = pfdi.n-1;
	      float fitmu = (float)gc.mu/10.f; 
	      float fitsigma = (float)gc.sigma/1000.f;
	      //exclude extreme fit results from the histograms
	      if(10*fitmu < 2*pfdi.x[lastbin] && 10*fitsigma < 2*(pfdi.x[lastbin]-pfdi.x[0])){

		//std::cout<<"we guess mu = "<<(float)gc.mu/10.f<<" ; sigma = "<<(float)gc.sigma/1000.f<<std::endl;
		m_fithisto[module][0]->set(chip*nCols+col,row,(float)gc.mu/10.f);
		m_fithisto[module][1]->set(chip*nCols+col,row,(float)gc.sigma/1000.f);
		m_fithisto[module][2]->set(chip*nCols+col,row,(float) m_fit.chi2/(float) (m_fit.ndf* 100));
		m_fithisto[module][3]->set(chip*nCols+col,row,(float) m_fit.nIters);
		
	      }

	    }

	  }
	  else{

	    m_fit.curve=&gc;
	    m_fit.maxIters=100;
	    gc.a0=maxVal;
	    int result=fitPixel(&pfdo,&m_fit);
	    if(!result) {
	      int lastbin = pfdi.n-1;
	      float fitmu = (float)gc.mu/10.f;
	      float fitsigma =  (float)4634050/(float) gc.sigma;
	      //exclude extreme fit results from the histograms
	      if(10*fitmu < 2*pfdi.x[lastbin] && 10*fitsigma < 2*(pfdi.x[lastbin]-pfdi.x[0])){
		//	    printf("%d %d %d %d %f %f %f %d\n",gc.a0,chip,col,row,gc.mu/10000.,4634040./gc.sigma,m_fit.chi2/100.,m_fit.nIters);
		m_fithisto[module][0]->set(chip*nCols+col,row,(float)gc.mu/10.f);
		m_fithisto[module][1]->set(chip*nCols+col,row,(float) 4634050/(float) gc.sigma);
		m_fithisto[module][2]->set(chip*nCols+col,row,(float) m_fit.chi2/(float) (m_fit.ndf* 100));
		m_fithisto[module][3]->set(chip*nCols+col,row,(float) m_fit.nIters);
	      }

	    }
	  }

	}  //rows
      } //columns
    } //chips
  } // modules
  delete [] m_x;
  delete [] m_y;
#if 0
  endtime= RceSvc::ticks();
  printf("total fit time %f\n",(float)(endtime-starttime)/350.);
#endif
  return 0;
}

template<typename TP, typename TE>
typename FitScurveLikelihoodFastInt<TP, TE>::INT32 *FitScurveLikelihoodFastInt<TP, TE>::log_ext(INT32 x, INT32 mu,INT32 sigma) {
  INT32 u;
  
  u=(x - mu)*sigma + sc_lut_width; //may be negative and larger than lut length
  if(u<0) u=0;
  if(u > sc_lut_length) u = sc_lut_length; //trancate
  u>>=15;
  u&=0xfffe;             // we need the index should always be even
  return (INT32*) &FitDataFastInt::data_logx[u];
}



template<typename TP, typename TE>
typename FitScurveLikelihoodFastInt<TP, TE>::UINT32 FitScurveLikelihoodFastInt<TP, TE>::errf_ext(INT32 x, GaussianCurve *psp) {
  INT32 u;  
  u=(x - psp->mu)*psp->sigma + sc_lut_width; //it may be negative and may exceed LUT length
  if(u < 0) u=0;
  if(u > sc_lut_length) u=sc_lut_length;
  u>>=16;
  return FitDataFastInt::data_errf[u];
}

template<typename TP, typename TE>
typename FitScurveLikelihoodFastInt<TP, TE>::UINT32 FitScurveLikelihoodFastInt<TP, TE>::logLikelihood(FitData *pfd, GaussianCurve *s) {
   INT32 N;
    UINT32 *x; 
    UINT8 *y;
    UINT32 acc;
    INT32 *p;
    UINT32 k;
    INT32 mu,sigma;
    mu=s->mu;
    sigma=s->sigma;
    
    x = pfd->x;     
    y =  pfd->y;     
    acc = 0;
    N = s->a0;
    for(k=0; k<pfd->n; ++k) 
    {
      p = log_ext(*x++,mu,sigma); /* pointer to log(probability) */
      acc -= ((s->a0-(*y))*(*(p+1))+(*y)*(*p));
      y++;      
    }
    return acc;
}

template<typename TP, typename TE>
int FitScurveLikelihoodFastInt<TP, TE>::initFit(Fit *pFit) {
  pFit->maxIters = 100;
  pFit->muEpsilon = 50; /* 0.01% */
  pFit->sigmaEpsilon = 50; /* 0.01% */
  for (unsigned int i=0;i<m_config->getNmodules();i++){
    std::vector<RceHisto2d<float, float>*> vh;
    m_fithisto.push_back(vh);
    char name[128];
    char title[128];
    char suffix[128]="";
    if(std::string(m_name)!=""){
      sprintf(suffix,"%s_",m_name);
    }

    int rows=m_config->getModuleInfo(i).getNRows();
    int cols=m_config->getModuleInfo(i).getNColumns()*m_config->getModuleInfo(i).getNFrontends();
    int id=m_config->getModuleInfo(i).getId();
    std::string modName=m_config->getModuleInfo(i).getName();
    sprintf(name,"Mod_%d_%sMean", id, suffix);
    sprintf(title,"MOD %d %s at %s SCURVE MEAN", id, suffix, modName.c_str());
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
    sprintf(title,"MOD %d %s at %s SCURVE SIGMA", id, suffix, modName.c_str());
    sprintf(name,"Mod_%d_%sSigma", id, suffix);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
    sprintf(title,"MOD %d %s at %s SCURVE CHI2", id, suffix, modName.c_str());
    sprintf(name,"Mod_%d_%sChiSquare", id, suffix);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
    sprintf(title,"MOD %d %s at %s SCURVE ITER", id, suffix, modName.c_str());
    sprintf(name,"Mod_%d_%sIter", id, suffix);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
  }
  return 0;
}

template<typename TP, typename TE>
int FitScurveLikelihoodFastInt<TP, TE>::extractGoodData(FitData *pfdi,FitData *pfdo, UINT32 nTriggers) {
  int hitsStart, hitsEnd, nBins;
  UINT32 a0; 
  UINT8 *y;
  
  nBins = pfdi->n;
  y = pfdi->y;
  
  //new method to find hitsStart
  
  for(hitsStart = nBins-1;hitsStart >= 0; --hitsStart)
    if(y[hitsStart] == 0) break;
  
  if(hitsStart ==0)
    return 0;
  if(hitsStart == nBins)
    return 0;
  
  a0 = y[nBins - 1]; // last bin has at least 90% hits
  if(a0*999 < nTriggers*900)
    return 0;
  //failure mode, never reaches A0
  
  //dan's new method
  for(hitsEnd = hitsStart; hitsEnd < nBins; ++hitsEnd)
    if(y[hitsEnd] >= a0) break;
  
  // add 2 for old method or 1 for new method
  //      1 because of where we start comparing and
  //      1 because we want to include one instance of the maximum 
  nBins = 1 + hitsEnd - hitsStart; 
  
  if(hitsStart < 0)  hitsStart =0;
  
  if(hitsStart == hitsEnd) return 0;
  
  if(nBins < 2) {
    return 0;
    //failure mode :: not enough data points
  }  
  if(nBins < 0) nBins = 0;
  pfdo->n = nBins;
  pfdo->y = &y[hitsStart];
  pfdo->x = &pfdi->x[hitsStart];
  return nBins;
}


template<typename TP, typename TE>
int FitScurveLikelihoodFastInt<TP, TE>::initialGuess(FitData *pfd, GaussianCurve *pSParams) {


 UINT32 pt1, pt2, pt3; 
  UINT8 *pdlo; 
  UINT32 y_lo, sigma;
  UINT32 *x; 
  UINT8 *y; 
  UINT32 a0; 
  UINT32 minSigma;
  UINT32 k, n, lo1, lo2, lo3, status;
  UINT32 x1,x2,x3;
  INT32  xdiff;
  
  n = pfd->n;
  if(n<5) return 1;
  pSParams->a0 = pfd->y[pfd->n-1]; // assumes last data point sets plateau 
  a0 = pSParams->a0;
  xdiff=pfd->x[1] - pfd->x[0]; 
  minSigma = (xdiff*2897)>>12; //This is actually division by sqrt of 2
  x = pfd->x; y = pfd->y;
  pt1 = (16* a0)/100;
  pt2 = (50* a0)/100;
  pt3 = (84* a0)/100;
  // find the range over which the data crosses the 16%, 50% and 84% points 
  lo1 = lo2 = lo3 = 0; // invalid values 
  pdlo = &y[0]; // y 
  for(k=0;k<n;++pdlo) 
  {
    y_lo = *pdlo; 
    if(!lo1 && (y_lo >= pt1)) lo1 = k;
    if(!lo2 && (y_lo >= pt2)) lo2 = k;
    if(!lo3 && (y_lo >= pt3)) lo3 = k;
    ++k;
  }
//  printf("sGuess lo1=%d lo2=%d lo3=%d \n",lo1,lo2,lo3);
  x1 = (x[lo1] + x[lo1-1]) >> 1; 
  x2 = (x[lo2] + x[lo2-1]) >> 1;
  x3 = (x[lo3] + x[lo3-1]) >> 1;
//   printf("sGuess x1 x2 x3 %d %d %d\n",x1, x2,x3);
  
  // mu = threshold 
  
  pSParams->mu = (UINT32) x2;
  xdiff=(x3 - x1);
  sigma = (xdiff*2897)>>12;  //This is actually division by sqrt of 2
   if(sigma < minSigma) {
    sigma = minSigma; 
  }
  // change for inv sigma
  pSParams->sigma=65536000/sigma; // do division *here* for speed; use large factor to prevent truncation; unclear this is large enough
  status = 0;
  return status;

}
#define DMUMUL 50
#define DSIGMUL 200
template<typename TP, typename TE>
int FitScurveLikelihoodFastInt<TP, TE>::fitPixel(FitData *pfd,void *vpfit) {
  INT32 deltaSigma, deltaMu, sigma, chi2Min, *fptr;
  INT32 chi2[9];
  GaussianCurve sParams, sParamsWork, *psp;
  int muEpsilon,sigmaEpsilon;
  int k, dirMin;
  Fit *pFit;
  pFit = (Fit *)vpfit;

  pFit->nIters = 0;
  pFit->converge = 0; // assume no convergence 
  muEpsilon = pFit->muEpsilon;
  sigmaEpsilon = pFit->sigmaEpsilon;
  
  pFit->ndf = pfd->n - 2; // degrees of freedom 
  // take a guess at the S-curve parameters 

  if(initialGuess(pfd,&sParams)) 
  {
    psp = (GaussianCurve *)pFit->curve;
    psp->a0 = sParams.a0;
    psp->mu = sParams.mu;
    psp->sigma = sParams.sigma;
    pFit->converge = 1;
    pFit->chi2 = 0;
    return 0;
  } 
  
  // initialize loop parameters 
  psp = &sParamsWork;
  psp->a0 = sParams.a0;
  deltaSigma = (sParams.sigma*DSIGMUL)/1000 ; // scaled estimated value
  deltaMu = (sParams.mu*DMUMUL)/1000 ; // scaled estimated value

  //  the loop begins 
 
  while(!pFit->converge && (pFit->nIters++ < pFit->maxIters)) 
  { 
   dirMin = 0;
   fptr = chi2;
   for(k=0;k<9;++k) *fptr++ = 0;
   while((dirMin >= 0) && (pFit->nIters++ < pFit->maxIters)) 
   { 
    // * calculate neighboring points *

    psp->sigma=sParams.sigma - deltaSigma;
    psp->mu = sParams.mu - deltaMu;      
    if(chi2[0] == 0) chi2[0] = logLikelihood(pfd,psp);
    psp->mu = sParams.mu;
    if(chi2[7] == 0) chi2[7] = logLikelihood(pfd,psp);
    psp->mu = sParams.mu + deltaMu;  
    if(chi2[6] == 0) chi2[6] = logLikelihood(pfd,psp);
    psp->sigma=sParams.sigma;
    psp->mu = sParams.mu - deltaMu;
    if(chi2[1] == 0) chi2[1] = logLikelihood(pfd,psp);
    psp->mu = sParams.mu;
    if(chi2[8] == 0) chi2[8] = logLikelihood(pfd,psp);
    psp->mu = sParams.mu + deltaMu;
    if(chi2[5] == 0) chi2[5] = logLikelihood(pfd,psp);
    psp->sigma=sParams.sigma+deltaSigma;
    psp->mu = sParams.mu - deltaMu;
    if(chi2[2] == 0) chi2[2] = logLikelihood(pfd,psp);
    psp->mu = sParams.mu;
    if(chi2[3] == 0)chi2[3] = logLikelihood(pfd,psp);
    psp->mu = sParams.mu + deltaMu;
    if(chi2[4] == 0) chi2[4] = logLikelihood(pfd,psp);
    
    dirMin = -1;
    chi2Min = chi2[8]; // first guess at minimum 
    for(k=0, fptr=chi2; k<8; ++k, ++fptr) 
    {
	 if(*fptr < chi2Min) 
     {
	  chi2Min = *fptr;
	  dirMin = k;
	 }
    }
//    printf("chi2: %d %d %d %d %d %d %d %d %d\n",
//      chi2[0],chi2[1],chi2[2],chi2[3],chi2[4],chi2[5],chi2[6],chi2[7],chi2[8]);
    switch(dirMin)
    {
    case -1:
	 if(!pFit->converge)
	 {
	  deltaSigma = deltaSigma >> 3;
	  deltaMu = deltaMu >> 3;
	 }
	 break;
    case 0:
	 sigma = sParams.sigma - deltaSigma;
	 sParams.sigma=sigma;
	 sParams.mu = sParams.mu - deltaMu;
	 chi2[8] = chi2[0];
	 chi2[3] = chi2[1];
	 chi2[4] = chi2[8];
	 chi2[5] = chi2[7];
	 chi2[0] = chi2[1] = chi2[2] = 
	 chi2[6] = chi2[7] = 0;
	 break;
    case 1:
	 sParams.mu = sParams.mu - deltaMu;
	 chi2[8] = chi2[1];
	 chi2[3] = chi2[2];
	 chi2[4] = chi2[3];
	 chi2[5] = chi2[8];
	 chi2[6] = chi2[7];
	 chi2[7] = chi2[0];
	 chi2[0] = chi2[1] = chi2[2] = 0;
	 break;
    case 2:
	 sigma = sParams.sigma + deltaSigma;
	 sParams.sigma=sigma;
	 sParams.mu = sParams.mu - deltaMu;
	 chi2[8] = chi2[2];
	 chi2[5] = chi2[3];
	 chi2[6] = chi2[8];
	 chi2[7] = chi2[1];
	 chi2[0] = chi2[1] = chi2[2] = 
	 chi2[3] = chi2[4] = 0;
	 break;
    case 3:
	 sigma = sParams.sigma + deltaSigma;
	 sParams.sigma=sigma;
	 chi2[8] = chi2[3];
	 chi2[5] = chi2[4];
	 chi2[6] = chi2[5];
	 chi2[7] = chi2[8];
	 chi2[0] = chi2[1];
	 chi2[1] = chi2[2];
	 chi2[2] = chi2[3] = chi2[4] = 0;
	 break;
    case 4:
	 sigma = sParams.sigma + deltaSigma;
	 sParams.sigma=sigma;
	 sParams.mu = sParams.mu + deltaMu;
	 chi2[8] = chi2[4];
	 chi2[7] = chi2[5];
	 chi2[0] = chi2[8];
	 chi2[1] = chi2[3];
	 chi2[2] = chi2[3] = chi2[4] = 
	 chi2[5] = chi2[6] = 0;
	 break;
    case 5:
	 sParams.mu = sParams.mu + deltaMu;
	 chi2[8] = chi2[5];
	 chi2[7] = chi2[6];
	 chi2[0] = chi2[7];
	 chi2[1] = chi2[8];
	 chi2[2] = chi2[3];
	 chi2[3] = chi2[4];
	 chi2[4] = chi2[5] = chi2[6] = 0;
	 break;
    case 6:
	 sigma = sParams.sigma - deltaSigma;
	 sParams.sigma=sigma;
	 sParams.mu = sParams.mu + deltaMu;
	 chi2[8] = chi2[6];
	 chi2[1] = chi2[7];
	 chi2[2] = chi2[8];
	 chi2[3] = chi2[5];
	 chi2[4] = chi2[5] = chi2[6] = 
	 chi2[7] = chi2[0] = 0;
	 break;
    case 7:
	 sigma = sParams.sigma - deltaSigma;
	 sParams.sigma=sigma;
	 chi2[8] = chi2[7];
	 chi2[1] = chi2[0];
	 chi2[2] = chi2[1];
	 chi2[3] = chi2[8];
	 chi2[4] = chi2[5];
	 chi2[5] = chi2[6];
	 chi2[6] = chi2[7] = chi2[0] = 0;
	 break;
    } //end switch
   } // end while dirMin >= 0
   if((deltaSigma < sigmaEpsilon) && (deltaMu < muEpsilon))    
   { 
    pFit->converge = 1;
    break;
   }
  } // end while !pFit->converge 
  psp = (GaussianCurve *)pFit->curve;
  *psp = sParams;
  pFit->chi2 = chiSquared(pfd,psp);

  //  tot_itt+=pFit->nIters;
    return pFit->converge ? 0 : 1; // 0 = success
 
}

template<typename TP, typename TE>
typename FitScurveLikelihoodFastInt<TP, TE>::UINT32 FitScurveLikelihoodFastInt<TP, TE>::chiSquared(FitData *pfd,GaussianCurve *s) {
  UINT32 acc,y3; 
  INT32 y0,y1,y2;
  UINT32 a0, chi2;
  UINT32 *x; 
  UINT8 *y;
  INT32 x0;
  int j, k, n;
  
  x = pfd->x;     
  y = pfd->y;     
  n = pfd->n;
  acc = 0;  
  a0 = s->a0;
  // use binomial weighting 
  for(k=0;k<n;++k) {
    x0 = *x++; 
    y0 =(INT32) *y++; 
    y1 = errf_ext(x0,s); 
    j=y1>>10; 
    y2 = y0*1000 - (a0 * j);
    y2/=100;                
    y3=(UINT32) (y2*y2);    
    y3*=FitDataFastInt::binomial_weight[j]; 
    y3>>=7;
    acc+=y3;
  
  }
  if(a0!=0)chi2=acc/a0;
  else chi2=0;
  return chi2;
}



//Modified from /DAQ/IBLDAQ-0-0-0/RodDaq/NewDsp/SLAVE/fittingRoutines.c

// \brief Guess S curve

template<typename TP, typename TE>
int FitScurveLikelihoodFastInt<TP, TE>::sGuess(FitData *pfd,  void *vpfit) {

	int j, k, n, lo1, lo2, lo3, hi1, hi2, hi3, status;
	UINT32 *x, a0, minSigma, sigma, pt1, pt2, pt3, x1, x2, x3, hits, y_lo, y_hi;
	UINT8 *y, *pdlo, *pdhi;


// 	clock_t time1,time2;
// #ifdef ppc405
// 	time1= RceSvc::ticks();
// #endif

	n = pfd->n;

	status = 1;  //1: failure

	if(n<2){
 	  return 1;  //not enough points
 	}

	UINT32 invroot6 = 4082;

	//	std::cout<<"doing sGuess with n = "<<n<<std::endl;

	GaussianCurve *sParams;

	Fit *pFit;
	pFit = (Fit *)vpfit;
  
	pFit->nIters = 0;
	pFit->converge = 0; // assume no convergence 

	sParams = (GaussianCurve *)pFit->curve;

	sParams->a0 = pfd->y[n-1]; // assumes last data point sets plateau 


// 	for(int i=0; i<n; i++){
// 	  std::cout<<" "<<pfd->x[i]<<" ;";
// 	}
// 	std::cout<<"  |  ";
// 	for(int i=0; i<n; i++){
// 	  std::cout<<" "<<(unsigned int)pfd->y[i]<<" ;";
// 	}
// 	std::cout<<std::endl;

	a0 = sParams->a0;


	minSigma = (pfd->x[1] - pfd->x[0]) * invroot6 / 100;
//	std::cout<<"minSigma = "<<minSigma<<std::endl;

	if(n == 2) {
	        // no interesting data point 
		sParams->mu = (5 * (pfd->x[1] + pfd->x[0]) ) / 10;
		sigma = minSigma; // we do not have accurate information 
		status = 0;
	}  else if(n == 3) {
	        // one interesting data point 
	  //    aSquared = a0 * a0;
	        hits = pfd->y[1];
	        
                // this next part is voodoo. being at such small number of interesting points,
		//the maximum likelihood lies along a curve. Intuitively, we have a feel for where
		///the mean and sigma go. sigma varies continuously between sigma0 and 2 * sigma0,
		//sigma0 = bin / root(6) 
		sigma = minSigma; 

		INT32 factor = 400000*hits/a0;
		factor *= (a0 - hits)/a0;
		//		std::cout<<"a0 = "<<a0<<" ; hits = "<<hits<<" ; factor = "<<factor<<std::endl;

		//	sigma = sigma * ( 1.0f + 4.0f * hits * (a0 - hits) / aSquared );

		factor = (100000 + factor)/1000;
		//		std::cout<<"now, factor = "<<factor<<" ; sigma = "<<sigma<<std::endl;

		sigma = (sigma/10) * factor;  // we now have sigma(*100)(*100)
		sigma = sigma/10;  //we now have sigma*1000
	       

		//crude estimate for mu, based on linear extrapolation
		pt1 = 5 * a0;
		hits = hits * 10;

		if(hits > pt1){
		  lo1 = 0;
		  hi1 = 1;
		}
		else{
		  lo1 = 1;
		  hi1 = 2;
		}

		y_lo = pfd->y[lo1];
		y_hi = pfd->y[hi1];

		x1 = pfd->x[lo1];
		x2 = pfd->x[hi1];


		INT32 invslope = (1000*(x2-x1))/ (y_hi-y_lo);  // 10000/slope

		//		std::cout<<"1/m = "<<invslope<<" pt1 = "<<pt1<<" ; y_l0  = "<<y_lo<<std::endl;

		sParams->mu = (pt1 - 10*y_lo) * invslope / 10000 + x1;

		//		std::cout<<"mu = "<<sParams->mu<<std::endl;

		status = 0;
	}	
	else {
		x = pfd->x; y = pfd->y;
		pt1 = 16 * a0;
		pt2 = 50 * a0;
		pt3 = 84 * a0;
	// find the range over which the data crosses the 16%, 50% and 84% points 
		hi1 = hi2 = hi3 = 0; // invalid values 
		lo1 = lo2 = lo3 = 0; // invalid values 
		pdlo = &y[0]; // y 
		pdhi = &y[n-1]; // y + n - 1 
	// important note: for the sake of speed we want to perform as few comparisons as
		//possible. Therefore, the integer comparison occurs first in each of the 
		//following operations. Depending on the logical value thereof, the next
		//comparison will be made only if necessary. To further expedite the code,
		//arrays are not used because there are only three members 
		j = n - 1;
		for(k=0;k<n;++pdlo, --pdhi) {
			y_lo = *pdlo;
			y_hi = *pdhi;

			y_lo = y_lo * 100;
			y_hi = y_hi * 100;

			if(!lo1 && (y_lo >= pt1)) lo1 = k;
			if(!lo2 && (y_lo >= pt2)) lo2 = k;
			if(!lo3 && (y_lo >= pt3)) lo3 = k;
			if(!hi1 && (y_hi <= pt1)) hi1 = j;
			if(!hi2 && (y_hi <= pt2)) hi2 = j;
			if(!hi3 && (y_hi <= pt3)) hi3 = j;
			--j;
			++k;
		}


// 		std::cout<<"xlo = "<<x[lo1]<<" ; "<<x[lo2]<<" ; "<<x[lo3]<<std::endl;
// 		std::cout<<"xhi = "<<x[hi1]<<" ; "<<x[hi2]<<" ; "<<x[hi3]<<std::endl;


		x1 = (x[lo1] + x[hi1]) / 2;
		x2 = (x[lo2] + x[hi2]) / 2;
		x3 = (x[lo3] + x[hi3]) / 2;

//	std::cout<<"x(1,2,3) = "<<x1<<" ; "<<x2<<" ; "<<x3<<std::endl;


		// mu = threshold 

		sParams->mu = x2;
		sigma = ( (x3 - x1) * 7071 ) / 100;
	
		if(sigma < minSigma) {
			sigma = minSigma; 
		}

		status = 0;
	}


	sParams->sigma= sigma; // this should actually be 1000 * sigma.

	// std::cout<<"mu = "<<sParams->mu<<" ; sigma = "<<sigma<<std::endl;

// #ifdef ppc405
// 	time2= RceSvc::ticks();
// 	printf("sGuess fit time %f\n",(float)(time2-time1)/350.);
// #endif


	return status;  //0 = success


}


template<typename TP, typename TE>
int FitScurveLikelihoodFastInt<TP, TE>::roughFitRange(FitData *pfdi, FitData *pfdo, int& maxVal) {
  //For use in crosstalk scan.  Determines the start and end of the fit range, without requirement
  //that the data y-values reach a plateau close to the number of triggers.  
  //Sets maxVal to the highest y-value in the data, and the start and end of the fit range are
  //the points where y<0.2*maxVal and y>0.8*maxVal, respectively.  


  int nBins, status;
  UINT32 *x;
  UINT8 *y;
  
  status = 1;  //1: failure
  
  int hitsStart, hitsEnd;

  nBins = pfdi->n;
  y = pfdi->y;
  x = pfdi->x;

  for(hitsStart = 0; hitsStart < nBins; ++hitsStart){
    if(y[hitsStart] != 0) break;
  }

  if(hitsStart == (nBins-1))  //require more than one bin containing hits
    return status;
  if(hitsStart == 0)  //require first bin to be empty
    return status;
 
  if(y[nBins - 1] ==0){  //require hits in final bin
    return status;
  }


  maxVal=0;
  //set maxVal equal to highest y value
  for(int iBin = nBins-1; iBin >= 0; --iBin){
    if(y[iBin] > maxVal){
      maxVal = y[iBin];
      hitsEnd = iBin;
    }
  }

  if(hitsEnd<hitsStart){
    return status;
  }

  int firstBin=0, lastBin=0;
  
  //std::cout<<"hitsStart = "<<hitsStart<<" ; hitsEnd = "<<hitsEnd<<std::endl;

  for(int iBin = (hitsStart-1); iBin <=hitsEnd; ++iBin){
    if(y[iBin] >= 0.8*maxVal){
      lastBin = iBin;
      break;
    }
  }

  for(int iBin = (hitsStart-1); iBin <=hitsEnd; ++iBin){
    if(y[iBin] >= 0.2*maxVal){
      firstBin = iBin-1;
      break;
    }
  }

  // std::cout<<"firstBin = "<<firstBin<<" ; LastBin = "<<lastBin<<std::endl;

  if(firstBin>=lastBin){
    return status;
  }

  //Successfully determined first and last bin
  nBins = 1 + lastBin - firstBin;

  pfdo->n = nBins;
  pfdo->y = &y[firstBin];
  pfdo->x = &x[firstBin];

  return nBins;

}
