#include <boost/property_tree/ptree.hpp>
#include "dataproc/AtlasDataProc.hh"
#include "dataproc/Channeldefs.hh"
#include "config/ConfigIF.hh"
#include "config/RceFWRegisters.hh"
#include "util/Monitoring.hh"
#include "util/exceptions.hh"
#include "HW/RCDImasterL.hh"
#include <iomanip>
#include <iostream>
#include <stdlib.h>

void* AtlasDataProc::monitoring( void *ptr ){
  AtlasDataProc* dataproc=(AtlasDataProc*)ptr;
  dataproc->monitoring();
  return 0;
}
void AtlasDataProc::monitoring(){
  while(1){
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
    sleep(5);
    pthread_testcancel();
    unsigned nhits=0;
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, 0);
    try{
      unsigned mask=m_fwregs->getDisabledMask(0);
      for(int i=0;i<m_nModules;i++){
	for(int j=0;j<8;j++){
	  //Monitoring::setErrorCounter(m_outlink[i], m_fwregs->getEfbCounter(0, m_outlink[i], 
				    //(FWRegisters::EFBCOUNTER)j), (FWRegisters::EFBCOUNTER)j);
	}
	unsigned hitsmod=m_fwregs->getEfbCounter(0,m_outlink[i], FWRegisters::OCCUPANCY); 
	if((mask&(1<<m_outlink[i]))==0)nhits+=hitsmod;
	//Monitoring::setOccupancy(m_outlink[i], (float)hitsmod/m_normalization);
	//std::cout<<m_outlink[i]<<": "<<hitsmod<<std::endl;
      }
      //for(int j=0;j<3;j++)Monitoring::setTtcClashCounter(m_fwregs->getTtcClashCounter(0, (FWRegisters::CLASH)j), (FWRegisters::CLASH)j);
      //Monitoring::setAverageOccupancy((float)nhits/m_normalization/(float)m_nModules);
      //std::cout<<"All: "<<nhits<<std::endl;
      //Monitoring::setNumberOfEvents(m_fwregs->getNumberOfEvents(0));
      //Monitoring::setNMissed(m_fwregs->getNumberOfMonMissed(0));
      //Monitoring::setDisabledMask(mask);
      //Monitoring::setDisabledInRunMask(m_fwregs->getDisabledInRunMask(0));
      //Monitoring::publish();
    }catch(rcecalib::Pgp_Problem pr){
      std::cout<<"Register R/W failed"<<std::endl;
    }
  }
}

int AtlasDataProc::fit(std::string command){
  //if(command=="CloseFile"){
  //  std::cout<<"Stopping monitoring thread. "<<m_nfrag<<std::endl;
  //  pthread_cancel(m_thread);
  //}
  return 0;
}

int AtlasDataProc::processData(unsigned rlink, unsigned *data, int size){
  const char* errors[8]={"Too many headers ", "Timeout ", "Bad header ", "Skipped trigger ", 
		   "Data no header ", "Missing trigger ", "Desynched ", "Disabled "};
  unsigned short *datas=(unsigned short*)data;
  int status=2*size-14;
  //std::cout<<"Fragment of size "<<size<<std::endl;
  bool dump=false;
  if(m_firstdis==true){
    for(int i=0; i<=7;i++){
      if(datas[status+i]!=0){
	if(i==7){
	  m_firstdis=false;
	  //dump=false;
	} else dump=true;
	std::cout<<errors[i]<<std::hex<<datas[status+i]<<std::dec<<std::endl;
      }
    }
  }
  if(dump==true) dumpEvent(data, size);
  int ecr=data[5]>>24;
  if(ecr!=m_ecr && m_ecr!=-1){
    unsigned *olddata=0;
    int oldsize=0;
    std::cout<<"ECR has occured:"<<std::endl;
    if(m_nfrag>1){
      PgpTrans::RCDImaster::instance()->getOldData(1, olddata, oldsize);
      std::cout<<"Last event before ECR:"<<std::endl;
      dumpEvent(olddata, oldsize);
    }
    std::cout<<"First event after ECR::"<<std::endl;
    dumpEvent(data, size);
  }
  m_ecr=ecr;
  if(m_nfrag%100000==0)dumpEvent(data, size);
  if((data[5]&0xffffff)!=0){
    if(data[5]!=m_l1id+1){
      std::cout<<"Found L1id="<<std::hex<<data[5]<<". Expecting "<<m_l1id+1<<std::dec<<std::endl;
    }
  } 
  m_l1id=data[5];  
  m_nfrag++;
  return 0;
}

void AtlasDataProc::dumpEvent(unsigned* data, int size){
  if(size==4){
    std::cout<<"Previous buffer was a register access"<<std::endl;
    return;
  }else if(size>1000){
    std::cout<<"Large event. Size="<<size<<std::endl;
    return;
  }
  std::cout<<"Event Fragment number: "<<m_nfrag<<std::endl;
  for(int i=0;i<size;i++){
    printf("%08d: %08x\n", i, data[i]);
  }
  std::cout<<"-------"<<std::endl;
}


AtlasDataProc::AtlasDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif), m_ecr(-1), m_bcr(-1), m_nfrag(0), m_firstdis(true){
  std::cout<<"Start AtlasDataProc Constructor"<<std::endl;
  m_fwregs=new RceFWRegisters;
  try{ //catch bad scan option parameters
    m_nModules=m_configIF->getNmodules();
    //m_nL1AperEv=scanOptions->get<int>("trigOpt.nL1AperEvent");
    std::string name=scanOptions->get<std::string>("Name");
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
  m_normalization=m_fwregs->getOccNormalization(0);
  if(m_normalization==0){
    std::cout<<"Bad normalization. Setting to 1000"<<std::endl;
    m_normalization=1000;
  }
  if(m_nModules>0)m_normalization*=m_configIF->getModuleInfo(0).getNRows()*m_configIF->getModuleInfo(0).getNColumns();
  m_outlink=new int[m_nModules];
  for(int i=0;i<m_nModules;i++){
    m_outlink[i]=m_configIF->getModuleInfo(i).getOutLink();
  }
  //Monitoring::reset();
  //Monitoring::publish();
  //pthread_create( &m_thread, 0, AtlasDataProc::monitoring, (void*)this);
}

AtlasDataProc::~AtlasDataProc(){
  //void *status;
  //pthread_cancel(m_thread);
  //pthread_join(m_thread, &status);
  //if(status != PTHREAD_CANCELED){
  //  std::cout<<"Thread was not canceled properly!"<<std::endl;
  //}else{
  //  std::cout<<"Thread exited normally"<<std::endl;
 // }
  delete m_fwregs;
  delete [] m_outlink;
}

unsigned AtlasDataProc::nEvents(){
  return m_nfrag;
}
