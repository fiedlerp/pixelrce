#ifndef COSMICEVENTITERATOR_HH
#define COSMICEVENTITERATOR_HH

#include <vector>
#include <list>
#include <fstream>

class CosmicEvent;
namespace eudaq{
  class FileSerializer;
}

class CosmicEventIterator{
public:
  CosmicEventIterator(bool monitor, eudaq::FileSerializer *fs, std::ofstream *pf, unsigned runno, 
		      int nMod, std::vector<int> rces);
  ~CosmicEventIterator();
  void resynch();
  bool setTdcData(unsigned* data, int rceIndex);
  bool checkForWrite();
  bool inUse(std::list<CosmicEvent*>::iterator it);
  void flushEvents();
  int nEvents(){return m_nEvents;}
  inline bool checkForNewEvent(unsigned l1a, std::list<CosmicEvent*>::iterator it);
  bool addPixelData(int mod, unsigned l1id, int bxfirst, int bxlast, int ntrg, int rceIndex, 
		    bool isdut, unsigned* data, unsigned size);
  CosmicEvent* m_refevent;
private:
  void set(std::list<CosmicEvent*>::iterator it);
  unsigned m_runNo;
  int m_nMod;
  std::vector<int> m_rces;
  int m_nRce;
  std::list<CosmicEvent*> m_events;
  std::vector<std::list<CosmicEvent*>::iterator> m_modit;
  std::vector<std::list<CosmicEvent*>::iterator> m_tdcit;
  bool m_monitor;
  eudaq::FileSerializer *m_file;
  std::ofstream *m_pfile;
  bool m_99, m_101;
  int m_badEvents;
  unsigned int m_nEvents;
};
#endif
