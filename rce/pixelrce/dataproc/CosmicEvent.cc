#include "dataproc/CosmicEvent.hh"
#include "eudaq/FileSerializer.hh"
#include "eudaq/Event.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/ProducerIF.hh"
#include "eudaq/DataSenderIF.hh"
#include "dataproc/CosmicEventReceiver.hh"
#include <memory>

bool CosmicEvent::m_synch=false;
int CosmicEvent::m_lastTLU=0;

CosmicEvent::CosmicEvent(unsigned runno, unsigned l1a, unsigned nMod, std::vector<int>& rces):
  m_tdctrig(false), m_l1a(l1a), m_nMod(nMod), 
  m_runno(runno), m_rces(rces){
  
  for (unsigned int i=0;i<nMod;i++){
    m_ntrig.push_back(0);
    m_bx.push_back(999);
    m_bxlast.push_back(999);
    m_modRceIndex.push_back(9999);
  }
  
  m_nRce = m_rces.size();
  for(unsigned int i=0; i<m_nRce; i++){
    m_timestamp.push_back(0);
    m_tdcbx.push_back(999);
  }
  
  m_ctel=new eudaq::RawDataEvent("CTEL",m_runno,0);
  m_ctel->AddBlock(0);
  m_dut=new eudaq::RawDataEvent("APIX-CT",m_runno,0);
  m_dut->AddBlock(0);

  for(unsigned int iRce=0; iRce<m_nRce; iRce++){
    m_dut->AddBlock(iRce+1);
    unsigned rceNum = m_rces[iRce];
    rceNum |= 0x40000000;  //HeaderTwo format
    m_dut->AppendBlock(iRce+1,(char*)&rceNum,sizeof(unsigned));
  }

}

CosmicEvent::~CosmicEvent(){
  delete m_dut;
  delete m_ctel;
}
void CosmicEvent::setTdcData(unsigned firstword, unsigned* data, unsigned rceIndex){
  m_tdctrig=true;

  unsigned rceNum = m_rces[rceIndex]; 
  
  //std::cout<<"Appending TDC data: "<<std::endl;
  //std::cout<<std::hex<<rceNum<<std::dec<<std::endl;
  //std::cout<<std::hex<<firstword<<std::dec<<std::endl;
  //for(unsigned int i=0; i<8; i++){
  //  std::cout<<std::hex<<data[i]<<std::dec<<" "<<std::endl;
  //}
  
  m_dut->AppendBlock(0,(char*)&rceNum,sizeof(unsigned));
  m_dut->AppendBlock(0,(char*)&firstword,sizeof(unsigned));
  m_dut->AppendBlock(0,(char*)data,8*sizeof(unsigned));
  
  //timestamp is a 32 bit number giving the time of arrival of the trigger, in
  //units of clock ticks (25 ns)
  m_timestamp[rceIndex]=data[3];
  m_timestamp[rceIndex]<<=32;
  m_timestamp[rceIndex] |= data[4];
  m_tdcbx[rceIndex] = data[4]&0xff;
}

void CosmicEvent::appendCtelData(unsigned* data, unsigned size){
  m_ctel->AppendBlock(0,(char*)data,size*sizeof(unsigned));
}

void CosmicEvent::appendDutData(unsigned* data, unsigned size, unsigned rceIndex){

  //unsigned rceNum = m_rces[rceIndex]; 
  //std::cout<<"Appending dut data for rce "<<rceNum<<" :"<<std::endl;
  //for(unsigned int i=0; i<size; i++){
  //  std::cout<<std::hex<<data[i]<<std::dec<<" "<<std::endl;
  //}

  m_dut->AppendBlock(rceIndex+1,(char*)data,size*sizeof(unsigned));
}

void CosmicEvent::incrementTrigger(unsigned mod, int ntrgin, unsigned bxfirst, unsigned bxlast, unsigned rceIndex){
  if(m_modRceIndex[mod]>m_nRce){
    m_modRceIndex[mod]=rceIndex;
  }
  if(m_ntrig[mod]==0)m_bx[mod]=bxfirst;
  m_bxlast[mod]=bxlast;
  m_ntrig[mod]+=ntrgin;
}

bool CosmicEvent::consistent(CosmicEvent* ref){
  for (unsigned i=0;i<m_nMod;i++){
    // Check that all modules have the correct number of triggers
    if(m_ntrig[i]!=ref->m_ntrig[i]){
      std::cout<<"Module "<<i<<" has "<<m_ntrig[i]<<" triggers instead of "<<ref->m_ntrig[i]<<std::endl;
      return false;
    }
    //Check that first and last bxid are consistent with the number of triggers.
    int diffbx=(m_bxlast[i]+1-m_bx[i])%256;
    if(diffbx!=m_ntrig[i]){
      std::cout<<"Bad Module "<<i<<" firstbx "<<m_bx[i]<<" lastbx "<<m_bxlast[i]<< " ntrig "<<m_ntrig[i]<<std::endl;
      return false; 
    }
  }
  if(this==ref){ //Reference event only
    //Make sure that every module from the same RCE has the same first bx number
    //It's OK if the bx numbers for different RCE's are different
    std::cout<<"Now checking reference event"<<std::endl;
    for(unsigned int j_rce=0; j_rce<m_nRce; j_rce++){
      std::cout<<"Module "<<j_rce<<" has "<<m_ntrig[j_rce]<<" triggers."<<std::endl;
      int modIndex = getFirstModWithRceIndex(j_rce);
      for (unsigned int i_mod=modIndex+1; i_mod<m_nMod; i_mod++){
	if(m_modRceIndex[i_mod]==j_rce && m_bx[i_mod]!=m_bx[modIndex]){
	  std::cout<<"Module "<<i_mod<<" on RCE "<<j_rce<<" has BXID "<<m_bx[i_mod]<<" while module "
		   <<modIndex<<" has BXID "<<m_bx[modIndex]<<std::endl;
	  return false;
	}
      }
    }
  }
  //Make sure that the BXID dfference between this event and the reference event agrees for all RCEs and all modules.
  int bxiddiff=0;
  for(unsigned int j_rce=0; j_rce<m_nRce; j_rce++){
    int difftdc=(m_tdcbx[j_rce] - ref->m_tdcbx[j_rce])%256;
    if(j_rce==0){ //use RCE 0 as the reference for the difference
      bxiddiff=difftdc;
    }else{ //check the other RCEs
      if(difftdc!=bxiddiff){
	std::cout<<"Events between different RCEs are inconsistent."<<std::endl;
	return false;
      }
    }
  }
  //Check module BXID differences against RCEs
  for(unsigned int i=0;i<m_nMod;i++){
    int diffmod=(m_bx[i]-ref->m_bx[i])%256;
    if(diffmod!=bxiddiff){
      std::cout<<"Module "<<i<<" has a BXID difference of "<<diffmod<<", the TDC difference is "<<bxiddiff<<std::endl;
      return false;
    }
  }
  return true;
}
  
void CosmicEvent::writeEvent(bool monitor, eudaq::FileSerializer* efile, std::ofstream* pfile, unsigned &evtno){
  
  if(monitor==true){
    unsigned long long firstTimeStamp = m_timestamp[0];
    for(unsigned i_rce=1; i_rce<m_nRce; i_rce++){
      if(m_timestamp[i_rce]<firstTimeStamp){
	firstTimeStamp = m_timestamp[i_rce];
      }
    }
    
    eudaq::DetectorEvent *dev=new eudaq::DetectorEvent(m_runno,evtno,firstTimeStamp);
    std::shared_ptr<eudaq::Event> ev(dev);
    //    m_ctel->SetEventNumber(evtno);
    std::shared_ptr<eudaq::Event> cpc(m_ctel);
    dev->AddEvent(cpc);
    // m_dut->SetEventNumber(evtno);
    std::shared_ptr<eudaq::Event> cpd(m_dut);
    dev->AddEvent(cpd);
    if(efile!=0)dev->Serialize(*efile);
    if(pfile!=0){
      const unsigned char* block0=&m_dut->GetBlock(0)[0];
      size_t lenTdc=m_dut->GetBlock(0).size();
      
      unsigned totalsize=8+lenTdc;
      
      for(unsigned iRce=0; iRce<m_nRce; iRce++){
	size_t lenPix=m_dut->GetBlock(iRce+1).size();
	totalsize += lenPix;
      }
      
      pfile->write((char*)&totalsize, 4);
      pfile->write((char*)&evtno, 4);
      pfile->write((const char*)block0, lenTdc);
      
      for(unsigned iRce=0; iRce<m_nRce; iRce++){
	size_t lenPix=m_dut->GetBlock(iRce+1).size();
	const unsigned char* blockRce=&m_dut->GetBlock(iRce+1)[0];
	pfile->write((const char*)blockRce, lenPix);
      }
      
    }
    CosmicEventReceiver::receiveEvent(ev);  //this function adds event to monitoring GUI
    m_ctel=0;
    m_dut=0;
    evtno++;
  }else if (DataSenderIF::hasInstance()) {
    // sending event to CosmicGui
    // K. Barry

    // Code for when CosmicGui is receiving events via TCP.
    // This code assumes that the DUT and CTEL data have 
    // been kept separate. It amalgamates them into one 
    // DetectorEvent and sends that. This is to be 
    // consistent with the way the data is written to file 
    // in file-ouput mode of running.
    
    //    m_ctel->SetEventNumber(evtno);
    std::shared_ptr<eudaq::Event> cpc(m_ctel);
    // m_dut->SetEventNumber(evtno);
    std::shared_ptr<eudaq::Event> cpd(m_dut);

    // Need to modify this to include the whole event -- DONE already?
    //    DataSenderIF::SendEvent(m_dut); 

    unsigned long long firstTimeStamp = m_timestamp[0];
    for(unsigned i_rce=1; i_rce<m_nRce; i_rce++){
      if(m_timestamp[i_rce]<firstTimeStamp){
        firstTimeStamp = m_timestamp[i_rce];
      }
    }
    
    eudaq::DetectorEvent dev(m_runno, evtno, firstTimeStamp);
    dev.AddEvent(cpc);
    dev.AddEvent(cpd);
    DataSenderIF::SendEvent(&dev);

    m_ctel = 0;
    m_dut = 0;
    evtno++;
  } else {
    // send event to eudaq
    //std::cout<<"Sending Eudet event."<<std::endl;
    int tlu=(m_dut->GetBlock(0)[37]<<8)|m_dut->GetBlock(0)[36];
    //if(!m_synch&&tlu-1!=m_lastTLU)std::cout<<"Bad tlu "<<tlu<<" last "<<m_lastTLU<<std::endl;
    if(m_synch&&(tlu-1!=m_lastTLU && (tlu!=0 || m_lastTLU!=32767))){
      if(tlu<m_lastTLU)tlu+=32768;
      std::cout<<"Inserting "<<tlu-m_lastTLU<<" events. TLU id="<<tlu<<" old tlu="<<m_lastTLU<<std::endl;
      for(int i=m_lastTLU+1;i<tlu;i++){
        CosmicEvent *ev=makeDummyEvent(m_runno, i, evtno++);
	ProducerIF::sendEvent(ev->m_dut);
	delete ev;
      }
      m_synch=false;
      std::cout<<"Done inserting events."<<std::endl;
    }
    m_lastTLU=tlu;
    //    m_dut->SetEventNumber(evtno++);
    ProducerIF::sendEvent(m_dut);
  }


} 

CosmicEvent* CosmicEvent::makeDummyEvent(int runno, int tlu, int evtno){
   std::vector<int> bla;
   bla.push_back(0);
   CosmicEvent* ev=new CosmicEvent(runno,0,0,bla);
   unsigned block[8]={0,0,0,0,0,0,0,0};
   unsigned char* bc=(unsigned char*)block;
   bc[29]=(tlu&0x7f00)>>8;
   bc[28]=tlu&0xff;
   ev->setTdcData(0,block, 0);
   //   ev->m_dut->SetEventNumber(evtno);
   return ev;
}
  

void CosmicEvent::print(){
  std::cout<<"Event printout"<<std::endl;
  std::cout<<"=============="<<std::endl;
  std::cout<<"L1A: "<<m_l1a<<std::endl;
  std::cout<<"Event with "<<m_nRce<<" RCEs."<<std::endl;
  std::cout<<"RCE's: ";
  for (unsigned int i=0;i<m_nRce;i++){
    std::cout<<"RCE "<<m_rces[i]<<":   Timestamp: "<<std::hex<<m_timestamp[i]
	     <<std::dec<<" , TDC BX: "<<m_tdcbx[i]<<std::endl;
  }
  
  if(m_nMod>(16*m_nRce))std::cout<<"ERROR: Number of modules is "<<m_nMod<<std::endl;
  else{
    for (unsigned int i=0;i<m_nMod;i++){
      std::cout<<"Module "<<i<<" BX: "<<m_bx[i]<<std::endl;
    }
    std::cout<<std::endl;
  }
}

unsigned CosmicEvent::getFirstModWithRceIndex(unsigned rceIndex){
  
  for(unsigned i=0; i<m_nMod; i++){

    if(m_modRceIndex[i] == rceIndex){
      return i;
    }
  
  }    
  
  //No module found with the given rce index. Return invalid index.
  return m_nMod+1;
  
}

CosmicEventReceiver* CosmicEventReceiver::s_receiver=0;
