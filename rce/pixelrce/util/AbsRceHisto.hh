#ifndef ABS_RCE_HISTO_HH
#define ABS_RCE_HISTO_HH

#include <string>

class Provider;

class AbsRceHisto{
public:
  AbsRceHisto(const char* name, const char* title, int ndim);
  virtual ~AbsRceHisto();
  std::string name() const {return m_name;}
  std::string title() const { return m_title; };
  int nDim() const { return m_ndim; };
  virtual int nBin(int d) const { if (d >=0 && d <= 1) return m_dim[d]; else return 0; };
  float min(int d) const { if (d >=0 && d <= 1) return m_lim[d][0]; else return 0; };
  float max(int d) const { if (d >=0 && d <= 1) return m_lim[d][1]; else return 0; };
  void setAxisTitle(int axis, const char* title);
  const char* axisTitle(int axis);
  //retrieve histograms
  virtual void publish(Provider*)=0;
protected:
  int m_ndim;             //! Number of dimensions (1 or 2)
  std::string m_name;     //! Histogram name
  std::string m_title;    //! Histogram title
  std::string m_axisTitle[2]; //! Axis titles
  unsigned int  m_dim[2]; //! Dimensions
  float m_lim[2][2];     //! Limits
  float m_quot[2];
  float m_const[2];
};

#endif
