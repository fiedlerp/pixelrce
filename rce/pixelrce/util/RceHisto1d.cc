#ifndef RCEHISTO1D_CC
#define RCEHISTO1D_CC

#include "util/RceHisto1d.hh"
#include "util/RceHistoAxis.hh"
#include "util/HistoManager.hh"
#include <string>
#include <assert.h>

template<typename T, typename TE>
RceHisto1d<T, TE>::RceHisto1d(std::string name, std::string title, unsigned int nbin, float xmin, float xmax, bool hasErrors)
  :AbsRceHisto(name.c_str(),title.c_str(),1), m_hasErrors(hasErrors){
  m_dim[0] = nbin;
  m_dim[1] = 1;
  m_lim[0][0] = xmin;
  m_lim[0][1] = xmax;
  m_lim[1][0] = 0;
  m_lim[1][1] = 0;
//  if(xmax-xmin!=0)m_quot[0]=float(nbin)/(xmax-xmin);
//  else m_quot[0]=0;
//  m_const[0]=xmin*m_quot[0];
  m_data=new T[nbin];
  if(m_hasErrors)m_err=new TE[nbin];
  else m_err=0;
  for (unsigned int i=0; i<m_dim[0]; i++){
    m_data[i]=0;
    if(m_hasErrors)m_err[i]=0;
  }
} 

template<typename T, typename TE>
RceHisto1d<T, TE>::RceHisto1d(const RceHisto1d<T, TE> &h):AbsRceHisto(std::string(h.name()+"_copy").c_str(),h.title().c_str(),1){
  m_dim[0] = h.m_dim[0];
  m_dim[1] = h.m_dim[1];
  m_lim[0][0] = h.m_lim[0][0];
  m_lim[0][1] = h.m_lim[0][1];
  m_lim[1][0] = h.m_lim[1][0];
  m_lim[1][1] = h.m_lim[1][1];
  m_hasErrors = h.m_hasErrors;
  m_quot[0]=h.m_quot[0];
  m_const[0]=h.m_const[0];
  m_data=new T[m_dim[0]];
  if(m_hasErrors)m_err=new TE[m_dim[0]];
  else m_err=0;
  for (int i=0;i<m_dim[0];i++){
    m_data[i]=h(i);
    if(m_hasErrors)m_err[i]=h.getBinError(i);
  }
}
  

template<typename T, typename TE>
RceHisto1d<T, TE>::~RceHisto1d(){
  delete [] m_data;
  if(m_hasErrors)delete [] m_err;
}

template<typename T, typename TE>
RceHisto1d<T, TE>& RceHisto1d<T, TE>::operator=(const RceHisto1d<T, TE>&h){
  if (&h == this) {
    return *this;
  } else { 
    HistoManager* mgr=HistoManager::instance();
    assert(mgr!=0);
    mgr->removeFromInventory(this);
    delete [] m_data;
    if(m_hasErrors)delete [] m_err;
    m_ndim = h.m_ndim;
    m_name = h.m_name+"_copy";
    m_title = h.m_title;
    m_dim[0] = h.m_dim[0];
    m_dim[1] = h.m_dim[1];
    m_lim[0][0] = h.m_lim[0][0];
    m_lim[0][1] = h.m_lim[0][1];
    m_lim[1][0] = h.m_lim[1][0];
    m_lim[1][1] = h.m_lim[1][1];
    m_hasErrors = h.m_hasErrors;
    m_quot[0]=h.m_quot[0];
    m_const[0]=h.m_const[0];
    m_data =  new T[m_ndim];
    if(m_hasErrors)m_err =  new TE[m_ndim];
    for (int i=0;i<m_dim[0];i++){
      m_data[i]=h(i);
      if(m_hasErrors)m_err[i]=h.getBinError(i);
    }
    mgr->addToInventory(this);
  }
}
    
    
template<typename T, typename TE>
T RceHisto1d<T, TE>::operator()(unsigned int i) const{
  return m_data[i];
 }
template<typename T, typename TE>
TE RceHisto1d<T,TE>::getBinError(unsigned i) const{
  if(!m_hasErrors)return 0;
  return m_err[i];
}
template<typename T, typename TE>
void RceHisto1d<T, TE>::set(unsigned int i, T val) {
  if(i<m_dim[0])m_data[i]=val;
 }
template<typename T, typename TE>
void RceHisto1d<T, TE>::setFast(unsigned int i, T val) {
  m_data[i]=val;
 }
template<typename T, typename TE>
void RceHisto1d<T, TE>::setBinError(unsigned int i, TE val) {
  if(!m_hasErrors)return;
  if(i<m_dim[0])m_err[i]=val;
 }
template<typename T, typename TE>
void RceHisto1d<T, TE>::setBinErrorFast(unsigned int i, TE val) {
  if(!m_hasErrors)return;
  m_err[i]=val;
 }

template<typename T, typename TE>
void RceHisto1d<T, TE>::fill(unsigned int i, T val) {
  if(i<m_dim[0])m_data[i]+=val;
 }
template<typename T, typename TE>
void RceHisto1d<T, TE>::fillFast(unsigned int i, T val) {
  m_data[i]+=val;
 }
//template<typename T, typename TE>
//void RceHisto1d<T, TE>::fillByValue(float x, T val) {
//  if(x>=m_lim[0][0]&&x<m_lim[0][1]){
//    unsigned bin=int (m_quot[0]*x-m_const[0] );
//    m_data[bin]+=val;
//  }
//}

template<typename T, typename TE>
void RceHisto1d<T, TE>::increment(unsigned int i) {
  if(i<m_dim[0])m_data[i]++;
}
template<typename T, typename TE>
void RceHisto1d<T, TE>::incrementFast(unsigned int i) {
  m_data[i]++;
}
  
template<typename T, typename TE>
void RceHisto1d<T, TE>::clear() {
  for (unsigned int i=0;i<m_dim[0];i++){
    m_data[i]=0;
    if(m_hasErrors)m_err[i]=0;
  }
}


template<typename T, typename TE>
void RceHisto1d<T, TE>::publish(Provider* p){
  double width=1;
  if(nBin(0)>0)width=(max(0)-min(0))/nBin(0);
  RceHistoAxis  xaxis(m_axisTitle[0].c_str(), m_dim[0], min(0), width );
  // Create Annotations
  std::vector<std::pair<std::string,std::string> > annotations;
  annotations.push_back( std::make_pair("RceHisto1d", "RawProvider publish") );
  
  p->publish(m_name, m_title, xaxis, m_data, m_err, false, -1, annotations);
}
  

#endif
