#ifndef DATACOND_HH
#define DATACOND_HH

#include "util/RceMutex.hh"

class DataCond{
public:
  DataCond(): cond(&mutex), waitingForData(false){}
  rce_mutex mutex ;
  rce_condition cond  ;
  bool waitingForData;
};

#endif
