#ifndef RCEHISTO2D_CC
#define RCEHISTO2D_CC

#include "util/RceHisto2d.hh"
#include "util/RceHistoAxis.hh"
#include "util/HistoManager.hh"
#include <iostream>
#include <assert.h>

template<typename T, typename TE>
RceHisto2d<T, TE>::RceHisto2d(std::string name, std::string title, unsigned int nbin, float xmin, float xmax,
			      unsigned int nbin2, float ymin, float ymax, bool isreversed, bool hasErrors)
  :AbsRceHisto(name.c_str(),title.c_str(),2), m_hasErrors(hasErrors), m_reverse(isreversed){
  m_dim[0] = nbin;
  m_dim[1] = nbin2;
  m_lim[0][0] = xmin;
  m_lim[0][1] = xmax;
  m_lim[1][0] = ymin;
  m_lim[1][1] = ymax;

//  if(xmax-xmin!=0)m_quot[0]=float(nbin)/(xmax-xmin);
//  else m_quot[0]=0;
//  m_const[0]=xmin*m_quot[0];  
//  if(ymax-ymin!=0)m_quot[1]=float(nbin2)/(ymax-ymin);
//  else m_quot[1]=0;
//  m_const[1]=ymin*m_quot[1];  

  m_data = new T[m_dim[0]*m_dim[1]];
  if(m_hasErrors) m_err = new TE[m_dim[0]*m_dim[1]];
  else m_err=0;
  for (unsigned int i=0; i<m_dim[0]*m_dim[1]; i++) {
    m_data[i] = 0;
    if(m_hasErrors)m_err[i]=0;
  }
}
template<typename T, typename TE>
RceHisto2d<T, TE>::RceHisto2d(const RceHisto2d<T, TE> &h):AbsRceHisto(std::string(h.name()+"_copy").c_str(),h.title().c_str(),2){
  m_dim[0] = h.m_dim[0];
  m_dim[1] = h.m_dim[1];
  m_lim[0][0] = h.m_lim[0][0];
  m_lim[0][1] = h.m_lim[0][1];
  m_lim[1][0] = h.m_lim[1][0];
  m_lim[1][1] = h.m_lim[1][1];
  m_hasErrors = h.m_hasErrors;
  m_reverse = h.m_reverse;
  m_quot[0]=h.m_quot[0];
  m_const[0]=h.m_const[0];
  m_quot[1]=h.m_quot[1];
  m_const[1]=h.m_const[1];
  m_data = new T[m_dim[0]*m_dim[1]];
  if(m_hasErrors) m_err = new TE[m_dim[0]*m_dim[1]];
  else m_err=0;
  for (unsigned int i=0; i<m_dim[0]*m_dim[1]; i++) {
    m_data[i] = 0;
    if(m_hasErrors)m_err[i]=0;
  }
}
  

template<typename T, typename TE>
RceHisto2d<T, TE>::~RceHisto2d(){
  delete [] m_data;
  if(m_hasErrors)delete [] m_err;
}

template<typename T, typename TE>
RceHisto2d<T, TE>& RceHisto2d<T, TE>::operator=(const RceHisto2d<T, TE>&h){
  if (&h == this) {
    return *this;
  } else { 
    HistoManager* mgr=HistoManager::instance();
    assert(mgr!=0);
    mgr->removeFromInventory(this);
    delete [] m_data;
    if(m_hasErrors)delete [] m_err;
    m_ndim = h.m_ndim;
    m_name = h.m_name+"_copy";
    m_title = h.m_title;
    m_dim[0] = h.m_dim[0];
    m_dim[1] = h.m_dim[1];
    m_lim[0][0] = h.m_lim[0][0];
    m_lim[0][1] = h.m_lim[0][1];
    m_lim[1][0] = h.m_lim[1][0];
    m_lim[1][1] = h.m_lim[1][1];
    m_hasErrors = h.m_hasErrors;
    m_reverse =  h.m_reverse;
    m_quot[0]=h.m_quot[0];
    m_const[0]=h.m_const[0];
    m_quot[1]=h.m_quot[1];
    m_const[1]=h.m_const[1];
    m_data = new T[m_dim[0]*m_dim[1]];
    if(m_hasErrors) m_err = new TE[m_dim[0]*m_dim[1]];
    else m_err=0;
    for (unsigned int i=0; i<m_dim[0]*m_dim[1]; i++) {
      m_data[i] = h.m_data[i];
      if(m_hasErrors)m_err[i]=h.m_err[i];
    }
    mgr->addToInventory(this);
  }
}
    
    
template<typename T, typename TE>
inline T RceHisto2d<T, TE>::operator()(unsigned int i, unsigned int j) {
  return m_data[index(i,j)];
 }

template<typename T, typename TE>
inline TE RceHisto2d<T, TE>::getBinError(unsigned int i, unsigned int j) {
  if(!m_hasErrors)return 0;
  return m_err[index(i,j)];
 }

template<typename T, typename TE>
inline void RceHisto2d<T, TE>::set(unsigned int i, unsigned j, T val) {
  if(i<m_dim[0]&&j<m_dim[1])m_data[index(i,j)]=val;
 }
template<typename T, typename TE>
inline void RceHisto2d<T, TE>::setFast(unsigned int i, unsigned j, T val) {
  m_data[index(i,j)]=val;
 }

template<typename T, typename TE>
inline void RceHisto2d<T, TE>::setBinError(unsigned int i, unsigned j, TE val) {
  if(!m_hasErrors)return;
  if(i<m_dim[0]&&j<m_dim[1])m_err[index(i,j)]=val;
 }
template<typename T, typename TE>
inline void RceHisto2d<T, TE>::setBinErrorFast(unsigned int i, unsigned j, TE val) {
  if(!m_hasErrors)return;
  m_err[index(i,j)]=val;
 }

template<typename T, typename TE>
inline void RceHisto2d<T, TE>::fill(unsigned int i, unsigned j, T val) {
  if(i<m_dim[0]&&j<m_dim[1])m_data[index(i,j)]+=val;
 }
template<typename T, typename TE>
inline void RceHisto2d<T, TE>::fillFast(unsigned int i, unsigned j, T val) {
  m_data[index(i,j)]+=val;
 }
//template<typename T, typename TE>
//inline void RceHisto2d<T, TE>::fillByValue(float x, float y, T val) {
//  if(x>=m_lim[0][0]&&x<m_lim[0][1] && y>=m_lim[1][0]&&y<m_lim[1][1]){
//    unsigned binx=int (m_quot[0]*x-m_const[0] );
//    unsigned biny=int (m_quot[1]*y-m_const[1] );
//    m_data[index(binx,biny)]+=val;
//  }
//}
template<typename T, typename TE>
inline void RceHisto2d<T, TE>::increment(unsigned int i, unsigned j) {
  if(i<m_dim[0]&&j<m_dim[1])m_data[index(i,j)]++;
}
template<typename T, typename TE>
inline void RceHisto2d<T, TE>::incrementFast(unsigned int i, unsigned j) {
  m_data[index(i,j)]++;
}


  
template<typename T, typename TE>
void RceHisto2d<T, TE>::clear() {
  for (unsigned int i=0;i<m_dim[0]*m_dim[1];i++){
    m_data[i]=0;
    if(m_hasErrors)m_err[i]=0;
  }
}


template<typename T, typename TE>
void RceHisto2d<T, TE>::publish(Provider* p){
  double widthx=1;
  if(nBin(0)>0)widthx=(max(0)-min(0))/nBin(0);
  double widthy=1;
  if(nBin(1)>0)widthy=(max(1)-min(1))/nBin(1);
  RceHistoAxis xaxis(m_axisTitle[0].c_str(), m_dim[0], min(0),widthx);
  RceHistoAxis yaxis(m_axisTitle[1].c_str(),m_dim[1], min(1), widthy);
  // Create Annotations
  std::vector<std::pair<std::string,std::string> > annotations;
  annotations.push_back( std::make_pair("RceHisto2d", "RawProvider publish") );

  T* sdata=0;
  TE* serr=0;
  // if x and y were swapped for performance reasons swap them back before shipping
  if(m_reverse){
    sdata=new T[m_dim[0]*m_dim[1]];
    if(m_err)serr=new TE[m_dim[0]*m_dim[1]];
    for (unsigned i=0;i<m_dim[0]*m_dim[1];i++){
      sdata[i]=m_data[(i%m_dim[1])*m_dim[0]+(i/m_dim[1])];
      if(m_err)serr[i]=m_err[(i%m_dim[1])*m_dim[0]+(i/m_dim[1])];
    }
  }
  
  // Publish in OH
  if(m_reverse) p->publish(m_name, m_title, yaxis, xaxis, sdata, serr, false, -1, annotations);
  else p->publish(m_name, m_title, xaxis, yaxis, m_data, m_err, false, -1, annotations);
  if(m_reverse){
    delete [] sdata;
    if(m_hasErrors)delete [] serr;
  }
}
  
template<typename T, typename TE>
inline int RceHisto2d<T, TE>::index(int x, int y){
  return x+y*m_dim[0];
}

#endif
