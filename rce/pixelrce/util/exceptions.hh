#ifndef SCANCTRLEXCEPTIONS_HH
#define SCANCTRLEXCEPTIONS_HH

#include <exception>

namespace rcecalib{

  struct Config_File_Error: public std::exception{
    const char* what() const throw(){
      return "Config File Error";
    }
  };
  struct Unknown_Module_Type: public std::exception{
    const char* what() const throw(){
      return "Unknown Module Type";
    }
  };
  struct Param_exists: public std::exception{
    const char* what() const throw(){
      return "Parameter exists";
    }
  };
  struct Pgp_Problem: public std::exception{
    const char* what() const throw(){
      return "Pgp not responsive";
    }
  };
};


#endif
