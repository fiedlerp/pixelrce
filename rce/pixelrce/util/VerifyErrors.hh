#ifndef VERIFY_ERRORS_HH
#define VERIFY_ERRORS_HH

namespace ModuleVerify{

  enum verify_errors{OK=0, GLOBAL_READBACK_FAILED=0x1, GLOBAL_READBACK_DIFFERENT=0x2, PIXEL_WRONG_N_WORDS=0x4, 
		     PIXEL_READBACK_DIFFERENT=0x8, NO_FORMATTER=0x100, NO_MODULE=0x200};

};

#endif
