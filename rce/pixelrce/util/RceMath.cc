#include "util/RceMath.hh"

namespace RceMath {
// Calculates SQRT fast, but not very precise
// Dirty playing with bits can give us the sqrt of a float
// Depends on the 32bit architecture
// See: http://bits.stephan-brumme.com/squareRoot.html
float fastCoarseSqrt(float x) { 
	// Get around dirty casting
	union {
		float x;
		unsigned int i;
	} u;
	u.x = x;
	// adjust bias
	u.i  += 127 << 23;
	// approximation of square root
	u.i >>= 1;
	//printf("sqrt(%f)=%f\n",x, u.x);
	return u.x;
}

// Calcutes SQRT fast and kinda precise, but no as fast as fastCoarseSqrt()
// Uses basically the same method, but adds a newton approximation step
// to increase precision
// Taken from Quake3 engine
// See: http://www.lomont.org/Math/Papers/2003/InvSqrt.pdf
float fastQuakeSqrt(const float x) {
	const float xhalf = 0.5f*x;
   	union { // get bits for floating value
	        float x;
		int i;
    	} u;
  	u.x = x;
  	u.i = 0x5f3759df - (u.i >> 1);  // gives initial guess y0
	float result = x*u.x*(1.5f - xhalf*u.x*u.x);// Newton step, repeating increases accuracy
	//printf("sqrt(%f)=%f\n",x, result);
	return result;
}	

}
