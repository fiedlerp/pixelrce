#include "HW/SerialPgpBw.hh"
#include "HW/RCDImasterL.hh"
#include "HW/BitStream.hh"
#include <iostream>
#include <assert.h>




SerialPgpBw::SerialPgpBw(): SerialIF() {
}


void SerialPgpBw::Send(BitStream* bs, int opt){
  PgpTrans::RCDImaster* pgp= PgpTrans::RCDImaster::instance();
  //  std::cout<<"sendWaitX"<<std::endl;
  unsigned serstat;
  if(opt&WAITFORDATA){
        //std::cout<<"SerialPgpBw::Trigger"<<std::endl;
    serstat=pgp->blockWrite(&((*bs)[0]),bs->size(),0,opt&SerialIF::BYTESWAP);// no handshake
  }else{
    //    std::cout<<"SerialPgpBw::handshake bitstream size is "<<bs->size()<<std::endl;
    //pgp->blockWrite(buf,sz,1,opt&SerialIF::BYTESWAP);// handshake
    serstat=pgp->blockWrite(&((*bs)[0]),bs->size(),1,opt&SerialIF::BYTESWAP);// handshake
  }
  assert(serstat==0);

  if((opt&SerialIF::DONT_CLEAR)==0)bs->clear();
}
void SerialPgpBw::SetChannelInMask(unsigned linkmask){
  BitStream bs;
  for(int i=0;i<5;i++)bs.push_back(0);
  Send(&bs, 0); //clear FIFO
  unsigned serstat=PgpTrans::RCDImaster::instance()->writeRegister(0,linkmask);
  assert(serstat==0);
}
void SerialPgpBw::SetChannelOutMask(unsigned linkmask){
  unsigned serstat=PgpTrans::RCDImaster::instance()->writeRegister(13,linkmask);
  assert(serstat==0);
}
int SerialPgpBw::EnableTrigger(bool on){
  unsigned serstat;
  if(on)
    serstat=PgpTrans::RCDImaster::instance()->sendCommand(3);
  else
    serstat=PgpTrans::RCDImaster::instance()->sendCommand(5);
  assert(serstat==0);
  return 0;
}
unsigned SerialPgpBw::SendCommand(unsigned char opcode){
  return PgpTrans::RCDImaster::instance()->sendCommand(opcode);
}
unsigned SerialPgpBw::WriteRegister(unsigned addr, unsigned val){
  return PgpTrans::RCDImaster::instance()->writeRegister(addr,val);
}
unsigned SerialPgpBw::ReadRegister(unsigned addr, unsigned& val){
  return PgpTrans::RCDImaster::instance()->readRegister(addr, val);
}
unsigned SerialPgpBw::WriteBlockData(std::vector<unsigned>& data){
  return PgpTrans::RCDImaster::instance()->blockWrite(&data[0],data.size(),1, 0);// handshake
}
unsigned SerialPgpBw::ReadBlockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec){
  return PgpTrans::RCDImaster::instance()->blockRead(&data[0],data.size(), retvec);
}
unsigned SerialPgpBw::ReadBuffers(std::vector<unsigned char>& retvec){
  return PgpTrans::RCDImaster::instance()->readBuffers(retvec);
}
