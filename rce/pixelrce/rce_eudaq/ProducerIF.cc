#include "eudaq/ProducerIF.hh"

#include "eudaq/Producer.hh"

eudaq::Producer* ProducerIF::m_producer=0;

void ProducerIF::setProducer(eudaq::Producer* producer){
  m_producer=producer;
}

void ProducerIF::sendEvent(eudaq::Event *ev){
  if(m_producer!=0){
    m_producer->SendEvent(*ev);
  }
}
