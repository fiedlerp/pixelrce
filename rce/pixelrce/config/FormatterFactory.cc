#include "config/FormatterFactory.hh"
#include "config/AbsFormatter.hh"
#include "config/DummyFormatter.hh"
#include "config/FEI3/JJFormatter.hh"
#include "config/FEI4/FEI4AFormatter.hh"
#include "config/FEI4/FEI4BFormatter.hh"
#include "config/FEI4/FEI4HitorFormatter.hh"
#include "config/FEI4/FEI4OccFormatter.hh"
#include "config/afp-hptdc/AFPHPTDCFormatter.hh"
#include "util/exceptions.hh"
#include "stdio.h"
#include <iostream>


AbsFormatter* FormatterFactory::createFormatter(const char* formatter, int id){
  
  // create formatter first
  if(std::string(formatter)=="JJ")
    return new JJFormatter(id);
  else if(std::string(formatter)=="" || std::string(formatter)=="Hitbus")
    return 0;
  else if(std::string(formatter)=="FEI4A")
    return new FEI4AFormatter(id);
  else if(std::string(formatter)=="FEI4B")
    return new FEI4BFormatter(id);
  else if(std::string(formatter)=="FEI4Hitor")
    return new FEI4HitorFormatter(id);
  else if(std::string(formatter)=="FEI4Occ")
    return new FEI4OccFormatter(id);
  else if(std::string(formatter)=="HPTDC")
    return new AFPHPTDCFormatter(id);
  else if(std::string(formatter)=="Dummy")
    return new DummyFormatter(id);
  else{
    std::cout<<"Unknown formatter "<<formatter<<std::endl;
    assert(0);
  }
}
