#ifndef PIXELCONFIGFACTORY_HH
#define PIXELCONFIGFACTORY_HH

class PixelConfig;

class PixelConfigFactory{
public:
  PixelConfig* createConfig(const char*);
};

#endif
