#ifndef CONFIGIF_HH
#define CONFIGIF_HH
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ModuleInfo.hh"
#include <vector>

class ModuleFactory;
class AbsModule;
class AbsModuleGroup;
class AbsTrigger;

class ConfigIF{
public:
  ConfigIF(ModuleFactory*);
  virtual ~ConfigIF(){}
  int setupParameter(const char* name, int val, bool enable=true);
  int setupMaskStage(int stage);
  int sendTrigger();
  int setChannelMask();
  void disableAllChannels();
  void configureModulesHW(bool enable=true);
  void configureModuleHW(int outlink);
  int verifyModuleConfigHW(int id);
  int enableTrigger();
  int disableTrigger();
  unsigned sendHWcommand(unsigned char opcode);
  unsigned writeHWregister(unsigned addr, unsigned val);
  unsigned readHWregister(unsigned addr, unsigned &val);
  unsigned writeHWblockData(std::vector<unsigned>& data);
  unsigned readHWblockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec);
  unsigned readHWbuffers(std::vector<unsigned char>& retvec);
  void resetCountersHW();
  void resetErrorCountersHW();
  int nTrigger();
  int configureScan(boost::property_tree::ptree *scanOptions);
  unsigned getNmodules();
  ModuleInfo getModuleInfo(int mod);
  const float dacToElectrons(int mod, int fe, int dac);
  void deleteModules();
  void resetFE();
  void enableDataTakingHW();
  int setupTriggerIF(const char* type);
  int setupModule(const char* name, const char* type, unsigned id, unsigned inLink, unsigned outLink, const char* formatter);
private:
  std::vector<AbsModule*> m_modules;
  std::vector<AbsModuleGroup*> m_modulegroups;
  ModuleFactory* m_moduleFactory;
  AbsTrigger* m_trigger;
  unsigned m_linkInMask;
  unsigned m_linkOutMask;

};

#endif
