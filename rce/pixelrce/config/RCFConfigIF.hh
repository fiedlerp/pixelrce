#ifndef RCFCONFIGIF_HH
#define RCFCONFIGIF_HH

#include "rcf/RCFConfigIFAdapter.hh"
#include "config/ConfigIF.hh"
#include "config/ModuleFactory.hh"

class ModuleFactory;

class RCFConfigIF: public RCFConfigIFAdapter, public ConfigIF {
public:
  RCFConfigIF(ModuleFactory* mf);
  ~RCFConfigIF();
  void RCFsendTrigger();
  uint32_t RCFsetupParameter(std::string name, int32_t val);
  void RCFsetupMaskStage(int32_t stage);
  void RCFenableTrigger();
  void RCFdisableTrigger();
  void RCFresetFE();
  void RCFconfigureModulesHW();
  void RCFconfigureModuleHW(int outlink);
  int32_t RCFverifyModuleConfigHW(int32_t id);
  uint32_t RCFwriteHWregister(uint32_t addr, uint32_t val);
  uint32_t RCFsendHWcommand(uint8_t opcode);
  uint32_t RCFwriteHWblockData(std::vector<uint32_t> data);
  uint32_t RCFreadHWblockData(std::vector<uint32_t>  data, std::vector<uint32_t>& retv);
  uint32_t RCFreadHWbuffers(std::vector<uint8_t> &retv);
  uint32_t RCFreadHWregister(uint32_t addr, uint32_t &val);
  int32_t RCFdeleteModules();
  int32_t RCFnTrigger();
  int32_t RCFsetupTriggerIF(std::string type);
  int32_t RCFsetupModule(std::string name, std::string type, ModSetup par, std::string formatter);

};
    

#endif
