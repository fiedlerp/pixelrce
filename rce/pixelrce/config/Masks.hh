#ifndef MASKS_HH
#define MASKS_HH

  struct Masks{
    unsigned short oMask;
    unsigned short iMask;
    unsigned short xtalk_on;
    unsigned short xtalk_off;
    bool crosstalking;
  };

#endif
