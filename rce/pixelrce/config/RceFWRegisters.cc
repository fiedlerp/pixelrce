#include "config/RceFWRegisters.hh"
#include "HW/SerialIF.hh"
#include "util/exceptions.hh"
#include <iostream>
void RceFWRegisters::writeRegister(int rce, unsigned reg, unsigned val){
  int badHSIOconnection=SerialIF::writeRegister(reg, val);
  if(badHSIOconnection){
    std::cout << "writeRegister failed: reg= "<<reg<< " val= " << val << std::endl;
    rcecalib::Pgp_Problem err;
    throw err;
  }
}
unsigned RceFWRegisters::readRegister(int rce, unsigned reg){
  unsigned val;
  int badHSIOconnection=SerialIF::readRegister(reg, val);
  if(badHSIOconnection){
    std::cout << "ReadRegister failed: reg= "<<reg<< " val= " << val << std::endl;
    rcecalib::Pgp_Problem err;
    throw err;
  }
  return val;
}
void RceFWRegisters::sendCommand(int rce, unsigned opcode){
    SerialIF::sendCommand(opcode);
}
