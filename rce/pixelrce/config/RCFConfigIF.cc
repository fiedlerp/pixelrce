
#include "config/RCFConfigIF.hh"

RCFConfigIF::RCFConfigIF(ModuleFactory* mf): ConfigIF(mf){
}

RCFConfigIF::~RCFConfigIF(){
}
  
uint32_t RCFConfigIF::RCFsetupParameter(std::string name, int32_t val){
  setupParameter(name.c_str(), val, false); //do not enable data taking because chip may be unconfigured
  return 0;
}
void RCFConfigIF::RCFsetupMaskStage(int32_t stage){
  setupMaskStage(stage);
}
void RCFConfigIF::RCFsendTrigger(){
  sendTrigger();
}
void RCFConfigIF::RCFenableTrigger(){
  enableTrigger();
}
void RCFConfigIF::RCFdisableTrigger(){
  disableTrigger();
}
void RCFConfigIF::RCFresetFE(){
  resetFE();
}
void RCFConfigIF::RCFconfigureModulesHW(){
  configureModulesHW();
}
void RCFConfigIF::RCFconfigureModuleHW(int outlink){
  configureModuleHW(outlink);
}
int32_t RCFConfigIF::RCFverifyModuleConfigHW(int32_t id){
  return verifyModuleConfigHW(id);
}
uint32_t RCFConfigIF::RCFwriteHWregister(uint32_t addr, uint32_t val){
  return writeHWregister(addr,val);
}

uint32_t RCFConfigIF::RCFreadHWregister(uint32_t addr, uint32_t& val){
  return readHWregister(addr, (uint32_t&)val);
}

uint32_t RCFConfigIF::RCFsendHWcommand(uint8_t opcode){
  return sendHWcommand(opcode);
}

uint32_t RCFConfigIF::RCFwriteHWblockData(std::vector<uint32_t> data){
  return writeHWblockData(data);
}

uint32_t RCFConfigIF::RCFreadHWblockData(std::vector<uint32_t> data, std::vector<uint32_t>& retv){
  unsigned retval=readHWblockData(data, retv);
  return retval;
}
uint32_t RCFConfigIF::RCFreadHWbuffers(std::vector<uint8_t> &retv){
  unsigned retval=readHWbuffers(retv);
  return retval;
}
  
int32_t RCFConfigIF::RCFnTrigger(){
  return nTrigger();
}
int32_t RCFConfigIF::RCFsetupTriggerIF(std::string type){
  setupTriggerIF(type.c_str());
  return 0;
}
int32_t RCFConfigIF::RCFsetupModule(std::string name, std::string type, const ModSetup par, std::string formatter){
  //std::cout<<"RCFsetupModule"<<std::endl;
  return setupModule(name.c_str(), type.c_str(), par.id, par.inLink, par.outLink, formatter.c_str());
}
int32_t RCFConfigIF::RCFdeleteModules(){
  deleteModules();
  return 0;
}

