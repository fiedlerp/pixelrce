#ifndef MCC_HH
#define MCC_HH

#include <map>
#include "HW/BitStream.hh"

namespace FEI3{

class Mcc{
public:
  enum Register{MCC_CSR, MCC_LV1, MCC_FEEN, MCC_WFE, MCC_WMCC, MCC_CNT, MCC_CAL, MCC_PEF, MCC_NREG};
  Mcc();
  ~Mcc();
  void setRegister(Register reg, unsigned val);
  void setNLvl1(unsigned val);
  void enableDelay(bool on);
  void setStrobeDelayRange(unsigned val);
  void setStrobeDelay(unsigned val);
  void setStrobeDuration(unsigned val);
  void setBandwidth(unsigned val);
  void setChipEnables(unsigned val);
  void writeRegister(BitStream* bs, Mcc::Register reg);
  void configureHW();
  void clear();
  int setParameter(const char* name, int val);
  // MCC related commands
  static void writeRegister(BitStream* bs, Mcc::Register reg, unsigned value);
  static void resetMCC(BitStream *bs);
  
  
private:
  unsigned short m_register[8];

};

};
#endif
