#include "config/FEI3/RegularMaskStaging.hh"
#include "config/FEI3/Module.hh"

namespace FEI3{
  
  RegularMaskStaging::RegularMaskStaging(FEI3::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI3::Module>(mod, masks, type){
    m_nStages=strtoul(type.substr(6).c_str(),0,10);
  }
  
  void RegularMaskStaging::setupMaskStageHW(int maskStage){
    if(maskStage==0){
      clearBitsHW();
      for (int i=0;i<Module::N_FRONTENDS;i++){
	int nPixelsPerChip = Frontend::N_ROWS * Frontend::N_COLS;
	for(int index=maskStage;index<nPixelsPerChip;index+=m_nStages){
	  int row = (int) index % Frontend::N_ROWS;
	  int col = (int) index / Frontend::N_ROWS;
	  if( col%2 ) row = 159 - row;
	  PixelRegister *pixel=m_module->frontend(i)->getPixelRegister(col,row);
	  pixel->set(pixel->get() | m_masks.iMask);
	}
      }
      m_module->configureHW();
    }else{
      m_module->resetHW(false); // only reset MCC, not FE
      m_module->mcc()->configureHW();
      //std::cout<<"Shifting enables"<<std::endl;
      for (int i=0;i<Module::N_FRONTENDS;i++){
	m_module->frontend(i)->shiftEnablesHW(m_masks.iMask);
      }
    }
  }
}
