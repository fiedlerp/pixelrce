
#include "config/FEI3/FECommands.hh"

namespace FEI3{

  void FECommands::mccWriteRegister(BitStream *bs, Mcc::Register reg, unsigned value){
    bs->push_back(0x1); /* header (10110) + field2 (1011) + field3 (0000) */
    bs->push_back(0x6B000000 | (reg << 16) | (value & 0xffff)); /* field4 = (address(3:0)) + data(15:0) */
    bs->push_back(0); // padding a la NewDsp
  }
  void FECommands::resetMCC(BitStream *bs){
    bs->push_back(0x16B900);
    bs->push_back(0); // padding a la NewDsp
  }
  void FECommands::resetFE(BitStream *bs, unsigned width) {
    bs->push_back(0x16BA00 | (width & 0xf));
    bs->push_back(0);
  }
  void FECommands::enableDataTaking(BitStream *bs) {
    bs->push_back(0);
    bs->push_back(0x0016B800);
    bs->push_back(0);
  }
  void FECommands::L1A(BitStream *bs){
    bs->push_back(0xE8000000);
  }
  void FECommands::sendECR(BitStream *bs){
    bs->push_back(0);
    bs->push_back(0x00162000); /* MCC ECR command */
    bs->push_back(0);
  }
  void FECommands::sendBCR(BitStream *bs){
    bs->push_back(0);
    bs->push_back(0x00161000); /* MCC BCR command */
    bs->push_back(0);
  }
  void FECommands::feWriteCommand(BitStream *bs, unsigned chip, int cmd, bool rw){
    if(rw){
      bs->push_back(0x16B50);	
    }else{
      bs->push_back(0x16B40); // write only
    }
    unsigned mask = 0x00800000;
    unsigned command=cmd;
    unsigned parity = 0;
    for(int k=1;k<24;++k,mask>>=1) {
      if(mask & command) parity ^= 1;
    }
    command |= parity;
    command <<= 5; /* for geographical address */	
    command |= chip;
    for(int k=7;k>-1;--k) {
      unsigned word=0;
      for (int i=3;i>=0;i--){
	word<<=8;
	if((command>>(k*4+i))&0x1)word|=0xff;
      }
      bs->push_back(word);
    }
  }
	

};
