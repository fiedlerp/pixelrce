#ifndef RCFFEI3MODULE_HH
#define RCFFEI3MODULE_HH

#include "config/FEI3/Module.hh"
#include "rcf/PixelModuleConfig.hh"
#include "rcf/RCFFEI3Adapter.hh"

class AbsFormatter;

namespace FEI3{
class RCFModule: public RCFFEI3Adapter, public FEI3::Module {
public:
  RCFModule(RCF::RcfServer& server, const char *name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt);
  ~RCFModule();
  int32_t RCFdownloadConfig(ipc::PixelModuleConfig config);    
  void globalLegacyToUsr(ipc::PixelFEConfig* legacy, unsigned chip);    
private:
  RCF::RcfServer& m_server;

};
};
  

#endif
