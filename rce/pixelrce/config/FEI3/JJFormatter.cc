#include "config/FEI3/JJFormatter.hh"
#include "config/FormattedRecord.hh"
#include "config/Endianness.hh"
#include "config/BFU.hh"
#include <stdio.h>
//#define DEBUG
/* ====================================================================== */
/* NEW DECODER                                                            */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/*! brief Maps out the sizes of the objects that are common amongst 
    various classes.                                                      */
/* ---------------------------------------------------------------------- */
namespace Size
{
    /* ------------------------------------------------------------------ */
    /*!< \brief The sizes of common objects                               */
    /* ------------------------------------------------------------------ */
  //  enum Size::Size
enum Size
    {
        HEADER = 5,
          L1ID = 8,
         SYNCH = 1,
          BCID = 8,
           KEY = 4,
         FENUM = 4,
          LEFT = 32-(HEADER +  L1ID + SYNCH + BCID + SYNCH + KEY),
        ZEROES = LEFT - 1
    };


    /* ------------------------------------------------------------------ */
    /*!< \brief Size of the three possible record types                   */
    /* ------------------------------------------------------------------ */
    namespace Record
    {
      enum Record
        {
             HEADER = 31,
              MCCFE =  9,
                HIT = 22,
            TRAILER = 23
        };
    };
}
/* ---------------------------------------------------------------------- */



/* ---------------------------------------------------------------------- *//*!

  \brief The value of the key field.

   The key field describes the 4-bit object identifier
                                                                          */
/* ---------------------------------------------------------------------- */
namespace Key
{
  enum Key
    {
        MCCFE    = 0xe,
        FEFLAG   = 0xf,
        TRAILER  = 0x0
    };
}
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- *//*!

  \brief The value of the left-justified key field including the SYNCH bit
                                                                          */
/* ---------------------------------------------------------------------- */
namespace SynchKey
{
  enum Key
    {
      MCCFE    = (1<<31) | (::Key::MCCFE   << 27),
      FEFLAG   = (1<<31) | (::Key::FEFLAG  << 27),
      TRAILER  = (1<<31) | (::Key::TRAILER << 27)
    };
}
/* ---------------------------------------------------------------------- */



/* ---------------------------------------------------------------------- *//*!

  \brief Maps out the first 32-bits of the record
                                                                          */
/* ---------------------------------------------------------------------- */
namespace Header
{
    /* -------------------------------- *\
     |                                  |
     | 0123456789abcdef0123456789abcdef |
     | -------------------------------- |
     | hhhhhllllllllsbbbbbbbbskkkkffffs |
     | 11101........1........11110....1 |
     |                                  |
    \* -------------------------------- */
    struct MccFe
    {
#if     ENDIANNESS_IS_BIG

        unsigned int header: Size::HEADER;
        unsigned int   l1id: Size::  L1ID;
        unsigned int synch0: Size:: SYNCH;
        unsigned int   bcid: Size::  BCID;
        unsigned int synch1: Size:: SYNCH;
        unsigned int    key: Size::   KEY;
        unsigned int  fenum: Size:: FENUM;
        unsigned int synch2: Size:: SYNCH;

#elif   ENDIANNESS_IS_LITTLE

        unsigned int synch2: ::Size::SYNCH;
        unsigned int  fenum: Size::FENUM;
        unsigned int    key: Size::  KEY;
        unsigned int synch1: Size:: SYNCH;
        unsigned int   bcid: Size::  BCID;
        unsigned int synch0: Size:: SYNCH;
        unsigned int   l1id: Size::  L1ID;
        unsigned int header: Size::HEADER;

#else

#error  ENDIANNESS is not defined

#endif
    };


    /* -------------------------------- *\
     |                                  |
     | 0123456789abcdef0123456789abcdef |
     | -------------------------------- |
     | hhhhhllllllllsbbbbbbbbskkkkxxxxx |
     | 11101........1........1........  |
     |                                  |
    \* -------------------------------- */
    struct Generic
    {
#if     ENDIANNESS_IS_BIG

        unsigned int header: Size::HEADER;
        unsigned int   l1id: Size::  L1ID;
        unsigned int synch0: Size:: SYNCH;
        unsigned int   bcid: Size::  BCID;
        unsigned int synch1: Size:: SYNCH;
        unsigned int    key: Size::   KEY;
        unsigned int   left: Size::  LEFT;

#elif   ENDIANNESS_IS_LITTLE

        unsigned int   left: Size::  LEFT;
        unsigned int    key: Size::   KEY;
        unsigned int synch1: Size:: SYNCH;
        unsigned int   bcid: Size::  BCID;
        unsigned int synch0: Size:: SYNCH;
        unsigned int   l1id: Size::  L1ID;
        unsigned int header: Size::HEADER;

#else

#error  ENDIANNESS is not defined

#endif
    };


    /* -------------------------------- *\
     |                                  |
     | 0123456789abcdef0123456789abcdef |
     | -------------------------------- |
     | hhhhhllllllllsbbbbbbbbskkkksxxxx |
     | 11101........1........1....1...  |
     |                                  |
    \* -------------------------------- */
    struct Trailer
    {
#if     ENDIANNESS_IS_BIG

        unsigned int header: Size::HEADER;
        unsigned int   l1id: Size::  L1ID;
        unsigned int synch0: Size:: SYNCH;
        unsigned int   bcid: Size::  BCID;
        unsigned int synch1: Size:: SYNCH;
        unsigned int    key: Size::   KEY;
        unsigned int synch2: Size:: SYNCH;
        unsigned int zeroes: Size::ZEROES;

#elif   ENDIANNESS_IS_LITTLE

        unsigned int zeroes: Size::ZEROES;
        unsigned int synch2: Size:: SYNCH;
        unsigned int    key: Size::   KEY;
        unsigned int synch1: Size:: SYNCH;
        unsigned int   bcid: Size::  BCID;
        unsigned int synch0: Size:: SYNCH;
        unsigned int   l1id: Size::  L1ID;
        unsigned int header: Size::HEADER;

#else

#error  ENDIANNESS is not defined

#endif
    };
    

    union Header
    {
        unsigned int  ui;
        Generic       bf;
        MccFe         fe;
        Trailer  trailer;
    }
    u;
}
/* ---------------------------------------------------------------------- */




/* ---------------------------------------------------------------------- *//*!

  \brief Maps out the extracted value word when it is interpretted as an
         MCC-FE record
                                                                          */
/* ---------------------------------------------------------------------- */
namespace MccFe
{
    struct MccFe_bf
    {
#if     ENDIANNESS_IS_BIG

        unsigned int synch: ::Size::SYNCH;
      unsigned int   key: ::Size::  KEY;
        unsigned int fenum: Size::FENUM;
        unsigned int   pad: 32 - (::Size::SYNCH + ::Size::KEY + Size::FENUM);

#elif   ENDIANNESS_IS_LITTLE

        unsigned int   pad: 32 - (::Size::SYNCH + ::Size::KEY + Size::FENUM);
        unsigned int fenum: Size::FENUM;
      unsigned int   key: ::Size::  KEY;
        unsigned int synch: ::Size::SYNCH;

#endif

    };

    union MccFe
    {
        unsigned int ui;
        MccFe_bf     bf;
    };
}
/* ---------------------------------------------------------------------- */




/* ---------------------------------------------------------------------- *//*!

  \brief Maps out the extracted value word when it is interpretted as a
         HIT record
                                                                          */
/* ---------------------------------------------------------------------- */
namespace Hit
{
  enum Size
    {
        ROW = 8,
        COL = 5,
        TOT = 8,
        VAL = ROW + COL + TOT
    };

    struct Hit_val
    {

#if     ENDIANNESS_IS_BIG

        unsigned int synch: ::Size::SYNCH;
        unsigned int   val: VAL;
        unsigned int   pad: 32 - (::Size::SYNCH + VAL);

#elif   ENDIANNESS_IS_LITTLE

      unsigned int   pad: 32 - (::Size::SYNCH + VAL);
        unsigned int   val: VAL;
      unsigned int synch: ::Size::SYNCH;

#endif
    };

    struct Hit_bf
    {

#if     ENDIANNESS_IS_BIG

      unsigned int synch: ::Size::SYNCH;
      unsigned int   row: ROW;
      unsigned int   col: COL;
      unsigned int   tot: TOT;
      unsigned int   pad: 32 - (::Size::SYNCH + VAL);

#elif   ENDIANNESS_IS_LITTLE

      unsigned int   pad: 32 - (::Size::SYNCH + VAL);
      unsigned int   tot: TOT;
      unsigned int   col: COL;
      unsigned int   row: ROW;
      unsigned int synch: ::Size::SYNCH;

#endif
    };

    union Hit
    {
        unsigned int ui;
        Hit_val     val;
        Hit_bf       bf;
    };

}
/* ---------------------------------------------------------------------- */



/* ---------------------------------------------------------------------- *//*!

  \brief Maps out the extracted value word when it is interpretted as a
         FEFLAG record
                                                                          */
/* ---------------------------------------------------------------------- */
namespace FeFlag
{
  enum Size
    {
        MB1 = 1,
        MCC = 8,
        FE  = 8,
        VAL = MB1 + MCC + FE
    };


    struct FeFlag_val
    {
#if     ENDIANNESS_IS_BIG

        unsigned int synch: ::Size::SYNCH;
      unsigned int   key: ::Size::  KEY;
        unsigned int   val: VAL;
        unsigned int   pad: 32 - (::Size::SYNCH + ::Size::KEY + VAL);

#elif   ENDIANNESS_IS_LITTLE

        unsigned int   pad: 32 - (::Size::SYNCH + ::Size::KEY + VAL);
        unsigned int   val: VAL;
      unsigned int   key: ::Size::  KEY;
        unsigned int synch: ::Size::SYNCH;

#endif
    };

    struct FeFlag_bf
    {

#if     ENDIANNESS_IS_BIG

        unsigned int synch: ::Size::SYNCH;
        unsigned int   key: Size::  KEY;
        unsigned int   mb1: MB1;
        unsigned int   mcc: MCC;
        unsigned int    fe: FE;
        unsigned int   pad: 32 - (::Size::SYNCH + ::Size::KEY + VAL);

#elif   ENDIANNESS_IS_LITTLE

        unsigned int   pad: 32 - (::Size::SYNCH + ::Size::KEY + VAL);
        unsigned int    fe: FE;
        unsigned int   mcc: MCC;
        unsigned int   mb1: MB1;
      unsigned int   key: ::Size::  KEY;
        unsigned int synch: ::Size::SYNCH;

#endif
    };

    union FeFlag
    {
        unsigned int ui;
        FeFlag_val  val;
        FeFlag_bf    bf;
    };
}
/* ---------------------------------------------------------------------- */


/* ====================================================================== */
/* DEBUGGING AIDs                                                         */
/* ---------------------------------------------------------------------- */
#ifdef DEBUG

static inline void print_header_fe (Header::Header hdr)
{           
    printf ("MccFe: %8.8x H:%2.2x L1:%2.2x Synch:%1u BC:%2.2x "
            "Synch:%1u Key:%1.1x Fenum:%1x Synch:%1x\n",
            hdr.ui, 
            hdr.fe.header,
            hdr.fe.l1id,
            hdr.fe.synch0,
            hdr.fe.bcid,
            hdr.fe.synch1,
            hdr.fe.key,
            hdr.fe.fenum,
            hdr.fe.synch2);
    return;
}

static inline void print_header_trailer (Header::Header hdr)
{
    printf ("Trailer: %8.8x H:%2.2x L1:%2.2x Synch:%1u BC:%2.2x "
            "Synch:%1u Key:%1.1x Synch:%1u zeroes:%2.2x\n",
            hdr.ui, 
            hdr.trailer.header,
            hdr.trailer.l1id,
            hdr.trailer.synch0,
            hdr.trailer.bcid,
            hdr.trailer.synch1,
            hdr.trailer.key,
            hdr.trailer.synch2,
            hdr.trailer.zeroes
            );
    return;
}

static inline void print_title (Header::Header hdr)
{
    printf (
"    Value  What        Raw    Fe Row Col Tot  Mb1 Mcc  Fe  L1id  BCid\n" 
" --------  ------- --------  ---- --- --- ---  --- --- ---   %2x    %2x\n",
    hdr.bf.l1id,
    hdr.bf.bcid);
    return;
}

static inline void print_mccfe0 (Header::Header header)
{
    printf ("           MccFe   ........  %4x ... ... ...  ... ... ...\n", 
            header.fe.fenum);
    return;
}


static inline void print_hit (unsigned int val)
{
    Hit::Hit hit;
    hit.ui = val;
    printf (" %8.8x  Hit     %8.8x  .... %3x %3x %3x  ... ... ...\n",
            hit.ui,
            hit.val.val,
            hit.bf.row,
            hit.bf.col,
            hit.bf.tot);
    return;
}

static inline void print_mccfe (unsigned int val)
{
    MccFe::MccFe mccfe;
    mccfe.ui = val;
    printf (" %8.8x  MccFe   ........  %4x ... ... ...  ... ... ...\n", 
            mccfe.ui,
            mccfe.bf.fenum);
    return;
}

static inline void print_feflag (unsigned int val)
{
    FeFlag::FeFlag feflag;
    feflag.ui = val;
    printf (" %8.8x  FeFlag  %8.8x  .... ... ... ...  %3x %3x %3x\n",
            feflag.ui,
            feflag.val.val,
            feflag.bf.mb1,
            feflag.bf.mcc,
            feflag.bf.fe);
    return;
}

static inline void print_error   (unsigned int val)
{
    printf (" %8.8x   Error\n", val);
    return;
}


static inline void print_trailer (unsigned int val)
{
    printf (" %8.8x  Trailer\n", val);
    return;
}

#else
#define print_header_fe(_hdr)
#define print_header_trailer(_hdr)
#define print_title(_hdr)
#define print_mccfe0(_intro)
#define print_hit(_val)
#define print_mccfe(_val)
#define print_feflag(_val)
#define print_error(_val)
#define print_trailer(_val)

#endif
/* ====================================================================== */





/* ---------------------------------------------------------------------- */
namespace Decoder {
namespace Status  {


/* ---------------------------------------------------------------------- *//*!

  \brief Enumerate the various return status codes that decoder can return

   The usual UNIX convention is used. If the value is
      - > 0, none currently
      - ==0, Successful
      - < 0, Error
                                                                          */
/* ---------------------------------------------------------------------- */
enum Status
{
    OK            =  0, /*!< Clean finish                                 */
    HeaderSynch   = -1, /*!< Initial synch bit in the header not found    */
    L1IdSynch     = -2, /*!< Synch bit preceeding the LI id not found     */
    BCIdSynch     = -3, /*!< Synch bit preceeding the BC id not found     */
    HeaderTrailer = -4, /*!< Malformed trailer key occurred in the header */
    HeaderKey     = -5, /*!< An illegal key occurred in the header, only
                             MccFe and Trailer keys are allowed           */
    PrematureEos  = -6, /*!< The bit stream-terminated too soon. There
                             must be at least a TRAILER records worth of
                             bits after the header record has been
                             processed.                                   */
    NoSynch       = -7, /*!< Synch bit on a record after the header was
                             not found.                                   */
};

}}



/* ====================================================================== *//*!

  \brief The record decoder

  \param buffer The record buffer to decoder
  \param buflen The length, in 32-bit words, of \a buffer
                                                                          */
/* ---------------------------------------------------------------------- */
int JJFormatter::decode (const unsigned int *buffer, int buflen, unsigned* parsedData, int &parsedsize, int &nL1A)
{
    using namespace Decoder;

    int            end = buflen * 32;
    int       position = 0;
    Header::Header hdr;
    const unsigned int *buf = buffer;
    Hit::Hit hit;
    MccFe::MccFe mccfe;

    // Loop over all events in this buffer
    while (1)
    {
        unsigned int val;
        unsigned int cur = buffer[position >> 5];
	while(position < end && (signed)_bfu_testBit(cur,position)>=0){
	  _bfu_wordL (val, cur, buf, position, 1);
	  position += 1; 
	}
	
        // Check if have enough bits to continue
        if (position + Size::Record::HEADER >= end) return Status::OK;


        _bfu_wordL (hdr.ui, cur, buf, position, Size::Record::HEADER);
        position += Size::Record::HEADER;
        print_title  (hdr);
	//	printf("%x\n",hdr.ui);

	if(hdr.ui==0)return Status::OK;
        // If the synch bit (its in the sign bit) is not set, then error 
        if ((signed)hdr.ui > 0) 
        {
            print_error (hdr.ui);
            return Status::HeaderSynch;
        }

        unsigned int key = hdr.bf.key;

        /*
         | In the usual case, the key should be MCCFE.
         | The only other legitimate value is the TRAILER.
        */
        if (key != Key::MCCFE)
        {
            // Either immediate end or error
            if (key == Key::TRAILER)
            {
                //print_header_trailer (hdr);

                // Need another 14 bits of 0s to complete the trailer
                _bfu_wordL (val, cur, buf, position, Size::Record::TRAILER
                                                   - Size::Record::MCCFE);
                position += Size::Record::TRAILER - Size::Record::MCCFE;

                // If non-zero, error
                if (val)
                {
                    print_error (hdr.ui);
                    return Status::HeaderTrailer;
                }

                print_trailer (hdr.ui);
		nL1A++;
		FormattedRecord fr(FormattedRecord::HEADER);
		fr.setL1id(hdr.trailer.l1id);
		fr.setBxid(hdr.trailer.bcid);
		parsedData[parsedsize++]=fr.getWord();
                continue;
            }

            // Illegitimate record key found 
            print_error (hdr.ui);
            return Status::HeaderKey;
        }


        // Print the header as holding an MCC FE record
        print_mccfe  (hdr.ui);
        mccfe.bf.fenum = hdr.fe.fenum;
	FormattedRecord fr(FormattedRecord::HEADER);
	fr.setL1id(hdr.fe.l1id);
	fr.setBxid(hdr.fe.bcid);
	parsedData[parsedsize++]=fr.getWord();
	nL1A++;


        /*
         | Start the real decoding
         | The loop grabs 22 bits at a time on the premise that the only 
         | object not at least that long is the MCC-FE. Note the trailer
         | is 23 bits, so if it is encountered, one more bit needs to be
         | grabbed. If the MCC-FE is found, special action is taken which
         | consists of (effectively) pushing the overread bits back into
         | the input stream.
        */
        while (1)
        {        
            // Must have at least 23 bits remaining for the trailer
            if (position + Size::Record::TRAILER >= end)
            {
                print_error (0xffffffff);
                return Status::PrematureEos;
            }

            _bfu_wordL (val, cur, buf, position, Size::Record::HIT);
            position += Size::Record::HIT;

            /*
             | Since the synch bit is in the sign bit, if the signed 
             | interpretation is greater than 0, then, error
            */
            if ((signed)val > 0)
            {
                // Error, the stream is lost
                print_error (val);
                return Status::NoSynch;
            }


            if (val < (unsigned)SynchKey::MCCFE)
            {
                // This is either a hit or the trailer
                if ((val << 1) == 0)
                {
                    /*
                     | Looks like the trailer, but maybe HIT = 0 exception
                     | The next bit determines which. If this is not the end
                     | then this bit is effectively the synch bit for the
                     | next record. If it is, we don't want to read it, that
                     | would leave the bit stream at the wrong place. So here
                     | we just test it.
                    */                    
                    val = _bfu_testBit (cur, position);

                    // If >=0, then have hit the trailer for this event
                    if ((signed)val >= 0) 
                    {
                        print_trailer (val);

                        // The test bit belongs with the trailer, update pos
			_bfu_wordL (val, cur, buf, position, 1);
                        position += 1;

                        // Check if any left
                        if (position +  Size::Record::HEADER >= end)
                        {
                            /*
                             | !!! Need to check that the remaining bits to
                             | the end of the 32-bit word are 0.
                            */
                            return Status::OK;
                        }

                        /* 
                         | Done with this event, but still more to process 
                         | The reestablishment of 'cur' happens at the top
                         | of loop that reads the header.
                        */
                        break;
                    }

                    // This was the case of ROW = 0, COL = 0 TOT = 0
                    // Just continue
                }

                // Just a hit
                print_hit (val);
	        hit.ui=val;
		FormattedRecord fr(FormattedRecord::DATA);
		fr.setToT(hit.bf.tot);
		fr.setCol(hit.bf.col);
		fr.setRow(hit.bf.row);
		fr.setFE(mccfe.bf.fenum);
		parsedData[parsedsize++]=fr.getWord();
            }


            else if (val < (unsigned)SynchKey::FEFLAG)
            {
                /*
                 | MCCFe, have pulled too many bits, put the excess back
                 | Have pulled 22 bits, 
                 |    5 are the synch and key,
                 |    4 are the fenum
                 | This means 22 - 9 must be pushed backed
                */ 
                print_mccfe (val);
                mccfe.ui=val;
                position -= (Size::Record::HIT - Size::Record::MCCFE);
                buf =  buffer + (position >> 5);
                cur = *buf;
            }

            else 
            {
                print_feflag (val);
                //printf("fe flag\n");
            }
        }
    }
}
/* ====================================================================== */
