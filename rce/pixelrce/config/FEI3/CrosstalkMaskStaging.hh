#ifndef FEI3CROSSTALKMASKSTAGING_HH
#define FEI3CROSSTALKMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI3{
  class Module;
  
  class CrosstalkMaskStaging: public MaskStaging<FEI3::Module>{
  public:
    CrosstalkMaskStaging(FEI3::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
  };
  
}
#endif
