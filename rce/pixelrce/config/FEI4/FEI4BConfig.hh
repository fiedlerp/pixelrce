#ifndef FEI4BCONFIG_HH
#define FEI4BCONFIG_HH

#include "config/PixelConfig.hh"
#include "PixelFEI4BConfig.hh"

class FEI4BConfig: public PixelConfig{
public:
  FEI4BConfig(std::string filename);
  virtual ~FEI4BConfig(){
    delete m_config;
  }

  virtual int downloadConfig(int rce, int id);
  virtual void* getStruct(){return (void*)m_config;}
  virtual FECalib getFECalib(int chip);
  virtual void setFECalib(int chip, FECalib fe);
  virtual unsigned getThresholdDac(int chip, int col, int row);
  virtual void setThresholdDac(int chip, int col, int row, int val);
  virtual unsigned getFeedbackDac(int chip, int col, int row);
  virtual void setFeedbackDac(int chip, int col, int row, int val);
  virtual unsigned getIf(int chip);
  virtual void setIf(int chip, int val);
  virtual unsigned getGDac(int chip);
  virtual void setGDac(int chip, int val);
  virtual void setGDacCoarse(int chip, int val);
  virtual unsigned getFEMask(int chip, int col, int row);
  virtual void setFEMask(int chip, int col, int row, int val);
  virtual void writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key);
  virtual void configureFormatter(AbsFormatter*);
  virtual std::string getEnableMaskFile(){return m_enablefile;}
  virtual std::string getHitbusMaskFile(){return m_hitbusfile;}

private:
  ipc::PixelFEI4BConfig *m_config;
  std::string m_enablefile, m_hitbusfile;
};

#endif
