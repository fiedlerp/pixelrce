#include "config/FEI4/AnalogSimpleColprMaskStaging.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  AnalogSimpleColprMaskStaging::AnalogSimpleColprMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    m_nStages=strtoul(type.substr(12).c_str(),0,10);
    m_colpr_mode=strtoul(type.substr(10,1).c_str(),0,10);
    if(m_colpr_mode==0)m_nColStages=40;
    else if(m_colpr_mode==1)m_nColStages=4;
    else if(m_colpr_mode==2)m_nColStages=8;
    else m_nColStages=1;
    std::cout<<"Set up COLPR mask staging with "<<m_nColStages<<" col stages and "<< m_nStages<<" row stages"<<std::endl;
  }
  
  void AnalogSimpleColprMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    unsigned dcolStage=maskStage%m_nColStages;
    unsigned stage=maskStage/m_nColStages;
    if((maskStage+m_nColStages)/m_nColStages!=(maskStage+m_nColStages-1)/m_nColStages){ //need to set up pixel mask 
      global->setField("Colpr_Mode", 3, GlobalRegister::SW); 
      for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
	if(m_masks.iMask&(1<<i)){
	  pixel->setupMaskStage(i, stage, m_nStages);
	  m_module->writeDoubleColumnHW(i, i, 0, 0); 
	}
      }
    }
    global->setField("Colpr_Mode", m_colpr_mode, GlobalRegister::SW); 
    global->setField("Colpr_Addr", dcolStage+1, GlobalRegister::HW); 
  }
}
