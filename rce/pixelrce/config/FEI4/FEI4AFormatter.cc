#include "config/FEI4/FEI4AFormatter.hh"
#include "config/FEI4/FEI4ARecord.hh"
#include "config/FormattedRecord.hh"
#include "scanctrl/RceCallback.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <stdio.h>

FEI4AFormatter::FEI4AFormatter(int id):
  AbsFormatter(id, "FEI4A"), m_hitDiscCnfg(0){
  char name[128], title[128];
  sprintf(name, "Mod_%d_FEI4_Errors", id);
  sprintf(title, "Module %d FEI4 Errors", id);
  m_errhist=new RceHisto1d<int, int>(name, title, 32, -.5, 31.5);
}
FEI4AFormatter::~FEI4AFormatter(){
  delete m_errhist;
  
}
void FEI4AFormatter::configure(boost::property_tree::ptree* config){
  try{
    std::cout<<"hitdiscconfig was "<<m_hitDiscCnfg<<std::endl;
    m_hitDiscCnfg=config->get<int>("HitDiscCnfg");
    std::cout<<"Setting hitdiscconfig to "<<m_hitDiscCnfg<<std::endl;
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
  }
  m_errhist->clear();
}

int FEI4AFormatter::decode (const unsigned int *buffer, int buflen, unsigned* parsedData, int &parsedsize, int &nL1A){
  if(buflen==0)return FEI4::FEI4ARecord::Empty;
  unsigned char* bytepointer=(unsigned char*)buffer;
  unsigned char* last=bytepointer+buflen*sizeof(unsigned)-3;
  FEI4::FEI4ARecord rec;
  //bool header=false;
  while(bytepointer<=last){
    rec.setRecord(bytepointer); 
    if (rec.isEmptyRecord()){
      //header=false;
    }else if(rec.isData()){
      //if(header==false)return FEI4::FEI4ARecord::NoHeader;
      //there are potentially 2 hits in one data record
      if(rec.getTotBottom()<0xe || (m_hitDiscCnfg!=0 && rec.getTotBottom()==0xe)){ //above threshold
	FormattedRecord fr(FormattedRecord::DATA);
	fr.setToT(decodeToT(rec.getTotBottom()));
	fr.setCol(rec.getColumn()-1);
	fr.setRow(rec.getRow()-1);
	parsedData[parsedsize++]=fr.getWord();
      }
      if(rec.getTotTop()<0xe || (m_hitDiscCnfg!=0 && rec.getTotTop()==0xe)){ //above threshold
	FormattedRecord fr(FormattedRecord::DATA);
	fr.setToT(decodeToT(rec.getTotTop()));
	fr.setCol(rec.getColumn()-1);
	fr.setRow(rec.getRow());
	parsedData[parsedsize++]=fr.getWord();
      }
      // std::cout<<std::hex<<rec.getHeader()<<std::endl;
    }else if(rec.isDataHeader()){
      //header=true;
      nL1A++;
      FormattedRecord fr(FormattedRecord::HEADER);
      //printf("Formatter word %x\n",rec.getValue());
      fr.setL1id((rec.getL1id()+1)&0x7f); //make l1id compatible with FEI3 where the first l1id is 1 not 0
      fr.setBxid(rec.getBxid());
      parsedData[parsedsize++]=fr.getWord();
    }else if(rec.isServiceRecord()){
      printf("Service record FE %d. Error code: %d. Count: %d \n",
	     getId(), rec.getErrorCode(), rec.getErrorCount());
      if(rec.getErrorCode()<32)m_errhist->fill(rec.getErrorCode(), rec.getErrorCount());
      //header=false;
    }else if(rec.isValueRecord()){ //val rec without addr rec
      //printf("Value record: %04x.\n",rec.getValue());
      if(m_rb)m_rb->push_back(rec.getValue());
      //header=false;
    }else if(rec.isAddressRecord()){ // address record
      std::cout<<"Address record for ";
      if(rec.isGlobal())std::cout<<" global register ";
      else std::cout<<" shift register ";
      std::cout<<rec.getAddress()<<std::endl;
      //if(m_rb)m_rb->push_back(rec.getAddress());
      //header=false;
    }else{
      std::cout<<"FE "<<getId()<<": Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
      return FEI4::FEI4ARecord::BadRecord;
    }
    bytepointer+=3;
  }
  return FEI4::FEI4ARecord::OK;
}

//FEI4 encodes TOT information
int FEI4AFormatter::decodeToT(int raw){
  if(raw==0xe)return 1;
  else return raw+m_hitDiscCnfg+1;
}
