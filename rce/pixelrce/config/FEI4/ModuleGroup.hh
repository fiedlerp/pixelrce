#ifndef MODULEGROUPFEI4_HH
#define MODULEGROUPFEI4_HH
#include "config/AbsModuleGroup.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <vector>

namespace FEI4{

  class Module;

class ModuleGroup: public AbsModuleGroup{
public:
  ModuleGroup():AbsModuleGroup(), m_oneByOne(false){};
  virtual ~ModuleGroup(){}
  void addModule(FEI4::Module* module);
  void deleteModules();
  int setupParameterHW(const char* name, int val, bool bcok);
  int setupMaskStageHW(int stage);
  void configureModulesHW();
  void configureModuleHW(int outlink);
  int verifyModuleConfigHW();
  void resetErrorCountersHW();
  int configureScan(boost::property_tree::ptree *scanOptions);
  void resetFE();
  void enableDataTakingHW();
  unsigned getNmodules(){return m_modules.size();}
private:
  void enableSrHW(bool on);
  void switchToConfigModeHW();
  std::vector<FEI4::Module*> m_modules;
  bool m_oneByOne;

};
}

#endif
