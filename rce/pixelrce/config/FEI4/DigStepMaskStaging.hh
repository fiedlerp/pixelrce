#ifndef FEI4DIGITALSTEPSMASKSTAGING_HH
#define FEI4DIGITALSTEPSMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{
  class Module;

  class DigStepMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    DigStepMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
};
  
}
#endif
