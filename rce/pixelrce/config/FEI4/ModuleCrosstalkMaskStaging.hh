#ifndef FEI4MODULECROSSTALKMASKSTAGING_HH
#define FEI4MODULECROSSTALKMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class ModuleCrosstalkMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    ModuleCrosstalkMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
  };
  
}
#endif
