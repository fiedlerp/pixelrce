
#include "config/FEI4/SNTrigger.hh"
#include "config/FEI4/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "HW/SerialIF.hh"
#include <iostream>


namespace FEI4{

  SNTrigger::SNTrigger():AbsTrigger(){
  }
  SNTrigger::~SNTrigger(){
  }
  int SNTrigger::configureScan(boost::property_tree::ptree* scanOptions){
    int retval=0;
    m_i=0; //reset the number of triggers
    try{
      //     int calL1ADelay = scanOptions->get<int>("trigOpt.CalL1ADelay");
      setupTriggerStream();
    }
     catch(boost::property_tree::ptree_bad_path ex){
       std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
       retval=1;
     }
    return retval;
  }
  int SNTrigger::setupParameter(const char* name, int val){
    return 0;
  }

  void SNTrigger::setupTriggerStream(){
    m_triggerStream.clear();
    FECommands commands;
    commands.setBroadcast(true);
    
    commands.switchMode(&m_triggerStream, FECommands::CONF);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    commands.writeGlobalRegister(&m_triggerStream, 27, 0xc000);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    commands.globalPulse(&m_triggerStream, 63);
    for(int i=0;i<128;i++)m_triggerStream.push_back(0);
    commands.readGlobalRegister(&m_triggerStream, 35);
  }
  
  int SNTrigger::sendTrigger(){
    SerialIF::send(&m_triggerStream,SerialIF::DONT_CLEAR|SerialIF::WAITFORDATA);
    m_i++;
    return 0;
  }

  int SNTrigger::enableTrigger(bool on){
    return SerialIF::enableTrigger(on);
    return 0;
  }
  int SNTrigger::resetCounters(){
    return 0;
  }
};
