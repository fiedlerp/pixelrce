#ifndef DIFFUSIONMASKSTAGING_HH
#define DIFFUSIONMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{
  class Module;

  class DiffusionMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    DiffusionMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
};
  
}
#endif
