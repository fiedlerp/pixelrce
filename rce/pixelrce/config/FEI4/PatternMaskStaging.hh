#ifndef FEI4PATTERNMASKSTAGING_HH
#define FEI4PATTERNMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class PatternMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    PatternMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  };
  
}
#endif
