#ifndef FEI4HITORFORMATTER_HH
#define FEI4HITORFORMATTER_HH

#include "config/AbsFormatter.hh"

class FEI4HitorFormatter:public AbsFormatter{
public:
  FEI4HitorFormatter(int id);
  virtual ~FEI4HitorFormatter();
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  void configure(boost::property_tree::ptree* scanOptions);

};
#endif
