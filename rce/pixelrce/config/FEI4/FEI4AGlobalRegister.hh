#ifndef FEI4AGLOBALREGISTER_FEI4_HH
#define FEI4AGLOBALREGISTER_FEI4_HH

#include <map>
#include <string>
#include <vector>
#include "HW/SerialIF.hh"
#include "config/FEI4/GlobalRegister.hh"


namespace FEI4{

class FECommands;
  
  class FEI4AGlobalRegister: public GlobalRegister{
  public:
    FEI4AGlobalRegister(FECommands* commands);
    virtual ~FEI4AGlobalRegister();
    void printFields(std::ostream &os);
  private:
    void initialize();
    static RegisterDef m_def;
    static bool m_initialized;
    RegisterDef* getDef(){return &m_def;}

		  
  };

};

#endif
