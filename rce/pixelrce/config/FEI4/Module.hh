#ifndef FEI4__MODULE_HH
#define FEI4__MODULE_HH

#include "config/AbsModule.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/FEI4/GlobalRegister.hh"
#include "config/FEI4/FECommands.hh"
#include "config/FEI4/PixelRegister.hh"
#include "config/MaskStaging.hh"
#include <string>

class AbsFormatter;

namespace FEI4{

  class Module: public AbsModule{
  public:
    Module(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter *fmt);
    virtual ~Module();
    enum {N_FRONTENDS=1};
    enum PAR {NOT_FOUND, GLOBAL, PIXEL, SPECIAL};
    enum READOUT_MODE {RAW, FRAMED};
    enum Errors{BAD_SIZE=222, BAD_PIXREG=333, BAD_DCOL=444};
    void configureHW();
    virtual void resetHW()=0;
    void resetFE();
    void setupMaskStageHW(int stage);
    void setupMaskStageInjHW(int stage);
    void enableSrHW(bool on);
    void setPixelRegisterParameterHW(PixelRegister::Field field, unsigned val, GlobalRegister::mode t);
    void enableDataTakingHW();
    void resetErrorCountersHW();
    virtual int verifyModuleConfigHW()=0;
    void switchModeHW(FECommands::MODE mode);
    void switchToConfigModeHW();
    int setupParameterHW(const char* name, int val); //HW setup
    PAR setParameter(const char* name, int val, GlobalRegister::mode t); // HW or SW 
    int configureScan(boost::property_tree::ptree* scanOptions);
    ModuleInfo getModuleInfo();
    const float dacToElectrons(int fe, int dac);
    const int electronsToDac(float ne);
    virtual void destroy()=0;
    void setVcalCoeff(unsigned i, float val);
    void setCapVal(unsigned i, float val);
    float getVcalCoeff(unsigned i);
    float getCapVal(unsigned i);
    void writeDoubleColumnHW(unsigned bitLow, unsigned bitHigh, unsigned dcolLow, unsigned dcolHigh);
    void writeGlobalRegisterHW();
    void setupS0S1HitldHW(int s0, int s1, int hitld);
    virtual void setupGlobalPulseHW(int pulsereg)=0;
    void setupPixelStrobesHW(unsigned bitmask);
    void dumpConfig(std::ostream &os);
    PixelRegister* pixel(){return m_pixel;}
    GlobalRegister* global(){return m_global;}
    void setBroadcast(bool on){m_commands->setBroadcast(on);}

protected:
    READOUT_MODE m_mode;
    GlobalRegister *m_global;
    PixelRegister *m_pixel;
    PixelRegister *m_pixel_readback;
    FECommands *m_commands;
    float m_vcalCoeff[4];
    float m_capVal[2];
    MaskStaging<FEI4::Module> *m_maskStaging;
    int m_gAddr;
    int m_oneByOne;
};

};
#endif
