#include "util/RceName.hh"
#include "config/FEI4/RCFFEI4BModule.hh"
#include "config/FEI4/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "HW/SerialIF.hh"


namespace FEI4{

  RCFFEI4BModule::RCFFEI4BModule(RCF::RcfServer &server, const char * name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt):
    FEI4BModule(name, id, inLink, outLink, fmt), m_server(server){
  char binding[32];
  sprintf(binding, "I_RCFFEI4BAdapter_%d", id);
  m_server.bind<I_RCFFEI4BAdapter>(*this, binding);
}

RCFFEI4BModule::~RCFFEI4BModule(){
  char binding[32];
  sprintf(binding, "I_RCFFEI4BAdapter_%d", m_id);
  m_server.unbind<I_RCFFEI4BAdapter>(binding);
}

void RCFFEI4BModule::RCFsetChipAddress(uint32_t addr){
  m_commands->setAddr(addr);
}
int32_t RCFFEI4BModule::RCFverifyModuleConfigHW(){
  return verifyModuleConfigHW();
}
int32_t RCFFEI4BModule::RCFdownloadConfig(ipc::PixelFEI4BConfig config){
  // geographical address
  m_gAddr=config.FECommand.address;
  m_commands->setAddr(m_gAddr);

  const ipc::PixelFEI4BGlobal *cfg=&config.FEGlobal;
  // global register
  m_global->setField("TrigCnt",            cfg->TrigCnt ,            GlobalRegister::SW);
  m_global->setField("Conf_AddrEnable",    cfg->Conf_AddrEnable ,    GlobalRegister::SW);
  m_global->setField("Reg2Spare",          cfg->Reg2Spare ,          GlobalRegister::SW);
  m_global->setField("ErrMask0",           cfg->ErrMask0 ,           GlobalRegister::SW);
  m_global->setField("ErrMask1",           cfg->ErrMask1 ,           GlobalRegister::SW);
  m_global->setField("PrmpVbpRight",       cfg->PrmpVbpRight ,       GlobalRegister::SW);
  m_global->setField("BufVgOpAmp",         cfg->BufVgOpAmp ,         GlobalRegister::SW);
  m_global->setField("Reg6Spare",          cfg->Reg6Spare ,          GlobalRegister::SW);
  m_global->setField("PrmpVbp",            cfg->PrmpVbp ,            GlobalRegister::SW);
  m_global->setField("TdacVbp",            cfg->TdacVbp ,            GlobalRegister::SW);
  m_global->setField("DisVbn",             cfg->DisVbn ,             GlobalRegister::SW);
  m_global->setField("Amp2Vbn",            cfg->Amp2Vbn ,            GlobalRegister::SW);
  m_global->setField("Amp2VbpFol",         cfg->Amp2VbpFol ,         GlobalRegister::SW);
  m_global->setField("Reg9Spare",          cfg->Reg9Spare ,          GlobalRegister::SW);
  m_global->setField("Amp2Vbp",            cfg->Amp2Vbp ,            GlobalRegister::SW);
  m_global->setField("FdacVbn",            cfg->FdacVbn ,            GlobalRegister::SW);
  m_global->setField("Amp2Vbpf",           cfg->Amp2Vbpf ,           GlobalRegister::SW);
  m_global->setField("PrmpVbnFol",         cfg->PrmpVbnFol ,         GlobalRegister::SW);
  m_global->setField("PrmpVbpLeft",        cfg->PrmpVbpLeft ,        GlobalRegister::SW);
  m_global->setField("PrmpVbpf",           cfg->PrmpVbpf ,           GlobalRegister::SW);
  m_global->setField("PrmpVbnLcc",         cfg->PrmpVbnLcc ,         GlobalRegister::SW);
  m_global->setField("Reg13Spare",         cfg->Reg13Spare ,         GlobalRegister::SW);
  m_global->setField("PxStrobes",          cfg->PxStrobes ,          GlobalRegister::SW);
  m_global->setField("S0",                 cfg->S0 ,                 GlobalRegister::SW);
  m_global->setField("S1",                 cfg->S1 ,                 GlobalRegister::SW);
  m_global->setField("LVDSDrvIref",        cfg->LVDSDrvIref ,        GlobalRegister::SW);
  m_global->setField("GADCOpAmp",          cfg->GADCOpAmp ,          GlobalRegister::SW);
  m_global->setField("PllIbias",           cfg->PllIbias ,           GlobalRegister::SW);
  m_global->setField("LVDSDrvVos",         cfg->LVDSDrvVos ,         GlobalRegister::SW);
  m_global->setField("TempSensBias",       cfg->TempSensBias ,       GlobalRegister::SW);
  m_global->setField("PllIcp",             cfg->PllIcp ,             GlobalRegister::SW);
  m_global->setField("Reg17Spare",         cfg->Reg17Spare ,         GlobalRegister::SW);
  m_global->setField("PlsrIdacRamp",       cfg->PlsrIdacRamp ,       GlobalRegister::SW);
  m_global->setField("VrefDigTune",        cfg->VrefDigTune ,        GlobalRegister::SW);
  m_global->setField("PlsrVgOPamp",        cfg->PlsrVgOPamp  ,       GlobalRegister::SW);
  m_global->setField("PlsrDacBias",        cfg->PlsrDacBias  ,       GlobalRegister::SW);
  m_global->setField("VrefAnTune",         cfg->VrefAnTune ,         GlobalRegister::SW);
  m_global->setField("Vthin_AltCoarse",    cfg->Vthin_AltCoarse ,    GlobalRegister::SW);
  m_global->setField("Vthin_AltFine",      cfg->Vthin_AltFine ,      GlobalRegister::SW);
  m_global->setField("PlsrDAC",            cfg->PlsrDAC ,            GlobalRegister::SW);
  m_global->setField("DIGHITIN_Sel",       cfg->DIGHITIN_Sel ,       GlobalRegister::SW);
  m_global->setField("DINJ_Override",      cfg->DINJ_Override ,      GlobalRegister::SW);
  m_global->setField("HITLD_In",           cfg->HITLD_In ,           GlobalRegister::SW);
  m_global->setField("Reg21Spare",         cfg->Reg21Spare ,         GlobalRegister::SW);
  m_global->setField("Reg22Spare2",        cfg->Reg22Spare2 ,        GlobalRegister::SW);
  m_global->setField("Colpr_Addr",         cfg->Colpr_Addr ,         GlobalRegister::SW);
  m_global->setField("Colpr_Mode",         cfg->Colpr_Mode ,         GlobalRegister::SW);
  m_global->setField("Reg22Spare1",        cfg->Reg22Spare1 ,        GlobalRegister::SW);
  m_global->setField("DisableColumnCnfg0", cfg->DisableColumnCnfg0 , GlobalRegister::SW);
  m_global->setField("DisableColumnCnfg1", cfg->DisableColumnCnfg1 , GlobalRegister::SW);
  m_global->setField("DisableColumnCnfg2", cfg->DisableColumnCnfg2 , GlobalRegister::SW);
  m_global->setField("TrigLat",            cfg->TrigLat ,            GlobalRegister::SW);
  m_global->setField("CMDcnt",             cfg->CMDcnt ,             GlobalRegister::SW);
  m_global->setField("StopModeCnfg",       cfg->StopModeCnfg ,       GlobalRegister::SW);
  m_global->setField("HitDiscCnfg",        cfg->HitDiscCnfg ,        GlobalRegister::SW);
  m_global->setField("EN_PLL",             cfg->EN_PLL ,             GlobalRegister::SW);
  m_global->setField("Efuse_sense",        cfg->Efuse_sense ,        GlobalRegister::SW);
  m_global->setField("Stop_Clk",           cfg->Stop_Clk ,           GlobalRegister::SW);
  m_global->setField("ReadErrorReq",       cfg->ReadErrorReq ,       GlobalRegister::SW);
  m_global->setField("Reg27Spare1",        cfg->Reg27Spare1 ,        GlobalRegister::SW);
  m_global->setField("GADC_Enable",        cfg->GADC_Enable ,        GlobalRegister::SW);
  m_global->setField("ShiftReadBack",      cfg->ShiftReadBack ,      GlobalRegister::SW);
  m_global->setField("Reg27Spare2",        cfg->Reg27Spare2 ,        GlobalRegister::SW);
  m_global->setField("GateHitOr",          cfg->GateHitOr ,          GlobalRegister::SW);
  m_global->setField("CalEn",              cfg->CalEn ,              GlobalRegister::SW);
  m_global->setField("SR_clr",             cfg->SR_clr ,             GlobalRegister::SW);
  m_global->setField("Latch_en",           cfg->Latch_en ,           GlobalRegister::SW);
  m_global->setField("SR_Clock",           cfg->SR_Clock ,           GlobalRegister::SW);
  m_global->setField("LVDSDrvSet06",       cfg->LVDSDrvSet06 ,       GlobalRegister::SW);
  m_global->setField("Reg28Spare",         cfg->Reg28Spare ,         GlobalRegister::SW);
  m_global->setField("EN40M",              cfg->EN40M ,              GlobalRegister::SW);
  m_global->setField("EN80M",              cfg->EN80M ,              GlobalRegister::SW);
  m_global->setField("CLK1",               cfg->CLK1 ,               GlobalRegister::SW);
  m_global->setField("CLK0",               cfg->CLK0 ,               GlobalRegister::SW);
  m_global->setField("EN160M",             cfg->EN160M ,             GlobalRegister::SW);
  m_global->setField("EN320M",             cfg->EN320M ,             GlobalRegister::SW);
  m_global->setField("Reg29Spare1",        cfg->Reg29Spare1 ,        GlobalRegister::SW);
  m_global->setField("no8b10b",            cfg->no8b10b ,            GlobalRegister::SW);
  m_global->setField("Clk2OutCnfg",        cfg->Clk2OutCnfg ,        GlobalRegister::SW);
  m_global->setField("EmptyRecord",        cfg->EmptyRecord ,        GlobalRegister::SW);
  m_global->setField("Reg29Spare2",        cfg->Reg29Spare2 ,        GlobalRegister::SW);
  m_global->setField("LVDSDrvEn",          cfg->LVDSDrvEn ,          GlobalRegister::SW);
  m_global->setField("LVDSDrvSet30",       cfg->LVDSDrvSet30 ,       GlobalRegister::SW);
  m_global->setField("LVDSDrvSet12",       cfg->LVDSDrvSet12 ,       GlobalRegister::SW);
  m_global->setField("TempSensDiodeSel",   cfg->TempSensDiodeSel ,   GlobalRegister::SW);
  m_global->setField("TempSensDisable",    cfg->TempSensDisable ,    GlobalRegister::SW);
  m_global->setField("IleakRange",         cfg->IleakRange ,         GlobalRegister::SW);
  m_global->setField("Reg30Spare",         cfg->Reg30Spare ,         GlobalRegister::SW);
  m_global->setField("PlsrRiseUpTau",      cfg->PlsrRiseUpTau ,      GlobalRegister::SW);
  m_global->setField("PlsrPwr",            cfg->PlsrPwr ,            GlobalRegister::SW);
  m_global->setField("PlsrDelay",          cfg->PlsrDelay ,          GlobalRegister::SW);
  m_global->setField("ExtDigCalSW",        cfg->ExtDigCalSW ,        GlobalRegister::SW);
  m_global->setField("ExtAnaCalSW",        cfg->ExtAnaCalSW ,        GlobalRegister::SW);
  m_global->setField("Reg31Spare",         cfg->Reg31Spare ,         GlobalRegister::SW);
  m_global->setField("GADCSel",            cfg->GADCSel ,            GlobalRegister::SW);
  m_global->setField("SELB0",              cfg->SELB0 ,              GlobalRegister::SW);
  m_global->setField("SELB1",              cfg->SELB1 ,              GlobalRegister::SW);
  m_global->setField("SELB2",              cfg->SELB2 ,              GlobalRegister::SW);
  m_global->setField("Reg34Spare1",        cfg->Reg34Spare1 ,        GlobalRegister::SW);
  m_global->setField("PrmpVbpMsnEn",       cfg->PrmpVbpMsnEn ,       GlobalRegister::SW);
  m_global->setField("Reg34Spare2",        cfg->Reg34Spare2 ,        GlobalRegister::SW);
  m_global->setField("Chip_SN",            cfg->Chip_SN ,            GlobalRegister::SW);
  m_global->setField("Reg1Spare",          cfg->Reg1Spare ,          GlobalRegister::SW);
  m_global->setField("SmallHitErase",      cfg->SmallHitErase ,      GlobalRegister::SW);
  m_global->setField("Eventlimit",         cfg->Eventlimit ,         GlobalRegister::SW);
  
  setVcalCoeff(0, config.FECalib.vcalCoeff[0]); 
  setVcalCoeff(1, config.FECalib.vcalCoeff[1]); 
  setVcalCoeff(2, config.FECalib.vcalCoeff[2]); 
  setVcalCoeff(3, config.FECalib.vcalCoeff[3]); 
  setCapVal(0, config.FECalib.cinjLo); 
  setCapVal(1, config.FECalib.cinjHi); 


  for(int i=0;i<ipc::IPC_N_I4_PIXEL_COLUMNS;i++){
    for(int j=0;j<ipc::IPC_N_I4_PIXEL_ROWS;j++){
      //Pixel DACs
      m_pixel->setField(PixelRegister::tdac, j+1, i+1, config.FETrims.dacThresholdTrim[i][j]);
      m_pixel->setField(PixelRegister::fdac, j+1, i+1, config.FETrims.dacFeedbackTrim[i][j]);
      //Mask bits
      m_pixel->setField(PixelRegister::enable, j+1, i+1, config.FEMasks[i][j]>>ipc::enable);
      m_pixel->setField(PixelRegister::largeCap, j+1, i+1, config.FEMasks[i][j]>>ipc::largeCap);
      m_pixel->setField(PixelRegister::smallCap, j+1, i+1, config.FEMasks[i][j]>>ipc::smallCap);
      m_pixel->setField(PixelRegister::hitbus, j+1, i+1, config.FEMasks[i][j]>>ipc::hitbus);
    }
  }
  // Configure the formatter
  AbsFormatter* fmt=getFormatter();
  if(fmt){
    std::cout<<"%%%%%%%%% Configuring Formatter"<<std::endl;
    boost::property_tree::ptree *pt=new boost::property_tree::ptree;
    pt->put("HitDiscCnfg",cfg->HitDiscCnfg);
    fmt->configure(pt);
    delete pt;
  }

  std::cout<<"Configure done"<<std::endl;
  //dumpConfig(std::cout);
  return 0;
}

uint32_t RCFFEI4BModule::RCFwriteHWglobalRegister(int32_t reg, uint16_t val){
  m_gAddr=8;
  m_commands->setAddr(8);
  switchModeHW(FECommands::CONF);
  SerialIF::setChannelInMask(1<<getInLink());
  return m_global->writeRegisterHW(reg, val);
  SerialIF::setChannelInMask(0);
}

uint32_t RCFFEI4BModule::RCFreadHWglobalRegister(int32_t reg, uint16_t &val){
  SerialIF::sendCommand(0x10); //phase calibration
  m_gAddr=8;
  m_commands->setAddr(8);
  SerialIF::setChannelInMask(1<<getInLink());
  SerialIF::setChannelOutMask(1<<getOutLink());
  return m_global->readRegisterHW(reg, val);
  SerialIF::setChannelInMask(0);
  SerialIF::setChannelOutMask(0);
}
uint32_t RCFFEI4BModule::RCFclearPixelLatches(){
  SerialIF::setChannelInMask(1<<getInLink());
  // choose double column
  m_global->setField("Colpr_Addr", 0, GlobalRegister::SW);
  m_global->setField("Colpr_Mode", 3, GlobalRegister::HW); // all columns
  setupS0S1HitldHW(0, 0, 0);
  setupGlobalPulseHW(GlobalRegister::SR_clr); //latch enable
  BitStream *bs=new BitStream;
  m_commands->globalPulse(bs, FECommands::PULSE_WIDTH);
  bs->push_back(0);
  SerialIF::send(bs, SerialIF::DONT_CLEAR); //global pulse
  //now clear latches
  setupGlobalPulseHW(GlobalRegister::Latch_en); //latch enable
  setupPixelStrobesHW(0x1fff);
  SerialIF::send(bs, SerialIF::DONT_CLEAR); //global pulse
  setupPixelStrobesHW(0);//clear register
  setupGlobalPulseHW(0); //clear register
  delete bs;
  return 0;
}
  
  // Function to be called directly from Linux for debugging purposes.
  uint32_t RCFFEI4BModule::RCFwriteDoubleColumnHW(uint32_t bit, uint32_t dcol, std::vector<uint32_t>  data, std::vector<uint32_t> & retvec){
  // clear pixel latches
  //IPCclearPixelLatches();
  //retv=new ipc::uvec;
  //return 0;
  SerialIF::setChannelInMask(1<<getInLink());
  SerialIF::setChannelOutMask(1<<getOutLink());

  if(data.size()!=PixelRegister::DROW_WORDS)return BAD_SIZE; //wrong amount of data
  if(bit>=PixelRegister::N_PIXEL_REGISTER_BITS)return BAD_PIXREG;

  switchModeHW(FECommands::CONF);

  // choose double column
  m_global->setField("Colpr_Addr", dcol, GlobalRegister::SW);
  m_global->setField("Colpr_Mode", 0, GlobalRegister::HW); // only write to 1 column pair
  m_global->setField("ShiftReadBack",0, GlobalRegister::HW); 
  setupS0S1HitldHW(0, 0, 0);
  unsigned stat=m_pixel->writeDoubleColumnHW((const unsigned*)&data[0], retvec);
  if(bit!=PixelRegisterFields::fieldPos[PixelRegister::diginj]){ //no latching for diginj
    setupGlobalPulseHW(GlobalRegister::Latch_en); //latch enable
    //set up strobe bit
    setupPixelStrobesHW(1<<bit);
    BitStream *bs=new BitStream;
    m_commands->globalPulse(bs, FECommands::PULSE_WIDTH);
    bs->push_back(0);
    SerialIF::send(bs); //global pulse
    delete bs;
    setupGlobalPulseHW(0); //clear
    setupPixelStrobesHW(0); //clear
  }
  return stat;
}

  // Function to be called directly from Linux for debugging purposes.
  uint32_t RCFFEI4BModule::RCFreadDoubleColumnHW(uint32_t bit, uint32_t dcol, std::vector<uint32_t>& retvec){
  SerialIF::setChannelInMask(1<<getInLink());
  SerialIF::setChannelOutMask(1<<getOutLink());
  if(bit>=PixelRegister::N_PIXEL_REGISTER_BITS)return BAD_PIXREG;
  if(dcol>=PixelRegister::N_COLS/2)return BAD_DCOL;
  // set config mode
  switchModeHW(FECommands::CONF);
  // choose double column
  m_global->setField("Colpr_Addr", dcol, GlobalRegister::SW);
  m_global->setField("Colpr_Mode", 0, GlobalRegister::HW); // only write to 1 column pair
  m_global->setField("ShiftReadBack",1, GlobalRegister::HW);  //enable readback
  if(bit!=PixelRegisterFields::fieldPos[PixelRegister::diginj]){ //no latching for diginj
    setupS0S1HitldHW(1, 1, 0);
    setupGlobalPulseHW(GlobalRegister::SR_Clock); //SR clock
    //set up strobe bit
    setupPixelStrobesHW(1<<bit);
    BitStream *bs=new BitStream;
    bs->push_back(0);
    m_commands->globalPulse(bs, FECommands::PULSE_WIDTH);
    SerialIF::send(bs); //global pulse
    delete bs;
  }
  //shift out bits and refill with config register.
  m_global->setField("ShiftReadBack",0, GlobalRegister::SW); //setupGlobalPulse does the HW write for that register
  setupGlobalPulseHW(0); //clear
  setupPixelStrobesHW(0); //clear
  setupS0S1HitldHW(0, 0, 0);
  unsigned stat=m_pixel->writeDoubleColumnHW((const unsigned*)m_pixel->getDoubleColumn(bit, dcol), retvec);

}


};

