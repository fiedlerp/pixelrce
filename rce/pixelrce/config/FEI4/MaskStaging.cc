#include "config/MaskStaging.hh"
#include "config/FEI4/Module.hh"

using namespace FEI4;

template<> bool MaskStaging<FEI4::Module>::cLow(){
  if (m_masks.crosstalking==0)
    return (m_masks.iMask&(1<<FEI4::PixelRegisterFields::fieldPos[FEI4::PixelRegister::smallCap]))!=0;
  else
    return (m_masks.xtalk_off&(1<<FEI4::PixelRegisterFields::fieldPos[FEI4::PixelRegister::smallCap]))!=0;
}
template<> bool MaskStaging<FEI4::Module>::cHigh(){
  if(m_masks.crosstalking==0)
    return (m_masks.iMask&(1<<FEI4::PixelRegisterFields::fieldPos[FEI4::PixelRegister::largeCap]))!=0;
  else
    return (m_masks.xtalk_off&(1<<FEI4::PixelRegisterFields::fieldPos[FEI4::PixelRegister::largeCap]))!=0;
}

template<> void MaskStaging<FEI4::Module>::clearBitsHW(){
  m_module->global()->setField("Colpr_Mode", 3, GlobalRegister::SW); 
  for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
    if(m_masks.oMask&(1<<i)){
      m_module->pixel()->setBitAll(i,0);
      m_module->writeDoubleColumnHW(i,i,0,0);
    }
  }
}
