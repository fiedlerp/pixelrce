#ifndef FEI4RECORD_HH
#define FEI4RECORD_HH

#include "config/Endianness.hh"
namespace FEI4{


namespace Size
{
  enum Size
    {
      UNUSED = 8,
      HEADER = 8,
      RTYPE = 1,
      ADDR = 15,
      VALUE = 16,
      ERRCODE = 6,
      ERRCOUNT = 10,
      REMAINDER = 16, 
      FLAG = 1, 
      LV1ID = 7,
      BXID = 8,
      COL = 7,
      ROW = 9,
      TOT = 4
    };
}
namespace Key
{
  enum Key
    {
      ADDR    = 0xea,
      VALUE   = 0xec,
      SERVICE  = 0xef,
      HEADER   = 0xe9,
      EMPTY    = 0x0,
      GLOBAL   = 0,
      SHIFT    = 1
    };
}
/* ---------------------------------------------------------------------- */

  struct Generic
  {
#if     ENDIANNESS_IS_BIG
    
    unsigned int unused: Size::UNUSED;
    unsigned int header: Size::HEADER;
    unsigned int remainder: Size::REMAINDER;
    
#elif   ENDIANNESS_IS_LITTLE
    
    unsigned int remainder: Size:: REMAINDER;
    unsigned int header: Size::HEADER;
    unsigned int unused: Size::UNUSED;
    
#else
    
#error  ENDIANNESS is not defined
#endif
  };
  struct Address
  {
#if     ENDIANNESS_IS_BIG
    
    unsigned int unused: Size::UNUSED;
    unsigned int header: Size::HEADER;
    unsigned int rtype: Size:: RTYPE;
    unsigned int address: Size::ADDR;
    
#elif   ENDIANNESS_IS_LITTLE
    
    unsigned int address: Size::ADDR;
    unsigned int rtype: Size:: RTYPE;
    unsigned int header: Size::HEADER;
    unsigned int unused: Size::UNUSED;
    
#else
    
#error  ENDIANNESS is not defined
#endif
  };
  
  struct Value
  {
#if     ENDIANNESS_IS_BIG
    
    unsigned int unused: Size::UNUSED;
    unsigned int header: Size::HEADER;
    unsigned int value: Size::VALUE;
    
#elif   ENDIANNESS_IS_LITTLE
    
    unsigned int value: Size::VALUE;
    unsigned int header: Size::HEADER;
    unsigned int unused: Size::UNUSED;
    
#else
#error  ENDIANNESS is not defined
#endif
  };
  
  struct Service
  {
#if     ENDIANNESS_IS_BIG
    
    unsigned int unused:   Size::UNUSED;
    unsigned int header:   Size::HEADER;
    unsigned int errcode:  Size::ERRCODE;
    unsigned int errcount: Size::ERRCOUNT;
    
#elif   ENDIANNESS_IS_LITTLE
    
    unsigned int errcount: Size::ERRCOUNT;
    unsigned int errcode:  Size::ERRCODE;
    unsigned int header: Size::HEADER;
    unsigned int unused: Size::UNUSED;
    
#else
    
#error  ENDIANNESS is not defined
#endif
  };

   struct DataHeader
  {
#if     ENDIANNESS_IS_BIG
    
    unsigned int unused: Size::UNUSED;
    unsigned int header: Size::HEADER;
    unsigned int flag: Size::FLAG;
    unsigned int lv1id: Size::LV1ID;
    unsigned int bxid: Size::BXID;
    
#elif   ENDIANNESS_IS_LITTLE
    
    unsigned int bxid: Size::BXID;
    unsigned int lv1id: Size::LV1ID;
    unsigned int flag: Size::FLAG;
    unsigned int header: Size::HEADER;
    unsigned int unused: Size::UNUSED;
    
#else
    
#error  ENDIANNESS is not defined
#endif
  };

   struct Data
  {
#if     ENDIANNESS_IS_BIG
    
    unsigned int unused: Size::UNUSED;
    unsigned int col: Size::COL;
    unsigned int row: Size::ROW;
    unsigned int totbottom: Size::TOT;
    unsigned int tottop: Size::TOT;
    
#elif   ENDIANNESS_IS_LITTLE
    
    unsigned int tottop: Size::TOT;
    unsigned int totbottom: Size::TOT;
    unsigned int row: Size::ROW;
    unsigned int col: Size::COL;
    unsigned int unused: Size::UNUSED;
    
#else
    
#error  ENDIANNESS is not defined
#endif
  };
 
  
  union Record
  {
    unsigned int  ui;
    Generic       ge;
    Address       ad;
    Value         va;
    Service       se;
    DataHeader    he;
    Data          da;
  };


  class FEI4ARecord{
  public:
    enum Status
      {
	OK            =  0, /*!< Clean finish                                 */
	Empty         =  1, /*!< Buffer does not contain any record    */
	NotGlobal     =  2, /*!< Address record is not a global register record     */
	WrongAddress  =  3, /*!< Address record does not contain the expected address     */
	BadAddrValSeq =  4, /*!< Address record not immediately followed by value record */
	NoValue       =  5, /*!< Value record missing. */ 
	NotShift      =  6, /*!< Address record is not a shift register record */
	OddNwords     =  7, /*!< Odd number of 16 bit words in shift register readback */
	NoHeader      =  8, /*!< Data word without a previous header word*/
	BadRecord     =  9  /*!< Unknown record type */
      };
    
    bool isAddressRecord(){return m_record.ge.header==Key::ADDR;}
    bool isValueRecord(){return m_record.ge.header==Key::VALUE;}
    bool isServiceRecord(){return m_record.ge.header==Key::SERVICE;}
    bool isEmptyRecord(){return m_record.ge.header==Key::EMPTY;}
    bool isDataHeader(){return m_record.ge.header==Key::HEADER;}
    bool isData(){return (m_record.ge.header&0xc0)!=0xc0;}
    unsigned getHeader(){return m_record.ge.header;}
    unsigned getAddress(){return m_record.ad.address;}
    unsigned getRegisterType(){return m_record.ad.rtype;}
    unsigned isGlobal(){return m_record.ad.rtype==Key::GLOBAL;}
    unsigned isShift(){return m_record.ad.rtype==Key::SHIFT;}
    unsigned getValue(){return m_record.va.value;}
    unsigned getUnsigned(){return m_record.ui;}
    unsigned getColumn(){return m_record.da.col;}
    unsigned getL1id(){return m_record.he.lv1id;}
    unsigned getBxid(){return m_record.he.bxid;}
    unsigned getRow(){return m_record.da.row;}
    unsigned getTotTop(){return m_record.da.tottop;}
    unsigned getTotBottom(){return m_record.da.totbottom;}
    unsigned getErrorCode(){return m_record.se.errcode;}
    unsigned getErrorCount(){return m_record.se.errcount;}
#if     ENDIANNESS_IS_BIG
    void setRecord(const unsigned char* bytepointer){
      unsigned char* rec=(unsigned char*)&m_record;
      rec[1]=*bytepointer;
      rec[2]=*(bytepointer+1);
      rec[3]=*(bytepointer+2);
    }
#elif   ENDIANNESS_IS_LITTLE
    void setRecord(const unsigned char* bytepointer){
      unsigned char* rec=(unsigned char*)&m_record;
      rec[2]=*bytepointer;
      rec[1]=*(bytepointer+1);
      rec[0]=*(bytepointer+2);
    }
#else
#error  ENDIANNESS is not defined
#endif

  private:
    Record m_record;
  };

}
#endif
