#ifndef PIXELCONFIG_HH
#define PIXELCONFIG_HH

#include <string>

struct FECalib{ 
	float cinjLo;
	float cinjHi;
	float vcalCoeff[4];
	float chargeCoeffClo;  
	float chargeCoeffChi;
	float chargeOffsetClo;
	float chargeOffsetChi;
	float monleakCoeff;
} ; 

class AbsFormatter;


class PixelConfig{
public:
  virtual int downloadConfig(int rce, int id)=0;
  PixelConfig(const char* type, std::string filename, bool producesdata, int ncols, int nrows, int nchips):
    m_id(-1), m_name("None"), m_type(type), m_filename(filename), m_valid(false), m_producesData(producesdata),
    m_nCols(ncols), m_nRows(nrows), m_nChips(nchips){}
  virtual ~PixelConfig(){}
  virtual void *getStruct()=0;
  virtual FECalib getFECalib(int chip){FECalib fe; return fe;}
  virtual void setFECalib(int chip,FECalib fe){}
  virtual unsigned getThresholdDac(int chip, int col, int row){return 0;}
  virtual void setThresholdDac(int chip, int col, int row, int val){}
  virtual unsigned getFeedbackDac(int chip, int col, int row){return 0;}
  virtual void setFeedbackDac(int chip, int col, int row, int val){}
  virtual unsigned getIf(int chip){return 0;}
  virtual void setIf(int chip, int val){}
  virtual unsigned getGDac(int chip){return 0;}
  virtual void setGDac(int chip, int val){}
  virtual void setGDacCoarse(int chip, int val){}
  virtual unsigned getFEMask(int chip, int col, int row){return 0;}
  virtual void setFEMask(int chip, int col, int row, int val){}
  virtual void writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key)=0;
  virtual std::string getHitbusMaskFile(){return "";}
  virtual std::string getEnableMaskFile(){return "";}
  virtual void configureFormatter(AbsFormatter*){};
  int getId(){return m_id;}
  const char* getName(){return m_name.c_str();}
  const char* getType(){return m_type.c_str();}
  const char* getFilename(){return m_filename.c_str();}
  bool isValid(){return m_valid;}
  bool producesData(){return m_producesData;}
  int nCols(){return m_nCols;}
  int nRows(){return m_nRows;}
  int nChips(){return m_nChips;}

protected:
  int m_id;
  std::string m_name;
  std::string m_type;
  std::string m_filename;
  bool m_valid;
  bool m_producesData;
  int m_nCols, m_nRows, m_nChips;
};

#endif
