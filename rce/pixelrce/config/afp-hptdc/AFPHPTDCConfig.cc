#include "config/afp-hptdc/AFPHPTDCConfig.hh"
#include "config/AbsFormatter.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>

#include <RCF/RCF.hpp>
#include "RCFAFPHPTDCAdapter.hh"
#include "util/RceName.hh"
int AFPHPTDCConfig::downloadConfig(int rce, int id){
  char binding[32];
  char rcename[32];
  sprintf(binding, "I_RCFAFPHPTDCAdapter_%d", id);
  sprintf(rcename, RCFHOST"%d", rce);
  try {
    RcfClient<I_RCFAFPHPTDCAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), binding);
    client.RCFdownloadConfig( *m_config );
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
}

AFPHPTDCConfig::AFPHPTDCConfig(std::string filename): 
    PixelConfig("HPTDC", filename, true, 0, 0, 0), m_config(new ipc::AFPHPTDCModuleConfig){
    AFPHPTDCConfigFile turbo;
    turbo.readModuleConfig(m_config,filename);
    m_name=(const char*)m_config->idStr;
    m_id=strtol((const char*)m_config->idStr,0,10);
    m_valid=true;
  }
void AFPHPTDCConfig::writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key){
    AFPHPTDCConfigFile cf;
    cf.writeModuleConfig(m_config, base, confdir, configname, key);
  }

void AFPHPTDCConfig::configureFormatter(AbsFormatter* formatter){
  boost::property_tree::ptree *pt=new boost::property_tree::ptree;
  for(int l=0;l<2;l++){
    for(int j=0;j<12;j++){
      for(int k=0;k<1024;k++){
	char name[32];
	sprintf(name, "c_%d_%d_%d", l, j, k);
	pt->put(name, m_config->calib[l][j][k]);
      }
    }
  }
  formatter->configure(pt);
  delete pt;
}
