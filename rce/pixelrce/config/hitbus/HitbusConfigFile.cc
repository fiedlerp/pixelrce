#include "config/hitbus/HitbusConfigFile.hh"
#include "util/exceptions.hh"
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/stat.h> 
#include <boost/algorithm/string.hpp>
#include <regex>
HitbusConfigFile::~HitbusConfigFile(){}
  
unsigned short HitbusConfigFile::lookupToUShort(std::string par){
  if( m_params.find(par)==m_params.end()){
    std::cout<<"Parameter "<<par<<" does not exist."<<std::endl;
    rcecalib::Config_File_Error err;
    throw err;
  }
  std::string vals=m_params[par];
  unsigned short val;
  int success=convertToUShort(vals, val);
  if(success==false){
    std::cout<<"Bad value "<<vals<< " for parameter "<<par<<std::endl;
    rcecalib::Config_File_Error err;
    throw err;
  }
  return val;
}

int HitbusConfigFile::convertToUShort(std::string par, unsigned short& val){
  char* end;
  val=strtoul(par.c_str(), &end, 0);
  if(end-par.c_str()!=(int)par.size()){
    return 0;
  }
  if((val&0xffff0000)!=0){
    std::cout<<"Value "<<val<<" too large."<<std::endl;
    rcecalib::Config_File_Error err;
    throw err;
  }
  return 1;
}

void HitbusConfigFile::writeModuleConfig(ipc::HitbusModuleConfig* config, const std::string &base, const std::string &confdir, 
				       const std::string &configname, const std::string &key){
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes
  intStat = stat(base.c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<"Directory "<<base<<" does not exist. Not writing config file"<<std::endl;
    return;
  }
  intStat = stat((base+"/"+confdir).c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    //std::cout<<"Directory "<<base<<"/"<<confdir<<" does not exist. Creating."<<std::endl;
    mkdir ((base+"/"+confdir).c_str(),0777);
  }
  std::string cfgname=configname;
  if(key.size()!=0)cfgname+="__"+key;
  std::string fullpath=base+"/"+confdir+"/"+cfgname+".cfg";
  std::ofstream cfgfile(fullpath.c_str());
  cfgfile<<"# Hitbus Chip Configuration"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"# Module name"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"ModuleID\t\t"<<config->idStr<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"# Registers"<<std::endl;
  cfgfile<<std::endl;
  //Register
  cfgfile<<"bpm\t\t\t"<<(unsigned)config->bpm<<std::endl;
  cfgfile<<"delay_tam1\t\t"<<(unsigned)config->delay_tam1<<std::endl;
  cfgfile<<"delay_tam2\t\t"<<(unsigned)config->delay_tam2<<std::endl;
  cfgfile<<"delay_tam3\t\t"<<(unsigned)config->delay_tam3<<std::endl;
  cfgfile<<"delay_tbm1\t\t"<<(unsigned)config->delay_tbm1<<std::endl;
  cfgfile<<"delay_tbm2\t\t"<<(unsigned)config->delay_tbm2<<std::endl;
  cfgfile<<"delay_tbm3\t\t"<<(unsigned)config->delay_tbm3<<std::endl;
  cfgfile<<"bypass_delay\t\t"<<(unsigned)config->bypass_delay<<std::endl;
  cfgfile<<"clock\t\t\t"<<"0x"<<std::hex<<(unsigned)config->clock<<std::dec<<std::endl;
  cfgfile<<"function_A\t\t"<<"0x"<<std::hex<<(unsigned)config->function_A<<std::dec<<std::endl;
  cfgfile<<"function_B\t\t"<<"0x"<<std::hex<<(unsigned)config->function_B<<std::dec<<std::endl;
}

void HitbusConfigFile::readModuleConfig(ipc::HitbusModuleConfig* config, std::string filename){
  //clear structure
  char* ccfg=(char*)config;
  for (unsigned int i=0;i<sizeof(ipc::HitbusModuleConfig);i++)ccfg[i]=0;
  //open file
  m_moduleCfgFile=new std::ifstream(filename.c_str());
  if(!m_moduleCfgFile->good()){
    std::cout<<"Cannot open file with name "<<filename<<std::endl;
    rcecalib::Config_File_Error err;
    throw err;
  }
  m_moduleCfgFilePath = filename;
  // parse config file
  std::string inpline;
  m_params.clear();
  while(true){
    getline(*m_moduleCfgFile, inpline);
    if(m_moduleCfgFile->eof())break;
    boost::trim(inpline);   
    if(inpline.size()!=0 && inpline[0]!='#'){ //remove comment lines and empty lines
      std::vector<std::string> splitVec; 
      split( splitVec, inpline, boost::is_any_of(" \t"), boost::token_compress_on ); 
      if(splitVec.size()<2){
	std::cout<<"Bad input line "<<inpline<<std::endl;
	continue;
      }
      m_params[splitVec[0]]=splitVec[1];
    }
  }
  // Module name
  std::string modname="";
  if( m_params.find("ModuleID")==m_params.end()){
    std::cout<<"No Module ID defined."<<std::endl;
  }else{
    modname=m_params["ModuleID"];
  }
  sprintf((char*)config->idStr, "%s", modname.c_str());
  //Registers
  config->bpm = lookupToUShort("bpm");
  config->delay_tam1 = lookupToUShort("delay_tam1");
  config->delay_tam2 = lookupToUShort("delay_tam2");
  config->delay_tam3 = lookupToUShort("delay_tam3");
  config->delay_tbm1 = lookupToUShort("delay_tbm1");
  config->delay_tbm2 = lookupToUShort("delay_tbm2");
  config->delay_tbm3 = lookupToUShort("delay_tbm3");
  config->bypass_delay = lookupToUShort("bypass_delay");
  config->clock = lookupToUShort("clock");
  config->function_A = lookupToUShort("function_A");
  config->function_B = lookupToUShort("function_B");

  delete m_moduleCfgFile;
}

void HitbusConfigFile::dump(const ipc::HitbusModuleConfig &config){
  std::cout<<"Registers:"<<std::endl;
  std::cout<<"=========="<<std::endl;
  std::cout<<"bpm = "<<config.bpm<<std::endl;
  std::cout<<"delay_tam1 = "<<config.delay_tam1<<std::endl;
  std::cout<<"delay_tam2 = "<<config.delay_tam2<<std::endl;
  std::cout<<"delay_tam3 = "<<config.delay_tam3<<std::endl;
  std::cout<<"delay_tbm1 = "<<config.delay_tbm1<<std::endl;
  std::cout<<"delay_tbm2 = "<<config.delay_tbm2<<std::endl;
  std::cout<<"delay_tbm3 = "<<config.delay_tbm3<<std::endl;
  std::cout<<"bypass_delay = "<<config.bypass_delay<<std::endl;
  std::cout<<"clock = "<<config.clock<<std::endl;
  std::cout<<"function_A = "<<config.function_A<<std::endl;
  std::cout<<"function_B = "<<config.function_B<<std::endl;
}
